﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gizmobaba.Repository.Interfaces;
using Gizmobaba.Utility;

namespace Gizmobaba.Repository
{
    public class WalletRepository : IWalletRepository
    {
        public int AddWallet(M_GB_WALLET wallet)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    var data = context.M_GB_WALLET.FirstOrDefault(a => a.WALLET_ID == wallet.WALLET_ID);
                    if (data != null)
                    {
                        data.STATUS = 3;
                        context.SaveChanges();
                    }
                    wallet.WALLET_ID = 0;
                    context.M_GB_WALLET.Add(wallet);
                    context.SaveChanges();
                    return wallet.WALLET_ID;
                }
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }

        public WalletDto GetWallet()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.M_GB_WALLET.Where(a => a.STATUS == 1).Select(w => new WalletDto
                    {
                        WALLET_ID = w.WALLET_ID,
                        LINKINGPOSTS = w.LINKINGPOSTS,
                        MAX_USER_PURCHASE = w.MAX_USER_PURCHASE,
                        OWN_PURCHASE = w.OWN_PURCHASE,
                        PURCHASE_CERTAIN_TIME = w.PURCHASE_CERTAIN_TIME,
                        REFERRING_FRIENDS = w.REFERRING_FRIENDS,
                        REFFERED_FRIENDS_PURCHASES = w.REFFERED_FRIENDS_PURCHASES,
                        ROLE_ID = w.ROLE_ID,
                        SHARINGPOSTS = w.SHARINGPOSTS
                    }).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        public IEnumerable<BonusDto> GetWalletList()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.GB_BONUS.Where(a => a.STATUS == (int)CustomStatus.DefaultStatus.Active && a.ROLE_ID == (int)CustomStatus.UserTypes.Customer).Select(w => new BonusDto
                    {
                        BONUS_TYPE_ID =w.BONUS_TYPE_ID,
                        BONUS_TYPE = w.M_GB_BONUS_TYPE.BONUS_TYPE,
                        VALUE = w.VALUE,
                        STATUS= w.STATUS,
                        ROLE_ID =  w.ROLE_ID
                    }).ToList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public int CreateWallet(GB_BONUS bonusData)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    var data = context.GB_BONUS.FirstOrDefault(a => a.BONUS_TYPE_ID == bonusData.BONUS_TYPE_ID && a.ROLE_ID == bonusData.ROLE_ID);
                    if (data != null)
                    {
                        data.VALUE = bonusData.VALUE;
                    }
                    else
                        context.GB_BONUS.Add(bonusData);
                    context.SaveChanges();
                    return (int)CustomStatus.ReturnStatus.success;
                }
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }


        public IEnumerable<BonusDto> GetWalletListByRole(int roleId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.GB_BONUS.Where(a => a.STATUS == (int)CustomStatus.DefaultStatus.Active && a.ROLE_ID == roleId).Select(w => new BonusDto
                    {
                        BONUS_TYPE_ID = w.BONUS_TYPE_ID,
                        BONUS_TYPE = w.M_GB_BONUS_TYPE.BONUS_TYPE,
                        VALUE = w.VALUE,
                        STATUS = w.STATUS,
                        ROLE_ID =  w.ROLE_ID
                    }).ToList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        public IEnumerable<M_GB_Payment_GatWay> GetPaymentgatwayList()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.M_GB_Payment_GatWay.ToList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        public M_GB_Payment_GatWay GetPaymentgatway(int id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.M_GB_Payment_GatWay.FirstOrDefault(a => a.Payment_GateWay_ID == id);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
