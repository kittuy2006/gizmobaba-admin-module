﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using Gizmobaba.Repository.Interfaces;
using Gizmobaba.Utility;

namespace Gizmobaba.Repository
{
    public class ProductRepository : IProductRepository
    {
        public Int64 AddProduct(GB_PRODUCT_DETAIL productDetails)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    if (productDetails.PRODUCT_ID == 0)
                        context.GB_PRODUCT_DETAIL.Add(productDetails);
                    else
                    {                      
                        var dbImages = context.GB_PRODUCT_IMAGES.Where(a => a.PRODUCT_ID == productDetails.PRODUCT_ID).ToList();

                        if (productDetails.GB_PRODUCT_IMAGES.Count != 0)
                        {
                            //delete source image if uploaded again
                            if (productDetails.GB_PRODUCT_IMAGES.Any(a => a.DISPLAY_IMAGE == 1))
                            {
                                dbImages.Where(a=>a.DISPLAY_IMAGE == 1).ToList().ForEach(d => context.GB_PRODUCT_IMAGES.Remove(d));
                        //        context.SaveChanges();
                            }
                            //delete Gallery image if uploaded again
                            if (productDetails.GB_PRODUCT_IMAGES.Any(a => a.DISPLAY_IMAGE == 0))
                            {
                                dbImages.Where(a => a.DISPLAY_IMAGE == 0).ToList().ForEach(d => context.GB_PRODUCT_IMAGES.Remove(d));
                            }

                            //delete Gif image if uploaded again
                            if (productDetails.GB_PRODUCT_IMAGES.Any(a => a.DISPLAY_IMAGE == 2))
                            {
                                dbImages.Where(a => a.DISPLAY_IMAGE == 2).ToList().ForEach(d => context.GB_PRODUCT_IMAGES.Remove(d));
                            }

                            context.SaveChanges();
                            //IList<GB_PRODUCT_IMAGES> proImgList = context.GB_PRODUCT_IMAGES.Where(a => a.PRODUCT_ID == productDetails.PRODUCT_ID).ToList();
                            //foreach (GB_PRODUCT_IMAGES img in proImgList)
                            //{
                            //    foreach (GB_PRODUCT_IMAGES i in productDetails.GB_PRODUCT_IMAGES)
                            //    {
                            //        if (i.ALTERNATE_NAME == img.ALTERNATE_NAME)
                            //        {
                            //            context.GB_PRODUCT_IMAGES.Remove(img);
                            //            context.SaveChanges();
                            //        }
                            //    }
                            //}

                           
                        }

                   //     productDetails.GB_PRODUCT_IMAGES
                        GB_PRODUCT_DETAIL dataOrg = context.GB_PRODUCT_DETAIL.FirstOrDefault(z => z.PRODUCT_ID == productDetails.PRODUCT_ID);
                        foreach (GB_PRODUCT_IMAGES img in productDetails.GB_PRODUCT_IMAGES)
                        {
                            if (dataOrg != null)
                            {
                                img.PRODUCT_ID = dataOrg.PRODUCT_ID;
                                dataOrg.GB_PRODUCT_IMAGES.Add(img);
                            }
                        }

                        if (dataOrg != null)
                        {
                            dataOrg.GB_PRODUCT_PAYMENT = productDetails.GB_PRODUCT_PAYMENT;
                            if (productDetails.GB_PRODUCT_DELIVERY != null && productDetails.GB_PRODUCT_DELIVERY.Count != 0)
                                dataOrg.GB_PRODUCT_DELIVERY = productDetails.GB_PRODUCT_DELIVERY;
                            if (productDetails.GB_PRODUCT_SHIPPING != null && productDetails.GB_PRODUCT_SHIPPING.Count != 0)
                                dataOrg.GB_PRODUCT_SHIPPING = productDetails.GB_PRODUCT_SHIPPING;
                            dataOrg.PRODUCT_NAME = productDetails.PRODUCT_NAME;
                            dataOrg.BRAND = productDetails.BRAND;
                            dataOrg.PRODUCT_CAPTION = productDetails.PRODUCT_CAPTION;
                            dataOrg.PRODUCT_SKU = productDetails.PRODUCT_SKU;
                            dataOrg.MRP = productDetails.MRP;
                            dataOrg.NET_PRICE = productDetails.NET_PRICE;
                            dataOrg.GROSS_PRICE = productDetails.GROSS_PRICE;
                            dataOrg.GIZMOBABA_PRICE = productDetails.GIZMOBABA_PRICE;
                            dataOrg.ACTIVATION_DATE = productDetails.ACTIVATION_DATE;
                            dataOrg.DEACTIVATION_DATE = productDetails.DEACTIVATION_DATE;
                            dataOrg.DELIVERY_TIME = productDetails.DELIVERY_TIME;
                            dataOrg.INVENTORY = productDetails.INVENTORY;
                            dataOrg.PKG_QUANTITY = productDetails.PKG_QUANTITY;
                            dataOrg.RESERVE_QUANTITY = productDetails.RESERVE_QUANTITY;
                            dataOrg.REORDER_STOCK_LEVEL = productDetails.REORDER_STOCK_LEVEL;
                            dataOrg.STOCK_ALERT_QUANTITY = productDetails.STOCK_ALERT_QUANTITY;
                            dataOrg.MINIMUM_ORDER_QUANTITY = productDetails.MINIMUM_ORDER_QUANTITY;
                            dataOrg.MAXIMUM_ORDER_QUANTITY = productDetails.MAXIMUM_ORDER_QUANTITY;
                            dataOrg.PREORDER = productDetails.PREORDER;
                            dataOrg.BACKORDER = productDetails.BACKORDER;
                            dataOrg.STOCK_AVAILABLE_MESS = productDetails.STOCK_AVAILABLE_MESS;
                            dataOrg.PRODUCT_DESC = productDetails.PRODUCT_DESC;
                            dataOrg.CATEGORY_ID = productDetails.CATEGORY_ID;
                            dataOrg.TAX_RATE_ID = productDetails.TAX_RATE_ID;
                            dataOrg.DEALER_PRICE = productDetails.DEALER_PRICE;
                            dataOrg.M_F_PRICE = productDetails.M_F_PRICE;
                        }
                    }
                    context.SaveChanges();
                    if (productDetails.PRODUCT_SKU == null || productDetails.PRODUCT_SKU == "")
                    {
                        GB_PRODUCT_DETAIL dataOrg = context.GB_PRODUCT_DETAIL.FirstOrDefault(z => z.PRODUCT_ID == productDetails.PRODUCT_ID);
                        dataOrg.PRODUCT_SKU = "GB -" + productDetails.PRODUCT_ID;
                        context.SaveChanges();
                    }
                    return productDetails.PRODUCT_ID;
                }
            }
            catch (DbEntityValidationException e)
            {
                string rs = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    rs = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    Console.WriteLine(rs);
                    rs = eve.ValidationErrors.Aggregate(rs, (current, ve) => current + ("<br />" + string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage)));
                }
                throw new Exception(rs);
            }
        }


        public IEnumerable<GB_PRODUCT_DETAIL> GetAll()
        {

            GizmobabadbEntities1 context = new GizmobabadbEntities1();
            return context.GB_PRODUCT_DETAIL.Where(a=>a.STATUS!=(int)CustomStatus.ProductStatus.Delete).ToList();
        }


        public int Delete(int productId)
        {
            using (var context = new GizmobabadbEntities1())
            {
                var gbProductDetail = context.GB_PRODUCT_DETAIL.FirstOrDefault(z => z.PRODUCT_ID == productId);
                if (gbProductDetail != null)
                    gbProductDetail.STATUS = (int)CustomStatus.ProductStatus.Delete;
                context.SaveChanges();
                return (int)CustomStatus.ReturnStatus.success;
            }
        }


        public IEnumerable<M_GB_CATEGORY> GetCategories()
        {
            GizmobabadbEntities1 context = new GizmobabadbEntities1();
            return context.M_GB_CATEGORY.Where(z => z.STATUS != (int)CustomStatus.DefaultStatus.Delete).AsEnumerable();
        }


        public IEnumerable<M_GB_SHIPPING> GetAvailableShippingCodes()
        {
            GizmobabadbEntities1 context = new GizmobabadbEntities1();
            return context.M_GB_SHIPPING.Where(z => z.STATUS == 1).AsEnumerable();
        }


        public GB_PRODUCT_DETAIL GetProduct(int productId)
        {
            GizmobabadbEntities1 context = new GizmobabadbEntities1();
            return context.GB_PRODUCT_DETAIL.Where(z => z.PRODUCT_ID == productId).FirstOrDefault();
        }


        public IEnumerable<ProductDTO> GetProductSearch(string product)
        {
            GizmobabadbEntities1 context = new GizmobabadbEntities1();
            return (from p in context.GB_PRODUCT_DETAIL
                    where p.STATUS!=(int)CustomStatus.ProductStatus.Delete && (p.PRODUCT_NAME.Contains(product) || p.PRODUCT_SKU == product)
                    select new ProductDTO
                    {
                        PRODUCT_ID = p.PRODUCT_ID,
                        PRODUCT_NAME = p.PRODUCT_NAME,
                        PRODUCT_SKU = p.PRODUCT_SKU,
                        QUANTITYS = p.QUANTITYS,
                        INVENTORY = p.INVENTORY,
                        MRP = p.MRP,
                        NET_PRICE = p.NET_PRICE,
                        GIZMOBABA_PRICE = p.GIZMOBABA_PRICE,
                        STATUS = p.STATUS,
                        PRODUCT_CAPTION = p.PRODUCT_CAPTION,
                        PRODUCT_DESC = p.PRODUCT_DESC
                    }).ToList();
        }

        public int ActivateProduct(int productId)
        {
            using (var context = new GizmobabadbEntities1())
            {
                var gbProductDetail = context.GB_PRODUCT_DETAIL.FirstOrDefault(z => z.PRODUCT_ID == productId);
                if (gbProductDetail != null)
                    gbProductDetail.STATUS = (int)CustomStatus.ProductStatus.Active;
                context.SaveChanges();
                return (int)CustomStatus.ReturnStatus.success;
            }
        }

        public int DeactivateProduct(int productId)
        {
            using (var context = new GizmobabadbEntities1())
            {
                var gbProductDetail = context.GB_PRODUCT_DETAIL.FirstOrDefault(z => z.PRODUCT_ID == productId);
                if (gbProductDetail != null)
                    gbProductDetail.STATUS = (int)CustomStatus.ProductStatus.DeActive;
                context.SaveChanges();
                return (int)CustomStatus.ReturnStatus.success;
            }
        }


        public int AddBanner(M_GB_BANNERS banner)
        {
            using (var context = new GizmobabadbEntities1())
            {
                context.M_GB_BANNERS.Add(banner);
                context.SaveChanges();
                return (int)CustomStatus.ReturnStatus.success;
            }
        }


        public IEnumerable<Banner> GetBanners()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (from b in context.M_GB_BANNERS
                            where b.STATUS != (int)CustomStatus.BannerStatus.Delete && b.BANNER_TYPE_ID == (int)CustomStatus.BannerTypes.Home
                            select new Banner
                            {
                                BANNER_ID = b.BANNER_ID,
                                BANNER_NAME = b.BANNER_NAME,
                                BANNER_URL = b.BANNER_URL,
                                STATUS = b.STATUS
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int DeleteBanner(int bannerId)
        {
            using (var context = new GizmobabadbEntities1())
            {
                var mGbBanners = context.M_GB_BANNERS.FirstOrDefault(b => b.BANNER_ID == bannerId);
                if (mGbBanners != null)
                    mGbBanners.STATUS = (int)CustomStatus.BannerStatus.Delete;
                context.SaveChanges();
                return (int)CustomStatus.ReturnStatus.success;
            }
        }


        public int SetBanner(int bannerId)
        {
            using (var context = new GizmobabadbEntities1())
            {
                //context.M_GB_BANNERS.Where(a => a.STATUS == (int)CustomStatus.BannerStatus.Active).ToList().ForEach(d => d.STATUS = (int)CustomStatus.BannerStatus.DeActive);
                //context.SaveChanges();
                var mGbBanners = context.M_GB_BANNERS.FirstOrDefault(b => b.BANNER_ID == bannerId);
                if (mGbBanners != null)
                    mGbBanners.STATUS = (int)CustomStatus.BannerStatus.Active;
                context.SaveChanges();
                return (int)CustomStatus.ReturnStatus.success;
            }
        }
        public int DeleteImage(int imageId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    var data = context.GB_PRODUCT_IMAGES.FirstOrDefault(a => a.PRODUCT_IMAGE_ID == imageId);
                    if (data != null)
                    {
                        context.GB_PRODUCT_IMAGES.Remove(data);
                        AutoGenerateCode.DeleteFile(data.IMAGE_URL);
                        context.SaveChanges();
                    }
                    return (int) CustomStatus.ReturnStatus.success;
                }
            }
            catch (Exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;

            }
        }

        public int EditImage(GB_PRODUCT_IMAGES img)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    var gbProductImages =
                        context.GB_PRODUCT_IMAGES.FirstOrDefault(a => a.PRODUCT_IMAGE_ID == img.PRODUCT_IMAGE_ID);
                    if (gbProductImages != null)
                    {
                        AutoGenerateCode.DeleteFile(gbProductImages.IMAGE_URL);
                        gbProductImages.IMAGE_URL = img.IMAGE_URL;
                        context.SaveChanges();
                    }
                    return (int) CustomStatus.ReturnStatus.success;
                }
            }
            catch (Exception)
            {
                return (int) CustomStatus.ReturnStatus.failure;
            }
        }

        public IEnumerable<ProductImagesDto> GetProductImages(int productId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.GB_PRODUCT_IMAGES.Where(i => i.PRODUCT_ID == productId)
                        .Select(i => new ProductImagesDto
                        {
                            PRODUCT_IMAGE_ID = i.PRODUCT_IMAGE_ID,
                            ALTERNATE_NAME = i.ALTERNATE_NAME,
                            DISPLAY_IMAGE = i.DISPLAY_IMAGE,
                            PRODUCT_ID = i.PRODUCT_ID,
                            IMAGE_URL = i.IMAGE_URL
                        })).ToList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public int CheckSku(string sku)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.GB_PRODUCT_DETAIL.Count(a => a.PRODUCT_SKU == sku);
                   
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public int AddProductImage(GB_PRODUCT_IMAGES productimages)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    context.GB_PRODUCT_IMAGES.Add(productimages);
                    context.SaveChanges();
                    return (int) CustomStatus.ReturnStatus.success;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public IEnumerable<VendorDto> GetVendors()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.GB_VENDOR
                        .Select(v => new VendorDto
                        {
                           VEN_ID = v.VEN_ID,
                           VEN_DESCRIPTION = v.VEN_DESCRIPTION,
                           NAME = v.NAME,
                           EMAIL = v.EMAIL
                        })).ToList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public IEnumerable<WarrantyDto> GetWarranty()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.M_GB_WARRANTY.Where(a=>a.STATUS != 3)
                        .Select(w => new WarrantyDto
                        {
                            WARRANTY_ID = w.WARRANTY_ID,
                            WARRANTY_DESC = w.WARRANTY_DESC
                        })).ToList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public IEnumerable<CustomFieldsDto> GetCustomFields()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.M_GB_CUSTOM_FIELDS.Where(a=>a.STATUS != 3)
                        .Select(c => new CustomFieldsDto
                        {
                          CUSTOM_FIELDS_ID = c.CUSTOM_FIELDS_ID,
                          CUSTOM_FIELDS_DESC = c.CUSTOM_FIELDS_DESC,
                          STATUS = c.STATUS
                        })).ToList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public IEnumerable<SectionDto> GetSection()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.M_GB_SECTION.Where(a=>a.STATUS != 3)
                        .Select(s => new SectionDto
                        {
                            SECTION_ID = s.SECTION_ID,
                            SECTION_DESC = s.SECTION_DESC,
                            STATUS = s.STATUS
                        })).ToList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public IEnumerable<ConditiongDto> GetCondition()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.M_GB_CONDITION.Where(a => a.STATAUS != 3)
                        .Select(c => new ConditiongDto
                        {
                           CONDITION_ID = c.CONDITION_ID,
                           CONDITION_DESC = c.CONDITION_DESC
                        })).ToList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public int AddProductAdditionalinfo(GB_PRODUCT_INFORMATION productInformation)
        {

            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    context.GB_PRODUCT_INFORMATION.Add(productInformation);
                    context.SaveChanges();
                    return (int) productInformation.PRODUCT_ID;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public ProductDTO GetProductInfomation(int productId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.GB_PRODUCT_INFORMATION.Where(p => p.PRODUCT_ID == productId).Select(p => new ProductDTO
                    {
                        PRODUCT_ID = p.PRODUCT_ID,
                        PRODUCT_INFO_ID = p.PRODUCT_INFO_ID,
                        PRO_OFFER_DESC = p.PRO_OFFER_DESC,
                        PRODUCT_NOTE = p.PRODUCT_NOTE,
                        PRO_OFFER_IMAGE = p.PRO_OFFER_IMAGE,
                        BARCODE = p.BARCODE,
                        CATALOG_CODE = p.CATALOG_CODE,
                        MODELNUMBER = p.MODELNUMBER,
                        VENDOR_ID = p.GB_PRODUCT_DETAIL.VENDOR_ID,
                        STANDARD_PRODUCT_CODE = p.STANDARD_PRODUCT_CODE,
                        STANDARD_PRODUCT_TYPE = p.STANDARD_PRODUCT_TYPE,
                        MPN = p.MPN,
                        PAGE_TITLE = p.PAGE_TITLE,
                        KEYWORD = p.KEYWORD,
                        SEO_DESC = p.SEO_DESC,
                        URL_KEY_REWRITER = p.URL_KEY_REWRITER,
                        SMALL_IMAGEALT = p.SMALL_IMAGEALT,
                        LARGE_IMAGEALT = p.LARGE_IMAGEALT,
                        CONDITION_ID = p.CONDITION_ID,
                        WARRANTY_ID = p.WARRANTY_ID,
                        CUSTOM_FIELDS_ID = p.CUSTOM_FIELDS_ID,
                        SECTION_ID = p.SECTION_ID
                    }).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public IEnumerable<TagsDto> GetProductTags()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.M_GB_TAGS.Where(a => a.STATUS != 3 )
                        .Select(t => new TagsDto
                        {
                            TAG_ID = t.TAG_ID,
                            TAG_DESC = t.TAG_DESC,
                            STATUS = t.STATUS
                        }).ToList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public IEnumerable<TagsDto> GetAllProductTags()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.M_GB_TAGS.Where(a => a.STATUS != 3)
                        .Select(t => new TagsDto
                        {
                            TAG_ID = t.TAG_ID,
                            TAG_DESC = t.TAG_DESC,
                            STATUS = t.STATUS,
                            ProductCount = context.GB_PRODUCT_TAG.Count(a=>a.TAG_ID == t.TAG_ID)
                        }).ToList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public TagsDto GetProductTags(int productId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.M_GB_TAGS.Where(a => a.TAG_ID == productId && a.STATUS != 3)
                        .Select(t => new TagsDto
                        {
                            TAG_ID = t.TAG_ID,
                            TAG_DESC = t.TAG_DESC,
                            STATUS = t.STATUS
                        }).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public int AddProductTag(M_GB_TAGS tagsDto)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    M_GB_TAGS tagsData =
                        context.M_GB_TAGS.FirstOrDefault(a => a.TAG_ID == tagsDto.TAG_ID);
                    if (tagsData != null)
                    {
                        tagsData.TAG_ID = tagsDto.TAG_ID;
                        tagsData.TAG_DESC = tagsDto.TAG_DESC;
                    }
                    else
                        context.M_GB_TAGS.Add(tagsDto);
                    context.SaveChanges();
                    return tagsDto.TAG_ID;
                }
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }

        public int DeleteProductTag(int tagId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    var data = context.M_GB_TAGS.FirstOrDefault(a => a.TAG_ID == tagId);
                    if (data != null)
                    {
                        data.STATUS = (int)CustomStatus.DefaultStatus.Delete;
                        context.SaveChanges();
                    }
                    return (int)CustomStatus.ReturnStatus.success;
                }
            }
            catch (Exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;

            }
        }

        public int AddTagToProduct(GB_PRODUCT_TAG tags)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    GB_PRODUCT_TAG tagsData =
                        context.GB_PRODUCT_TAG.FirstOrDefault(a => a.TAG_ID == tags.TAG_ID && a.PRODUCT_ID == tags.PRODUCT_ID);
                    if (tagsData == null)
                    {
                        context.GB_PRODUCT_TAG.Add(tags);
                        context.SaveChanges();
                    }
                    return tags.PRODUCT_TAG_ID;
                }
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }

        public IEnumerable<Banner> GetPredefinedBanners()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (from b in context.M_GB_BANNERS
                            where b.STATUS != (int)CustomStatus.BannerStatus.Delete && b.BANNER_TYPE_ID == (int)CustomStatus.BannerTypes.Predefined
                            select new Banner
                            {
                                BANNER_ID = b.BANNER_ID,
                                BANNER_NAME = b.BANNER_NAME,
                                BANNER_URL = b.BANNER_URL,
                                STATUS = b.STATUS,
                                BANNER_DEC = b.BANNER_DEC,
                                BANNER_EXPIRY_DATE = b.BANNER_EXPIRY_DATE
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Banner GetBanner(int bannerId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.M_GB_BANNERS.Where(
                        b => b.STATUS != (int) CustomStatus.BannerStatus.Delete && b.BANNER_ID == bannerId)
                        .Select(b => new Banner
                        {
                            BANNER_ID = b.BANNER_ID,
                            BANNER_NAME = b.BANNER_NAME,
                            BANNER_URL = b.BANNER_URL,
                            STATUS = b.STATUS,
                            BANNER_EXPIRY_DATE = b.BANNER_EXPIRY_DATE,
                            CAREATED_ON = b.CAREATED_ON,
                            BANNER_DEC = b.BANNER_DEC
                        })).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Banner> GetCategoryBanners()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.M_GB_BANNERS.Where(
                        b => b.STATUS != (int)CustomStatus.BannerStatus.Delete && b.BANNER_TYPE_ID == (int)CustomStatus.BannerTypes.Category)
                        .Select(b => new Banner
                        {
                            BANNER_ID = b.BANNER_ID,
                            BANNER_NAME = b.BANNER_NAME,
                            BANNER_URL = b.BANNER_URL,
                            STATUS = b.STATUS,
                            BANNER_EXPIRY_DATE = b.BANNER_EXPIRY_DATE,
                            CAREATED_ON = b.CAREATED_ON,
                            CATEGORY_ID = b.CATEGORY_ID,
                            SUB_CAT_ID = b.SUB_CAT_ID,
                            Sub_Cat_Name = context.M_GB_SUB_CATEGORY.FirstOrDefault(a=>a.SUB_CAT_ID == b.SUB_CAT_ID).SUB_CAT_DESC
                        })).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IEnumerable<Banner> GetAddsBanners()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.M_GB_BANNERS.Where(
                        b => b.STATUS != (int)CustomStatus.BannerStatus.Delete && b.BANNER_TYPE_ID == (int)CustomStatus.BannerTypes.Adds)
                        .Select(b => new Banner
                        {
                            BANNER_ID = b.BANNER_ID,
                            BANNER_NAME = b.BANNER_NAME,
                            BANNER_URL = b.BANNER_URL,
                            BANNER_DEC = b.BANNER_DEC,
                            STATUS = b.STATUS,
                            BANNER_EXPIRY_DATE = b.BANNER_EXPIRY_DATE,
                            CAREATED_ON = b.CAREATED_ON,
                        })).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IEnumerable<M_GB_SUB_CATEGORY> GetAllSubCat()
        {
            var context = new GizmobabadbEntities1();
            return context.M_GB_SUB_CATEGORY.Where(a => a.STATUS != (int)CustomStatus.DefaultStatus.Delete).AsEnumerable();

        }


        public IEnumerable<GB_BULK_UPLOAD> GetBulkuploadList()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.GB_BULK_UPLOAD.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int AddBulkProUploadList(GB_BULK_UPLOAD bulkUpload)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    context.GB_BULK_UPLOAD.Add(bulkUpload);
                    context.SaveChanges();
                    return (int)CustomStatus.ReturnStatus.success;
                }
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }


        public IEnumerable<PRODUCTREVIEWSDTO> GetReviews()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.GB_PRODUCT_REVIEWS.Where(
                        b => b.STATUS != (int)CustomStatus.DefaultStatus.Delete)
                        .Select(b => new PRODUCTREVIEWSDTO
                        {
                            ID= b.ID,
                            REVIEW = b.REVIEW,
                            REVIEW_DESC =b.REVIEW_DESC,
                            UserName = context.M_GB_USER.FirstOrDefault(a=>a.USER_ID == b.USER_ID.ToString()).USER_NAME,
                            STATUS =b.STATUS
                        })).OrderByDescending(a=>a.ID).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int UpdateReviewStatus(int id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    GB_PRODUCT_REVIEWS review = context.GB_PRODUCT_REVIEWS.FirstOrDefault(a=>a.ID == id);
                    review.STATUS = (int)CustomStatus.DefaultStatus.Active;
                    context.SaveChanges();
                    return (int)CustomStatus.ReturnStatus.success;
                }
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }

        public int DeleteReview(int id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    GB_PRODUCT_REVIEWS review = context.GB_PRODUCT_REVIEWS.FirstOrDefault(a => a.ID == id);
                    context.GB_PRODUCT_REVIEWS.Remove(review);
                    context.SaveChanges();
                    return (int)CustomStatus.ReturnStatus.success;
                }
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }


        public IEnumerable<M_GB_TAX> GetTaxes()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.M_GB_TAX.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int AddTax(M_GB_TAX tax)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    context.M_GB_TAX.Add(tax);
                    context.SaveChanges();
                    return (int)CustomStatus.ReturnStatus.success;
                }
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }


        public M_GB_TAX GetProductTax(int id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.M_GB_TAX.FirstOrDefault(a => a.TAX_ID == id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string DeleteProductTax(int id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    M_GB_TAX tax = context.M_GB_TAX.FirstOrDefault(a => a.TAX_ID == id);
                    if(tax != null)
                        tax.STATUS = (int)CustomStatus.DefaultStatus.Delete;
                    context.SaveChanges();
                    return CustomStatus.ReturnStatus.success.ToString();
                }
            }
            catch (Exception exception)
            {
                return CustomStatus.ReturnStatus.failure.ToString();
            }
        }


        public string GetProSku()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    long sku = context.GB_PRODUCT_DETAIL.OrderByDescending(a=>a.PRODUCT_ID).FirstOrDefault().PRODUCT_ID;
                  return (sku + 1).ToString();
                }
            }
            catch (Exception exception)
            {
                return CustomStatus.ReturnStatus.failure.ToString();
            }
        }


        public IEnumerable<ProductDTO> GetProductInvAdvSearch(DateTime fromDate, DateTime toDate)
        {
            GizmobabadbEntities1 context = new GizmobabadbEntities1();
            return (from p in context.GB_PRODUCT_DETAIL
                    where p.STATUS != (int)CustomStatus.ProductStatus.Delete && (p.CREATED_ON >= fromDate && p.CREATED_ON <= toDate)
                    select new ProductDTO
                    {
                        PRODUCT_ID = p.PRODUCT_ID,
                        PRODUCT_NAME = p.PRODUCT_NAME,
                        PRODUCT_SKU = p.PRODUCT_SKU,
                        QUANTITYS = p.QUANTITYS,
                        INVENTORY = p.INVENTORY,
                        MRP = p.MRP,
                        NET_PRICE = p.NET_PRICE,
                        GIZMOBABA_PRICE = p.GIZMOBABA_PRICE,
                        STATUS = p.STATUS,
                        PRODUCT_CAPTION = p.PRODUCT_CAPTION,
                        PRODUCT_DESC = p.PRODUCT_DESC
                    }).ToList();
        }


        public ProductDTO GetProductbySku(string sku)
        {


            using (var context = new GizmobabadbEntities1())
            {
                return (from p in context.GB_PRODUCT_DETAIL
                        where p.STATUS != (int)CustomStatus.ProductStatus.Delete && p.PRODUCT_SKU == sku
                        select new ProductDTO
                        {
                            PRODUCT_ID = p.PRODUCT_ID,
                            PRODUCT_NAME = p.PRODUCT_NAME,
                            PRODUCT_SKU = p.PRODUCT_SKU,
                            QUANTITYS = p.QUANTITYS,
                            INVENTORY = p.INVENTORY,
                            MRP = p.MRP,
                            NET_PRICE = p.NET_PRICE,
                            GIZMOBABA_PRICE = p.GIZMOBABA_PRICE,
                            STATUS = p.STATUS,
                            PRODUCT_CAPTION = p.PRODUCT_CAPTION,
                            PRODUCT_DESC = p.PRODUCT_DESC
                        }).FirstOrDefault();
            }
          
        }
    }
}
