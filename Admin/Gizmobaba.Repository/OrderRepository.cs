﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Gizmobaba.Repository.Interfaces;
using Gizmobaba.Utility;

namespace Gizmobaba.Repository
{
    public class OrderRepository : IOrderRepository
    {
        public IEnumerable<OrderDto> List
        {
            get { throw new NotImplementedException(); }
        }

        public OrderDto FindById(int id)
        {
            throw new NotImplementedException();
        }


        public int Add(OrderDto orderDto)
        {
            try
            {
                // Insert entity query
                return (int)CustomStatus.ReturnStatus.success;
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }

        public int Delete(int orderId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    var mGbShipping = context.GB_ORDER.FirstOrDefault(o=> o.ORDER_ID == orderId);
                    if (mGbShipping != null)
                        mGbShipping.STATUS = (int)CustomStatus.ProductStatus.Delete;
                    context.SaveChanges();
                }
                return (int)CustomStatus.ReturnStatus.success;
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }


        public OrderDto Update(OrderDto gbOrderDto)
        {
            try
            {
                // Update entity query
                return gbOrderDto;
            }
            catch (Exception exception)
            {
                return gbOrderDto;
            }
        }

        public IEnumerable<OrderDetailDto> GetPaymentPendingOrders(int paymentStatus)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.GB_ORDER.Where(r => r.STATUS == 1).Select(r => new OrderDetailDto
                   {
                       ORDER_ID = r.ORDER_ID,
                       CREATED_AT = r.CREATED_AT,
                       UserName = r.M_GB_USER.USER_NAME + r.M_GB_USER.FIRST_NAME,
                       STATUS = r.STATUS,
                       Qty = r.GB_ORDER_DETAIL.FirstOrDefault().QUANTITY,
                       CHECKOUTTYPE = r.GB_ORDER_DETAIL.FirstOrDefault().CHECKOUTTYPE,
                       UNIT_PRICE = r.GB_ORDER_DETAIL.FirstOrDefault().UNIT_PRICE
                   })).ToList();

                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }

        public int CancelOrder(int orderId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    var mGbShipping = context.GB_ORDER.FirstOrDefault(o => o.ORDER_ID == orderId);
                    if (mGbShipping != null)
                        mGbShipping.STATUS = (int)CustomStatus.OrderStatus.Cancel;
                    context.SaveChanges();
                }
                return (int)CustomStatus.ReturnStatus.success;
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }

        public IEnumerable<CartDto> GetAbandonedCart()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.GB_CART.Where(c => !string.IsNullOrEmpty(c.USER_ID)).Select(c => new CartDto
                    {
                        CART_ID = c.CART_ID,
                        CREATED_AT = c.CREATED_AT,
                        STATUS = c.STATUS,
                        SESSION_ID = c.SESSION_ID,
                        PRICE_PER_QYT = c.PRICE_PER_QYT,
                        QYT = c.QYT,
                        TOTAL_AMOUNT = c.TOTAL_AMOUNT,
                        TOTAL_QTY = c.TOTAL_QTY,
                        EMAIL_ID = context.M_GB_USER.FirstOrDefault(a => a.USER_ID == c.USER_ID).FIRST_NAME != null ? context.M_GB_USER.FirstOrDefault(a => a.USER_ID == c.USER_ID).FIRST_NAME : context.M_GB_USER.FirstOrDefault(a => a.USER_ID == c.USER_ID).USER_NAME,
                        USER_ID = c.USER_ID
                    })).ToList();

                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }

        public IEnumerable<CartDto> AbandonedCartSearch(DateTime fromDate, DateTime toDate, string searchKey)
        {
            try
            {
                var context1 = new GizmobabadbEntities1();
                DateTime dt = new DateTime();
                string query = "select a.CART_ID,a.PRODUCT_ID,a.COUPAN_ID,a.CREATED_AT,a.STATUS,a.MOB_NO,a.ADDRESS_ID,a.SESSION_ID,a.PRICE_PER_QYT,a.QYT,a.TOTAL_AMOUNT,a.TOTAL_QTY,b.FIRST_NAME as EMAIL_ID,a.USER_ID from GB_CART a inner join M_GB_USER b on a.USER_ID=b.USER_ID where a.SESSION_ID!='' ";
                if ( fromDate != dt &&  toDate != dt)
                    query += " and (a.CREATED_AT >= '" + fromDate + "' and a.CREATED_AT <= '" + toDate + "') ";
                if (!string.IsNullOrEmpty(searchKey))
                    query += " and b.FIRST_NAME='" + searchKey + "' ";

                IEnumerable<GB_CART> orders = context1.GB_CART.SqlQuery(query).ToList<GB_CART>();

                return (orders.Select(c => new CartDto
                {
                    CART_ID = c.CART_ID,
                    CREATED_AT = c.CREATED_AT,
                    STATUS = c.STATUS,
                    SESSION_ID = c.SESSION_ID,
                    PRICE_PER_QYT = c.PRICE_PER_QYT,
                    QYT = c.QYT,
                    TOTAL_AMOUNT = c.TOTAL_AMOUNT,
                    TOTAL_QTY = c.TOTAL_QTY,
                    EMAIL_ID = c.EMAIL_ID,
                    USER_ID = c.USER_ID
                })).ToList();


                //using (var context = new GizmobabadbEntities1())
                //{



                //    return (context.GB_CART.Where(t => (t.USER_ID != "") && (t.CREATED_AT > fromDate && (t.CREATED_AT < toDate) || (t.EMAIL_ID.Contains(searchKey)))).Select(c => new CartDto
                //    {
                //        CART_ID = c.CART_ID,
                //        CREATED_AT = c.CREATED_AT,
                //        STATUS = c.STATUS,
                //        SESSION_ID = c.SESSION_ID,
                //        PRICE_PER_QYT = c.PRICE_PER_QYT,
                //        QYT = c.QYT,
                //        TOTAL_AMOUNT = c.TOTAL_AMOUNT,
                //        TOTAL_QTY = c.TOTAL_QTY,
                //        EMAIL_ID = context.M_GB_USER.FirstOrDefault(a => a.USER_ID == c.USER_ID).FIRST_NAME,
                //        USER_ID = c.USER_ID
                //    })).ToList();

                //}
            }
            catch (Exception exception)
            {
                throw;
            }
        }

        public IEnumerable<OrderDetailDto> GetOrdersList(int status)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.GB_ORDER.Where(r => r.STATUS == status).Select(r => new OrderDetailDto
                    {
                        ORDER_ID = r.ORDER_ID,
                        CREATED_AT = r.CREATED_AT,
                        UserName = r.M_GB_USER.USER_NAME ?? r.M_GB_USER.FIRST_NAME,
                        STATUS = r.STATUS,
                        Qty = r.GB_ORDER_DETAIL.Sum(a=>a.QUANTITY),
                        CHECKOUTTYPE = r.GB_ORDER_DETAIL.FirstOrDefault().CHECKOUTTYPE,
                        UNIT_PRICE = r.GB_ORDER_DETAIL.Sum(a=>a.UNIT_PRICE),
                        PAYMENT_TYPE = r.PAYMENT_TYPE,
                        LOGISTIC_TOKEN = r.LOGISTIC_TOKEN,
                        LOGISTIC_AWB = r.LOGISTIC_AWB,
                        DISCOUNT_AMOUNT = r.GB_ORDER_DETAIL.Sum(a=>a.DISCOUNT_AMOUNT),
                        DISCOUNT_VOUCHER = r.GB_ORDER_DETAIL.FirstOrDefault().DISCOUNT_VOUCHER
                    })).ToList();

                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }

        public IEnumerable<OrderDetailDto> GetOrder(int orderId)
        {
            try
            {
                var context = new GizmobabadbEntities1();
                    return (context.GB_ORDER.Where(r => r.ORDER_ID == orderId).Select(r => new OrderDetailDto
                    {
                        ORDER_ID = r.ORDER_ID,
                        CREATED_AT = r.CREATED_AT,
                        UserName = r.M_GB_USER.USER_NAME + r.M_GB_USER.FIRST_NAME,
                        STATUS = r.STATUS,
                        Qty = r.GB_ORDER_DETAIL.FirstOrDefault().QUANTITY,
                        CHECKOUTTYPE = r.GB_ORDER_DETAIL.FirstOrDefault().CHECKOUTTYPE,
                        UNIT_PRICE = r.GB_ORDER_DETAIL.FirstOrDefault().UNIT_PRICE,
                        OdDetail = r.GB_ORDER_DETAIL.FirstOrDefault(a=>a.ORDER_ID == orderId),
                        User = r.M_GB_USER,
                        PRODUCT_ID = r.GB_ORDER_DETAIL.FirstOrDefault().PRODUCT_ID,
                        Product = r.GB_ORDER_DETAIL.FirstOrDefault().GB_PRODUCT_DETAIL
                    })).ToList();
            }
            catch (Exception exception)
            {
                throw;
            }
        }

        public IEnumerable<OrderDetailDto> OrderSerch(OrderSerch orderDetail)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.GB_ORDER.Where(entry => (entry.CREATED_AT >= orderDetail.From_Date && entry.CREATED_AT <= orderDetail.To_Date) && entry.STATUS == orderDetail.OrderType).Select(r => new OrderDetailDto
                    {
                        ORDER_ID = r.ORDER_ID,
                        CREATED_AT = r.CREATED_AT,
                        UserName = r.M_GB_USER.USER_NAME + r.M_GB_USER.FIRST_NAME,
                        STATUS = r.STATUS,
                        Qty = r.GB_ORDER_DETAIL.FirstOrDefault().QUANTITY,
                        CHECKOUTTYPE = r.GB_ORDER_DETAIL.FirstOrDefault().CHECKOUTTYPE,
                        UNIT_PRICE = r.GB_ORDER_DETAIL.FirstOrDefault().UNIT_PRICE
                    })).ToList();

                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }

        public OrderDetailDto GetOrderDetails(int orderId)
        {
            try
            {
                var context = new GizmobabadbEntities1();
                return (context.GB_ORDER_DETAIL.Where(r => r.ORDER_ID == orderId).Select(r => new OrderDetailDto
                {
                    ORDER_ID = r.ORDER_ID,
                    CREATED_AT = r.CREATED_AT,
                    UserName = r.GB_ORDER.M_GB_USER.USER_NAME + r.GB_ORDER.M_GB_USER.FIRST_NAME,
                    STATUS = r.STATUS,
                    Qty = r.QUANTITY,
                    CHECKOUTTYPE = r.CHECKOUTTYPE,
                    UNIT_PRICE = r.UNIT_PRICE,
                    OdDetail = r,
                    User = r.GB_ORDER.M_GB_USER,
                    PRODUCT_ID = r.PRODUCT_ID,
                    Product = r.GB_PRODUCT_DETAIL,
                    CUSTOMER_ID = r.GB_ORDER.CUSTOMER_ID,
                    EMAIL = r.GB_ORDER.EMAIL,
                    PAYMENT_TYPE = r.GB_ORDER.PAYMENT_TYPE,
                    Address_Id = r.GB_ORDER.ADDRESS_ID ?? 0,
                })).FirstOrDefault();
            }
            catch (Exception exception)
            {
                throw;
            }
        }

        public int CancelOrder(OrderDto order)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    GB_ORDER mGbShipping = context.GB_ORDER.FirstOrDefault(o => o.ORDER_ID == order.ORDER_ID);
                    if (mGbShipping != null)
                    {
                        mGbShipping.STATUS = (int) CustomStatus.OrderStatus.Cancel;
                        mGbShipping.RESON = order.RESON;
                        mGbShipping.COMMENTS = order.COMMENTS;
                        mGbShipping.USERSHOWTHECOMMENTS = order.USERSHOWTHECOMMENTS;
                    }
                    context.SaveChanges();
                }
                return (int)CustomStatus.ReturnStatus.success;
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }

        public long AddOrder(GB_ORDER order)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    context.GB_ORDER.Add(order);
                    context.SaveChanges();
                    return order.ORDER_ID;
                }
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }

        public int AddOrderDetails(GB_ORDER_DETAIL orderDetails)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    context.GB_ORDER_DETAIL.Add(orderDetails);
                    context.SaveChanges();
                    return (int) CustomStatus.ReturnStatus.success;
                }
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }

        //public IEnumerable<OrderDetailDto> GetOrder()
        //{
        //        List<OrderDetailDto> detailDtos = new List<OrderDetailDto>
        //        {
        //            new OrderDetailDto(10,"Product 1", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 100,6,"User 1"),
        //            new OrderDetailDto(11,"Product 2", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 101,6,"User 2"),
        //            new OrderDetailDto(13,"Product 3", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 102,6,"User 3"),
        //            new OrderDetailDto(14,"Product 4", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 103,6,"User 4"),
        //            new OrderDetailDto(15,"Product 5", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 104,6,"User 5"),
        //            new OrderDetailDto(16,"Product 6", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 105,6,"User 6"),
        //            new OrderDetailDto(17,"Product 7", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 106,6,"User 7"),
        //            new OrderDetailDto(18,"Product 8", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 107,6,"User 8"),
        //            new OrderDetailDto(19,"Product 9", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 108,6,"User 9"),
        //            new OrderDetailDto(10,"Product 10", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 109,6,"User 10"),
        //            new OrderDetailDto(11,"Product 11", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 110,6,"User 11"),
        //            new OrderDetailDto(12,"Product 12", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 111,6,"User 12"),
        //            new OrderDetailDto(13,"Product 13", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 112,6,"User 13"),
        //            new OrderDetailDto(14,"Product 14", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 113,6,"User 14"),
        //            new OrderDetailDto(15,"Product 15", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 114,6,"User 15"),
        //            new OrderDetailDto(16,"Product 16", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 115,6,"User 16"),
        //            new OrderDetailDto(17,"Product 17", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 116,6,"User 17"),
        //            new OrderDetailDto(18,"Product 18", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 117,6,"User 18"),
        //            new OrderDetailDto(19,"Product 19", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 118,6,"User 19"),
        //            new OrderDetailDto(20,"Product 20", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 119,6,"User 20"),
        //            new OrderDetailDto(21,"Product 21", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 120,6,"User 21"),
        //            new OrderDetailDto(22,"Product 22", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 121,6,"User 22"),
        //            new OrderDetailDto(23,"Product 23", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 122,6,"User 23"),
        //            new OrderDetailDto(24,"Product 24", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 123,6,"User 24"),
        //            new OrderDetailDto(25,"Product 25", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 124,6,"User 25"),
        //            new OrderDetailDto(26,"Product 26", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 125,6,"User 26"),
        //            new OrderDetailDto(27,"Product 27", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 126,6,"User 27"),
        //            new OrderDetailDto(28,"Product 28", 10, 1500, "test", 0, 20, DateTime.Now, DateTime.Now, 1, 127,6,"User 28")
        //        };
        //    return detailDtos;
        //}


        public IEnumerable<OrderDetailDto> GetOrdersList(int orderStatus, int count)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.GB_ORDER.Where(r => r.STATUS == orderStatus).Select(r => new OrderDetailDto
                    {
                        ORDER_ID = r.ORDER_ID,
                        CREATED_AT = r.CREATED_AT,
                        UserName = r.M_GB_USER.USER_NAME + r.M_GB_USER.FIRST_NAME,
                        STATUS = r.STATUS,
                        Qty = r.GB_ORDER_DETAIL.FirstOrDefault().QUANTITY,
                        CHECKOUTTYPE = r.GB_ORDER_DETAIL.FirstOrDefault().CHECKOUTTYPE,
                        UNIT_PRICE = r.GB_ORDER_DETAIL.FirstOrDefault().UNIT_PRICE
                    })).ToList().Take(count);

                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }


        public IEnumerable<OrderDetailDto> GetOrderDetailsByOrderID(int orderId)
        {
            try
            {
                var context = new GizmobabadbEntities1();
                return (context.GB_ORDER_DETAIL.Where(r => r.ORDER_ID == orderId).Select(r => new OrderDetailDto
                {
                    ORDER_ID = r.ORDER_ID,
                    ORDER_DETAIL_ID = r.ORDER_DETAIL_ID,
                    CREATED_AT = r.CREATED_AT,
                    UserName = r.GB_ORDER.M_GB_USER.USER_NAME + r.GB_ORDER.M_GB_USER.FIRST_NAME,
                    STATUS = r.GB_ORDER.STATUS,
                    Qty = r.QUANTITY,
                    CHECKOUTTYPE = r.CHECKOUTTYPE,
                    UNIT_PRICE = r.UNIT_PRICE,
                    OdDetail = r,
                    User = r.GB_ORDER.M_GB_USER,
                    PRODUCT_ID = r.PRODUCT_ID,
                   // Product = r.GB_PRODUCT_DETAIL,
                    SourceImage = r.GB_PRODUCT_DETAIL.GB_PRODUCT_IMAGES.FirstOrDefault().IMAGE_URL,
                    PRODUCT_NAME = r.GB_PRODUCT_DETAIL.PRODUCT_NAME,
                    NUM_OF_PRO = r.NUM_OF_PRO,
                    ProSku =r.GB_PRODUCT_DETAIL.PRODUCT_SKU,
                    VideoURL = r.GB_PRODUCT_DETAIL.VIDEO_URL,
                    PAYMENT_TYPE = r.GB_ORDER.PAYMENT_TYPE,
                    CATEGORY_ID = r.GB_PRODUCT_DETAIL.CATEGORY_ID,
                    SPLIT_ORDER_ID = r.SPLIT_ORDER_ID,
                    LOGISTIC_TOKEN = r.GB_ORDER.LOGISTIC_TOKEN,
                    LOGISTIC_AWB = r.GB_ORDER.LOGISTIC_AWB,
                    DISCOUNT_AMOUNT = r.DISCOUNT_AMOUNT,
                    DISCOUNT_VOUCHER = r.DISCOUNT_VOUCHER
                })).ToList();
            }
            catch (Exception exception)
            {
                throw;
            }
        }


        public IEnumerable<OrderDetailDto> GetOrderList(int status, int paymentTypeId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.GB_ORDER.Where(r => r.STATUS == status && r.PAYMENT_TYPE == paymentTypeId).Select(r => new OrderDetailDto
                    {
                        ORDER_ID = r.ORDER_ID,
                        CREATED_AT = r.CREATED_AT,
                        UserName = r.M_GB_USER.USER_NAME + r.M_GB_USER.FIRST_NAME,
                        STATUS = r.STATUS,
                        Qty = r.GB_ORDER_DETAIL.Sum(a=>a.QUANTITY),
                        CHECKOUTTYPE = r.GB_ORDER_DETAIL.FirstOrDefault().CHECKOUTTYPE,
                        UNIT_PRICE = r.GB_ORDER_DETAIL.Sum(a=>a.UNIT_PRICE),
                        PAYMENT_TYPE = r.PAYMENT_TYPE,
                        Address_Id = r.ADDRESS_ID ?? 0,
                        MERGEDORDERID = r.MERGEDORDERID,
                        ISMERGEITEM = r.ISMERGEITEM,
                        CREATED_BY = r.CREATED_BY,
                        IS_SPLIT = r.IS_SPLIT,
                        SPLIT_ORDER_ID = r.SPLIT_ORDER_ID,
                        DISCOUNT_AMOUNT = r.GB_ORDER_DETAIL.FirstOrDefault().DISCOUNT_AMOUNT,
                        DISCOUNT_VOUCHER = r.GB_ORDER_DETAIL.FirstOrDefault().DISCOUNT_VOUCHER
                    })).ToList();

                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }


        public int UpdateOrderStatus(int orderId, int status)
        {
            try
            {
                // Update entity query
                using (var context = new GizmobabadbEntities1())
                {
                    GB_ORDER data = context.GB_ORDER.FirstOrDefault(a=>a.ORDER_ID == orderId);
                    if(data != null)
                    {
                        data.STATUS = status;
                        data.UPDATED_AT = DateTime.Now;
                    }
                    context.SaveChanges();
                }
                return (int)CustomStatus.ReturnStatus.success;
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }


        public int UpdatePickupOrderList(int orderId, int status, string LogisticType, string logisticTokens)
        {
            try
            {
                // Update entity query
                using (var context = new GizmobabadbEntities1())
                {
                    GB_ORDER data = context.GB_ORDER.FirstOrDefault(a => a.ORDER_ID == orderId);
                    if (data != null)
                    {
                        data.STATUS = status;
                        data.LOGISTIC_TYPE = LogisticType;
                        data.UPDATED_AT = DateTime.Now;
                        data.LOGISTIC_TOKEN = logisticTokens;
                    }
                    context.SaveChanges();
                }
                return (int)CustomStatus.ReturnStatus.success;
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }


        public int UpdateMergeStatus(int orderId,long newOrderId)
        {
            try
            {
                // Update entity query
                using (var context = new GizmobabadbEntities1())
                {
                    GB_ORDER data = context.GB_ORDER.FirstOrDefault(a => a.ORDER_ID == orderId);
                    if (data != null)
                    {
                        data.MERGEDORDERID = newOrderId;
                        data.UPDATED_AT = DateTime.Now;
                    }
                    context.SaveChanges();
                }
                return (int)CustomStatus.ReturnStatus.success;
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }


        public int UpdateSplitStatus(int orderId, bool SplitStatus)
        {
            try
            {
                // Update entity query
                using (var context = new GizmobabadbEntities1())
                {
                    GB_ORDER data = context.GB_ORDER.FirstOrDefault(a => a.ORDER_ID == orderId);
                    if (data != null)
                    {
                        data.IS_SPLIT = SplitStatus;
                        data.UPDATED_AT = DateTime.Now;
                    }
                    context.SaveChanges();
                }
                return (int)CustomStatus.ReturnStatus.success;
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }


        public IEnumerable<OrderDetailDto> GetAllShippedOrdersList()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.GB_ORDER.Where(r => r.STATUS == (int)CustomStatus.OrderStatus.ReadyToPickUp || r.STATUS == (int)CustomStatus.OrderStatus.InTransit || r.STATUS == (int)CustomStatus.OrderStatus.Delivered || r.STATUS == (int)CustomStatus.OrderStatus.RtoReturns).Select(r => new OrderDetailDto
                    {
                        ORDER_ID = r.ORDER_ID,
                        CREATED_AT = r.CREATED_AT,
                        UserName = r.M_GB_USER.USER_NAME + r.M_GB_USER.FIRST_NAME,
                        STATUS = r.STATUS,
                        Qty = r.GB_ORDER_DETAIL.FirstOrDefault().QUANTITY,
                        CHECKOUTTYPE = r.GB_ORDER_DETAIL.FirstOrDefault().CHECKOUTTYPE,
                        UNIT_PRICE = r.GB_ORDER_DETAIL.Sum(a => a.UNIT_PRICE),
                        PAYMENT_TYPE = r.PAYMENT_TYPE
                    })).ToList();

                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }


        public int AddProQty(int orderId, int quantity, int price)
        {
            try
            {
                // Update entity query
                using (var context = new GizmobabadbEntities1())
                {
                    GB_ORDER_DETAIL order = context.GB_ORDER_DETAIL.FirstOrDefault(a => a.ORDER_ID == orderId);
                    if (order != null )
                    {
                        if(order.QUANTITY == quantity)
                            return (int)CustomStatus.ReturnStatus.message;

                        order.QUANTITY = quantity;
                        order.UNIT_PRICE = price;
                        order.UPDATED_AT = DateTime.Now;
                    }
                    context.SaveChanges();
                }
                return (int)CustomStatus.ReturnStatus.success;
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }


        public IEnumerable<GB_ORDER_DETAIL> GetSubOrderList(int orderId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.GB_ORDER_DETAIL.Where(a => a.ORDER_ID == orderId).ToList();
                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }


        public int updateSubOrder(long subOrderId, int orderId)
        {
            try
            {
                // Update entity query
                using (var context = new GizmobabadbEntities1())
                {
                    GB_ORDER_DETAIL order = context.GB_ORDER_DETAIL.FirstOrDefault(a => a.ORDER_DETAIL_ID == subOrderId);
                    if (order != null)
                    {
                        order.SPLIT_ORDER_ID = orderId;
                        order.UPDATED_AT = DateTime.Now;
                    }
                    context.SaveChanges();
                }
                return (int)CustomStatus.ReturnStatus.success;
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }


        public IEnumerable<GB_ORDER> GetNewOrderList(int status, int paymentTypeId)
        {
            try
            {
                var context = new GizmobabadbEntities1();
                return context.GB_ORDER.Where(a => a.STATUS == status && a.PAYMENT_TYPE == paymentTypeId).ToList();
            }
            catch
            {
                throw;
            }
        }


        public GB_ORDER GetNewOrder(int id)
        {
            try
            {
                var context = new GizmobabadbEntities1();
                return context.GB_ORDER.FirstOrDefault(a => a.ORDER_ID == id);
            }
            catch
            {
                throw;
            }
        }


        public IList<GB_ORDER> GetMergedOrders(int id)
        {
            try
            {
                var context = new GizmobabadbEntities1();
                return context.GB_ORDER.Where(a => a.MERGEDORDERID == id).ToList();
            }
            catch
            {
                throw;
            }
        }

        public OrderDetailDto GetSingleOrderDetails(int orderId)
        {
            try
            {
                var context = new GizmobabadbEntities1();
                return (context.GB_ORDER.Where(r => r.ORDER_ID == orderId).Select(r => new OrderDetailDto
                {
                    ORDER_ID = r.ORDER_ID,
                    CREATED_AT = r.CREATED_AT,
                    STATUS = r.STATUS,
                    Qty = r.GB_ORDER_DETAIL.Where(a=>a.ORDER_ID == r.ORDER_ID).Sum(a=>a.QUANTITY),
                    UNIT_PRICE = r.GB_ORDER_DETAIL.Where(a => a.ORDER_ID == r.ORDER_ID).Sum(a => a.UNIT_PRICE),
                    CUSTOMER_ID = r.CUSTOMER_ID,
                    EMAIL = r.EMAIL,
                    PAYMENT_TYPE = r.PAYMENT_TYPE,
                    Address_Id = r.ADDRESS_ID ?? 0,
                    userAddress = context.GB_USER_ADDRESS.FirstOrDefault(a=>a.ADDRESS_ID == r.ADDRESS_ID),
                })).FirstOrDefault();
            }
            catch (Exception exception)
            {
                throw;
            }
        }


        public int UpdateOrderListToShipping(int orderId, int status, string awb)
        {
            try
            {
                // Update entity query
                using (var context = new GizmobabadbEntities1())
                {
                    GB_ORDER data = context.GB_ORDER.FirstOrDefault(a => a.ORDER_ID == orderId);
                    if (data != null)
                    {
                        data.STATUS = status;
                        data.LOGISTIC_AWB = awb;
                        data.UPDATED_AT = DateTime.Now;
                    }
                    context.SaveChanges();
                }
                return (int)CustomStatus.ReturnStatus.success;
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }
        public GB_ORDER_DETAIL GetSubOrder(int orderId)
        {
            using (var context = new GizmobabadbEntities1())
            {
                return context.GB_ORDER_DETAIL.FirstOrDefault(a => a.ORDER_ID == orderId);
            }
        }
        public GB_ORDER_DETAIL GetSubOrderDetails(int orderId)
        {
            using (var context = new GizmobabadbEntities1())
            {
                return context.GB_ORDER_DETAIL.FirstOrDefault(a => a.ORDER_DETAIL_ID == orderId);
            }
        }

        public IEnumerable<GB_ORDER_DETAIL> GetSplitOrderDetails(long splitOrderId)
        {
            using (var context = new GizmobabadbEntities1())
            {
                return context.GB_ORDER_DETAIL.Where(a => a.SPLIT_ORDER_ID == splitOrderId).ToList();
            }
        }

        public IEnumerable<GB_ORDER> SearchAuthorizeCodOrder(OrderSerch serch)
        {
            var context = new GizmobabadbEntities1();
            IEnumerable<GB_ORDER> orders =
                context.GB_ORDER.Where(
                    entry =>
                       (entry.STATUS == serch.STATUS) && (entry.PAYMENT_TYPE == serch.PAYMENT_TYPE) && (entry.CREATED_AT >= serch.From_Date && entry.CREATED_AT <= serch.To_Date )).ToList();
            return orders;

        }

        public int VoucherApply(long orderId, string voucherCode, decimal descountAmount)
        {
            try
            {
                // Update entity query
                using (var context = new GizmobabadbEntities1())
                {
                    GB_ORDER_DETAIL order = context.GB_ORDER_DETAIL.FirstOrDefault(a => a.ORDER_DETAIL_ID == orderId);
                    if (order != null)
                    {
                        order.DISCOUNT_AMOUNT = descountAmount;
                        order.DISCOUNT_VOUCHER = voucherCode;
                        order.UPDATED_AT = DateTime.Now;
                    }
                    context.SaveChanges();
                }
                return (int)CustomStatus.ReturnStatus.success;
            }
            catch (Exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }


        public IEnumerable<GB_ORDER> SearchOrders(OrderSerch serch)
        {
            var context = new GizmobabadbEntities1();
            //IEnumerable<GB_ORDER> orders =
            //    context.GB_ORDER.Where(
            //        entry =>
            //           (entry.STATUS == serch.STATUS) && (entry.PAYMENT_TYPE == serch.PAYMENT_TYPE) && (entry.CREATED_AT >= serch.From_Date && entry.CREATED_AT <= serch.To_Date)).ToList();

            //string query = "select * from GB_ORDER a inner join GB_USER_ADDRESS b on a.ADDRESS_ID=b.ADDRESS_ID where  a.STATUS=" + serch.STATUS + "  ";
            string query = "select * from GB_ORDER a inner join GB_USER_ADDRESS b on a.ADDRESS_ID=b.ADDRESS_ID inner join M_GB_USER c on a.CUSTOMER_ID=c.ID where  a.STATUS=" + serch.STATUS + " ";
            if (serch.PAYMENT_TYPE != 0)
                query += " and a.PAYMENT_TYPE=" + serch.PAYMENT_TYPE + "";
            if (serch.From_Date != null && serch.To_Date != null)
                query += " and (a.CREATED_AT >= '"+serch.From_Date+"' and a.CREATED_AT <= '"+serch.To_Date+"') ";
            if (!string.IsNullOrEmpty(serch.Order_No))
                query += " and a.ORDER_ID=" + serch.Order_No + " ";
            if (!string.IsNullOrEmpty(serch.Email_Id))
                query += " and a.EMAIL='"+serch.Email_Id+"' ";
               if (!string.IsNullOrEmpty(serch.Phone_No))
                query += " and b.mob_no="+serch.Phone_No+" ";
            if(serch.Created_By == ((int)CustomStatus.UserTypes.Dealer).ToString())
                query += " and c.ROLE=" + serch.Created_By + " ";

            IEnumerable<GB_ORDER> orders = context.GB_ORDER.SqlQuery(query).ToList<GB_ORDER>();

            return orders;

        }
        public IEnumerable<OrderDetailDto> GetPaymentPendingOrdersList()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.GB_ORDER.Where(r => (r.PAYMENT_TYPE == (int)CustomStatus.PaymentTypes.COD && r.STATUS != (int)CustomStatus.OrderStatus.Delivered && r.GB_ORDER_DETAIL.Count() != 0 )).Select(r => new OrderDetailDto
                    {
                        ORDER_ID = r.ORDER_ID,
                        CREATED_AT = r.CREATED_AT,
                        UserName = r.M_GB_USER.USER_NAME ?? r.M_GB_USER.FIRST_NAME,
                        STATUS = r.STATUS,
                        Qty = r.GB_ORDER_DETAIL.Sum(a=>a.QUANTITY),
                        CHECKOUTTYPE = r.PAYMENT_TYPE.ToString(),
                        UNIT_PRICE = r.GB_ORDER_DETAIL.Sum(a => a.UNIT_PRICE),
                        PAYMENT_TYPE = r.PAYMENT_TYPE,
                        LOGISTIC_TOKEN = r.LOGISTIC_TOKEN,
                        LOGISTIC_AWB = r.LOGISTIC_AWB,
                        DISCOUNT_AMOUNT = r.GB_ORDER_DETAIL.Sum(a => a.DISCOUNT_AMOUNT),
                        DISCOUNT_VOUCHER = r.GB_ORDER_DETAIL.FirstOrDefault().DISCOUNT_VOUCHER,
                        ORDER_DETAIL_ID = r.GB_ORDER_DETAIL.FirstOrDefault().ORDER_DETAIL_ID
                    })).ToList();

                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }


        public IEnumerable<GB_ORDER> SearchPaymentPendingOrder(OrderSerch serch)
        {
            GizmobabadbEntities1 context = new GizmobabadbEntities1();
            //using (var context = new GizmobabadbEntities1())
            //{
                string query = "select * from GB_ORDER  where  PAYMENT_TYPE = " + (int)CustomStatus.PaymentTypes.COD + " and  STATUS !=" + (int)CustomStatus.OrderStatus.Delivered + "  ";
                //if (serch.PAYMENT_TYPE != 0)
                //    query += " and a.PAYMENT_TYPE=" + serch.PAYMENT_TYPE + "";
                if (serch.From_Date != null && serch.To_Date != null)
                    query += " and (CREATED_AT >= '" + serch.From_Date + "' and CREATED_AT <= '" + serch.To_Date + "') ";
                if (!string.IsNullOrEmpty(serch.Order_No))
                    query += " and ORDER_ID =" + serch.Order_No + " ";
                if (!string.IsNullOrEmpty(serch.Email_Id))
                    query += " and EMAIL='" + serch.Email_Id + "' ";
                if (!string.IsNullOrEmpty(serch.Phone_No))
                    query += " and mob_no =" + serch.Phone_No + " ";
                if (serch.Created_By == ((int)CustomStatus.UserTypes.Dealer).ToString())
                    query += " and CREATED_BY =" + serch.Created_By + " ";

                IEnumerable<GB_ORDER> orders = context.GB_ORDER.SqlQuery(query).ToList<GB_ORDER>();
                return orders.Where(a=>a.GB_ORDER_DETAIL != null && a.GB_ORDER_DETAIL.Count() != 0).ToList();
           // }
        }


        public IEnumerable<OrderDetailDto> GetVendorOrderList(string id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    List<OrderDetailDto> orderList  = new List<OrderDetailDto>();

                    List<ProductDTO> pro = context.GB_PRODUCT_DETAIL.Where(a=>a.CREATED_BY == id).Select(p => new ProductDTO {
                    PRODUCT_ID = p.PRODUCT_ID,
                    CREATED_BY = p.CREATED_BY
                    }).ToList();

                    foreach(ProductDTO item in pro)
                    {
                        //List<OrderDetailDto> order = context.GB_ORDER_DETAIL.Where(a=>a.PRODUCT_ID == item.PRODUCT_ID).ToList();
                        List<OrderDetailDto> order = (context.GB_ORDER_DETAIL.Where(a=>a.PRODUCT_ID == item.PRODUCT_ID).Select(r => new OrderDetailDto
                        {
                            ORDER_ID = r.ORDER_ID,
                            CREATED_AT = r.CREATED_AT,
                            STATUS = context.GB_ORDER.FirstOrDefault(a=>a.ORDER_ID == r.ORDER_ID).STATUS,
                            Qty = r.QUANTITY,
                            UNIT_PRICE = r.UNIT_PRICE,
                            DISCOUNT_AMOUNT = r.DISCOUNT_AMOUNT,
                            DISCOUNT_VOUCHER = r.DISCOUNT_VOUCHER,
                            ORDER_DETAIL_ID = r.ORDER_DETAIL_ID
                        })).ToList();
                        foreach(OrderDetailDto ord in order)
                        {
                            orderList.Add(ord);
                        }
                    }
                    return orderList;

                    //return (context.GB_ORDER_DETAIL.Where(r => ( r..Count() != 0)).Select(r => new OrderDetailDto
                    //{
                    //    ORDER_ID = r.ORDER_ID,
                    //    CREATED_AT = r.CREATED_AT
                    //    UserName = r.M_GB_USER.USER_NAME ?? r.M_GB_USER.FIRST_NAME,
                    //    STATUS = r.STATUS,
                    //    Qty = r.GB_ORDER_DETAIL.Sum(a => a.QUANTITY),
                    //    CHECKOUTTYPE = r.PAYMENT_TYPE.ToString(),
                    //    UNIT_PRICE = r.GB_ORDER_DETAIL.Sum(a => a.UNIT_PRICE),
                    //    PAYMENT_TYPE = r.PAYMENT_TYPE,
                    //    LOGISTIC_TOKEN = r.LOGISTIC_TOKEN,
                    //    LOGISTIC_AWB = r.LOGISTIC_AWB,
                    //    DISCOUNT_AMOUNT = r.GB_ORDER_DETAIL.Sum(a => a.DISCOUNT_AMOUNT),
                    //    DISCOUNT_VOUCHER = r.GB_ORDER_DETAIL.FirstOrDefault().DISCOUNT_VOUCHER,
                    //    ORDER_DETAIL_ID = r.GB_ORDER_DETAIL.FirstOrDefault().ORDER_DETAIL_ID
                    //})).ToList();

                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }
    }
}
