﻿using System;
using System.Linq;
using Gizmobaba.Repository.Interfaces;
using Gizmobaba.Utility;

namespace Gizmobaba.Repository
{
    public class DashboardRepository : IDashboardRepository
    {

        public DashboardDto Get()
        {
            DashboardDto dashDto= new DashboardDto();
            using (var context = new GizmobabadbEntities1())
            {
                var sum = context.GB_PRODUCT_DETAIL.Where(a=>a.STATUS != (int)CustomStatus.DefaultStatus.Delete).Sum(a=>a.RESERVE_QUANTITY);
                if (sum != null)
                {
                    dashDto = new DashboardDto
                    {
                        TotalUsers = context.M_GB_USER.Count(a=>a.ROLE == (int)CustomStatus.UserTypes.Customer && a.STATUS != (int)CustomStatus.DefaultStatus.Delete),
                        TotalDealer = context.M_GB_USERDETAILS.Count(a => a.ROLE_ID == (int)CustomStatus.UserTypes.Dealer && a.STATE == (int)CustomStatus.DefaultStatus.Active),
                        TotalMasterFranchise = context.M_GB_USERDETAILS.Count(a=>a.ROLE_ID == (int)CustomStatus.UserTypes.Master_Franchise && a.STATE == (int)CustomStatus.DefaultStatus.Active),
                        TotalOrders = context.GB_ORDER.Count(),
                        TotalSales = context.GB_ORDER.Count(x => x.STATUS != (int)CustomStatus.OrderStatus.Cancel),
                        TotalInventory = sum.Value,
                        TotalVendor = context.M_GB_USER.Count(a => a.ROLE == (int)CustomStatus.UserTypes.Global_Vender && a.STATUS == (int)CustomStatus.DefaultStatus.Active),
                    };
                }
                return dashDto;
            }
        }


        public DashboardDto GetGlobalVenderData(string id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    DashboardDto dashDto = new DashboardDto
                    {
                        TotalProductCount = context.GB_PRODUCT_DETAIL.Count(a=>a.CREATED_BY == id),
                        TotalOrdersCount = context.GB_ORDER_DETAIL.Count(a=>a.GB_PRODUCT_DETAIL.CREATED_BY == id.ToString()),
                        NewOrdersCount = context.GB_ORDER_DETAIL.Count(a=>a.STATUS == (int)CustomStatus.DefaultStatus.DeActive && a.GB_PRODUCT_DETAIL.CREATED_BY == id.ToString()),
                        ApprovedOrdersCount = context.GB_ORDER_DETAIL.Count(a=>a.STATUS == (int)CustomStatus.DefaultStatus.Active && a.GB_PRODUCT_DETAIL.CREATED_BY == id.ToString())
                    };
                    return dashDto;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DashboardDto GetMasterFranchiseDashboard(string id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    int UserId = 0;
                    if (context.M_GB_USERDETAILS.Count(a => a.USER_ID == id) != 0)
                        UserId = context.M_GB_USERDETAILS.FirstOrDefault(a => a.USER_ID == id).USERDETAIL_ID;
                    DashboardDto dashDto = new DashboardDto
                    {
                        TotalDealer = context.M_GB_USERDETAILS.Count(a => a.CREATED_BY == UserId)
                        //TotalOrdersCount = context.GB_ORDER_DETAIL.Count(a => a.GB_PRODUCT_DETAIL.CREATED_BY == id.ToString()),
                        //NewOrdersCount = context.GB_ORDER_DETAIL.Count(a => a.STATUS == (int)CustomStatus.DefaultStatus.DeActive && a.GB_PRODUCT_DETAIL.CREATED_BY == id.ToString()),
                        //ApprovedOrdersCount = context.GB_ORDER_DETAIL.Count(a => a.STATUS == (int)CustomStatus.DefaultStatus.Active && a.GB_PRODUCT_DETAIL.CREATED_BY == id.ToString())
                    };
                    return dashDto;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
