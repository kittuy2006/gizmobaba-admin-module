﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizmobaba.Repository.Interfaces;
using Gizmobaba.Utility;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Gizmobaba.Repository
{
    public class UserRepository : IUserRepository
    {
        public UserDto UserDetails(string name)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (from u in context.M_GB_USER
                            where u.USER_NAME.Contains(name) || u.USER_ID.Contains(name)
                            select new UserDto
                            {
                                USER_NAME = u.FIRST_NAME +" "+ u.LAST_NAME,
                                EMAIL = u.EMAIL,
                                USER_ID = u.USER_ID,
                                MOB_NO = u.MOB_NO,
                                UserAddresss = context.GB_USER_ADDRESS.Where(a => a.CUSTOMER_ID == u.USER_ID).ToList()
                            }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string AddUser(M_GB_USER user)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    M_GB_USER userData = context.M_GB_USER.FirstOrDefault(a => a.USER_ID == user.USER_ID);
                    if (userData == null  )
                    {
                        DataTable dt = new DataTable("UserDetails");
                        DataSet ds = new DataSet("AMD");
                        try
                        {
                            using (SqlConnection con = new SqlConnection("data source=182.50.133.111;initial catalog=psquaredb;user id=psquareadmin;password=Psqaureqwerty"))
                            {
                                if (string.IsNullOrEmpty(user.ALT_MOB))
                                    user.ALT_MOB = "0";
                                user.ALT_MOB = Regex.Replace(user.ALT_MOB, "[^a-zA-Z0-9_]+", "");

                                SqlCommand sqlComm = new SqlCommand("Proc_InsertUserInfo", con);
                                sqlComm.Parameters.Add("@Name", SqlDbType.VarChar).Value = user.USER_NAME;
                                sqlComm.Parameters.Add("@Password", SqlDbType.VarChar).Value = user.PASSWORD;
                                sqlComm.Parameters.Add("@email", SqlDbType.VarChar).Value = user.EMAIL != null ? user.EMAIL : " ";
                                sqlComm.Parameters.Add("@mob", SqlDbType.BigInt).Value = user.ALT_MOB != null ? user.ALT_MOB : "0";
                                sqlComm.Parameters.Add("@LOGIN_TYPE", SqlDbType.Int).Value = Convert.ToInt32(0);
                                sqlComm.Parameters.Add("@FIRST_NAME", SqlDbType.VarChar).Value = user.FIRST_NAME != null ? user.LAST_NAME : " ";
                                sqlComm.Parameters.Add("@PICTURE", SqlDbType.VarChar).Value = "";
                                sqlComm.Parameters.Add("@LAST_NAME", SqlDbType.VarChar).Value = user.LAST_NAME;
                                sqlComm.Parameters.Add("@F_ID", SqlDbType.VarChar).Value = "";
                                sqlComm.CommandType = CommandType.StoredProcedure;
                                SqlDataAdapter da = new SqlDataAdapter();
                                da.SelectCommand = sqlComm;
                                da.Fill(ds);
                                dt = ds.Tables[0];
                                if (dt != null && dt.Rows.Count != 0)
                                {
                                    if (dt.Rows[0]["resultcode"].ToString() != "0")
                                    {
                                        return dt.Rows[0]["USER_ID"].ToString();
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            return "0";
                        }







                        userData = context.M_GB_USER.FirstOrDefault(a => a.EMAIL == user.EMAIL);
                        if (userData == null )
                        {
                            var mGbUser = context.M_GB_USER.OrderByDescending(a => a.USER_ID).FirstOrDefault();
                            if (mGbUser != null)
                            {
                                Int64 userId = Convert.ToInt64(mGbUser.USER_ID);
                                user.USER_ID = (userId + 1).ToString();
                            }
                            user.USER_NAME = user.FIRST_NAME + " " + user.LAST_NAME;
                            context.M_GB_USER.Add(user);
                            context.SaveChanges();
                            return user.USER_ID;
                        }
                        return "0";
                    }
                    else
                    {
                        userData.USER_NAME = user.USER_NAME ?? userData.USER_NAME;
                        userData.EMAIL = user.EMAIL ?? userData.EMAIL;
                        userData.MOB_NO = user.MOB_NO ?? userData.MOB_NO;
                        userData.ALT_EMAIL = user.ALT_EMAIL ?? userData.ALT_EMAIL;
                        userData.ALT_MOB  = user.ALT_MOB ?? userData.ALT_MOB;
                        userData.CITY = user.CITY ?? userData.CITY;
                        userData.STATE =user.STATE ?? userData.STATE;
                        userData.ADD1 = user.ADD1 ?? userData.ADD1;
                        userData.ADD2 = user.ADD2 ?? userData.ADD2;
                        userData.PICTURE = user.PICTURE ?? userData.PICTURE;
                    }
                    context.SaveChanges();
                    return CustomStatus.ReturnStatus.success.ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public int AddUserAddress(GB_USER_ADDRESS userAddress)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    context.GB_USER_ADDRESS.Add(userAddress);
                    context.SaveChanges();
                    return (int)CustomStatus.ReturnStatus.success;
                }
            }
            catch (Exception ex)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }


        public List<UserDto> GetAllUsers(int id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (from u in context.M_GB_USER
                            select new UserDto
                            {
                                USER_NAME = u.FIRST_NAME + " " + u.LAST_NAME,
                                EMAIL = u.EMAIL,
                                USER_ID = u.USER_ID,
                                MOB_NO = u.MOB_NO,
                                PASSWORD = u.PASSWORD,
                                CREATED_ON = u.CREATED_ON,
                                UPDATED_ON = u.UPDATED_ON,
                                STATUS = u.STATUS
                               // OrderList = context.GB_ORDER.Where(a=>a.CUSTOMER_ID == Convert.ToInt32(u.USER_ID)).ToList()
                               // UserAddresss = context.GB_USER_ADDRESS.Where(a => a.CUSTOMER_ID == u.USER_ID).ToList()
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<UserDto> UserSearch(int key, string serarchKey, DateTime frmDate, DateTime toDate)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (from u in context.M_GB_USER
                            where (u.CREATED_ON >= frmDate && u.CREATED_ON <= toDate) || u.USER_ID == key.ToString() 
                            select new UserDto
                            {
                                USER_NAME = u.FIRST_NAME + " " + u.LAST_NAME,
                                EMAIL = u.EMAIL,
                                USER_ID = u.USER_ID,
                                MOB_NO = u.MOB_NO,
                                PASSWORD = u.PASSWORD,
                                OrderList = context.GB_ORDER.Where(a => a.CUSTOMER_ID == Convert.ToInt32(u.USER_ID)).ToList()
                                // UserAddresss = context.GB_USER_ADDRESS.Where(a => a.CUSTOMER_ID == u.USER_ID).ToList()
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int UpdateUserStatus(int userId, int statusId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    M_GB_USER user = context.M_GB_USER.FirstOrDefault(a=>a.USER_ID == userId.ToString());
                    if (user != null)
                    {
                        user.ROLE = statusId;
                        user.UPDATED_ON = DateTime.Now;
                    }
                    context.SaveChanges();
                    return (int)CustomStatus.ReturnStatus.success;
                }
            }
            catch (Exception ex)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }


        public IEnumerable<UserDto> GetUsersbyCount(int count, int roleId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    if(roleId == 0)
                    return (from u in context.M_GB_USER
                            where u.ROLE != (int)CustomStatus.UserTypes.Admin && !string.IsNullOrEmpty(u.EMAIL)
                            select new UserDto
                            {
                                USER_NAME = u.FIRST_NAME + " " + u.LAST_NAME,
                                EMAIL = u.EMAIL,
                                USER_ID = u.USER_ID,
                                MOB_NO = u.MOB_NO,
                                PASSWORD = u.PASSWORD,
                                CREATED_ON = u.CREATED_ON,
                                UPDATED_ON = u.UPDATED_ON,
                                STATUS = u.STATUS,
                                ID = u.ID
                                // OrderList = context.GB_ORDER.Where(a=>a.CUSTOMER_ID == Convert.ToInt32(u.USER_ID)).ToList()
                                // UserAddresss = context.GB_USER_ADDRESS.Where(a => a.CUSTOMER_ID == u.USER_ID).ToList()
                            }).Take(count).OrderByDescending(a=>a.ID).ToList();
                    else if(roleId == (int)CustomStatus.UserTypes.Dealer || roleId == (int)CustomStatus.UserTypes.Master_Franchise)
                        return (from u in context.M_GB_USERDETAILS
                            where u.ROLE_ID == roleId && !string.IsNullOrEmpty(u.EMAIL)
                            select new UserDto
                            {
                                USER_NAME = u.NAME,
                                EMAIL = u.EMAIL,
                                USER_ID = u.USER_ID,
                                MOB_NO = u.MOBILE_NUMBER,
                                PASSWORD = "",
                                CREATED_ON = u.CREATED_ON,
                                UPDATED_ON = DateTime.Now,
                                STATUS = u.STATUS
                                // OrderList = context.GB_ORDER.Where(a=>a.CUSTOMER_ID == Convert.ToInt32(u.USER_ID)).ToList()
                                // UserAddresss = context.GB_USER_ADDRESS.Where(a => a.CUSTOMER_ID == u.USER_ID).ToList()
                            }).Take(count).ToList();

                    return (from u in context.M_GB_USER
                            where u.ROLE == roleId 
                            select new UserDto
                            {
                                USER_NAME = u.USER_NAME ?? u.FIRST_NAME + " " + u.LAST_NAME,
                                EMAIL = u.EMAIL,
                                USER_ID = u.USER_ID,
                                MOB_NO = u.MOB_NO,
                                PASSWORD = u.PASSWORD,
                                CREATED_ON = u.CREATED_ON,
                                UPDATED_ON = u.UPDATED_ON,
                                STATUS = u.STATUS,
                                ID = u.ID
                                // OrderList = context.GB_ORDER.Where(a=>a.CUSTOMER_ID == Convert.ToInt32(u.USER_ID)).ToList()
                                // UserAddresss = context.GB_USER_ADDRESS.Where(a => a.CUSTOMER_ID == u.USER_ID).ToList()
                            }).Take(count).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public M_GB_USER GetUser(int userId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.M_GB_USER.FirstOrDefault(a => a.USER_ID == userId.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IEnumerable<UserDto> GetUsers(DateTime frmDate, DateTime toDate)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (from u in context.M_GB_USER
                            where u.ROLE != (int)CustomStatus.UserTypes.Admin && (u.CREATED_ON >= frmDate && u.CREATED_ON <= toDate)  
                            select new UserDto
                            {
                                USER_NAME = u.FIRST_NAME + " " + u.LAST_NAME,
                                EMAIL = u.EMAIL,
                                USER_ID = u.USER_ID,
                                MOB_NO = u.MOB_NO,
                                PASSWORD = u.PASSWORD,
                                CREATED_ON = u.CREATED_ON,
                                UPDATED_ON = u.UPDATED_ON,
                                STATUS = u.STATUS
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IEnumerable<UserDto> GetUsersSearch(UserDto data)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (from u in context.M_GB_USER
                            where ( (u.ROLE != (int)CustomStatus.UserTypes.Admin && u.ROLE == data.ROLE)  || (u.CREATED_ON >= data.FromDate && u.CREATED_ON <= data.ToDate) || u.EMAIL == data.EMAIL)
                            select new UserDto
                            {
                                USER_NAME = u.FIRST_NAME + " " + u.LAST_NAME,
                                EMAIL = u.EMAIL,
                                USER_ID = u.USER_ID,
                                MOB_NO = u.MOB_NO,
                                PASSWORD = u.PASSWORD,
                                CREATED_ON = u.CREATED_ON,
                                UPDATED_ON = u.UPDATED_ON,
                                STATUS = u.STATUS
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public M_GB_USER GetUserByEmailID(string email)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.M_GB_USER.FirstOrDefault(a => a.EMAIL == email);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IEnumerable<GB_USER_ADDRESS> GetUserAddress(string userId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.GB_USER_ADDRESS.Where(a => a.CUSTOMER_ID == userId).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Dealer> GetActiveDealers(int status)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (from u in context.M_GB_USERDETAILS
                            where u.ROLE_ID == (int)CustomStatus.UserTypes.Dealer && u.STATUS == status
                            select new Dealer
                            {
                                USER_NAME = u.NAME,
                                EMAIL = u.EMAIL,
                                USER_ID = u.USER_ID,
                                WALLET = u.WALLET,
                                AMOUNT = u.AMOUNT,
                                CREATED_ON = u.CREATED_ON,
                                STATE = u.STATE,
                                USERDETAIL_ID = u.USERDETAIL_ID,
                                STATUS = u.STATUS,
                                CITY_NAME = context.M_GB_REGION.FirstOrDefault(a=>a.REGION_ID == u.CITY).REGION
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Dealer> GetSearchDealer(DateTime frmDate, DateTime tDate, string userName, string userId, string pincode)
        {
            try
            {
                decimal PinCode =0;
                 if (!string.IsNullOrEmpty(pincode))
                 PinCode = Convert.ToDecimal(pincode);
                using (var context = new GizmobabadbEntities1())
                {
                    return (from u in context.M_GB_USERDETAILS
                            where ((u.ROLE_ID == (int)CustomStatus.UserTypes.Dealer) &&  (u.USER_ID == userId || u.NAME == userName || u.PINCODE == PinCode))
                            select new Dealer
                            {
                                USER_NAME = u.NAME,
                                EMAIL = u.EMAIL,
                                USER_ID = u.USER_ID,
                                ALT_MOB = u.MOBILE_NUMBER,
                                CREATED_ON = u.CREATED_ON,
                                STATUS = u.STATUS,
                                WALLET = u.WALLET,
                                CITY_NAME = context.M_GB_REGION.FirstOrDefault(a=>a.REGION_ID == u.CITY).REGION
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Dealer> GetMasterFranchise(int status)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (from u in context.M_GB_USERDETAILS
                            where u.ROLE_ID == (int)CustomStatus.UserTypes.Master_Franchise && u.STATUS == status
                            select new Dealer
                            {
                                USER_NAME = u.NAME,
                                EMAIL = u.EMAIL,
                                USER_ID = u.USER_ID,
                                WALLET = u.WALLET,
                                AMOUNT = u.AMOUNT,
                                CREATED_ON = u.CREATED_ON,
                                STATE = u.STATE,
                                USERDETAIL_ID = u.USERDETAIL_ID
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Dealer> GetSearchMasterFranchise(DateTime fromDate, DateTime toDate)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (from u in context.M_GB_USERDETAILS
                            where u.ROLE_ID == (int)CustomStatus.UserTypes.Master_Franchise && (u.CREATED_ON >= fromDate && u.CREATED_ON < toDate)
                            select new Dealer
                            {
                                USER_NAME = u.NAME,
                                EMAIL = u.EMAIL,
                                USER_ID = u.USER_ID,
                                WALLET = u.WALLET,
                                AMOUNT = u.AMOUNT,
                                CREATED_ON = u.CREATED_ON,
                                STATE = u.STATE,
                                USERDETAIL_ID = u.USERDETAIL_ID
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string AddDealerDetails(M_GB_USERDETAILS userdetails)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    M_GB_USERDETAILS userData = context.M_GB_USERDETAILS.FirstOrDefault(a => a.USERDETAIL_ID == userdetails.USERDETAIL_ID);
                    if (userData == null)
                    {
                        context.M_GB_USERDETAILS.Add(userdetails);
                        int masterFcId = Convert.ToInt32(userdetails.CREATED_BY); 
                        M_GB_USERDETAILS recharge =
                            context.M_GB_USERDETAILS.FirstOrDefault(
                                a => a.ROLE_ID == (int)CustomStatus.UserTypes.Master_Franchise && a.STATUS == (int)CustomStatus.DefaultStatus.Active && a.USERDETAIL_ID == masterFcId);
                        decimal wat = Convert.ToDecimal(recharge.WALLET);
                        int wallet = Convert.ToInt32(wat);
                        if (recharge != null)
                        {
                            recharge.WALLET = (Convert.ToDecimal(recharge.WALLET) - Convert.ToDecimal(userdetails.WALLET)).ToString();

                        }
                        decimal dealerWallet = Convert.ToDecimal(userdetails.WALLET);
                        GB_DEALER_RECHARGE MFRechargerecharge = new GB_DEALER_RECHARGE
                            {
                                RECHARGE_POINTS = Convert.ToInt32(dealerWallet),
                                USERDETAIL_ID = masterFcId,
                                ROLE_ID = (int)CustomStatus.UserTypes.Master_Franchise,
                                STATUS = (int)CustomStatus.DefaultStatus.Active,
                                USER_ADMIN_ID = (int)CustomStatus.UserTypes.Admin,
                                CREATED_BY = userdetails.USER_ID,
                                CREATED_ON = DateTime.Now
                            };
                        context.GB_DEALER_RECHARGE.Add(MFRechargerecharge);
                        //GB_DEALER_RECHARGE dealerRechargerecharge = new GB_DEALER_RECHARGE();
                        //dealerRechargerecharge.RECHARGE_POINTS = wallet;
                        //dealerRechargerecharge.USERDETAIL_ID = masterFcId;
                        //dealerRechargerecharge.ROLE_ID = (int) CustomStatus.UserTypes.Master_Franchise;
                        //dealerRechargerecharge.USER_ADMIN_ID = (int) CustomStatus.UserTypes.Admin;
                        //dealerRechargerecharge.STATUS = (int) CustomStatus.DefaultStatus.Active;
                        //dealerRechargerecharge.CREATED_BY = masterFcId.ToString();
                        //dealerRechargerecharge.CREATED_ON = DateTime.Now;
                        //context.GB_DEALER_RECHARGE.Add(dealerRechargerecharge);

                      
                        context.SaveChanges();
                       // string status = UpdateRecharge(dealerRechargerecharge);
                    }
                    else
                    {
                        userData.EMAIL = userdetails.EMAIL;
                        userData.COMPANY = userdetails.COMPANY;
                        userData.CITY = userdetails.CITY;
                        userData.STATE = userdetails.STATE;
                        if (userdetails.AMOUNT != 0 && userdetails.AMOUNT != userData.AMOUNT)
                        {
                            userData.AMOUNT = userData.AMOUNT + userdetails.AMOUNT;
                            userData.WALLET = userData.AMOUNT.ToString();
                        }
                        userData.ADDRESS = userdetails.ADDRESS;
                        userData.ADDRESS_LINE = userdetails.ADDRESS_LINE;
                        userData.MOBILE_NUMBER = userdetails.MOBILE_NUMBER;
                        userData.PINCODE = userdetails.PINCODE;
                        if (userdetails.GB_USER_CATEGORY != null && userdetails.GB_USER_CATEGORY.Count() != 0)
                        {
                            context.GB_USER_CATEGORY.RemoveRange(userData.GB_USER_CATEGORY);
                            context.SaveChanges();
                           foreach(GB_USER_CATEGORY item in userdetails.GB_USER_CATEGORY)
                           {
                               context.GB_USER_CATEGORY.Add(item);
                               context.SaveChanges();
                           }
                        }
                    }
                    M_GB_USERDETAILS masterMGbUserdetailsData = context.M_GB_USERDETAILS.FirstOrDefault(a => a.USERDETAIL_ID == userdetails.CREATED_BY);
                    if (masterMGbUserdetailsData != null)
                    {
                        masterMGbUserdetailsData.AMOUNT = masterMGbUserdetailsData.AMOUNT - userdetails.AMOUNT;
                    }
                    context.SaveChanges();
                    return userdetails.USERDETAIL_ID.ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public string AddMasterFranchiseDetails(M_GB_USERDETAILS userdetails)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    M_GB_USERDETAILS userData = context.M_GB_USERDETAILS.FirstOrDefault(a => a.USERDETAIL_ID == userdetails.USERDETAIL_ID);
                    if (userData == null)
                        context.M_GB_USERDETAILS.Add(userdetails);
                    else
                    {
                        userData.EMAIL = userdetails.EMAIL;
                        userData.COMPANY = userdetails.COMPANY;
                        userData.CITY = userdetails.CITY;
                        userData.STATE = userdetails.STATE;
                        userData.WALLET = userdetails.WALLET;
                        if (userData.AMOUNT != userdetails.AMOUNT)
                        {
                            userData.AMOUNT = userdetails.AMOUNT + userData.AMOUNT;
                            userData.WALLET = userData.AMOUNT.ToString();
                        }
                        userData.NAME = userdetails.NAME;
                        userData.COUNTRY = userdetails.COUNTRY;
                        userData.STATE = userdetails.STATE;
                        userData.CITY = userdetails.CITY;
                        userData.ADDRESS_LINE = userdetails.ADDRESS_LINE;
                        userData.ADDRESS = userdetails.ADDRESS;
                        userData.MOBILE_NUMBER = userdetails.MOBILE_NUMBER;
                        userData.PINCODE = userdetails.PINCODE;
                    }
                    context.SaveChanges();
                    return userdetails.USERDETAIL_ID.ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public IEnumerable<UserDto> GetMasterFranchisebyStateId(int id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (from u in context.M_GB_USERDETAILS
                            where u.ROLE_ID == (int)CustomStatus.UserTypes.Master_Franchise && u.STATUS == (int)CustomStatus.DefaultStatus.Active && u.STATE == id
                            select new UserDto
                            {
                                USER_NAME = u.NAME,
                                EMAIL = u.EMAIL,
                                USER_ID = u.USER_ID,
                                STATE = u.STATE.ToString(),
                                STATUS = u.STATUS
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Dealer GetMasterWallet(int masterWalletId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (from u in context.M_GB_USERDETAILS
                        where
                            u.ROLE_ID == (int) CustomStatus.UserTypes.Master_Franchise &&
                           ( u.USER_ID == masterWalletId.ToString() || u.USERDETAIL_ID == masterWalletId )
                        select new Dealer
                        {
                            USER_NAME = u.NAME,
                            EMAIL = u.EMAIL,
                            USER_ID = u.USER_ID,
                            WALLET = u.WALLET,
                            AMOUNT = u.AMOUNT,
                            CREATED_ON = u.CREATED_ON,
                            STATE = u.STATE,
                            USERDETAIL_ID = u.USERDETAIL_ID,
                            COUNTRY = u.COUNTRY,
                            CITY = u.CITY,
                            PIN =  u.PINCODE,
                            ADDRESS = u.ADDRESS,
                            ALT_MOB = u.MOBILE_NUMBER,
                            COMPANY = u.COMPANY
                        }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Dealer GetSearchDealer(int id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (from u in context.M_GB_USERDETAILS
                            where
                                u.ROLE_ID == (int)CustomStatus.UserTypes.Dealer &&
                               (u.USER_ID == id.ToString() || u.USERDETAIL_ID == id)
                            select new Dealer
                            {
                                USER_NAME = u.NAME,
                                EMAIL = u.EMAIL,
                                USER_ID = u.USER_ID,
                                WALLET = u.WALLET,
                                AMOUNT = u.AMOUNT,
                                CREATED_ON = u.CREATED_ON,
                                STATE = u.STATE,
                                USERDETAIL_ID = u.USERDETAIL_ID,
                                COUNTRY = u.COUNTRY,
                                CITY = u.CITY,
                                COMPANY = u.COMPANY,
                                ALT_MOB = u.MOBILE_NUMBER,
                                ADDRESS = u.ADDRESS,
                                PAN_NUMBER = u.ADDRESS_LINE,
                                PIN = u.PINCODE,
                                CREATED_BY =  u.CREATED_BY.ToString(),
                                CategoryList = u.GB_USER_CATEGORY
                            }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public GB_DEALER_RECHARGE GetMasterFranchiseRecharge(int id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return
                        context.GB_DEALER_RECHARGE.FirstOrDefault(
                            a => a.USERDETAIL_ID == id && a.STATUS == (int) CustomStatus.DefaultStatus.Active);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Dealer GetVender(int id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (from u in context.M_GB_USER
                            where
                                u.ROLE == (int)CustomStatus.UserTypes.Global_Vender &&
                               (u.USER_ID == id.ToString())
                            select new Dealer
                            {
                                USER_NAME = u.USER_NAME ?? u.FIRST_NAME +" "+ u.LAST_NAME,
                                EMAIL = u.EMAIL,
                                USER_ID = u.USER_ID,
                                CREATED_ON = u.CREATED_ON,
                                ADD1 = u.ADD1,
                                ALT_MOB = u.MOB_NO,
                                PIN = u.PIN,
                                CITY_NAME = u.CITY
                            }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Dealer> GetrechargeWalletDeatils(int userDetailsId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (from u in context.GB_DEALER_RECHARGE
                        where u.USERDETAIL_ID == userDetailsId
                        select new Dealer
                        {
                            WALLET = u.RECHARGE_POINTS.ToString(),
                            AMOUNT = u.RECHARGE_POINTS,
                            CREATED_ON = u.CREATED_ON,
                            USERDETAIL_ID = u.USERDETAIL_ID,
                            RECHARGE_ID =  u.RECHARGE_ID,
                            CREATED_BY = u.CREATED_BY
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IEnumerable<UserDto> GetGlobalvendor(int status)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (from u in context.M_GB_USER
                            where u.ROLE == (int)CustomStatus.UserTypes.Global_Vender && u.STATUS == status
                            select new UserDto
                            {
                                USER_NAME = u.USER_NAME ?? u.FIRST_NAME + " " + u.LAST_NAME,
                                EMAIL = u.EMAIL,
                                USER_ID = u.USER_ID,
                                MOB_NO = u.MOB_NO,
                                PASSWORD = u.PASSWORD,
                                CREATED_ON = u.CREATED_ON,
                                UPDATED_ON = u.UPDATED_ON,
                                STATUS = u.STATUS
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string AddDealerUser(Dealer user)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    M_GB_USER userData = context.M_GB_USER.FirstOrDefault(a => a.USER_ID == user.USER_ID);
                    if (userData == null)
                    {

                  DataTable dt = new DataTable("UserDetails");
                 DataSet ds = new DataSet("AMD");

                 try
                 {
                     using (SqlConnection con = new SqlConnection("data source=182.50.133.111;initial catalog=psquaredb;user id=psquareadmin;password=Psqaureqwerty"))
                     {
                         if (string.IsNullOrEmpty(user.ALT_MOB))
                             user.ALT_MOB = "0";

                         SqlCommand sqlComm = new SqlCommand("Proc_InsertUserInfo", con);
                         sqlComm.Parameters.Add("@Name", SqlDbType.VarChar).Value = user.USER_NAME;
                         sqlComm.Parameters.Add("@Password", SqlDbType.VarChar).Value = user.PASSWORD;
                         sqlComm.Parameters.Add("@email", SqlDbType.VarChar).Value = user.EMAIL != null ? user.EMAIL : " ";
                         sqlComm.Parameters.Add("@mob", SqlDbType.BigInt).Value = user.ALT_MOB != null ? user.ALT_MOB : "0";
                         sqlComm.Parameters.Add("@LOGIN_TYPE", SqlDbType.Int).Value = Convert.ToInt32(0);
                         sqlComm.Parameters.Add("@FIRST_NAME", SqlDbType.VarChar).Value = user.FIRST_NAME != null ? user.LAST_NAME : " ";
                         sqlComm.Parameters.Add("@PICTURE", SqlDbType.VarChar).Value = "";
                         sqlComm.Parameters.Add("@LAST_NAME", SqlDbType.VarChar).Value = user.LAST_NAME;
                         sqlComm.Parameters.Add("@F_ID", SqlDbType.VarChar).Value = "";
                         sqlComm.CommandType = CommandType.StoredProcedure;
                         SqlDataAdapter da = new SqlDataAdapter();
                         da.SelectCommand = sqlComm;
                         da.Fill(ds);
                         dt = ds.Tables[0];
                         if (dt != null && dt.Rows.Count != 0)
                         {
                             if (dt.Rows[0]["resultcode"].ToString() != "0")
                             {
                                 string user_Id = dt.Rows[0]["USER_ID"].ToString();
                                 M_GB_USER userData1 = context.M_GB_USER.FirstOrDefault(a => a.USER_ID == user_Id);
                                 userData1.ROLE = user.ROLE_ID;
                                 context.SaveChanges();
                                 return user_Id;
                             }
                         }
                     }
                 }
                  catch (Exception ex)
                 {
                     return "0";
                 }
                        //userData = context.M_GB_USER.FirstOrDefault(a => a.EMAIL == user.EMAIL);
                        //if (userData == null)
                        //{
                        //    var mGbUser = context.M_GB_USER.OrderByDescending(a => a.USER_ID).FirstOrDefault();
                        //    if (mGbUser != null)
                        //    {
                        //        Int64 userId = Convert.ToInt64(mGbUser.USER_ID);
                        //        user.USER_ID = (userId + 1).ToString();
                        //    }
                        //  var data =  context.PROC_INSERT_DEALER(user.USER_NAME, user.PASSWORD, user.EMAIL,Convert.ToInt64(user.ALT_MOB),Convert.ToInt32(user.ROLE_ID),user.FIRST_NAME,"", user.LAST_NAME, "",user.ADD1);
                        //    return user.USER_ID;
                        //}
                        //return "0";
                    }
                    else
                    {
                        int? pinCode = Convert.ToInt32(user.PIN);
                        userData.USER_NAME = user.USER_NAME ?? userData.USER_NAME;
                        userData.CITY = user.CITY_NAME ?? userData.CITY;
                        userData.EMAIL = user.EMAIL ?? userData.EMAIL;
                        userData.ALT_EMAIL = user.ALT_EMAIL ?? userData.ALT_EMAIL;
                        userData.ALT_MOB = user.ALT_MOB ?? userData.ALT_MOB;
                        userData.ADD1 = user.ADD1 ?? userData.ADD1;
                        userData.ADD2 = user.ADD2 ?? userData.ADD2;
                        userData.PICTURE = user.PICTURE ?? userData.PICTURE;
                        userData.PIN = pinCode ?? userData.PIN;
                    }
                    context.SaveChanges();
                    return CustomStatus.ReturnStatus.success.ToString();
                }
            }
            catch (Exception ex)
            {
                return user.USER_ID;
            }
        }

        public string UpdateRecharge(GB_DEALER_RECHARGE recharge)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    GB_DEALER_RECHARGE dealerRecharge =
                        context.GB_DEALER_RECHARGE.FirstOrDefault( a => a.USERDETAIL_ID == recharge.USERDETAIL_ID );
                    if (dealerRecharge != null)
                    {





                        if ( dealerRecharge.RECHARGE_POINTS != recharge.RECHARGE_POINTS)
                        {
                            dealerRecharge.STATUS = (int) CustomStatus.DefaultStatus.DeActive;
                            context.SaveChanges();
                            dealerRecharge.RECHARGE_POINTS = recharge.RECHARGE_POINTS;
                            if (recharge.ROLE_ID == (int) CustomStatus.UserTypes.Master_Franchise)
                            {
                                recharge.USER_ADMIN_ID = (int) CustomStatus.UserTypes.Admin;
                            }
                            context.GB_DEALER_RECHARGE.Add(recharge);
                            context.SaveChanges();
                        }
                    }
                    else
                    {
                        context.GB_DEALER_RECHARGE.Add(recharge);
                        context.SaveChanges();
                    }
                }
                return CustomStatus.ReturnStatus.success.ToString();
            }
            catch (Exception exception)
            {
                return exception.Message;
            }
        }


        public int CheckEmail(string email)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    if(context.AspNetUsers.Count(a=>a.Email == email) == 0)
                        return (int)CustomStatus.ReturnStatus.success;
                    else
                        return (int)CustomStatus.ReturnStatus.failure;

                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }

        public string UpdateMFRecharge(GB_DEALER_RECHARGE recharge)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    M_GB_USERDETAILS dealerRecharge = context.M_GB_USERDETAILS.FirstOrDefault(a => a.USERDETAIL_ID == recharge.USERDETAIL_ID);
                    if (dealerRecharge != null)
                    {
                        context.GB_DEALER_RECHARGE.Add(recharge);
                        dealerRecharge.WALLET = (Convert.ToInt64(dealerRecharge.WALLET) + Convert.ToInt64(recharge.RECHARGE_POINTS)).ToString();
                        context.SaveChanges();
                    }
                }
                return CustomStatus.ReturnStatus.success.ToString();
            }
            catch (Exception exception)
            {
                return exception.Message;
            }
        }


        public string UpdateDealerRecharge(GB_DEALER_RECHARGE recharge)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    GB_DEALER_RECHARGE dealerRecharge = context.GB_DEALER_RECHARGE.FirstOrDefault(a => a.USERDETAIL_ID == recharge.USERDETAIL_ID);
                    if (dealerRecharge != null)
                    {

                        if (dealerRecharge.RECHARGE_POINTS != recharge.RECHARGE_POINTS)
                        {
                            dealerRecharge.STATUS = (int)CustomStatus.DefaultStatus.DeActive;
                            context.SaveChanges();
                            dealerRecharge.RECHARGE_POINTS = recharge.RECHARGE_POINTS;
                            if (recharge.ROLE_ID == (int)CustomStatus.UserTypes.Master_Franchise)
                            {
                                recharge.USER_ADMIN_ID = (int)CustomStatus.UserTypes.Admin;
                            }
                            context.GB_DEALER_RECHARGE.Add(recharge);
                            context.SaveChanges();
                        }
                    }
                    else
                    {
                        context.GB_DEALER_RECHARGE.Add(recharge);
                        context.SaveChanges();
                    }
                }
                return CustomStatus.ReturnStatus.success.ToString();
            }
            catch (Exception exception)
            {
                return exception.Message;
            }
        }

        public int CheckMasterFranchiseByState(int stateId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.M_GB_USERDETAILS.Count(a => a.STATE == stateId && a.STATE == (int)CustomStatus.DefaultStatus.Active);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        public int UpdateUserStatus(int userId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    M_GB_USERDETAILS userDetails = context.M_GB_USERDETAILS.FirstOrDefault(a => a.USERDETAIL_ID == userId);
                    if(userDetails != null)
                    {
                        if (userDetails.STATUS == (int)CustomStatus.DefaultStatus.Active)
                            userDetails.STATUS = (int)CustomStatus.DefaultStatus.DeActive;
                        else
                            userDetails.STATUS = (int)CustomStatus.DefaultStatus.Active;
                        context.SaveChanges();
                    }
                    return (int)CustomStatus.ReturnStatus.success;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        public string findUserNamebyId(string userId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    if (context.M_GB_USER.Count(a => a.USER_ID == userId) != 0)
                        return context.M_GB_USER.FirstOrDefault(a => a.USER_ID == userId).USER_NAME;
                    else
                        return CustomStatus.UserTypes.Admin.ToString();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        public IEnumerable<Dealer> GetDealersnyMFId(int id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    M_GB_USERDETAILS mf = context.M_GB_USERDETAILS.FirstOrDefault(a => a.USER_ID ==  id.ToString());
                    int mFId = 0;
                    if(mf != null)
                    {
                        mFId = mf.USERDETAIL_ID;
                    }
                    return (from u in context.M_GB_USERDETAILS
                            where u.ROLE_ID == (int)CustomStatus.UserTypes.Dealer && u.CREATED_BY == mFId
                            select new Dealer
                            {
                                USER_NAME = u.NAME,
                                EMAIL = u.EMAIL,
                                USER_ID = u.USER_ID,
                                WALLET = u.WALLET,
                                AMOUNT = u.AMOUNT,
                                CREATED_ON = u.CREATED_ON,
                                STATE = u.STATE,
                                USERDETAIL_ID = u.USERDETAIL_ID,
                                STATUS = u.STATUS,
                                CITY_NAME = context.M_GB_REGION.FirstOrDefault(a => a.REGION_ID == u.CITY).REGION
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
