﻿using Gizmobaba.Repository.Interfaces;
using Gizmobaba.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizmobaba.Repository
{
    public class DiscountRepository : IDiscountRepository
    {
        public int AddUsersDiscount(M_GB_USER_DISCOUNT usersDiscount)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    if (usersDiscount != null && usersDiscount.DISCOUNT_ID == 0)
                    {
                        usersDiscount.STATUS = (int)CustomStatus.DefaultStatus.Active;
                        context.M_GB_USER_DISCOUNT.Add(usersDiscount);
                    }
                    else
                    {
                        M_GB_USER_DISCOUNT data = context.M_GB_USER_DISCOUNT.FirstOrDefault(a => a.DISCOUNT_ID == usersDiscount.DISCOUNT_ID);
                        if (data != null)
                        {
                            data.DISCOUNT = usersDiscount.DISCOUNT;
                            data.VOUCHER_CODE = usersDiscount.VOUCHER_CODE;
                            data.VOUCHER_DATE = usersDiscount.VOUCHER_DATE;
                            data.VOUCHER_EXPIRY_DATE = usersDiscount.VOUCHER_EXPIRY_DATE;
                            data.CAMPAIGN_TITLE = usersDiscount.CAMPAIGN_TITLE ?? data.CAMPAIGN_TITLE;
                            data.NUMBEROFVOUCHER = usersDiscount.NUMBEROFVOUCHER ?? data.NUMBEROFVOUCHER;
                            data.NUMBEROFUSERS = usersDiscount.NUMBEROFUSERS ?? data.NUMBEROFUSERS;
                            data.REMARKS = usersDiscount.REMARKS ?? data.REMARKS;
                        }
                    }
                    context.SaveChanges();
                    return (int)CustomStatus.ReturnStatus.success;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public IEnumerable<M_GB_USER_DISCOUNT> GetUsersDiscounts()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.M_GB_USER_DISCOUNT.Where(a => a.STATUS == 1).ToList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        public M_GB_USER_DISCOUNT GetDiscountVoucher(string voucher)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.M_GB_USER_DISCOUNT.FirstOrDefault(a=>a.VOUCHER_CODE == voucher);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        public M_GB_USER_DISCOUNT GetDiscountVoucher(int id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.M_GB_USER_DISCOUNT.FirstOrDefault(a => a.DISCOUNT_ID == id);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        public int DeleteDiscount(int id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    M_GB_USER_DISCOUNT data = context.M_GB_USER_DISCOUNT.FirstOrDefault(a=>a.DISCOUNT_ID == id);
                    if(data != null)
                    {
                        data.STATUS = (int)CustomStatus.DefaultStatus.Delete;
                        context.SaveChanges();
                    }
                    return (int)CustomStatus.ReturnStatus.success;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        public IEnumerable<M_GB_USER_DISCOUNT> DiscountVouchersSearch(string title, DateTime? fromDate, DateTime? toDate, string voucher)
        {
            using (var context = new GizmobabadbEntities1())
                {
                    return (context.M_GB_USER_DISCOUNT.Where(a => (a.CREATED_ON >= fromDate && a.CREATED_ON <= toDate) || a.CAMPAIGN_TITLE == title || a.VOUCHER_CODE == voucher)).ToList();
            }
        }
    }
}
