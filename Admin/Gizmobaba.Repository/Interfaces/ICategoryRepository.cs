﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizmobaba.Utility;

namespace Gizmobaba.Repository.Interfaces
{
    public interface ICategoryRepository
    {
        Int64 Create(M_GB_CATEGORY category);
        IEnumerable<CategoryDto> GetCategories();
        CategoryDto GetCategory(int categoryId);
        Int64 UpdateCategory(M_GB_CATEGORY category);
        int DeleteCategory(int categoryId);
        IEnumerable<SubCategoryDTO> GetSubCategories(int categoryId);
        Int64 AddSubCategory(M_GB_SUB_CATEGORY subCategory);
        IEnumerable<CategoryDto> CategorySearch(string categoty);
        int DeleteSubCategory(int subCategoryId);

        SubCategoryDTO GetSubCategory(int subCategoryId);

        int GetProductCountbySubCatId(long subCatId);
    }
}
