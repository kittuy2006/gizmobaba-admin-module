﻿using Gizmobaba.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizmobaba.Repository.Interfaces
{
    public interface IDashboardRepository
    {
        DashboardDto Get();

        DashboardDto GetGlobalVenderData(string id);

        DashboardDto GetMasterFranchiseDashboard(string id);
    }
}
