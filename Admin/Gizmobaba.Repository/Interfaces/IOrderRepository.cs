﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizmobaba.Utility;

namespace Gizmobaba.Repository.Interfaces
{
    public interface IOrderRepository : ICommonRepository<OrderDto>
    {
        IEnumerable<OrderDetailDto> GetPaymentPendingOrders(int status);

        int CancelOrder(int orderId);

        IEnumerable<CartDto> GetAbandonedCart();

        IEnumerable<CartDto> AbandonedCartSearch(DateTime fromDate, DateTime toDate, string searchKey);

        IEnumerable<OrderDetailDto> GetOrdersList(int status);

        IEnumerable<OrderDetailDto> GetOrder(int orderId);

        IEnumerable<OrderDetailDto> OrderSerch(OrderSerch orderDetail);

        OrderDetailDto GetOrderDetails(int orderId);

        int CancelOrder(OrderDto order);

        long AddOrder(GB_ORDER order);

        int AddOrderDetails(GB_ORDER_DETAIL orderDetails);

        IEnumerable<OrderDetailDto> GetOrdersList(int orderStatus, int count);

        IEnumerable<OrderDetailDto> GetOrderDetailsByOrderID(int orderId);

        IEnumerable<OrderDetailDto> GetOrderList(int status, int paymentTypeId);

        int UpdateOrderStatus(int orderId, int status);

        int UpdatePickupOrderList(int orderId, int status, string LogisticType, string logisticToken);

        int UpdateMergeStatus(int orderId, long newOrderId);

        int UpdateSplitStatus(int orderId, bool SplitStatus);

        IEnumerable<OrderDetailDto> GetAllShippedOrdersList();

        int AddProQty(int orderId, int quantity, int price);

        IEnumerable<GB_ORDER_DETAIL> GetSubOrderList(int orderId);

        int updateSubOrder(long p, int orderId);

        IEnumerable<GB_ORDER> GetNewOrderList(int status, int paymentTypeId);

        GB_ORDER GetNewOrder(int id);

        IList<GB_ORDER> GetMergedOrders(int id);

        OrderDetailDto GetSingleOrderDetails(int orderId);

        int UpdateOrderListToShipping(int orderId, int status, string awb);
        GB_ORDER_DETAIL GetSubOrder(int orderId);

        int VoucherApply(long orderId, string voucherCode, decimal descountAmount);

        GB_ORDER_DETAIL GetSubOrderDetails(int subOrderId);

        IEnumerable<GB_ORDER_DETAIL> GetSplitOrderDetails(long splitOrderId);

        IEnumerable<GB_ORDER> SearchAuthorizeCodOrder(OrderSerch serch);

        IEnumerable<GB_ORDER> SearchOrders(OrderSerch serch);

        IEnumerable<OrderDetailDto> GetPaymentPendingOrdersList();

        IEnumerable<GB_ORDER> SearchPaymentPendingOrder(OrderSerch serch);

        IEnumerable<OrderDetailDto> GetVendorOrderList(string id);
    }
}
