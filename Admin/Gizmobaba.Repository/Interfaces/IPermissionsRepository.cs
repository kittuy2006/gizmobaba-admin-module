﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizmobaba.Utility;

namespace Gizmobaba.Repository.Interfaces
{
    public interface IPermissionsRepository
    {
        IEnumerable<FeatureTypeDto> GetFeatureTypes();

        int AddRole(M_GB_ROLES role);

        int AddFeatureAccessToRole(M_GB_FEATURE_ACCESS featureAccess);

        IEnumerable<FeatureTypeDto> GetFeatureAccessByRoleId(int roleId);

        M_GB_ROLES GetRoleDetails(int roleId);
        IEnumerable<RolesDto> GetAllRoles();

        int DeleteRole(int roleId);

        int AddFeatureAccessToUser(GB_USER_FEATURE_ACCESS userData);

        IEnumerable<FeatureTypeDto> GetFeatureAccessByUser(string userId);
    }
}
