﻿using Gizmobaba.Utility;
using System.Collections.Generic;

namespace Gizmobaba.Repository.Interfaces
{
    public interface IWalletRepository
    {
        int AddWallet(M_GB_WALLET wallet);
        WalletDto GetWallet();

        IEnumerable<BonusDto> GetWalletList();
        int CreateWallet(GB_BONUS bonusData);

        IEnumerable<BonusDto> GetWalletListByRole(int roleId);

        IEnumerable<M_GB_Payment_GatWay> GetPaymentgatwayList();

        M_GB_Payment_GatWay GetPaymentgatway(int id);
    }
}
