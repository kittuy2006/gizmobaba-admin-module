﻿using Gizmobaba.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizmobaba.Repository.Interfaces
{
    public interface IPromotionRepository : ICommonRepository<GB_FLASH_PROMOTION>
    {
        IEnumerable<GB_FLASH_PROMOTION> GetflashPromotionList();
        int AddMailTemplate(GB_MAIL_TEMPLATES mailTemplate);
        int DeleteMailTemplate(int id);
        GB_MAIL_TEMPLATES Update(GB_MAIL_TEMPLATES mailTemplate);
        GB_MAIL_TEMPLATES GetmailTemplateById(int id);
        IEnumerable<GB_MAIL_TEMPLATES> GetmailTemplateList();


        int UpdateFlashPromotionsStatus(int id);

        int updateMailTemplateStatus(int id);
    }
}
