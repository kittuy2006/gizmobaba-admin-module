﻿using Gizmobaba.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizmobaba.Repository.Interfaces
{
    public interface IProductRepository
    {
        Int64 AddProduct(GB_PRODUCT_DETAIL productDetail);
        IEnumerable<GB_PRODUCT_DETAIL> GetAll();

        int Delete(int id);

        IEnumerable<M_GB_CATEGORY> GetCategories();

        IEnumerable<M_GB_SHIPPING> GetAvailableShippingCodes();

        GB_PRODUCT_DETAIL GetProduct(int productId);

        IEnumerable<ProductDTO> GetProductSearch(string product);

        int DeactivateProduct(int productId);

        int ActivateProduct(int productId);

        int AddBanner(M_GB_BANNERS banner);

        IEnumerable<Banner> GetBanners();

        int DeleteBanner(int bannerId);

        int SetBanner(int bannerId);

        int DeleteImage(int imageId);

        int EditImage(GB_PRODUCT_IMAGES img);

        IEnumerable<ProductImagesDto> GetProductImages(int productId);

        int CheckSku(string sku);

        int AddProductImage(GB_PRODUCT_IMAGES productimages);

        IEnumerable<VendorDto> GetVendors();

        IEnumerable<WarrantyDto> GetWarranty();

        IEnumerable<CustomFieldsDto> GetCustomFields();

        IEnumerable<SectionDto> GetSection();

        IEnumerable<ConditiongDto> GetCondition();

        int AddProductAdditionalinfo(GB_PRODUCT_INFORMATION productInformation);

        ProductDTO GetProductInfomation(int productId);

        IEnumerable<TagsDto> GetProductTags();

        IEnumerable<TagsDto> GetAllProductTags();

        TagsDto GetProductTags(int productId);

        int AddProductTag(M_GB_TAGS tagsDto);

        int DeleteProductTag(int tagId);

        int AddTagToProduct(GB_PRODUCT_TAG tags);

        IEnumerable<Banner> GetPredefinedBanners();

        Banner GetBanner(int bannerId);

        IEnumerable<Banner> GetCategoryBanners();

        IEnumerable<Banner> GetAddsBanners();

        IEnumerable<M_GB_SUB_CATEGORY> GetAllSubCat();

        IEnumerable<GB_BULK_UPLOAD> GetBulkuploadList();

        int AddBulkProUploadList(GB_BULK_UPLOAD bulkUpload);

        IEnumerable<PRODUCTREVIEWSDTO> GetReviews();

        int UpdateReviewStatus(int id);

        int DeleteReview(int id);

        IEnumerable<M_GB_TAX> GetTaxes();

        int AddTax(M_GB_TAX tax);

        M_GB_TAX GetProductTax(int id);

        string DeleteProductTax(int id);

        string GetProSku();

        IEnumerable<ProductDTO> GetProductInvAdvSearch(DateTime fromDate, DateTime toDate);

        ProductDTO GetProductbySku(string sku);
    }
}
