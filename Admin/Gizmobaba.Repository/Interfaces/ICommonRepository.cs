﻿using System.Collections.Generic;

namespace Gizmobaba.Repository.Interfaces
{
    public interface ICommonRepository<T>
    {
        IEnumerable<T> List { get; }
        int Add(T entity);
        int Delete(int id);
        T Update(T entity);
        T FindById(int id);

    }
}
