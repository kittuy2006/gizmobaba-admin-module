﻿using Gizmobaba.Repository.Interfaces;
using Gizmobaba.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizmobaba.Repository
{
    public class PermissionsRepository : IPermissionsRepository
    {

        public IEnumerable<FeatureTypeDto> GetFeatureTypes()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (from f in context.M_GB_FEATURE_TYPE
                            where f.STATUS != (int)CustomStatus.BannerStatus.DeActive
                            select new FeatureTypeDto
                            {
                                FEATURE_TYPE = f.FEATURE_TYPE,
                                FEATURE_TYPE_ID = f.FEATURE_TYPE_ID,
                                STATUS = f.STATUS,
                                ROLE_ID = f.ROLE_ID,
                                ID = f.ID
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddRole(M_GB_ROLES role)
        {
            try
            {
                GizmobabadbEntities1 context = new GizmobabadbEntities1();
                    M_GB_ROLES data = context.M_GB_ROLES.FirstOrDefault(a => a.ROLE_ID == role.ROLE_ID);
                    if (data == null)
                    {
                        role.ROLE_ID = context.M_GB_ROLES.OrderByDescending(a => a.ROLE_ID).FirstOrDefault().ROLE_ID + 1;
                        context.M_GB_ROLES.Add(role);
                        context.SaveChanges();
                        return role.ROLE_ID;
                    }
                    else
                    {
                        data.ROLE_DESC = role.ROLE_DESC;
                        data.DESCRIPTION = role.DESCRIPTION;
                        if (role.Value != 0)
                            data.Value = role.Value;
                        context.SaveChanges();
                        return data.ROLE_ID;
                    }
                    
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddFeatureAccessToRole(M_GB_FEATURE_ACCESS featureAccess)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    M_GB_FEATURE_ACCESS data = context.M_GB_FEATURE_ACCESS.FirstOrDefault(a => a.FEATURE_TYPE_ID == featureAccess.FEATURE_TYPE_ID && a.ROLE_ID == featureAccess.ROLE_ID);
                    if (data == null)
                        context.M_GB_FEATURE_ACCESS.Add(featureAccess);
                    else
                        data.STATUS = featureAccess.STATUS;
                    context.SaveChanges();
                    return featureAccess.FEATURE_ACCESS_ID;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<FeatureTypeDto> GetFeatureAccessByRoleId(int roleId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.M_GB_FEATURE_ACCESS.Where(f => f.ROLE_ID == roleId).Select(f => new FeatureTypeDto
                    {
                        FEATURE_TYPE = f.M_GB_FEATURE_TYPE.FEATURE_TYPE,
                        FEATURE_TYPE_ID = (int) f.FEATURE_TYPE_ID,
                        STATUS = (int) f.STATUS,
                        ROLE_ID = f.ROLE_ID,
                        ID = f.M_GB_FEATURE_TYPE.ID
                    })).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public M_GB_ROLES GetRoleDetails(int roleId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.M_GB_ROLES.FirstOrDefault(a => a.ROLE_ID == roleId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public IEnumerable<RolesDto> GetAllRoles()
        //{
        //    try
        //    {
        //        using (var context = new GizmobabadbEntities1())
        //        {
        //            return context.M_GB_ROLES.Where(f => f.STATUS == (int)CustomStatus.Status.Active).Select(r => new RolesDto
        //            {
        //               ROLE_ID = r.ROLE_ID,
        //               ROLE_DESC = r.ROLE_DESC,
        //               UserAllow = r.UserAllow ?? false,
        //               Value =r.Value,
        //              // PERMISSIONSCOUNT = r.M_GB_FEATURE_ACCESS.Count(a=>a.STATUS == (int)CustomStatus.Status.Active)
        //            }).ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public IEnumerable<RolesDto> GetAllRoles()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    IEnumerable<RolesDto> roleList = (from r in context.M_GB_ROLES
                                                      where r.STATUS == (int)CustomStatus.Status.Active
                                                      select new RolesDto
                                                      {
                                                          ROLE_ID = r.ROLE_ID,
                                                          ROLE_DESC = r.ROLE_DESC,
                                                          Value = r.Value,
                                                          UserAllow = r.UserAllow ?? false,
                                                          PERMISSIONSCOUNT = r.M_GB_FEATURE_ACCESS.Where(a=>a.ROLE_ID == r.ROLE_ID && a.STATUS == (int)CustomStatus.DefaultStatus.Active).Count(),
                                                          DESCRIPTION = r.DESCRIPTION,
                                                          UserCount = context.M_GB_USER.Count(a=>a.ROLE == r.ROLE_ID)
                                                      }).ToList();

                    //return context.M_GB_ROLES.Where(f => f.STATUS == (int)CustomStatus.Status.Active).Select(r => new RolesDto
                    //{
                    //    ROLE_ID = r.ROLE_ID,
                    //    ROLE_DESC = r.ROLE_DESC,
                    //    UserAllow = r.UserAllow ?? false,
                    //    Value = r.Value,
                    //    // PERMISSIONSCOUNT = r.M_GB_FEATURE_ACCESS.Count(a=>a.STATUS == (int)CustomStatus.Status.Active)
                    //}).ToList();
                    return roleList;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteRole(int roleId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    M_GB_ROLES data = context.M_GB_ROLES.FirstOrDefault(a => a.ROLE_ID == roleId);
                    if (data != null)
                        data.STATUS = (int)CustomStatus.Status.DeActive;
                    context.SaveChanges();
                    return (int) CustomStatus.ReturnStatus.success;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int AddFeatureAccessToUser(GB_USER_FEATURE_ACCESS featureAccess)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    GB_USER_FEATURE_ACCESS data = context.GB_USER_FEATURE_ACCESS.FirstOrDefault(a => a.FEATURE_TYPE_ID == featureAccess.FEATURE_TYPE_ID && a.USER_ID == featureAccess.USER_ID);
                    if (data == null)
                        context.GB_USER_FEATURE_ACCESS.Add(featureAccess);
                    else
                        data.STATUS = featureAccess.STATUS;
                    context.SaveChanges();
                    return featureAccess.USER_F_ACCESS_ID;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IEnumerable<FeatureTypeDto> GetFeatureAccessByUser(string userId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.GB_USER_FEATURE_ACCESS.Where(f => f.USER_ID == userId).Select(f => new FeatureTypeDto
                    {
                        FEATURE_TYPE_ID = (int)f.FEATURE_TYPE_ID,
                        FEATURE_TYPE = context.M_GB_FEATURE_TYPE.FirstOrDefault(a=>a.FEATURE_TYPE_ID == (int)f.FEATURE_TYPE_ID).FEATURE_TYPE,
                        STATUS = (int)f.STATUS,
                        ROLE_ID = f.ROLE_ID,
                        ID = context.M_GB_FEATURE_TYPE.FirstOrDefault(a => a.FEATURE_TYPE_ID == (int)f.FEATURE_TYPE_ID).ID
                    })).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
