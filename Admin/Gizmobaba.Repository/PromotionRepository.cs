﻿using Gizmobaba.Repository.Interfaces;
using Gizmobaba.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizmobaba.Repository
{
    public class PromotionRepository : IPromotionRepository
    {
        public IEnumerable<GB_FLASH_PROMOTION> List
        {
            get { throw new NotImplementedException(); }
        }

        public int Add(GB_FLASH_PROMOTION flashPromotion)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    context.GB_FLASH_PROMOTION.Add(flashPromotion);
                    context.SaveChanges();
                }
                return (int)CustomStatus.ReturnStatus.success;
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }

        public int Delete(int id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    var pROMOTION = context.GB_FLASH_PROMOTION.FirstOrDefault(f => f.PROMOTION_ID == id);
                    if (pROMOTION != null)
                        pROMOTION.STATUS = (int)CustomStatus.ProductStatus.Delete;
                    context.SaveChanges();
                }
                return (int)CustomStatus.ReturnStatus.success;
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }

        public GB_FLASH_PROMOTION Update(GB_FLASH_PROMOTION flashPromotion)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    GB_FLASH_PROMOTION data = context.GB_FLASH_PROMOTION.FirstOrDefault(a => a.PROMOTION_ID == flashPromotion.PROMOTION_ID);
                   if(data != null)
                   {
                       data.CODE = flashPromotion.CODE;
                       data.FROM_DATE = flashPromotion.FROM_DATE;
                       data.TO_DATE = flashPromotion.TO_DATE;
                       data.TITLE = flashPromotion.TITLE;
                   }
                    context.SaveChanges();
                }
                return flashPromotion;
            }
            catch (Exception exception)
            {
                return flashPromotion;
            }
        }

        public GB_FLASH_PROMOTION FindById(int id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.GB_FLASH_PROMOTION.Where(a => a.STATUS != (int)CustomStatus.DefaultStatus.Delete && a.PROMOTION_ID == id).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        public IEnumerable<GB_FLASH_PROMOTION> GetflashPromotionList()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.GB_FLASH_PROMOTION.Where(a => a.STATUS != (int)CustomStatus.DefaultStatus.Delete).ToList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        public int AddMailTemplate(GB_MAIL_TEMPLATES mailTemplate)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    context.GB_MAIL_TEMPLATES.Add(mailTemplate);
                    context.SaveChanges();
                }
                return (int)CustomStatus.ReturnStatus.success;
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }
        public int DeleteMailTemplate(int id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    var data = context.GB_MAIL_TEMPLATES.FirstOrDefault(f => f.ID == id);
                    if (data != null)
                        data.STATUS = (int)CustomStatus.ProductStatus.Delete;
                    context.SaveChanges();
                }
                return (int)CustomStatus.ReturnStatus.success;
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }
        public GB_MAIL_TEMPLATES Update(GB_MAIL_TEMPLATES mailTemplate)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    GB_MAIL_TEMPLATES data = context.GB_MAIL_TEMPLATES.FirstOrDefault(a => a.ID == mailTemplate.ID);
                    data = mailTemplate;
                    context.SaveChanges();
                }
                return mailTemplate;
            }
            catch (Exception exception)
            {
                return mailTemplate;
            }
        }
        public GB_MAIL_TEMPLATES GetmailTemplateById(int id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.GB_MAIL_TEMPLATES.Where(a => a.STATUS != (int)CustomStatus.DefaultStatus.Delete && a.ID == id).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        public IEnumerable<GB_MAIL_TEMPLATES> GetmailTemplateList()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.GB_MAIL_TEMPLATES.Where(a => a.STATUS != (int)CustomStatus.DefaultStatus.Delete).ToList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        public int UpdateFlashPromotionsStatus(int id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    var pROMOTION = context.GB_FLASH_PROMOTION.FirstOrDefault(f => f.PROMOTION_ID == id);
                    if (pROMOTION != null)
                        pROMOTION.STATUS = (int)CustomStatus.ProductStatus.Active;
                    context.SaveChanges();
                }
                return (int)CustomStatus.ReturnStatus.success;
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }


        public int updateMailTemplateStatus(int id)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    var data = context.GB_MAIL_TEMPLATES.FirstOrDefault(f => f.ID == id);
                    if (data != null)
                    {
                        if(data.STATUS == 0)
                        data.STATUS = (int)CustomStatus.ProductStatus.Active;
                        else if(data.STATUS == 1)
                        {
                            data.STATUS = (int)CustomStatus.DefaultStatus.DeActive;
                        }
                    }
                    context.SaveChanges();
                    return data.STATUS;
                }
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }
    }
}
