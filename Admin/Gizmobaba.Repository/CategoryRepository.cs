﻿using Gizmobaba.Repository.Interfaces;
using Gizmobaba.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Gizmobaba.Repository
{
    public class CategoryRepository : ICategoryRepository
    {

        public Int64 Create(M_GB_CATEGORY category)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    M_GB_CATEGORY categorydata =
                        context.M_GB_CATEGORY.FirstOrDefault(a => a.CATEGORY_ID == category.CATEGORY_ID);
                    if (categorydata == null)
                    context.M_GB_CATEGORY.Add(category);
                    else
                    {
                        categorydata.CATEGORY_DESC = category.CATEGORY_DESC;
                        categorydata.REFERENCE_CODE = category.REFERENCE_CODE;
                        categorydata.CATEGORY_RANK = Convert.ToInt32(category.CATEGORY_RANK);
                        categorydata.CATEGORY_ID = category.CATEGORY_ID;
                        categorydata.UPDATED_ON = DateTime.Now;
                        if (category.CATEGORY_SEO_INFORMATION != null)
                        {
                            Int64 id = category.CATEGORY_SEO_INFORMATION.FirstOrDefault().CATEGORY_INFORMATION_ID;
                            CATEGORY_SEO_INFORMATION categorySeoInformation =
                                categorydata.CATEGORY_SEO_INFORMATION.FirstOrDefault(a => a.CATEGORY_INFORMATION_ID == id);
                            if (categorySeoInformation != null)
                            {
                                categorySeoInformation.META_KEYWORD =
                                    category.CATEGORY_SEO_INFORMATION.FirstOrDefault().META_KEYWORD;
                                categorySeoInformation.META_DESC =
                                    category.CATEGORY_SEO_INFORMATION.FirstOrDefault().META_DESC;
                                categorySeoInformation.META_REFERENCE_CODE =
                                    category.CATEGORY_SEO_INFORMATION.FirstOrDefault().META_REFERENCE_CODE;
                                categorySeoInformation.CREATED_ON = DateTime.Now;
                            }
                            else
                            {
                                categorydata.CATEGORY_SEO_INFORMATION.Add(category.CATEGORY_SEO_INFORMATION.FirstOrDefault());
                            }
                        }
                        if (category.CATEGORY_CONTENT_INFORMATION != null)
                        {
                            Int64 id = category.CATEGORY_CONTENT_INFORMATION.FirstOrDefault().CATEGORY_CONTENT_ID;
                            CATEGORY_CONTENT_INFORMATION categoryContentInformation =
                                categorydata.CATEGORY_CONTENT_INFORMATION.FirstOrDefault(a => a.CATEGORY_CONTENT_ID == id);
                            if (categoryContentInformation != null)
                            {
                                categoryContentInformation.TITLE = category.CATEGORY_CONTENT_INFORMATION.FirstOrDefault().TITLE;
                                categoryContentInformation.BODY = category.CATEGORY_CONTENT_INFORMATION.FirstOrDefault().BODY;
                            }
                            else
                            {
                                categorydata.CATEGORY_CONTENT_INFORMATION.Add(category.CATEGORY_CONTENT_INFORMATION.FirstOrDefault());
                            }
                        }
                    }
                    context.SaveChanges();
                    return category.CATEGORY_ID;
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                string rs = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    rs = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    Console.WriteLine(rs);

                    foreach (var ve in eve.ValidationErrors)
                    {
                        rs += "<br />" + string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw new Exception(rs);
            }
        }

        public IEnumerable<CategoryDto> GetCategories()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.M_GB_CATEGORY.Where(c => c.STATUS == (int)CustomStatus.DefaultStatus.DeActive).Select(c => new CategoryDto
                    {
                        CATEGORY_ID = c.CATEGORY_ID,
                        CATEGORY_DESC = c.CATEGORY_DESC,
                        CREATEDF_ON = c.CREATEDF_ON,
                        ICON = c.ICON,
                        SEQ_NO = c.SEQ_NO,
                        REFERENCE_CODE = c.REFERENCE_CODE,
                        ProductsCount = context.GB_PRODUCT_DETAIL.Count(a => a.CATEGORY_ID == c.CATEGORY_ID && a.STATUS == (int)CustomStatus.DefaultStatus.Active),
                        SubCategory = c.M_GB_SUB_CATEGORY.Where(a=>a.STATUS == (int)CustomStatus.DefaultStatus.Active )
                    })).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CategoryDto GetCategory(int categoryId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.M_GB_CATEGORY.Where(c => c.CATEGORY_ID == categoryId).Select(c => new CategoryDto
                    {
                        CATEGORY_ID = c.CATEGORY_ID,
                        CATEGORY_DESC = c.CATEGORY_DESC,
                        CREATEDF_ON = c.CREATEDF_ON,
                        ICON = c.ICON,
                        SEQ_NO = c.SEQ_NO,
                        REFERENCE_CODE = c.REFERENCE_CODE,
                        CATEGORY_RANK = c.CATEGORY_RANK.ToString(),
                        CategoryContentInformation = c.CATEGORY_CONTENT_INFORMATION.FirstOrDefault(),
                        CategorySeoInformation = c.CATEGORY_SEO_INFORMATION.FirstOrDefault()
                    })).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int64 UpdateCategory(M_GB_CATEGORY category)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    context.M_GB_CATEGORY.Add(category);
                    context.SaveChanges();
                    return category.CATEGORY_ID;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteCategory(int categoryId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    var mGbCategory = context.M_GB_CATEGORY.FirstOrDefault(c => c.CATEGORY_ID == categoryId);
                    if (mGbCategory != null)
                    {
                        mGbCategory.STATUS = (int) CustomStatus.CategoryStatus.Delete;
                        mGbCategory.UPDATED_ON = DateTime.Now;
                    }
                    context.SaveChanges();
                    return (int)CustomStatus.ReturnStatus.success;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IEnumerable<SubCategoryDTO> GetSubCategories(int categoryId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.M_GB_SUB_CATEGORY.Where(c => c.CATEGORY_ID == categoryId && c.STATUS != (int)CustomStatus.DefaultStatus.Delete)
                        .Select(c => new SubCategoryDTO
                        {
                            CATEGORY_ID = c.CATEGORY_ID,
                            SEQ_NO = c.SEQ_NO,
                            REFERENCE_CODE = c.REFERENCE_CODE,
                            SUB_CAT_DESC = c.SUB_CAT_DESC,
                            SUB_CAT_GROUP = c.SUB_CAT_GROUP,
                            RANK = c.RANK,
                            SUB_CAT_ID = c.SUB_CAT_ID,
                            DESCRIPTION = c.DESCRIPTION,
                            NUMBER_OF_COLUMNS = c.NUMBER_OF_COLUMNS,
                            STATUS = c.STATUS
                        })).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int64 AddSubCategory(M_GB_SUB_CATEGORY subCategory)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    M_GB_SUB_CATEGORY subCategoryData =
                        context.M_GB_SUB_CATEGORY.FirstOrDefault(a => a.SUB_CAT_ID == subCategory.SUB_CAT_ID);
                    if (subCategoryData != null)
                    {
                        subCategoryData.SUB_CAT_DESC = subCategory.SUB_CAT_DESC;
                        subCategoryData.REFERENCE_CODE = subCategory.REFERENCE_CODE;
                        subCategoryData.DESCRIPTION = subCategory.DESCRIPTION;
                        subCategoryData.NUMBER_OF_COLUMNS = subCategory.NUMBER_OF_COLUMNS;
                        subCategoryData.SUB_CAT_GROUP = subCategory.SUB_CAT_GROUP;
                        subCategoryData.RANK = subCategory.RANK;
                        subCategoryData.STATUS = subCategory.STATUS;
                    }
                    else
                     context.M_GB_SUB_CATEGORY.Add(subCategory);
                    context.SaveChanges();
                    return subCategory.CATEGORY_ID;
                }
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }

        public IEnumerable<CategoryDto> CategorySearch(string categoty)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.M_GB_CATEGORY.Where(c=>c.STATUS != (int)CustomStatus.CategoryStatus.Delete && c.CATEGORY_DESC.Contains(categoty))
                        .Select(c => new CategoryDto
                        {
                            CATEGORY_ID = c.CATEGORY_ID,
                            CATEGORY_DESC = c.CATEGORY_DESC,
                            STATUS = c.STATUS,
                            SEQ_NO = c.SEQ_NO,
                            ProductsCount = context.GB_PRODUCT_DETAIL.Count(p => p.CATEGORY_ID == c.CATEGORY_ID && p.STATUS == (int)CustomStatus.DefaultStatus.Active),
                            REFERENCE_CODE = c.REFERENCE_CODE,
                            SubCategory = c.M_GB_SUB_CATEGORY.Where(a=>a.CATEGORY_ID == c.CATEGORY_ID && a.STATUS == (int)CustomStatus.DefaultStatus.Active).ToList()
                        })).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteSubCategory(int subCategoryId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    var mGbSubCategory = context.M_GB_SUB_CATEGORY.FirstOrDefault(c => c.SUB_CAT_ID == subCategoryId);
                    if (mGbSubCategory != null)
                    {
                        mGbSubCategory.UPDATED_ON =  DateTime.Now;
                        mGbSubCategory.STATUS = (int) CustomStatus.CategoryStatus.Delete;
                    }
                    context.SaveChanges();
                    return (int)CustomStatus.ReturnStatus.success;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SubCategoryDTO GetSubCategory(int subCategoryId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.M_GB_SUB_CATEGORY.Where(c => c.SUB_CAT_ID == subCategoryId && c.STATUS != (int)CustomStatus.ProductStatus.Delete).Select(c => new SubCategoryDTO
                    {
                        CATEGORY_ID = c.CATEGORY_ID,
                        SEQ_NO = c.SEQ_NO,
                        REFERENCE_CODE = c.REFERENCE_CODE,
                        SUB_CAT_DESC = c.SUB_CAT_DESC,
                        SUB_CAT_GROUP = c.SUB_CAT_GROUP,
                        RANK = c.RANK,
                        STATUS = c.STATUS,
                SUB_CAT_ID = c.SUB_CAT_ID,
                DESCRIPTION = c.DESCRIPTION,
                NUMBER_OF_COLUMNS = c.NUMBER_OF_COLUMNS,
                    })).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int GetProductCountbySubCatId(long subCatId)
        {
            using (var context = new GizmobabadbEntities1())
            {
                return context.GB_PRODUCT_DETAIL.Count(a => a.SUB_CAT_ID == subCatId && a.STATUS == (int)CustomStatus.DefaultStatus.Active);
            }
        }
    }
}
