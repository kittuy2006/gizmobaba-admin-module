﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizmobaba.Repository.Interfaces;
using Gizmobaba.Utility;

namespace Gizmobaba.Repository
{
    public class ShippingRepository : IShippingRepository
    {
        public IEnumerable<Shippingdto> List()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return (context.M_GB_SHIPPING.Where(c => c.STATUS == 0).Select(c => new Shippingdto
                    {
                        SHIPPING_ID = c.SHIPPING_ID,
                        SHIPPING_CODE = c.SHIPPING_CODE,
                        SHIPPING_DESC = c.SHIPPING_DESC
                    })).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int Add(M_GB_SHIPPING shippingdto)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    context.M_GB_SHIPPING.Add(shippingdto);
                    context.SaveChanges();
                }
                return (int) CustomStatus.ReturnStatus.success;
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;                
            }
        }

        public int Delete(int shippingId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    var mGbShipping = context.M_GB_SHIPPING.FirstOrDefault(s => s.SHIPPING_ID == shippingId);
                    if (mGbShipping != null)
                        mGbShipping.STATUS = (int)CustomStatus.ProductStatus.Delete;
                    context.SaveChanges();
                }
                return (int)CustomStatus.ReturnStatus.success;
            }
            catch (Exception exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }

        public M_GB_SHIPPING Update(M_GB_SHIPPING shipping)
        {
            try
            {
                // Update entity query
                return shipping;
            }
            catch (Exception exception)
            {
                return shipping;
            }
        }

        public M_GB_SHIPPING FindById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ShippingMethodDto> GetShippingMethods()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.M_GB_SHIPPINGMETHOD.Where(a => a.STATUS != 3).Select(s => new ShippingMethodDto
                    {
                        SHIPPINGMETHOD_ID = s.SHIPPINGMETHOD_ID,
                        METHOD_NAME = s.METHOD_NAME,
                        STATUS = s.STATUS
                    }).ToList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public IEnumerable<M_GB_SHIPPING> GetShippingCodes()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.M_GB_SHIPPING.Where(a => a.STATUS != 3).ToList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public IEnumerable<M_GB_COUNTRY> GetShippingCountries()
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.M_GB_COUNTRY.Where(a => a.STATUS != 3).ToList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        public IEnumerable<StatesDto> GetStates(int countryId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return
                        context.M_GB_SATES.Where(a => a.COUNTRY_ID == countryId && a.STATUS != 3)
                            .Select(s => new StatesDto
                            {
                                STATE_ID = s.STATE_ID,
                                STATE = s.STATE,
                                STATE_DESC = s.STATE_DESC,
                                STATUS = s.STATUS
                            }).ToList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public IEnumerable<RegionDto> GetRegions(int stateId)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.M_GB_REGION.Where(a => a.STATE_ID == stateId && a.STATUS != 3).Select(r=> new RegionDto
                    {
                        REGION = r.REGION,
                        REGION_DESC = r.REGION_DESC,
                        REGION_ID = r.REGION_ID
                    }).ToList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        IEnumerable<M_GB_SHIPPING> ICommonRepository<M_GB_SHIPPING>.List
        {
            get { throw new NotImplementedException(); }
        }


        M_GB_SHIPPING ICommonRepository<M_GB_SHIPPING>.Update(M_GB_SHIPPING entity)
        {
            throw new NotImplementedException();
        }


        public IEnumerable<M_GB_SHIPPING> GetShippingCodesBycount(int count)
        {
            try
            {
                using (var context = new GizmobabadbEntities1())
                {
                    return context.M_GB_SHIPPING.Where(a => a.STATUS != 3).Take(count).ToList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
