﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Gizmobaba.Utility;

namespace Gizmobaba.DataImport
{
    public class UserImportResponse : ImportResponse
    {
        public List<ProductDTO> Results { get; set; }
    }
    public class CustomerImportResponse : ImportResponse
    {
        public List<M_GB_USER> Results { get; set; }
    }
}