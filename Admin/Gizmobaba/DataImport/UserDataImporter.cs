﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Gizmobaba.Service;
using Gizmobaba.Service.Interfaces;
using Gizmobaba.Utility;
using System.Data;
using System.IO;

namespace Gizmobaba.DataImport
{
    public class UserDataImporter
    {
        private IProductService _productService;

        public UserDataImporter(IProductService productService)
        {
            _productService = productService;
        }

        public List<ProductDTO> TransformImport(DataTable dt)
        {
            var _return = new List<ProductDTO>();
            IEnumerable<M_GB_CATEGORY> category = _productService.GetCategories();
            IEnumerable<M_GB_SUB_CATEGORY> subCat = _productService.GetAllSubCat();
            foreach (DataRow row in dt.Rows)
            {
                GB_PRODUCT_IMAGES img = new GB_PRODUCT_IMAGES();
                var product = new ProductDTO();
                List<GB_PRODUCT_IMAGES> imgList = new List<GB_PRODUCT_IMAGES>();
                try
                {

                    if (string.IsNullOrEmpty(row.ItemArray[0].ToString()))
                        continue;
                    //product.PRODUCT_NAME = row.ItemArray[0].ToString();
                    //product.PRODUCT_CAPTION = row.ItemArray[4].ToString();
                    //string categoryName = row.ItemArray[2].ToString();
                    //if (category.Count(a => a.CATEGORY_DESC.Contains(categoryName)) != 0)
                    //    product.CATEGORY_ID = category.FirstOrDefault(a => a.CATEGORY_DESC.Contains(categoryName)).CATEGORY_ID;
                    //else
                    //    product.CATEGORY_ID = 1;
                    //string subCatName = row.ItemArray[3].ToString();
                    //if (subCat.Count(a => a.SUB_CAT_DESC.Contains(subCatName)) != 0)
                    //    product.SUB_CAT_ID = subCat.FirstOrDefault(a => a.SUB_CAT_DESC.Contains(subCatName)).SUB_CAT_ID;
                    //else
                    //    product.SUB_CAT_ID = 1;
                    //product.PRODUCT_SKU = row.ItemArray[1].ToString();
                    //product.BRAND = row.ItemArray[6].ToString();
                    //product.PRODUCT_DESC = row.ItemArray[4].ToString();
                    //product.VIDEO_URL = row.ItemArray[23].ToString();
                    //product.NET_PRICE = Convert.ToDecimal(row.ItemArray[8].ToString());
                    //product.MRP = Convert.ToDecimal(row.ItemArray[7].ToString());
                    //// product.GROSS_PRICE = Convert.ToDecimal(row["COST_PRICE"].ToString());
                    //product.GIZMOBABA_PRICE = Convert.ToDecimal(row.ItemArray[10].ToString() ?? "0");
                    //product.QUANTITYS = Convert.ToInt32(row.ItemArray[11].ToString());

                    //product.DELIVERY_TIME = row.ItemArray[14].ToString();
                    //product.INVENTORY = row.ItemArray[15].ToString();
                    //product.RESERVE_QUANTITY = Convert.ToInt32(row.ItemArray[16].ToString() ?? "0");
                    //product.PKG_QUANTITY = Convert.ToInt32(row.ItemArray[17].ToString() ?? "0");
                    //product.CREATED_ON = DateTime.Now;
                    //product.UPDATED_ON = DateTime.Now;
                    //product.CREATED_BY = "Admin";

                    //product.UPDATED_BY = "1";
                    //product.STATUS = 1;

                    //// ** Source Image ** //
                    //GB_PRODUCT_IMAGES Sourceimg = new GB_PRODUCT_IMAGES();
                    //string sourceFile = row.ItemArray[18].ToString();
                    //string destinationFile = @"C:\Publish\Images\Product\" + sourceFile;
                    ////sourceFile = @"E:\Gizmobaba_New\Gizmobaba_22_2\Gizmobaba\Images\Bulk\" + sourceFile;
                    //sourceFile = @"C:\Publish\Images\Bulk\" + sourceFile;

                    //Sourceimg.IMAGE_URL = MoveImage(sourceFile, destinationFile);
                    //Sourceimg.DISPLAY_IMAGE = 1;
                    //Sourceimg.ALTERNATE_NAME = "source Image";
                    //imgList.Add(Sourceimg);

                    //// ** GIF Image ** //
                    //GB_PRODUCT_IMAGES Gifimg = new GB_PRODUCT_IMAGES();
                    //string gifFile = row.ItemArray[19].ToString();
                    //string destinationGifFile = @"C:\Publish\Images\Product\" + gifFile;
                    //gifFile = @"C:\Publish\Images\Bulk\" + gifFile;
                    ////gifFile = @"E:\Gizmobaba_New\Gizmobaba_22_2\Gizmobaba\Images\Bulk\" + gifFile;
                    //Gifimg.IMAGE_URL = MoveImage(gifFile, destinationGifFile);
                    //Gifimg.DISPLAY_IMAGE = 2;
                    //Gifimg.ALTERNATE_NAME = "Gif Image";
                    //imgList.Add(Gifimg);

                    //// ** IMAGE_1 ** //
                    //if (row.ItemArray[20].ToString() != null)
                    //{
                    //    GB_PRODUCT_IMAGES img_1 = new GB_PRODUCT_IMAGES();
                    //    string IMG_1File = row.ItemArray[20].ToString();
                    //    string destinationIMG_1File = @"C:\Publish\Images\Product\" + IMG_1File;
                    //    IMG_1File = @"C:\Publish\Images\Bulk\" + IMG_1File;
                    //    img_1.IMAGE_URL = MoveImage(IMG_1File, destinationIMG_1File);
                    //    img_1.DISPLAY_IMAGE = 2;
                    //    img_1.ALTERNATE_NAME = "Gif Image";
                    //    imgList.Add(img_1);
                    //}
                    //if (row.ItemArray[21].ToString() != null && row.ItemArray[21].ToString() != "")
                    //{
                    //    // ** IMAGE_2 ** //
                    //    GB_PRODUCT_IMAGES img_2 = new GB_PRODUCT_IMAGES();
                    //    string IMG_2File = row.ItemArray[21].ToString();
                    //    string destinationIMG_2File = @"C:\Publish\Images\Product\" + IMG_2File;
                    //    IMG_2File = @"C:\Publish\Images\Bulk\" + IMG_2File;
                    //    img_2.IMAGE_URL = MoveImage(IMG_2File, destinationIMG_2File);
                    //    img_2.DISPLAY_IMAGE = 2;
                    //    img_2.ALTERNATE_NAME = "Gif Image";
                    //    imgList.Add(img_2);
                    //}
                    //if (row.ItemArray[22].ToString() != null && row.ItemArray[22].ToString() != "")
                    //{
                    //    // ** IMAGE_3 ** //
                    //    GB_PRODUCT_IMAGES img_3 = new GB_PRODUCT_IMAGES();
                    //    string IMG_3File = row.ItemArray[22].ToString();
                    //    string destinationIMG_3File = @"C:\Publish\Images\Product\" + IMG_3File;
                    //    IMG_3File = @"C:\Publish\Images\Bulk\" + IMG_3File;
                    //    img_3.IMAGE_URL = MoveImage(IMG_3File, destinationIMG_3File);
                    //    img_3.DISPLAY_IMAGE = 2;
                    //    img_3.ALTERNATE_NAME = "Gif Image";
                    //    imgList.Add(img_3);
                    //}
                    //product.productImages = imgList;



                    if (string.IsNullOrEmpty(row["Product Title"].ToString()))
                        continue;
                    product.PRODUCT_NAME = row["Product Title"].ToString();
                    product.PRODUCT_CAPTION = row["Short Description"].ToString();
                    //product.CATEGORY_ID = Convert.ToInt32(row["CATEGORY_ID"].ToString());
                    product.CATEGORY_ID = 1;
                    //product.SUB_CAT_ID = Convert.ToInt32(row["SUB_CAT_ID"].ToString());
                    product.SUB_CAT_ID = 1;
                    product.PRODUCT_SKU = row["SKU"].ToString();
                    product.BRAND = row["Brand ID"].ToString();
                    product.PRODUCT_DESC = row["Product Description"].ToString();
                    //product.VIDEO_URL = row["VIDEO_URL"].ToString();
                    product.NET_PRICE = Convert.ToDecimal(row["Web price"].ToString());
                    product.MRP = Convert.ToDecimal(row["MRP"].ToString());
                    // product.GROSS_PRICE = Convert.ToDecimal(row["COST_PRICE"].ToString());
                    product.GIZMOBABA_PRICE = Convert.ToDecimal(row["MRP"].ToString());
                    //product.QUANTITYS = Convert.ToInt32(row["QUANTITYS"].ToString());

                    //product.DELIVERY_TIME = row["DELIVERY_TIME"].ToString();
                    product.INVENTORY = row["Inventory"].ToString();
                    //product.RESERVE_QUANTITY = Convert.ToInt32(row["RESERVE_QUANTITY"].ToString());
                    //product.PKG_QUANTITY = Convert.ToInt32(row["PKG_QUANTITY"].ToString());
                    product.CREATED_ON = DateTime.Now;
                    product.UPDATED_ON = DateTime.Now;
                    product.CREATED_BY = "1";
                    product.UPDATED_BY = "1";
                    product.STATUS = 1;

                    // ** Source Image ** //
                    GB_PRODUCT_IMAGES Sourceimg = new GB_PRODUCT_IMAGES();
                    string sourceFile = row["Small Image 100*100"].ToString();
                    string destinationFile = @"C:\Publish\Images\Product\" + sourceFile;
                    //sourceFile = @"E:\Gizmobaba_New\Gizmobaba_22_2\Gizmobaba\Images\Bulk\" + sourceFile;
                    sourceFile = @"C:\Publish\Images\Bulk\" + sourceFile;

                    Sourceimg.IMAGE_URL = MoveImage(sourceFile, destinationFile);
                    Sourceimg.DISPLAY_IMAGE = 1;
                    Sourceimg.ALTERNATE_NAME = "source Image";
                    imgList.Add(Sourceimg);

                    // ** GIF Image ** //
                    GB_PRODUCT_IMAGES Gifimg = new GB_PRODUCT_IMAGES();
                    string gifFile = row["Large Image 300*300"].ToString();
                    string destinationGifFile = @"C:\Publish\Images\Product\" + gifFile;
                    gifFile = @"C:\Publish\Images\Bulk\" + gifFile;
                    //gifFile = @"E:\Gizmobaba_New\Gizmobaba_22_2\Gizmobaba\Images\Bulk\" + gifFile;
                    Gifimg.IMAGE_URL = MoveImage(gifFile, destinationGifFile);
                    Gifimg.DISPLAY_IMAGE = 2;
                    Gifimg.ALTERNATE_NAME = "Gif Image";
                    imgList.Add(Gifimg);

                    // ** IMAGE_1 ** //
                    //if (row["IMAGE_1"].ToString() != null)
                    //{
                    //    GB_PRODUCT_IMAGES img_1 = new GB_PRODUCT_IMAGES();
                    //    string IMG_1File = row["IMAGE_1"].ToString();
                    //    string destinationIMG_1File = @"C:\Publish\Images\Product\" + IMG_1File;
                    //    IMG_1File = @"C:\Publish\Images\Bulk\" + IMG_1File;
                    //    img_1.IMAGE_URL = MoveImage(IMG_1File, destinationIMG_1File);
                    //    img_1.DISPLAY_IMAGE = 2;
                    //    img_1.ALTERNATE_NAME = "Gif Image";
                    //    imgList.Add(img_1);
                    //}
                    //if (row["IMAGE_2"].ToString() != null && row["IMAGE_2"].ToString() != "")
                    //{
                    //    // ** IMAGE_2 ** //
                    //    GB_PRODUCT_IMAGES img_2 = new GB_PRODUCT_IMAGES();
                    //    string IMG_2File = row["IMAGE_2"].ToString();
                    //    string destinationIMG_2File = @"C:\Publish\Images\Product\" + IMG_2File;
                    //    IMG_2File = @"C:\Publish\Images\Bulk\" + IMG_2File;
                    //    img_2.IMAGE_URL = MoveImage(IMG_2File, destinationIMG_2File);
                    //    img_2.DISPLAY_IMAGE = 2;
                    //    img_2.ALTERNATE_NAME = "Gif Image";
                    //    imgList.Add(img_2);
                    //}
                    //if (row["IMAGE_3"].ToString() != null && row["IMAGE_3"].ToString() != "")
                    //{
                    //    // ** IMAGE_3 ** //
                    //    GB_PRODUCT_IMAGES img_3 = new GB_PRODUCT_IMAGES();
                    //    string IMG_3File = row["IMAGE_3"].ToString();
                    //    string destinationIMG_3File = @"C:\Publish\Images\Product\" + IMG_3File;
                    //    IMG_3File = @"C:\Publish\Images\Bulk\" + IMG_3File;
                    //    img_3.IMAGE_URL = MoveImage(IMG_3File, destinationIMG_3File);
                    //    img_3.DISPLAY_IMAGE = 2;
                    //    img_3.ALTERNATE_NAME = "Gif Image";
                    //    imgList.Add(img_3);
                    //}

                    //img.IMAGE_URL = "Sample.jpg";
                    //img.DISPLAY_IMAGE = 0;
                    //img.ALTERNATE_NAME = "Gallery Image";
                    //imgList.Add(img);

                    //img.IMAGE_URL = "Sample.jpg";
                    //img.DISPLAY_IMAGE = 0;
                    //img.ALTERNATE_NAME = "Gallery Image";
                    //imgList.Add(img);

                    //img.IMAGE_URL = "Sample.jpg";
                    //img.DISPLAY_IMAGE = 0;
                    //img.ALTERNATE_NAME = "Gallery Image";
                    //imgList.Add(img);

                    //img.IMAGE_URL = "Sample.jpg";
                    //img.DISPLAY_IMAGE = 0;
                    //img.ALTERNATE_NAME = "Gallery Image";
                    //imgList.Add(img);

                    //img.IMAGE_URL = "Sample.jpg";
                    //img.DISPLAY_IMAGE = 0;
                    //img.ALTERNATE_NAME = "Gallery Image";
                    //imgList.Add(img);

                    product.productImages = imgList;
                }
                catch
                {
                    if (row.ItemArray[0].ToString() != null && row.ItemArray[0].ToString() != "")
                    {
                        // Session["Error"] = "";
                        product.STATUS = 0;
                    }
                }
                finally
                {

                }

                _return.Add(product);
            }

            return _return;
        }

        public List<M_GB_USER> TransformCustomerImport(DataTable dt)
        {
            var _return = new List<M_GB_USER>();

            foreach (DataRow row in dt.Rows)
            {
                var user = new M_GB_USER();
                try
                {

                    if (string.IsNullOrEmpty(row["Login_ID_*"].ToString()))
                        continue;
                    user.PASSWORD = row["Password"].ToString();
                    user.FIRST_NAME = row["First_Name_*"].ToString();
                    user.LAST_NAME = row["Last_Name_*"].ToString();
                    user.ADD1 = row["Address_Line"].ToString();
                    user.ADD2 = row["Area"].ToString();
                    user.COUNTRY = row["Country_*"].ToString();
                    user.STATE = row["State/Province_*"].ToString();
                    user.CITY = row["City_*"].ToString();
                    user.PIN = Convert.ToInt32(row["Postal/Zip_Code_*"].ToString());
                    user.MOB_NO = row["Mobile_No"].ToString();
                    user.ALT_MOB = row["Phone_No"].ToString();
                    user.ALT_EMAIL = row["Alternate_E-Mail"].ToString();
                    user.GENDER = row["Gender"].ToString();
                   // user.O = Convert.ToInt32(row["Occupation"].ToString());
                    user.DOB = Convert.ToDateTime(row["Birth_Date"].ToString());
                    user.CREATED_ON = Convert.ToDateTime( row["CreateDate"].ToString());
                   // user. = row["LastLoginDate"].ToString();
                  //  user.i = row["IsReceiveOffers"].ToString();
                    user.STATUS = 1;
                }
                catch
                {

                }
                finally
                {
                    if (string.IsNullOrEmpty(row["First_Name_*"].ToString()) || row["First_Name_*"].ToString() != "")
                    {
                        user.STATUS = 0;
                    }
                }

                _return.Add(user);
            }

            return _return;
        }

 

        private List<string> ExtractColumnNamesFromSpreadsheet(DataTable dt)
        {
            var columnNames = new List<string>();

            ////Grab the existing column names and stop when we hit a blank.
            for (int col = 0; col < dt.Columns.Count; col++)
            {
                //When this statement is reached, we have hit the end of the columns names to use in the template
                if (WeHaveReadAllTheColumns(dt, col))
                    break;


                string columnName = dt.Columns[col].ToString();
                columnNames.Add(columnName);
            }
            return columnNames;
        }

        private bool WeHaveReadAllTheColumns(DataTable dt, int col)
        {
            return string.IsNullOrEmpty(dt.Columns[col].ToString());
        }

        public string MoveImage(string sourceFile, string destinationFile)
        {
            string imagetype = Path.GetExtension(sourceFile).ToLower();
            //string strThisDirectory = System.IO.Path.GetDirectoryName(sourceFile);
            File.Copy(sourceFile, destinationFile);
            File.Delete(sourceFile);
            var rondom = @"C:\Publish\Images\Product\" + Guid.NewGuid() + imagetype;
            string imageName = Path.GetFileNameWithoutExtension(rondom) + imagetype;
            File.Move(destinationFile, rondom);
            return imageName;
        }

    }



    public class CustomerImporter
    {
        private IUserService _userService;

        public CustomerImporter(IUserService userService)
        {
            _userService = userService;
        }
        public List<M_GB_USER> TransformCustomerImport(DataTable dt)
        {
            var _return = new List<M_GB_USER>();

            foreach (DataRow row in dt.Rows)
            {
                var user = new M_GB_USER();
                try
                {
                    if (string.IsNullOrEmpty(row.ItemArray[0].ToString()))
                        continue;
                    user.EMAIL = row.ItemArray[0].ToString();
                    user.PASSWORD = row.ItemArray[1].ToString();
                    user.FIRST_NAME = row.ItemArray[2].ToString();
                    user.LAST_NAME = row.ItemArray[3].ToString();
                    user.ADD1 = row.ItemArray[4].ToString();
                    user.ADD2 = row.ItemArray[5].ToString();
                    user.COUNTRY = row.ItemArray[6].ToString();
                    user.STATE = row.ItemArray[7].ToString();
                    user.CITY = row.ItemArray[8].ToString();
                    user.PIN = Convert.ToInt32(row.ItemArray[9].ToString());
                    user.MOB_NO = row.ItemArray[10].ToString();
                    user.ALT_MOB = row.ItemArray[11].ToString();
                    user.ALT_EMAIL = row.ItemArray[12].ToString();
                    user.GENDER = row.ItemArray[13].ToString();
                    // user.O = Convert.ToInt32(row["Occupation"].ToString());
                    user.DOB = Convert.ToDateTime(row.ItemArray[15].ToString());
                    user.CREATED_ON = Convert.ToDateTime(row.ItemArray[16].ToString());
                    // user. = row["LastLoginDate"].ToString();
                    //  user.i = row["IsReceiveOffers"].ToString();
                    user.STATUS = 1;

                    //if (string.IsNullOrEmpty(row["Login ID *"].ToString()))
                    //    continue;
                    //user.PASSWORD = row["Password"].ToString();
                    //string name = row["First Name *"].ToString();
                    //string x = row.ItemArray[0].ToString();
                    //user.FIRST_NAME = row["First_Name_*"].ToString();
                    //user.LAST_NAME = row["Last_Name_*"].ToString();
                    //user.ADD1 = row["Address_Line"].ToString();
                    //user.ADD2 = row["Area"].ToString();
                    //user.COUNTRY = row["Country_*"].ToString();
                    //user.STATE = row["State/Province_*"].ToString();
                    //user.CITY = row["City_*"].ToString();
                    //user.PIN = Convert.ToInt32(row["Postal/Zip_Code_*"].ToString());
                    //user.MOB_NO = row["Mobile_No"].ToString();
                    //user.ALT_MOB = row["Phone_No"].ToString();
                    //user.ALT_EMAIL = row["Alternate_E-Mail"].ToString();
                    //user.GENDER = row["Gender"].ToString();
                    //// user.O = Convert.ToInt32(row["Occupation"].ToString());
                    //user.DOB = Convert.ToDateTime(row["Birth_Date"].ToString());
                    //user.CREATED_ON = Convert.ToDateTime(row["CreateDate"].ToString());
                    //// user. = row["LastLoginDate"].ToString();
                    ////  user.i = row["IsReceiveOffers"].ToString();
                    //user.STATUS = 1;
                }
                catch
                {
                    if (row.ItemArray[0].ToString() != null && row.ItemArray[0].ToString() != "")
                    {
                        user.STATUS = 0;
                    }
                }
                finally
                {
                   
                }

                _return.Add(user);
            }

            return _return;
        }

    }
}