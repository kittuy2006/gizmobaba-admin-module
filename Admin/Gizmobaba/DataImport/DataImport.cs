﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using SmartXLS;
using System.Data;
using Gizmobaba.Service;
using Gizmobaba.Service.Interfaces;

namespace Gizmobaba.DataImport
{
    public class DataImportHandler
    {
        private IProductService _productService;

        public DataImportHandler(IProductService productService)
        {
            _productService = productService;
        }

        public UserImportResponse Process(ImportRequest request)
        {
            var workBook = new WorkBook();
            //request.ImportStream.Position = 0;
            workBook.readXLSX(request.ImportStream);

            int lastDataColumn = workBook.LastCol + 1;
            //int lastDataRow = workBook.LastRow;

            var dt = workBook.ExportDataTable(0, 0, 10000, lastDataColumn, true, true);

            var _return = ProcessUserImport(dt);

            return _return;
        }

        private UserImportResponse ProcessUserImport(DataTable dataTable)
        {
            var _return = new UserDataImporter(_productService).TransformImport(dataTable);

            var response = new UserImportResponse() { IsSuccessful = true, Results = _return };

            return response;
        }



    }

    public class CustomerImportHandler
    {
        private IUserService _userService;

        public CustomerImportHandler(IUserService userService)
        {
            _userService = userService;
        }

        public CustomerImportResponse Process(ImportRequest request)
        {
            var workBook = new WorkBook();
            //request.ImportStream.Position = 0;
            workBook.readXLSX(request.ImportStream);

            int lastDataColumn = workBook.LastCol + 1;
            //int lastDataRow = workBook.LastRow;

            var dt = workBook.ExportDataTable(0, 0, 10000, lastDataColumn, true, true);

            var _return = ProcessCustomerImport(dt);

            return _return;
        }

        private CustomerImportResponse ProcessCustomerImport(DataTable dataTable)
        {
            var _return = new CustomerImporter(_userService).TransformCustomerImport(dataTable);

            var response = new CustomerImportResponse() { IsSuccessful = true, Results = _return };

            return response;
        }



    }
}