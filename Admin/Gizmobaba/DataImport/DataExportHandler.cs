﻿using FastMember;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace Gizmobaba.DataImport
{
    public class DataExportHandler<T> where T : class, new()
    {
        public Stream ExportExcel(List<T> dataToExport, string[] dataMembersToPublish)
        {
            var dt = new DataTable();

            //dt.Columns.Add(new DataColumn("Work"));

            using (var reader = ObjectReader.Create(dataToExport, dataMembersToPublish))
            {
                dt.Load(reader);
            }

            var wb = new SmartXLS.WorkBook();

            wb.ImportDataTable(dt, true, 0, 0, -1, -1);

            var _return = new MemoryStream();

            wb.writeXLSX(_return);

            return _return;
        }
    }
}