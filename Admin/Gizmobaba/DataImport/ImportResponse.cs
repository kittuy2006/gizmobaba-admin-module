﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gizmobaba.DataImport
{
    public class ImportResponse
    {
        public bool IsSuccessful { get; set; }
    }
}