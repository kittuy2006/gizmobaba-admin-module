﻿using System.IO;

namespace Gizmobaba.DataImport
{
    public class ImportRequest
    {
        public Stream ImportStream { get; set; }
    }
}