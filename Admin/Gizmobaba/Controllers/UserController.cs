﻿using AutoMapper;
using Gizmobaba.DataImport;
using Gizmobaba.Models;
using Gizmobaba.Service.Interfaces;
using Gizmobaba.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.IO;
using System.Configuration;

namespace Gizmobaba.Controllers
{
    public class UserController : BaseController<UserController>
    {
          private readonly IProductService _productService;
          private readonly IPermissionsService _permissionsService;
        private readonly IDashboardService _dashboardService;
        private readonly IUserService _userService;
        private readonly IShippingService _shippingService;
        private readonly ICategoryService _categoryService;
        private readonly IOrderService _orderService;

        public UserController(IProductService productService, IPermissionsService permissionsService, IDashboardService dashboardService, IUserService userService, IShippingService shippingService, ICategoryService categoryService, IOrderService orderService)
        {
            _productService = productService;
            _permissionsService = permissionsService;
            _dashboardService = dashboardService;
            _userService = userService;
            _shippingService = shippingService;
            _categoryService = categoryService;
            _orderService = orderService;
        }
        // GET: User/Create
        public ActionResult Create(int id = 0)
        {
            UserModel data = new UserModel();

            if (id != 0)
            {
                Mapper.CreateMap<M_GB_USER, UserModel>();
                M_GB_USER userData = _userService.GetUser(id);
                data = Mapper.Map<UserModel>(userData);
            }

            if (TempData["User"] != null)
                data = (UserModel)TempData["User"];
            ViewBag.Roles = new SelectList(_permissionsService.GetAllRoles(), "ROLE_ID", "ROLE_DESC");
            return View(data);
        }

        // POST: User/Create
        [HttpPost]
        public ActionResult Create(UserModel collection)
        {
            try
            {
                // TODO: Add insert logic here
                return View(collection);
            }
            catch
            {
                return View();
            }
        }
        //[HttpGet]
        public ActionResult Profile()
        {
            DashboardViewModel dashModel = new DashboardViewModel();
            try
            {
                //Mapper.CreateMap<DashboardDto, DashboardViewModel>();
                //DashboardDto dashDTO = _dashboardService.Get();
                //dashModel = Mapper.Map<DashboardViewModel>(dashDTO);

                //Mapper.CreateMap<RolesDto, RolesModel>();
                //IEnumerable<RolesDto> rolesModels = _permissionsService.GetAllRoles().Where(a => a.UserAllow == true);
                //IEnumerable<RolesModel> roles = Mapper.Map<List<RolesModel>>(rolesModels);

                //IEnumerable<RolesDto> rolesModels = _permissionsService.GetAllRoles();
                //IEnumerable<RolesModel> roles = rolesModels.Select(a => new RolesModel
                //{
                //    ROLE_ID = a.ROLE_ID,
                //    ROLE_DESC = a.ROLE_DESC,
                //    PERMISSIONSCOUNT = a.PERMISSIONSCOUNT,
                //    Value = a.Value
                //});
                //dashModel.roleModel = roles;
            }catch(Exception exception)
            {
                ViewBag.Error = exception;
                return View(dashModel);
            }
            return View(dashModel);
        }
        [HttpPost]
        [AllowAnonymous]
        public JsonResult UpdateUserListStatus(string[] userIdList, int statusId)
        {
            foreach (var Id in userIdList)
            {
                _userService.UpdateUserStatus(Convert.ToInt32(Id),statusId);
            }
            return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Success), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult AddUserGroup(int id = 0)
        {
            RolesModel rolesModel = new RolesModel();
            if (id <= 0)
            {
            }
            else
            {
                    Mapper.CreateMap<M_GB_ROLES, RolesModel>();
                    M_GB_ROLES rolesDetails = _permissionsService.GetRoleDetails(id);
                    rolesModel = Mapper.Map<RolesModel>(rolesDetails);
                    //rolesModel.ROLE_ID = roles.ROLE_ID;
                    //rolesModel.ROLE_DESC = roles.ROLE_DESC;
                    //rolesModel.DESCRIPTION = roles.DESCRIPTION;
                    //rolesModel.Value = roles.Value;
                    //roles.UserAllow = roles.UserAllow;
            }
            return View(rolesModel);
        }
        [HttpPost]
        public ActionResult AddUserGroup(M_GB_ROLES role)
        {
            role.STATUS = 1;
           int roleID = _permissionsService.AddRole(role);
            return View();
        }
        [HttpGet]
        public ActionResult Users()
        {

            RolesModel roleModel = new RolesModel();

            Mapper.CreateMap<UserDto, UserModel>();
            IEnumerable<UserDto> userList = _userService.GetUsersbyCount(10, 0);
            IEnumerable<UserModel> userData = Mapper.Map<List<UserModel>>(userList);
            roleModel.userModel = userData;
            return View(roleModel);
        }

        [HttpGet]
        public ActionResult Customer(int Id = 0)
        {
            RolesModel roleModel = new RolesModel();
            Mapper.CreateMap<UserDto, UserModel>();
            if (Id == 0)
                Id = (int)CustomStatus.UserTypes.Customer;
            IEnumerable<UserDto> userList = _userService.GetUsersbyCount(100, Id);
            IEnumerable<UserModel> userData = Mapper.Map<List<UserModel>>(userList);
            roleModel.userModel = userData;
            return View(roleModel);
        }


        [HttpGet]
        public ActionResult GetUsersData(int count, int roleId = 0)
        {
            IEnumerable<UserDto> userList;
            Mapper.CreateMap<UserDto, UserModel>();
             userList = _userService.GetUsersbyCount(count, roleId);
            IEnumerable<UserModel> userData = Mapper.Map<List<UserModel>>(userList);
            return View("_usersPartial", userData);
        }
        public ActionResult UserProfile(int id=0)
        {
            UserModel user = new UserModel();
            if (id != 0)
            {
                if (id == (int)CustomStatus.UserTypes.Dealer)
                {
                    user.ROLE = (int)CustomStatus.UserTypes.Dealer;
                    return View(user);
                }


                Mapper.CreateMap<M_GB_USER, UserModel>();
                M_GB_USER userData = _userService.GetUser(id);
                user = Mapper.Map<UserModel>(userData);
            }else
            {
                string email = User.Identity.GetUserName();
                if (!string.IsNullOrEmpty(email))
                {
                    Mapper.CreateMap<M_GB_USER, UserModel>();
                    M_GB_USER userData = _userService.GetUserByEmailID(email);
                    user = Mapper.Map<UserModel>(userData);
                    return View(user);
                }
            }
            Mapper.CreateMap<M_GB_COUNTRY, ShippingCountriesModel>();
            IEnumerable<M_GB_COUNTRY> countries = _shippingService.GetShippingCountries().OrderBy(a => a.COUNTRY_DESC);
            IEnumerable<ShippingCountriesModel> countriesList = Mapper.Map<List<ShippingCountriesModel>>(countries);
            user.Countries = countriesList;
            return View(user);
        }
        [HttpPost]
        public ActionResult UserProfile(M_GB_USER user)
        {
            string success = "";
            string error = "";
            try
            {
                user.CREATED_ON = DateTime.Now;
                user.UPDATED_ON = DateTime.Now;
              string userId =  _userService.AddUser(user);
              if (userId != "0")
              {
                  if (userId == "success")
                      success = "Details Successfully Updated";
                  else
                  {
                      UserModel model = new UserModel();
                      Mapper.CreateMap<M_GB_USER, UserModel>();
                      if (user != null && user.EMAIL != null)
                      {
                          M_GB_USER userData = _userService.GetUserByEmailID(user.EMAIL);
                          model = Mapper.Map<UserModel>(userData);
                      }
                      else
                          model = Mapper.Map<UserModel>(user);
                      model.RoleName = CustomStatus.UserStatusType(Convert.ToInt32(user.ROLE)).ToString();

                     

                      success = "Details Successfully Added";
                      return RedirectToAction("CreateUser", "Account", model);
                  }
                 
              }
              else
                  error = "User already exists please choose another Email / Login Id";

            }
            catch (Exception exception)
            {
                error = exception.ToString();
            }
            //finally
            //{
            //    TempData["SuccessMessage"] = success;
            //    TempData["ErrorMessage"] = error;
            //}
            TempData["SuccessMessage"] = success;
            TempData["ErrorMessage"] = error;
            Mapper.CreateMap<M_GB_USER, UserModel>();
            UserModel userModel = Mapper.Map<UserModel>(user);
          //  ViewBag.Roles = new SelectList(_permissionsService.GetAllRoles(), "ROLE_ID", "ROLE_DESC");
           // return View("Create", userModel);
            return View(userModel);
        }
        public ActionResult GlobalvendorProfile(int id = 0)
        {
            //M_GB_USER userData = _userService.GetUser(id);
            UserModel user = new UserModel();
            if (id != 0)
            {
                //Mapper.CreateMap<M_GB_USER, UserModel>();
                //M_GB_USER userData = _userService.GetUser(id);
                //user = Mapper.Map<UserModel>(userData);
            }
            return View(user);
        }
        public ActionResult MemberProfile()
        {
           // DashboardViewModel dashModel = new DashboardViewModel();
            Mapper.CreateMap<DashboardDto, DashboardViewModel>();
            DashboardDto dashDTO = _dashboardService.Get();
            DashboardViewModel dashModel = Mapper.Map<DashboardViewModel>(dashDTO);

            Mapper.CreateMap<RolesDto, RolesModel>();
            IEnumerable<RolesDto> rolesModels = _permissionsService.GetAllRoles();
            IEnumerable<RolesModel> roles = Mapper.Map<List<RolesModel>>(rolesModels);

            dashModel.roleModel = roles;
            return View(dashModel);
        }
        [HttpPost]
        public ActionResult UserImport(HttpPostedFileBase file)
        {
            try
            {
                string sucMessage = "";
                string errorMessage = "";
                if (file != null)
                {
                    if (file.FileName.EndsWith("xls") || file.FileName.EndsWith("xlsx"))
                    {
                        string error = "";
                        var imported =
                         new CustomerImportHandler(_userService).Process(new ImportRequest() { ImportStream = file.InputStream });
                        foreach (M_GB_USER userList in imported.Results)
                        {
                            if (userList.STATUS == (int)CustomStatus.DefaultStatus.Active){
                                _userService.AddUser(userList);
                                sucMessage = "User List Successfully Uploaded.";
                            }
                            else if (userList.STATUS == (int)CustomStatus.DefaultStatus.DeActive)
                                errorMessage += userList.FIRST_NAME + "User not uploaded";
                        }
                        TempData["SuccessMessage"] = sucMessage;
                        TempData["ErrorMessage"] = sucMessage;
                       // ViewBag.ErrorMessage = errorMessage;
                    }
                    else
                    {
                        ViewBag.ErrorMessage = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Filetype);
                    }
                    RolesModel roleModel = new RolesModel();

                    Mapper.CreateMap<UserDto, UserModel>();
                    IEnumerable<UserDto> userDataList = _userService.GetUsersbyCount(10, 2);
                    IEnumerable<UserModel> userData = Mapper.Map<List<UserModel>>(userDataList);
                    roleModel.userModel = userData;
                    return View("Users",roleModel);
                }
                TempData["ErrorMessage"] = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Excelfile);
                return View();
            }
            catch (Exception)
            {
                ViewBag.ErrorMsg = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.FaileAgian);
                return View();
            }
        }

        [Authorize(Roles = "Admin")]
        public ActionResult DownloadUserSheet()
        {
            // download sample rec
            var usersList = new List<M_GB_USER>();
            
            var columns = new List<string> { "Login ID *", "Password", "First Name *", "Last Name *", "Address Line", "Area", "Country *", "State/Province *", "City *", "Postal/Zip Code *", "Mobile No", "Phone No", "Alternate E-Mail", "Gender", "Occupation", "Birth Date", "CreateDate", "LastLoginDate", "IsReceiveOffers" }.ToArray();
            var stream = new DataExportHandler<M_GB_USER>().ExportExcel(usersList.ToList(), columns);
            stream.Position = 0;
            return File(stream, "application/vnd.ms-excel", "Users.xlsx");
        }
        [HttpGet]
        [AllowAnonymous]
        public JsonResult UserSearch(UserDto data)
        {
            Mapper.CreateMap<UserDto, UserModel>();
            IEnumerable<UserDto> userList = _userService.GetUsersSearch(data);
            IEnumerable<UserModel> userData = Mapper.Map<List<UserModel>>(userList);
            return Json(userData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Search(UserDto data)
        {
            Mapper.CreateMap<UserDto, UserModel>();
            IEnumerable<UserDto> userList = _userService.GetUsersSearch(data);
            IEnumerable<UserModel> userData = Mapper.Map<List<UserModel>>(userList);
            return View("_usersPartial", userData);
        }
        [HttpGet]
        public ActionResult AddFeatureAccess(int id = 0)
        {
            FeatureTypeViewModel featureTypeViewModel = new FeatureTypeViewModel();
            UserModel user = new UserModel();
            int roleId = 0;
            M_GB_USER userData = new M_GB_USER();
            if (id >= 0)
            {
                Mapper.CreateMap<M_GB_USER, UserModel>();
                userData = _userService.GetUser(id);
                featureTypeViewModel.UserModel = Mapper.Map<UserModel>(userData);
                roleId = Convert.ToInt32(userData.ROLE);
            }
            //IEnumerable<FeatureTypeDto> getFeatureTypes = _permissionsService.GetFeatureTypes();
            //Mapper.CreateMap<FeatureTypeDto, FeatureTypeModel>();
            //IEnumerable<FeatureTypeDto> getFeatureType = _permissionsService.GetFeatureAccessByRoleId(roleId);
            //IEnumerable<FeatureTypeModel> getFeatureTypeList = Mapper.Map<List<FeatureTypeModel>>(getFeatureTypes);

            IEnumerable<FeatureTypeDto> getFeatureTypes = _permissionsService.GetFeatureAccessByRoleId(roleId).Where(a=>a.STATUS == 1);
            Mapper.CreateMap<FeatureTypeDto, FeatureTypeModel>();
            IEnumerable<FeatureTypeDto> getFeatureType = _permissionsService.GetFeatureAccessByUser(id.ToString()).Where(a=>a.STATUS == 1);
            IEnumerable<FeatureTypeModel> getFeatureTypeList = Mapper.Map<List<FeatureTypeModel>>(getFeatureTypes);

            //IEnumerable<FeatureTypeDto> getFeatureType = _permissionsService.GetFeatureAccessByRoleId(id);
            //IEnumerable<FeatureTypeModel> getFeatureTypeList = getFeatureType.Select(featureTypeDto => new FeatureTypeModel
            //{
            //    FEATURE_TYPE = featureTypeDto.FEATURE_TYPE,
            //    FEATURE_TYPE_ID = featureTypeDto.FEATURE_TYPE_ID,
            //    STATUS = featureTypeDto.STATUS,
            //    ROLE_ID = featureTypeDto.ROLE_ID,
            //    ID = featureTypeDto.ID,
            //}).ToList();
            foreach (FeatureTypeDto item in getFeatureType)
            {
                if (item.STATUS == 1)
                {
                    FeatureTypeModel featureTypeModel =
                        getFeatureTypeList.FirstOrDefault(a => a.FEATURE_TYPE_ID == item.FEATURE_TYPE_ID);
                    if (featureTypeModel != null)
                        featureTypeModel.chkBox = 1;
                }
            }
            featureTypeViewModel.FeatureType = getFeatureTypeList;
            return View(featureTypeViewModel);
        }
        [HttpPost]
        public ActionResult AddFeatureAccess(FormCollection froCollection)
        {
            int id = 0;
            string sucMessage = "";
            string errorMessage = "";
            try
            {
                int roleId = Convert.ToInt32(Request.Form["UserModel.ROLE"]);
                if (roleId != 0)
                {
                    IEnumerable<FeatureTypeDto> getFeatureType = _permissionsService.GetFeatureTypes().Where(a => a.STATUS == 2);
                    List<GB_USER_FEATURE_ACCESS> featureTypesList = getFeatureType.Select(item => new GB_USER_FEATURE_ACCESS
                    {
                        FEATURE_TYPE_ID = item.FEATURE_TYPE_ID,
                        ROLE_ID = roleId,
                        STATUS = 0,
                        CREATED_ON = DateTime.Now,
                        USER_ID = Request.Form["UserModel.USER_ID"]
                    }).ToList();
                    int count = 2;
                    foreach (var key in froCollection.AllKeys)
                    {
                        if (count < froCollection.AllKeys.Length)
                        {
                            string list = Request.Form[froCollection.AllKeys[count]];
                            if (list.Split(',').Count() != 0)
                            {
                                foreach (string featureTypeId in list.Split(','))
                                {
                                    int Id = Convert.ToInt32(featureTypeId);
                                    featureTypesList.FirstOrDefault(x => x.FEATURE_TYPE_ID == Id).STATUS = 1;
                                }
                            }
                        }
                        count++;
                    }
                    foreach (GB_USER_FEATURE_ACCESS featureAccess in featureTypesList)
                    {
                        _permissionsService.AddFeatureAccessToUser(featureAccess);
                    }
                    sucMessage = "succefuly " + sucMessage;
                }
            }
            catch (Exception exception)
            {
                errorMessage = exception.ToString();
            }
            TempData["SuccessMessage"] = sucMessage;
            TempData["ErrorMessage"] = errorMessage;
            return RedirectToAction("AddFeatureAccess");
        }
        [HttpPost]
        public ActionResult UpdateUserImage(HttpPostedFileBase file, string USER_ID)
        {
            try
            {
                if (file != null && file.ContentLength > 0 && USER_ID != "")
                {
                    string imagetype = Path.GetExtension(file.FileName).ToLower();
                    var rondom = Guid.NewGuid() + imagetype;
                    var path = Path.Combine(ConfigurationManager.AppSettings["UserImage"] + "\\" + rondom);
                    if (imagetype == ".jpg" || imagetype == ".png" || imagetype == ".gif" || imagetype == ".jpeg")
                    {
                        M_GB_USER userData = new M_GB_USER();
                        userData.USER_ID = USER_ID;
                        userData.PICTURE = rondom;
                        userData.UPDATED_ON = DateTime.Now;
                        file.SaveAs(path);

                      string Id =  _userService.AddUser(userData);
                        TempData["Message"] = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Uploaded);

                        return RedirectToAction("UserProfile");
                    }
                    else
                    {

                        TempData["Error"] = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Filetype);
                        return RedirectToAction("UserProfile");

                    }
                }
                else
                {
                    TempData["Error"] = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Excelfile);
                    return RedirectToAction("UserProfile");
                }
            }
            catch (Exception)
            {
                TempData["Error"] = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.FaileAgian);
                return RedirectToAction("UserProfile");
            }
        }
        [HttpGet]
        public ActionResult GlobalvendorDetails()
        {
            //GlobalvendorModel data = new GlobalvendorModel();
            //int id = 0;
            //GlobalvendorDTO datas = _dashboardService.GetGlobalVenderData(id);

            string id = "";
            if (TempData["User"] != null)
            {
                UserModel userData = (UserModel)TempData["User"];
                TempData["User"] = userData;
                id = userData.USER_ID;
            }



            Mapper.CreateMap<OrderDetailDto, OrderViewModel>();
            int paymentTypeId = (int)CustomStatus.PaymentTypes.COD;
            IEnumerable<OrderDetailDto> orderDetail = _orderService.GetVendorOrderList(id);
            IEnumerable<OrderViewModel> orderList = Mapper.Map<List<OrderViewModel>>(orderDetail);
            //return PartialView("_globalvendorDetails", data);
            return PartialView("_globalvendorOrderDetails", orderList);
        }

        [HttpGet]
        public ActionResult Dealer()
        {
            int status = 1;
            Mapper.CreateMap<Dealer, DealerDto>();
            IEnumerable<Dealer> userList = _userService.GetActiveDealers(status);
            IEnumerable<DealerDto> activeDealers = Mapper.Map<List<DealerDto>>(userList);
            return View(activeDealers);
        }
        [HttpPost]
        public ActionResult GetDealer(int status = 0)
        {
            Mapper.CreateMap<Dealer, DealerDto>();
            IEnumerable<Dealer> userList = _userService.GetActiveDealers(Convert.ToInt32(status));
            IEnumerable<DealerDto> activeDealers = Mapper.Map<List<DealerDto>>(userList);
            return PartialView("_dealerpartial", activeDealers);

        }

        [HttpGet]
        public ActionResult SearchDealer(string fromDate, string toDate,string userName,string userId,string pincode)
        {
            DateTime frmDate = DateTime.Now.AddDays(-10);
            DateTime toDa = DateTime.Now.AddDays(-10);
            if (!string.IsNullOrEmpty(fromDate))
                frmDate = Convert.ToDateTime(fromDate);
            if (!string.IsNullOrEmpty(toDate))
                toDa = Convert.ToDateTime(toDate);

            Mapper.CreateMap<Dealer, DealerDto>();
            IEnumerable<Dealer> userList = _userService.GetSearchDealer(frmDate, toDa, userName, userId, pincode);
            IEnumerable<DealerDto> Dealers = Mapper.Map<List<DealerDto>>(userList);
            return PartialView("_dealerpartial", Dealers);
        }

        [HttpGet]
        public ActionResult MasterFranchise()
        {
            Mapper.CreateMap<Dealer, DealerDto>();
            IEnumerable<Dealer> userList = _userService.GetMasterFranchise((int) CustomStatus.DefaultStatus.Active);
            IEnumerable<DealerDto> users = Mapper.Map<List<DealerDto>>(userList);
            return View(users);
        }
        [HttpGet]
        public ActionResult MasterFranchiseProfile(int id=0)
        {
            DealerView dealerView = new DealerView();
            dealerView.Dealer = new DealerDto();
            if (id != 0)
            {
                Mapper.CreateMap<Dealer, DealerDto>();
                Dealer masterFc = _userService.GetMasterWallet(id);
                dealerView.Dealer = Mapper.Map<DealerDto>(masterFc);
                if (dealerView.Dealer != null && dealerView.Dealer.USERDETAIL_ID != 0)
                {
                    Mapper.CreateMap<Dealer, DealerDto>();
                    IEnumerable<Dealer> rechargeWalletDeatils = _userService.GetrechargeWalletDeatils(dealerView.Dealer.USERDETAIL_ID);
                    dealerView.DealerList = Mapper.Map<List<DealerDto>>(rechargeWalletDeatils);
                    if(dealerView.DealerList.Count() != 0)
                    {
                        foreach(var item in dealerView.DealerList)
                        {
                            if (!string.IsNullOrEmpty(item.CREATED_BY) || item.CREATED_BY == CustomStatus.UserTypes.Admin.ToString() )
                                item.NAME = _userService.findUserNamebyId(item.CREATED_BY);
                            else
                                item.NAME = CustomStatus.UserTypes.Admin.ToString();
                        }
                    }
                }
                if (dealerView.Dealer.COUNTRY != null && dealerView.Dealer.COUNTRY != 0)
                {
                    Mapper.CreateMap<StatesDto, StatesModel>();
                    IEnumerable<StatesDto> states = _shippingService.GetStates(Convert.ToInt32(dealerView.Dealer.COUNTRY)).OrderBy(a => a.STATE);
                    IEnumerable<StatesModel> StatesList = Mapper.Map<List<StatesModel>>(states);
                    dealerView.Dealer.States = StatesList;
                }
                if (dealerView.Dealer.STATE != 0)
                {
                    Mapper.CreateMap<RegionDto, RegionModel>();
                    IEnumerable<RegionDto> Cities = _shippingService.GetRegions(dealerView.Dealer.STATE).OrderBy(a => a.REGION);
                    IEnumerable<RegionModel> CityList = Mapper.Map<List<RegionModel>>(Cities);
                    dealerView.Dealer.Cityes = CityList;
                }

            }
            Mapper.CreateMap<M_GB_COUNTRY, ShippingCountriesModel>();
            IEnumerable<M_GB_COUNTRY> countries = _shippingService.GetShippingCountries().OrderBy(a => a.COUNTRY_DESC);
            IEnumerable<ShippingCountriesModel> countriesList = Mapper.Map<List<ShippingCountriesModel>>(countries);
            dealerView.Dealer.Countries = countriesList;
            return View(dealerView);
        }
        [HttpGet]
        public ActionResult GetMasterFranchise(int status = 0)
        {
            Mapper.CreateMap<Dealer, DealerDto>();
            IEnumerable<Dealer> userList = _userService.GetMasterFranchise(status);
            IEnumerable<DealerDto> users = Mapper.Map<List<DealerDto>>(userList);
            return PartialView("masterFranchisepartial", users);
        }
        [HttpGet]
        public ActionResult GetDealerListByMFId(int id)
        {
            Mapper.CreateMap<Dealer, DealerDto>();
            IEnumerable<Dealer> userList = _userService.GetDealersnyMFId(id);
            IEnumerable<DealerDto> Dealers = Mapper.Map<List<DealerDto>>(userList);
            if (Dealers.Count() != 0)
            {
                foreach (var item in Dealers)
                {
                    if (!string.IsNullOrEmpty(item.CREATED_BY) || item.CREATED_BY == CustomStatus.UserTypes.Admin.ToString())
                        item.NAME = _userService.findUserNamebyId(item.CREATED_BY);
                    else
                        item.NAME = CustomStatus.UserTypes.Admin.ToString();
                }
            }
            return PartialView("_dealerpartial", Dealers);
        }
        [HttpGet]
        public ActionResult SearchMasterFranchise(string fromDate, string toDate)
        {
            Mapper.CreateMap<Dealer, DealerDto>();
            IEnumerable<Dealer> userList = _userService.GetSearchMasterFranchise(Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate));
            IEnumerable<DealerDto> activeDealers = Mapper.Map<List<DealerDto>>(userList);
            return PartialView("_dealerpartial", activeDealers);
        }
        [HttpGet]
        public ActionResult DealerProfile(int id=0)
        {
            DealerView dealerView = new DealerView();
            dealerView.Dealer = new DealerDto();
            Mapper.CreateMap<M_GB_COUNTRY, ShippingCountriesModel>();
            IEnumerable<M_GB_COUNTRY> countries = _shippingService.GetShippingCountries().OrderBy(a => a.COUNTRY_DESC);
            IEnumerable<ShippingCountriesModel> countriesList = Mapper.Map<List<ShippingCountriesModel>>(countries);


            Mapper.CreateMap<CategoryDto, Category>();
            IEnumerable<CategoryDto> cateList = _categoryService.GetCategories();
            IEnumerable<Category> CategoryList = Mapper.Map<List<Category>>(cateList);
            dealerView.CategoryList = CategoryList;
            if (id != 0)
            {
                if (id == (int) CustomStatus.UserTypes.Dealer)
                    dealerView.Dealer.ROLE = (int)CustomStatus.UserTypes.Dealer;
                else
                {
                    Mapper.CreateMap<Dealer, DealerDto>();
                    Dealer masterFc = _userService.GethDealer(id);
                    dealerView.Dealer = Mapper.Map<DealerDto>(masterFc);
                    if(masterFc != null && masterFc.CategoryList != null && masterFc.CategoryList.Count() != 0)
                    {
                        dealerView.SelectedCategory = new List<int>();
                        foreach(GB_USER_CATEGORY item in masterFc.CategoryList)
                        {
                            dealerView.SelectedCategory.Add(Convert.ToInt32(item.CATEGORY_ID));
                        }
                    }
                    if (dealerView.Dealer != null && dealerView.Dealer.USERDETAIL_ID != 0)
                    {
                        Mapper.CreateMap<Dealer, DealerDto>();
                        IEnumerable<Dealer> rechargeWalletDeatils = _userService.GetrechargeWalletDeatils(dealerView.Dealer.USERDETAIL_ID);
                        dealerView.DealerList = Mapper.Map<List<DealerDto>>(rechargeWalletDeatils);
                    }
                    if (dealerView.Dealer.COUNTRY != null && dealerView.Dealer.COUNTRY != 0)
                    {
                        Mapper.CreateMap<StatesDto, StatesModel>();
                        IEnumerable<StatesDto> states = _shippingService.GetStates(Convert.ToInt32(dealerView.Dealer.COUNTRY)).OrderBy(a => a.STATE);
                        IEnumerable<StatesModel> StatesList = Mapper.Map<List<StatesModel>>(states);
                        dealerView.Dealer.States = StatesList;
                    }
                    if (dealerView.Dealer.STATE != 0)
                    {
                        Mapper.CreateMap<RegionDto, RegionModel>();
                        IEnumerable<RegionDto> Cities = _shippingService.GetRegions(dealerView.Dealer.STATE).OrderBy(a => a.REGION);
                        IEnumerable<RegionModel> CityList = Mapper.Map<List<RegionModel>>(Cities);
                        dealerView.Dealer.Cityes = CityList;
                    }

                }
            }
            dealerView.Dealer.COUNTRY = dealerView.Dealer.COUNTRY ?? 1;
            dealerView.Dealer.Countries = countriesList;
            return View(dealerView);
        }
        [HttpPost]
        public ActionResult DealerProfile(UserModel user)
        {
            string success = "";
            string error = "";
            try
            {
                user.CREATED_ON = DateTime.Now;
                user.UPDATED_ON = DateTime.Now;
                M_GB_USER m_Gb_users = new M_GB_USER
                {
                    ID = user.ID,
                    USER_ID = user.USER_ID,
                    USER_NAME = user.USER_NAME,
                    FIRST_NAME = user.FIRST_NAME,
                    LAST_NAME = user.LAST_NAME,
                    EMAIL = user.EMAIL,
                    COUNTRY =  user.CountryId.ToString(),
                    STATE =  user.StateId.ToString(),
                    ADD1 = user.ADD1,
                    ADD2 = user.ADD2
                };
                M_GB_USERDETAILS userdetails = new M_GB_USERDETAILS
                {
                    NAME = user.USER_NAME,
                    COMPANY = user.COMPANY,
                    ROLE_ID = (int)CustomStatus.UserTypes.Dealer,
                    CREATED_ON = DateTime.Now,
                    AMOUNT = user.AMOUNT,
                    WALLET = user.WALLET
                };
                string userId = _userService.AddUser(m_Gb_users);
                string statusId = _userService.AddDealerDetails(userdetails);
                if (userId != "0")
                {
                        success = "Details Successfully Updated";
                }
                else
                    error = "User already exists please choose another Email / Login Id";

            }
            catch (Exception exception)
            {
                error = exception.ToString();
            }
            TempData["SuccessMessage"] = success;
            TempData["ErrorMessage"] = error;
            Mapper.CreateMap<M_GB_USER, UserModel>();
            UserModel userModel = Mapper.Map<UserModel>(user);
            //  ViewBag.Roles = new SelectList(_permissionsService.GetAllRoles(), "ROLE_ID", "ROLE_DESC");
            // return View("Create", userModel);
            Mapper.CreateMap<M_GB_COUNTRY, ShippingCountriesModel>();
            IEnumerable<M_GB_COUNTRY> countries = _shippingService.GetShippingCountries().OrderBy(a => a.COUNTRY_DESC);
            IEnumerable<ShippingCountriesModel> countriesList = Mapper.Map<List<ShippingCountriesModel>>(countries);
            userModel.Countries = countriesList;
            return View(userModel);
        }

        [HttpGet]
        public ActionResult Globalvendor()
        {
            int status = (int)CustomStatus.DefaultStatus.Active;
            Mapper.CreateMap<UserDto, UserModel>();
            IEnumerable<UserDto> userList = _userService.GetGlobalvendor(status);
            IEnumerable<UserModel> activeDealers = Mapper.Map<List<UserModel>>(userList);
            return View(activeDealers);
        }
        [HttpPost]
        public ActionResult GetGlobalvendor(int status = 0)
        {
            Mapper.CreateMap<UserDto, UserModel>();
            IEnumerable<UserDto> userList = _userService.GetGlobalvendor(Convert.ToInt32(status));
            IEnumerable<UserModel> activeDealers = Mapper.Map<List<UserModel>>(userList);
            return PartialView("globalvendorpartial", activeDealers);
        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetMasterFranchiseByState(int id)
        {
            IEnumerable<Dealer> masterFcList;
            Mapper.CreateMap<Dealer, DealerDto>();
             IEnumerable<Dealer> userList = _userService.GetMasterFranchise((int)CustomStatus.DefaultStatus.Active);
             if (userList != null && !userList.All(a => a.STATE != id))
                 masterFcList = userList.Where(a => a.STATE == id).ToList();
            else
                 masterFcList = userList;

             IEnumerable<DealerDto> activemasterFc = Mapper.Map<List<DealerDto>>(masterFcList);
            return Json(activemasterFc, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetMasterWallet(int id)
        {
            Mapper.CreateMap<Dealer, DealerDto>();
            Dealer masterFc = _userService.GetMasterWallet(id);
            DealerDto activemasterFc = Mapper.Map<DealerDto>(masterFc);
            return Json(activemasterFc, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult VenderProfile(int id = 0)
        {
            DealerDto dealer = new DealerDto();
            if (id != 0)
            {
                Mapper.CreateMap<Dealer, DealerDto>();
                Dealer masterFc = _userService.GetVender(id);
                if(masterFc != null)
                dealer = Mapper.Map<DealerDto>(masterFc);
            }
            Mapper.CreateMap<M_GB_COUNTRY, ShippingCountriesModel>();
            IEnumerable<M_GB_COUNTRY> countries = _shippingService.GetShippingCountries().OrderBy(a => a.COUNTRY_DESC);
            IEnumerable<ShippingCountriesModel> countriesList = Mapper.Map<List<ShippingCountriesModel>>(countries);
            dealer.Countries = countriesList;
            return View(dealer);
        }
        [HttpPost]
        public ActionResult UpdateWalletPoints(Dealer dealer)
        {
            GB_DEALER_RECHARGE recharge = new GB_DEALER_RECHARGE
            {
                RECHARGE_POINTS = Convert.ToInt32(dealer.AMOUNT),
                ROLE_ID = (int)CustomStatus.UserTypes.Master_Franchise,
                USERDETAIL_ID = Convert.ToInt32(dealer.USERDETAIL_ID),
                USER_ADMIN_ID = (int)CustomStatus.UserTypes.Admin,
                STATUS = (int)CustomStatus.DefaultStatus.Active,
                CREATED_ON = DateTime.Now,
                CREATED_BY = ((int)CustomStatus.UserTypes.Admin).ToString()
            };
           string status = _userService.UpdateMFRecharge(recharge);
           if (status == CustomStatus.ReturnStatus.success.ToString())
               TempData["SuccessMessage"] = "Master Franchise wallet successfully updated";
           return RedirectToAction("MasterFranchiseProfile", "User", new { id = dealer.USER_ID });
        }
        [HttpGet]
        [AllowAnonymous]
        public JsonResult CheckMasterFranchiseByState(int id)
        {
            int status = _userService.CheckMasterFranchiseByState(id);
            if(status == 0)
                return Json("Success", JsonRequestBehavior.AllowGet);
            else
                return Json("Master Franchise already exists in selected state", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [AllowAnonymous]
        public JsonResult UpdateUserStatus(int id)
        {
            int status = _userService.UpdateUserStatus(id);
                return Json("Success", JsonRequestBehavior.AllowGet);
        }

    }
}
