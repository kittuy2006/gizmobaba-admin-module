﻿using Gizmobaba.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Gizmobaba.Service.Interfaces;
using Gizmobaba.Utility;
using AutoMapper;
using System.IO;


namespace Gizmobaba.Controllers
{
    [Authorize]
    public class CategoryManageController : BaseController<CategoryManageController>
    {
        private readonly ICategoryService _categoryService;
        // GET: CategoryManage

        public CategoryManageController(ICategoryService _categoryService) {
            this._categoryService = _categoryService;
        }

        public ActionResult Index()
        {
            try
            {
                //Mapper.CreateMap<CategoryDto, Category>();
                IEnumerable<CategoryDto> cateList = _categoryService.GetCategories();
                //IEnumerable<Category> cateLists = Mapper.Map<List<Category>>(cateList);
                 IList<Category> cateViewLists = new List<Category>();
                foreach (CategoryDto data in cateList)
                {
                    Category categoryVIew = new Category
                    {
                        CATEGORY_ID = data.CATEGORY_ID,
                        CATEGORY_DESC = data.CATEGORY_DESC,
                        CREATEDF_ON = data.CREATEDF_ON,
                        ICON = data.ICON,
                        SEQ_NO = data.SEQ_NO,
                        ProductCount = data.ProductsCount,
                        REFERENCE_CODE = data.REFERENCE_CODE,
                        SubCategories = data.SubCategory.Where(a=>a.CATEGORY_ID == data.CATEGORY_ID).Select(s => new SubCategoriesViewModel
                        {
                            REFERENCE_CODE = s.REFERENCE_CODE,
                            SUB_CAT_DESC = s.SUB_CAT_DESC,
                            CATEGORY_ID = s.CATEGORY_ID,
                            DESCRIPTION = s.DESCRIPTION,
                            NUMBER_OF_COLUMNS = s.NUMBER_OF_COLUMNS,
                            SUB_CAT_GROUP = s.SUB_CAT_GROUP,
                            RANK = s.RANK,
                            SUB_CAT_ID = s.SUB_CAT_ID
                        }).ToList()
                    };

                    foreach (SubCategoriesViewModel subCategoryModel in categoryVIew.SubCategories)
                    {
                        subCategoryModel.ProductCount = _categoryService.GetProductCountbySubCatId(subCategoryModel.SUB_CAT_ID);
                    }
                    cateViewLists.Add(categoryVIew);
                    //SubCategoriesViewModel subCategories = new SubCategoriesViewModel();
                    //foreach (M_GB_SUB_CATEGORY subCategoryModel in data.SubCategory)
                    //{
                    //    subCategories.REFERENCE_CODE = subCategoryModel.REFERENCE_CODE;
                    //    subCategories.SUB_CAT_DESC = subCategoryModel.SUB_CAT_DESC;
                    //    subCategories.CATEGORY_ID = subCategoryModel.CATEGORY_ID;
                    //    subCategories.DESCRIPTION = subCategoryModel.DESCRIPTION;
                    //    subCategories.NUMBER_OF_COLUMNS = subCategoryModel.NUMBER_OF_COLUMNS;
                    //    subCategories.SUB_CAT_GROUP = subCategoryModel.SUB_CAT_GROUP;
                    //    subCategories.RANK = subCategoryModel.RANK;
                    //    subCategories.SUB_CAT_ID = subCategoryModel.SUB_CAT_ID;
                    //    categoryVIew.SubCategories.Add(subCategories);
                    //}
                }
                IEnumerable<Category> cateLists = cateViewLists;
                CategoryViewModel category = new CategoryViewModel();
                category.REFERENCE_CODE = "GBC0" + (cateList.LastOrDefault().CATEGORY_ID + 1);
                category.CategoryContentInformation = new CategoryContentInformation();
                category.CategorySeoInformation = new CategorySeoInformation();
                CategoryList model = new CategoryList
                {
                    categoryList = cateLists,
                    categoryViewModel = category
                };
                return View(model);
            }
            catch
            {
                return View();
            }
        }

        // GET: CategoryManage/Create
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult EditCategory(int id)
        {
            //CategoryDto category = _categoryService.GetCategory(id);
            //CategoryViewModel cvm = new CategoryViewModel
            //{
            //    CATEGORY_ID = category.CATEGORY_ID,
            //    CATEGORY_DESC = category.CATEGORY_DESC,
            //    CREATEDF_ON = category.CREATEDF_ON,
            //    ICON = category.ICON,
            //    SEQ_NO = category.SEQ_NO,
            //    REFERENCE_CODE = category.REFERENCE_CODE,
            //    CATEGORY_RANK = category.CATEGORY_RANK,
            //};

            Mapper.CreateMap<CategoryDto, CategoryViewModel>();
            CategoryDto category = _categoryService.GetCategory(id);
            //CategoryViewModel cvm = Mapper.Map<CategoryViewModel>(category);
            CategoryViewModel cvm = new CategoryViewModel
            {
                CATEGORY_ID = category.CATEGORY_ID,
                CATEGORY_DESC = category.CATEGORY_DESC,
                CATEGORY_RANK = category.CATEGORY_RANK,
                CREATEDF_ON = category.CREATEDF_ON,
                REFERENCE_CODE = category.REFERENCE_CODE,
                STATUS = category.STATUS,
                UPDATED_ON = category.UPDATED_ON,
                ICON = category.ICON
            };

            if (category.CategoryContentInformation != null)
            {
                CategoryContentInformation categoryContentInformation = new CategoryContentInformation
                {
                    TITLE = category.CategoryContentInformation.TITLE,
                    BODY = category.CategoryContentInformation.BODY,
                    IMAGE_URL = category.CategoryContentInformation.IMAGE_URL
                };
                cvm.CategoryContentInformation = categoryContentInformation;
            }
            if (category.CategorySeoInformation != null)
                {
                    CategorySeoInformation categorySeoInformation = new CategorySeoInformation
                    {
                        META_KEYWORD = category.CategorySeoInformation.META_KEYWORD,
                        META_DESC = category.CategorySeoInformation.META_DESC,
                        META_REFERENCE_CODE = category.CategorySeoInformation.META_REFERENCE_CODE,
                    };
                    cvm.CategorySeoInformation = categorySeoInformation;
                }

            return PartialView("~/Views/Shared/CategoryPartial.cshtml", cvm);
        }

        // POST: CategoryManage/Create
        [HttpPost]
        public ActionResult Create(CategoryViewModel categoryViewModel)
        {
            try
            {
                CATEGORY_CONTENT_INFORMATION categoryContentInformation = new CATEGORY_CONTENT_INFORMATION
                {
                    TITLE = categoryViewModel.CategoryContentInformation.TITLE,
                    BODY = categoryViewModel.CategoryContentInformation.BODY,
                    CREATEDF_ON = DateTime.Now
                };
                CATEGORY_SEO_INFORMATION categorySeoInformation = new CATEGORY_SEO_INFORMATION
                {
                    META_KEYWORD = categoryViewModel.CategorySeoInformation.META_KEYWORD,
                    META_DESC = categoryViewModel.CategorySeoInformation.META_DESC,
                    META_REFERENCE_CODE = categoryViewModel.CategorySeoInformation.META_REFERENCE_CODE,
                    CREATED_ON = DateTime.Now
                };
                CategoryDto category = new CategoryDto
                {
                    CATEGORY_DESC = categoryViewModel.CATEGORY_DESC,
                    CATEGORY_RANK = categoryViewModel.CATEGORY_RANK,
                    REFERENCE_CODE = categoryViewModel.REFERENCE_CODE,
                    CategoryContentInformation = categoryContentInformation,
                    CategorySeoInformation = categorySeoInformation,
                    CATEGORY_ID = categoryViewModel.CATEGORY_ID,
                    CREATEDF_ON = DateTime.Now
                };
                if (categoryViewModel.Image != null && categoryViewModel.Image.ContentLength > 0)
                {
                    var extension = Path.GetExtension(categoryViewModel.Image.FileName);
                    if (extension != null)
                    {
                        string imagetype = extension.ToLower();
                        var rondom = Guid.NewGuid() + imagetype;
                        var path = Path.Combine(ConfigurationManager.AppSettings["CatimageUrl"] + "\\" + rondom);
                        if (imagetype == ".jpg" || imagetype == ".png" || imagetype == ".gif" || imagetype == ".jpeg")
                        {
                            categoryViewModel.Image.SaveAs(path);
                            category.CategoryContentInformation.IMAGE_URL = rondom;
                        }
                    }
                }
                _categoryService.Create(category);
                if (category.CATEGORY_ID == 0)
                    TempData["SuccessMessage"] = "Category successfully added.";
                else
                    TempData["SuccessMessage"] = "Category successfully updated.";
            }
            catch (Exception exception)
            {
                TempData["ErrorMessage"] = exception.Message;
            }
            return RedirectToAction("Index");

        }

        // POST: CategoryManage/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, CategoryDto category)
        {
            try
            {
                _categoryService.UpdateCategory(category);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // POST: CategoryManage/Delete/5
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Delete(int id)
        {
            try
            {
                _categoryService.DeleteCategory(id);
                return Json(CustomStatus.CategoryMessages(CustomStatus.CategoryMessage.Deleted), JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                return Json(CustomStatus.CategoryMessages(CustomStatus.CategoryMessage.Failed), JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        [AllowAnonymous]
        public JsonResult CategorySearch(string categoty)
        {
            try
            {
                IEnumerable<CategoryDto> categoryes = _categoryService.CategorySearch(categoty.Trim());
                IList<Category> cateViewLists = categoryes.Select(data => new Category
                {
                    CATEGORY_ID = data.CATEGORY_ID, CATEGORY_DESC = data.CATEGORY_DESC, CREATEDF_ON = data.CREATEDF_ON, ICON = data.ICON,REFERENCE_CODE = data.REFERENCE_CODE, SEQ_NO = data.SEQ_NO, ProductCount = data.ProductsCount, SubCategories = data.SubCategory.Where(a => a.CATEGORY_ID == data.CATEGORY_ID).Select(s => new SubCategoriesViewModel
                    {
                        REFERENCE_CODE = s.REFERENCE_CODE, SUB_CAT_DESC = s.SUB_CAT_DESC, CATEGORY_ID = s.CATEGORY_ID, DESCRIPTION = s.DESCRIPTION, NUMBER_OF_COLUMNS = s.NUMBER_OF_COLUMNS, SUB_CAT_GROUP = s.SUB_CAT_GROUP, RANK = s.RANK, SUB_CAT_ID = s.SUB_CAT_ID
                    }).ToList()
                }).ToList();
                IEnumerable<Category> cateLists = cateViewLists;
                return Json(cateLists, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(CustomStatus.CategoryMessages(CustomStatus.CategoryMessage.Failed), JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SubCategories(int id = 0)
        {
            if (id != 0)
            {
                Mapper.CreateMap<SubCategoryDTO, SubCategoriesViewModel>();
                IEnumerable<SubCategoryDTO> subcatList = _categoryService.GetSubCategories(id);
                IEnumerable<SubCategoriesViewModel> subcatLists = Mapper.Map<List<SubCategoriesViewModel>>(subcatList);
                SubCategoriesViewModel subCategoriesViewModel = new SubCategoriesViewModel();
                if(subcatList.Count() != 0)
                subCategoriesViewModel.REFERENCE_CODE = "GBSC" + (subcatLists.LastOrDefault().SUB_CAT_ID + 1);
                else
                    subCategoriesViewModel.REFERENCE_CODE = "GBSC10";

                subCategoriesViewModel.CATEGORY_ID = id;
                SubCategoriesList model = new SubCategoriesList
                {
                    subCategoriesList = subcatLists,
                    subCategoriesViewModel = subCategoriesViewModel
                };
                return View(model);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult AddSubCategory(M_GB_SUB_CATEGORY subCategoryModel)
        {
            try
            {
                //if(subCategoryModel.STATUS == null)
               // subCategoryModel.STATUS = (int)CustomStatus.DefaultStatus.Active;
                _categoryService.AddSubCategory(subCategoryModel);
                if (subCategoryModel.SUB_CAT_ID == 0)
                    TempData["SuccessMessage"] = "SubCategory successfully added.";
                else
                    TempData["SuccessMessage"] = "SubCategory successfully updated.";
            }
            catch(Exception exception)
            {
                TempData["ErrorMessage"] = exception.Message;
            }
            return RedirectToAction("SubCategories", new { id = subCategoryModel.CATEGORY_ID });
        }
        [HttpPost]
        [AllowAnonymous]
        public JsonResult DeleteSubCategory(int id)
        {
            try
            {
                _categoryService.DeleteSubCategory(id);
                return Json(CustomStatus.CategoryMessages(CustomStatus.CategoryMessage.Deleted), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(CustomStatus.CategoryMessages(CustomStatus.CategoryMessage.Failed), JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditSubCategory(int id)
        {
            //SubCategoryDTO subCategoryDto = _categoryService.GetSubCategory(id);
            //SubCategoriesViewModel model = new SubCategoriesViewModel
            //{
            //    CATEGORY_ID = Convert.ToInt64(subCategoryDto.CATEGORY_ID),
            //    SEQ_NO = subCategoryDto.SEQ_NO,
            //    REFERENCE_CODE = subCategoryDto.REFERENCE_CODE,
            //    SUB_CAT_DESC = subCategoryDto.SUB_CAT_DESC,
            //    SUB_CAT_GROUP = subCategoryDto.SUB_CAT_GROUP,
            //    RANK = subCategoryDto.RANK,
            //    SUB_CAT_ID = id,
            //    DESCRIPTION = subCategoryDto.DESCRIPTION,
            //    NUMBER_OF_COLUMNS = subCategoryDto.NUMBER_OF_COLUMNS
            //};

            Mapper.CreateMap<SubCategoryDTO, SubCategoriesViewModel>();
            SubCategoryDTO subCategoryDto = _categoryService.GetSubCategory(id);
            SubCategoriesViewModel model = Mapper.Map<SubCategoriesViewModel>(subCategoryDto);

            return PartialView("~/Views/Shared/_subCategoryPartial.cshtml", model);
        }
        public ActionResult EditCatSubCategory(int id = 0,int subCatID = 0)
        {
            if (id != 0)
            {
                Mapper.CreateMap<SubCategoryDTO, SubCategoriesViewModel>();
                IEnumerable<SubCategoryDTO> subcatList = _categoryService.GetSubCategories(id);
                IEnumerable<SubCategoriesViewModel> subcatLists = Mapper.Map<List<SubCategoriesViewModel>>(subcatList);

                SubCategoriesViewModel subCategoriesViewModel = subcatLists.FirstOrDefault(a => a.SUB_CAT_ID == subCatID);
               // subCategoriesViewModel.CATEGORY_ID = id;
                SubCategoriesList model = new SubCategoriesList
                {
                    subCategoriesList = subcatLists,
                    subCategoriesViewModel = subCategoriesViewModel
                };
                return View("SubCategories", model);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
    }
}
