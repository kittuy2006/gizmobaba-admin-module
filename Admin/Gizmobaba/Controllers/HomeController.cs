﻿using AutoMapper;
using Gizmobaba.Models;
using Gizmobaba.Service.Interfaces;
using Gizmobaba.Utility;
//using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Diagnostics;
using Gizmobaba.DataImport;
using System.Dynamic;
using System.Net;
using MailKit.Net.Imap;
using System.Threading;
using MailKit;

namespace Gizmobaba.Controllers
{
    [Authorize]
    public class HomeController : BaseController<HomeController>
    {
        //private static readonly ILog log = LogManager.GetLogger(typeof(HomeController));

        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IDashboardService _dashboardService;
        private readonly IUserService _userService;
        private readonly IPromotionService _promotionService;


        public HomeController(IProductService productService, ICategoryService categoryService, IDashboardService dashboardService, IUserService userService, IPromotionService promotionService)
        {
            _productService = productService;
            _categoryService = categoryService;
            _dashboardService = dashboardService;
            _userService = userService;
            _promotionService = promotionService;
        }

        public ActionResult Index()
        {

            //string smsBody = SMSTempletes.GetSMSBody(SMSTempleteTypes.OTP);
            //smsBody = string.Format(smsBody, "Nagul", "234567");
            //int result = Notifications.SendSMS("9989286563", smsBody);


            if (TempData["User"] != null)
            {
                UserModel userData = (UserModel)TempData["User"];
                TempData["User"] = userData;
                if (userData.ROLE == (int)CustomStatus.UserTypes.Master_Franchise)
                    return RedirectToAction("MasterFranchiseDashboard", "Home");
                else if (userData.ROLE == (int)CustomStatus.UserTypes.Global_Vender)
                    return RedirectToAction("VendorDashboard", "Home");
            }
            else
                return RedirectToAction("Logoff", "Account");




            Mapper.CreateMap<DashboardDto, DashboardViewModel>();
            DashboardDto dashDTO = _dashboardService.Get();
            DashboardViewModel dashModel = Mapper.Map<DashboardViewModel>(dashDTO);
            return View(dashModel);
        }
        public ActionResult VendorDashboard()
        {
            string id = "";
            DashboardDto dashDTO = new DashboardDto();
            if (TempData["User"] != null)
            {
                UserModel userData = (UserModel)TempData["User"];
                TempData["User"] = userData;
                id = userData.USER_ID;
                dashDTO = _dashboardService.GetGlobalVenderData(id);
            }
            else
                return RedirectToAction("Login", "Account");
            Mapper.CreateMap<DashboardDto, DashboardViewModel>();
            DashboardViewModel dashModel = Mapper.Map<DashboardViewModel>(dashDTO);
            return View(dashModel);
        }
        public ActionResult MasterFranchiseDashboard()
        {
            string id = "";
            Mapper.CreateMap<DashboardDto, DashboardViewModel>();
            DashboardDto dashDTO = new DashboardDto();
            if (TempData["User"] != null)
            {
                UserModel userData = (UserModel)TempData["User"];
                TempData["User"] = userData;
                id = userData.USER_ID;
                dashDTO = _dashboardService.GetMasterFranchiseDashboard(id);
            }
            else
                return RedirectToAction("Login", "Account");
            DashboardViewModel dashModel = Mapper.Map<DashboardViewModel>(dashDTO);
            dashModel.UserId = id;
            return View(dashModel);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
       // [Authorize(Roles = "Admin")]
        public ActionResult AddProduct(int id = 0)
        {
            if (id != 0)
            {
                GB_PRODUCT_DETAIL product = _productService.GetProduct(id);
                return View(BindAddProductModel(product));
            }
            else
                return View(BindAddProductModel());
        }

        public ProductViewModel BindAddProductModel(GB_PRODUCT_DETAIL product)
        {
            Mapper.CreateMap<GB_PRODUCT_DETAIL, ProductDetailsDTO>();
            GB_PRODUCT_DETAIL pro = product;
            ProductDetailsDTO productDetails = Mapper.Map<ProductDetailsDTO>(pro);

            Mapper.CreateMap<M_GB_CATEGORY, Category>();
            IEnumerable<M_GB_CATEGORY> catList = _productService.GetCategories();
            IEnumerable<Category> categoryList = Mapper.Map<List<Category>>(catList);

             Mapper.CreateMap<SubCategoryDTO, SubCategoriesViewModel>();
             IEnumerable<SubCategoryDTO> subcatList = _categoryService.GetSubCategories(Convert.ToInt32(product.CATEGORY_ID));
             IEnumerable<SubCategoriesViewModel> subcategoryList = Mapper.Map<List<SubCategoriesViewModel>>(subcatList);


            Mapper.CreateMap<M_GB_SHIPPING, AvailableShippingCodes>();
            IEnumerable<M_GB_SHIPPING> shipList = _productService.GetAvailableShippingCodes();
            IEnumerable<AvailableShippingCodes> shippingCodeList = Mapper.Map<List<AvailableShippingCodes>>(shipList);

            Mapper.CreateMap<GB_PRODUCT_IMAGES, Images>();
            IEnumerable<GB_PRODUCT_IMAGES> productImagesList = product.GB_PRODUCT_IMAGES;
            IEnumerable<Images> productImages = Mapper.Map<List<Images>>(productImagesList);

            Mapper.CreateMap<M_GB_TAX, TaxModel>();
            IEnumerable<M_GB_TAX> taxList = _productService.GetTaxes();
            IEnumerable<TaxModel> taxesList = Mapper.Map<List<TaxModel>>(taxList);

            IEnumerable<M_GB_SHIPPING> shippingCodes = _productService.GetAvailableShippingCodes();

            List<int> SelectedShippingCodes = product.GB_PRODUCT_SHIPPING.Select(z => z.SHIPPING_ID).ToList<int>();

            ProductViewModel model = new ProductViewModel
            {
                categories = categoryList,
                SubCategories = subcategoryList,
                shippingCodes = shippingCodeList,
                details = productDetails,
                productImages = productImages,
                SelectedShippingCodes = SelectedShippingCodes,
                taxes = taxesList
            };
            foreach (GB_PRODUCT_PAYMENT p in product.GB_PRODUCT_PAYMENT)
            {
                switch (p.PAYMENT_ID)
                {
                    case (int)CustomStatus.PaymentTypes.CreditCard:
                        model.CreditCard = true;
                        break;
                    case (int)CustomStatus.PaymentTypes.Online:
                        model.Online = true;
                        break;
                    case (int)CustomStatus.PaymentTypes.COD:
                        model.COD = true;
                        break;
                    case (int)CustomStatus.PaymentTypes.Cheque:
                        model.Cheque = true;
                        break;
                }
            }
            foreach (GB_PRODUCT_DELIVERY p in product.GB_PRODUCT_DELIVERY)
            {
                switch (p.DELIVERY_ID)
                {
                    case (int)CustomStatus.DeliveryTypes.Instore_Pickup:
                        model.Instore_Pickup = true;
                        break;
                    case (int)CustomStatus.DeliveryTypes.Offline:
                        model.Offline = true;
                        break;
                    case (int)CustomStatus.DeliveryTypes.Deli_Online:
                        model.Deli_Online = true;
                        break;
                    case (int)CustomStatus.DeliveryTypes.Ship:
                        model.Ship = true;
                        break;

                }
            }
            if (TempData["User"] != null)
            {
                UserModel userData = (UserModel)TempData["User"];
                TempData["User"] = userData;
                model.details.CREATED_BY = userData.USER_ID;
            }

            model.PreOrder = product.PREORDER ?? false;
            model.BackOrder = product.BACKORDER ?? false;
            return model;
        }
        public ProductViewModel BindAddProductModel()
        {
            Mapper.CreateMap<M_GB_CATEGORY, Category>();
            IEnumerable<M_GB_CATEGORY> catList = _productService.GetCategories();
            IEnumerable<Category> categoryList = Mapper.Map<List<Category>>(catList);

            Mapper.CreateMap<M_GB_SHIPPING, AvailableShippingCodes>();
            IEnumerable<M_GB_SHIPPING> shipList = _productService.GetAvailableShippingCodes();
            IEnumerable<AvailableShippingCodes> shippingCodeList = Mapper.Map<List<AvailableShippingCodes>>(shipList);

            Mapper.CreateMap<M_GB_TAX, TaxModel>();
            IEnumerable<M_GB_TAX> taxList = _productService.GetTaxes();
            IEnumerable<TaxModel> taxesList = Mapper.Map<List<TaxModel>>(taxList);

            IEnumerable<M_GB_SHIPPING> shippingCodes = _productService.GetAvailableShippingCodes();
            ProductDetailsDTO detail = new ProductDetailsDTO();
            string sku = ConfigurationManager.AppSettings["ProSkuCode"].ToString();
            sku =  sku + _productService.GetProSku();
            if (_productService.CheckSku(sku) != 0 )
            {
                for (int x = 0; x != 1; x++)
                {
                    sku = sku + _productService.GetProSku();
                    int skuCount = _productService.CheckSku(sku);
                    x = skuCount;
                }
            }
           
           detail.PRODUCT_SKU = detail.SKUCODE =  sku;
            ProductViewModel model = new ProductViewModel
            {
                categories = categoryList,
                shippingCodes = shippingCodeList,
                details = detail,
                taxes = taxesList
            };
            if (TempData["User"] != null)
            {
                UserModel userData = (UserModel)TempData["User"];
                TempData["User"] = userData;
                detail.CREATED_BY = userData.USER_ID;
            }
            return model;
        }

        //[Authorize(Roles = "Admin")]
        [HttpPost, ValidateInput(false)]
        public ActionResult AddProduct(ProductViewModel obj, string command)
        {
            
            Mapper.CreateMap<ProductDetailsDTO, GB_PRODUCT_DETAIL>();
            ProductDetailsDTO pro = obj.details;
            GB_PRODUCT_DETAIL product = Mapper.Map<GB_PRODUCT_DETAIL>(pro);
            List<GB_PRODUCT_SHIPPING> selectedShippingCodes = new List<GB_PRODUCT_SHIPPING>();
            List<Images> images = new List<Images>();
            List<ProductPayment> payments = new List<ProductPayment>();
            List<ProductDelivery> delivery = new List<ProductDelivery>();
            if (obj.details.PRODUCT_ID == 0)
            {
                if (obj.sourceImage == null)
                {
                    TempData["Alert"] = "Source Images is required";
                    return RedirectToAction("AddProduct", "Home");
                }
                if (obj.gifImage == null)
                {
                    TempData["Alert"] = "Gif Images is required";
                    return RedirectToAction("AddProduct", "Home");
                }
                if (obj.sourceImage.ContentLength > 0)
                {
                    var extension = Path.GetExtension(obj.sourceImage.FileName);
                    if (extension != null)
                    {
                        string imagetype = extension.ToLower();
                        var rondom = Guid.NewGuid() + imagetype;
                        var path = Path.Combine(ConfigurationManager.AppSettings["ProimageUrl"] + "\\" + rondom);
                        Images sourceImg = new Images();
                        if (imagetype == ".jpg" || imagetype == ".png" || imagetype == ".gif" || imagetype == ".jpeg")
                        {
                            product.PHOTO_URL = rondom;
                            obj.sourceImage.SaveAs(path);

                            sourceImg.ALTERNATE_NAME = "Sourec image";
                            sourceImg.IMAGE_URL = rondom;
                            sourceImg.DISPLAY_IMAGE = (int)CustomStatus.ImageTypes.Source;
                            //   obj.sourceImage.SaveAs(path);
                            images.Add(sourceImg);
                        }
                    }
                }
                if (obj.gifImage.ContentLength > 0)
                {
                    Images gifImg = new Images();
                    var extension = Path.GetExtension(obj.gifImage.FileName);
                    if (extension != null)
                    {
                        string imagetype = extension.ToLower();
                        var rondom = Guid.NewGuid() + imagetype;
                        var path = Path.Combine(ConfigurationManager.AppSettings["ProimageUrl"] + "\\" + rondom);
                        if (imagetype == ".jpg" || imagetype == ".png" || imagetype == ".gif" || imagetype == ".jpeg")
                        {
                            gifImg.ALTERNATE_NAME = "Gif image";
                            gifImg.IMAGE_URL = rondom;
                            gifImg.DISPLAY_IMAGE = (int)CustomStatus.ImageTypes.Gif;
                            obj.gifImage.SaveAs(path);
                            images.Add(gifImg);
                        }
                    }
                }
                if (obj.image1 != null && obj.image1.ContentLength > 0)
                {
                    Images image1 = new Images();
                    var extension = Path.GetExtension(obj.image1.FileName);
                    if (extension != null)
                    {
                        string imagetype = extension.ToLower();
                        var rondom = Guid.NewGuid() + imagetype;
                        var path = Path.Combine(ConfigurationManager.AppSettings["ProimageUrl"] + "\\" + rondom);
                        if (imagetype == ".jpg" || imagetype == ".png" || imagetype == ".gif" || imagetype == ".jpeg")
                        {
                            image1.ALTERNATE_NAME = "image1";
                            image1.IMAGE_URL = rondom;
                            image1.DISPLAY_IMAGE = (int)CustomStatus.ImageTypes.Gallery;
                            obj.image1.SaveAs(path);
                            images.Add(image1);
                        }
                    }
                }
                if (obj.image2 != null && obj.image2.ContentLength > 0)
                {
                    Images image2 = new Images();
                    string imagetype = Path.GetExtension(obj.image2.FileName).ToLower();
                    var rondom = Guid.NewGuid() + imagetype;
                    var path = Path.Combine(ConfigurationManager.AppSettings["ProimageUrl"].ToString() + "\\" + rondom);
                    if (imagetype == ".jpg" || imagetype == ".png" || imagetype == ".gif" || imagetype == ".jpeg")
                    {
                        image2.ALTERNATE_NAME = "image2";
                        image2.IMAGE_URL = rondom;
                        image2.DISPLAY_IMAGE = (int)CustomStatus.ImageTypes.Gallery;
                        obj.image2.SaveAs(path);
                        images.Add(image2);
                    }
                }
                if (obj.image3 != null && obj.image3.ContentLength > 0)
                {
                    Images image3 = new Images();
                    string imagetype = Path.GetExtension(obj.image3.FileName).ToLower();
                    var rondom = Guid.NewGuid() + imagetype;
                    var path = Path.Combine(ConfigurationManager.AppSettings["ProimageUrl"] + "\\" + rondom);
                    if (imagetype == ".jpg" || imagetype == ".png" || imagetype == ".gif" || imagetype == ".jpeg")
                    {
                        image3.ALTERNATE_NAME = "image3";
                        image3.IMAGE_URL = rondom;
                        image3.DISPLAY_IMAGE = (int)CustomStatus.ImageTypes.Gallery;
                        Debug.Assert(obj.image2 != null, "obj.image2 != null");
                        obj.image2.SaveAs(path);
                        images.Add(image3);
                    }
                }
                if (obj.image4 != null && obj.image4.ContentLength > 0)
                {
                    Images image4 = new Images();
                    string imagetype = Path.GetExtension(obj.image4.FileName).ToLower();
                    var rondom = Guid.NewGuid() + imagetype;
                    var path = Path.Combine(ConfigurationManager.AppSettings["ProimageUrl"] + "\\" + rondom);
                    if (imagetype == ".jpg" || imagetype == ".png" || imagetype == ".gif" || imagetype == ".jpeg")
                    {
                        image4.ALTERNATE_NAME = "image4";
                        image4.IMAGE_URL = rondom;
                        image4.DISPLAY_IMAGE = (int)CustomStatus.ImageTypes.Gallery;
                        obj.image4.SaveAs(path);
                        images.Add(image4);
                    }
                }
                if (obj.image5 != null && obj.image5.ContentLength > 0)
                {
                    Images image5 = new Images();
                    var extension = Path.GetExtension(obj.image5.FileName);
                    if (extension != null)
                    {
                        string imagetype = extension.ToLower();
                        var rondom = Guid.NewGuid() + imagetype;
                        var path = Path.Combine(ConfigurationManager.AppSettings["ProimageUrl"] + "\\" + rondom);
                        if (imagetype == ".jpg" || imagetype == ".png" || imagetype == ".gif" || imagetype == ".jpeg")
                        {
                            image5.ALTERNATE_NAME = "image5";
                            image5.IMAGE_URL = rondom;
                            image5.DISPLAY_IMAGE = (int)CustomStatus.ImageTypes.Gallery;
                            obj.image5.SaveAs(path);
                            images.Add(image5);
                        }
                    }
                }
                foreach (Images i in images)
                {
                    product.GB_PRODUCT_IMAGES.Add(new GB_PRODUCT_IMAGES
                    {
                        ALTERNATE_NAME = i.ALTERNATE_NAME,
                        DISPLAY_IMAGE = i.DISPLAY_IMAGE,
                        IMAGE_URL = i.IMAGE_URL
                    });
                }
            }
          
            if (obj.CreditCard)
            {
                ProductPayment proPay = new ProductPayment { PAYMENT_ID = (int)CustomStatus.PaymentTypes.CreditCard };
                payments.Add(proPay);
            }
            if (obj.Online)
            {
                ProductPayment proPay = new ProductPayment { PAYMENT_ID = (int)CustomStatus.PaymentTypes.Online };
                payments.Add(proPay);
            }
            if (obj.Cheque)
            {
                ProductPayment proPay = new ProductPayment { PAYMENT_ID = (int)CustomStatus.PaymentTypes.Cheque };
                payments.Add(proPay);
            }
            if (obj.COD)
            {
                ProductPayment proPay = new ProductPayment { PAYMENT_ID = (int)CustomStatus.PaymentTypes.COD };
                payments.Add(proPay);
            }
            if (obj.Ship)
            {
                ProductDelivery proDel = new ProductDelivery { DELIVERY_ID = (int)CustomStatus.DeliveryTypes.Ship };
                delivery.Add(proDel);
            }
            if (obj.Offline)
            {
                ProductDelivery proDel = new ProductDelivery { DELIVERY_ID = (int)CustomStatus.DeliveryTypes.Offline };
                delivery.Add(proDel);
            }
            if (obj.Deli_Online)
            {
                ProductDelivery proDel = new ProductDelivery { DELIVERY_ID = (int)CustomStatus.DeliveryTypes.Deli_Online };
                delivery.Add(proDel);
            }
            if (obj.Instore_Pickup)
            {
                ProductDelivery proDel = new ProductDelivery
                {
                    DELIVERY_ID = (int)CustomStatus.DeliveryTypes.Instore_Pickup
                };
                delivery.Add(proDel);
            }
            if (obj.SelectedShippingCodes != null)
                foreach (int i in obj.SelectedShippingCodes)
                {
                    product.GB_PRODUCT_SHIPPING.Add(new GB_PRODUCT_SHIPPING
                    {
                        SHIPPING_ID = i
                    });
                }
            foreach (ProductPayment i in payments)
            {
                product.GB_PRODUCT_PAYMENT.Add(new GB_PRODUCT_PAYMENT
                {
                    PAYMENT_ID = i.PAYMENT_ID
                });
            }
            foreach (ProductDelivery i in delivery)
            {
                product.GB_PRODUCT_DELIVERY.Add(new GB_PRODUCT_DELIVERY
                {
                    DELIVERY_ID = i.DELIVERY_ID
                });
            }
            if (!string.IsNullOrEmpty(obj.details.CREATED_BY) && obj.details.CREATED_BY == "1")
                product.STATUS = (int)CustomStatus.ProductStatus.Active;
            else
                product.STATUS = (int)CustomStatus.ProductStatus.DeActive;
            product.ISAVAILABLE = (int)CustomStatus.ProductStatus.ISAVAILABLE;
            product.CREATED_ON = DateTime.Now;
            product.UPDATED_ON = DateTime.Now;
            product.PREORDER = obj.PreOrder;
            product.BACKORDER = obj.BackOrder;
            product.QUANTITYS = obj.details.RESERVE_QUANTITY;
            if(obj.details.ACTIVATION_DATE == null)
                obj.details.ACTIVATION_DATE = DateTime.Now;
            long addProduct = _productService.AddProduct(product);
            if (command == "Save")
                return View(BindAddProductModel());
            else if (command == "Save&List")
                return RedirectToAction("InventorySnapshot");
            else if (command == "Save&New")
                return RedirectToAction("AddProduct");
            else if (command == "Save&Next")
                return RedirectToAction("ProductAdditionalinfo",new {id = addProduct});
            return View();
        }
        [Authorize(Roles = "Admin")]
        public ActionResult InventorySnapshot()
        {
            Mapper.CreateMap<GB_PRODUCT_DETAIL, ProductDetailsDTO>();
            IEnumerable<GB_PRODUCT_DETAIL> proList = _productService.GetAll();
            IEnumerable<ProductDetailsDTO> productList = Mapper.Map<List<ProductDetailsDTO>>(proList);

            Mapper.CreateMap<TagsDto, ProductTagsViewModel>();
            IEnumerable<TagsDto> proTagsList = _productService.GetProductTags();
            IEnumerable<ProductTagsViewModel> productTagList = Mapper.Map<List<ProductTagsViewModel>>(proTagsList);
            if (proList != null)
                foreach (GB_PRODUCT_DETAIL item in proList)
                {
                    if (item.GB_PRODUCT_TAG.Count() != 0)
                    {
                        foreach (ProductDetailsDTO list  in productList)
                        {
                            if (item.PRODUCT_ID == list.PRODUCT_ID)
                            {
                                Mapper.CreateMap<GB_PRODUCT_TAG, ProductTagsViewModel>();
                                IEnumerable<GB_PRODUCT_TAG> proTagList = item.GB_PRODUCT_TAG;
                                IEnumerable<ProductTagsViewModel> productTagsList = Mapper.Map<List<ProductTagsViewModel>>(proTagList);
                                list.ProductTagsList = productTagsList;
                            }
                        }
                    }
              
                }

            InventorySnapshotViewModel model = new InventorySnapshotViewModel
            {
                productsList = productList,
                tagsList = productTagList
            };
            return View(model);
        }
        [AllowAnonymous]
        public JsonResult DeleteProduct(int id)
        {
            _productService.Delete(id);
            return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Success), JsonRequestBehavior.AllowGet);
        }
        [AllowAnonymous]
        public JsonResult ActivateProduct(int id)
        {
            _productService.Activate(id);
            return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Success), JsonRequestBehavior.AllowGet);
        }
        [AllowAnonymous]
        public JsonResult DeActivateProduct(int id)
        {
            _productService.DeActivate(id);
            return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Success), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetProductSearch(string productkey)
        {
            IEnumerable<ProductDTO> data = _productService.GetProductSearch(productkey);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [AllowAnonymous]
        public JsonResult AddTagToProduct(string[] tagIdList, string[] productIdList)
        {
          //  IEnumerable<ProductDTO> data = _productService.GetProductSearch(productkey);
            if (tagIdList != null && productIdList != null)
            {
                foreach (string tagId in tagIdList)
                {
                    foreach (string i in productIdList)
                    {
                        GB_PRODUCT_TAG data = new GB_PRODUCT_TAG
                        {
                            PRODUCT_ID = Convert.ToInt32(i),
                            TAG_ID = Convert.ToInt32(tagId),
                            STATUS = (int)CustomStatus.DefaultStatus.Active
                        };
                        _productService.AddTagToProduct(data);
                    }
                }
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }
        [AllowAnonymous]        
        public JsonResult DeleteProductList(string[] productIdList)
        {
            try
            {
                if (productIdList == null)
                    return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Empty),
                        JsonRequestBehavior.DenyGet);
                else
                {
                    foreach (string i in productIdList)
                    {
                        _productService.Delete(Convert.ToInt32(i));
                    }
                    return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Success),
                        JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception)
            {
                return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Failed), JsonRequestBehavior.DenyGet);
            }

        }
        [AllowAnonymous]
        public JsonResult DeactivateProductList(string[] productIdList)
        {
            try
            {
                if (productIdList != null)
                {
                    foreach (string i in productIdList)
                    {
                        _productService.DeactivateProduct(Convert.ToInt32(i));
                    }
                    return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Success),
                        JsonRequestBehavior.DenyGet);
                }
                return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Empty),
                    JsonRequestBehavior.DenyGet);
            }
            catch (Exception)
            {
                return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Failed), JsonRequestBehavior.DenyGet);
            }

        }
        //[Authorize(Roles = "Admin")]
        public ActionResult ProductImport()
        {
            BulkProductUploadModel model = new BulkProductUploadModel();
            Mapper.CreateMap<GB_BULK_UPLOAD, BulkProductUploadview>();
            IEnumerable<GB_BULK_UPLOAD> bulkUploadList = _productService.GetBulkuploadList();
            IEnumerable<BulkProductUploadview> bulkmodel = Mapper.Map<List<BulkProductUploadview>>(bulkUploadList);
            model.bulkProductUploadview = bulkmodel;
            return View(model);
        }
        //[Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult ProductImport(HttpPostedFileBase file)
        {
            BulkProductUploadModel model = new BulkProductUploadModel();
            Mapper.CreateMap<GB_BULK_UPLOAD, BulkProductUploadview>();
            IEnumerable<GB_BULK_UPLOAD> bulkUploadList = _productService.GetBulkuploadList();
            IEnumerable<BulkProductUploadview> bulkmodel = Mapper.Map<List<BulkProductUploadview>>(bulkUploadList);
            model.bulkProductUploadview = bulkmodel;
            try
            {
                if (file != null)
                {
                    if (file.FileName.EndsWith("xls") || file.FileName.EndsWith("xlsx"))
                    {
                        string error = "";
                        var imported =
                         new DataImportHandler(_productService).Process(new ImportRequest() { ImportStream = file.InputStream });
                        foreach(var item in imported.Results.Where(a=>a.STATUS == (int)CustomStatus.ProductStatus.DeActive))
                        {
                            error += item.PRODUCT_NAME + " && ";
                        }
                        if(imported.Results.Count(a=>a.STATUS == (int)CustomStatus.ProductStatus.Active) != 0)
                        _productService.AddProductList(imported.Results.Where(a=>a.STATUS == 1).ToList());
                        TempData["Message"] = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Uploaded);
                        if (imported.Results.Count(a => a.STATUS == (int)CustomStatus.ProductStatus.DeActive) != 0)
                            TempData["Message"] = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Uploaded) + " but " + error + "Products not uploaded";
                        GB_BULK_UPLOAD bulkUpload = new GB_BULK_UPLOAD
                        {
                            CREATED_ON = DateTime.Now,
                            FILE_NAME = file.FileName,
                            UPLOAD_STATUS = 1,
                            TOTAL_RECORDS = imported.Results.Count(),
                            UPDATED_RECORDS = imported.Results.Where(a => a.STATUS == 1).Count(),
                            DESCRIPTION = ViewBag.Message,
                            STATUS = (int)CustomStatus.DefaultStatus.Active
                        };
                        _productService.AddBulkProUploadList(bulkUpload);
                        var path = Path.Combine(ConfigurationManager.AppSettings["BulkImage"] + "\\" + file.FileName);
                        file.SaveAs(path);
                        return View(model);
                    }
                    else
                    {
                        TempData["Error"] = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Filetype);
                        return View(model);
                    }
                }
                else
                {
                    ViewBag.Error = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Excelfile);
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.FaileAgian);
                TempData["Error"] = ex.Message;
                return View(model);
            }
        }

        //[Authorize(Roles = "Admin")]
        public ActionResult DownloadProductSheet()
        {
            // download sample rec
            var proList = new List<ProductExportModel>();
            //ProductExportModel pro = new ProductExportModel();
            //pro.PRODUCT_NAME = "Gizmobaba";
            //pro.CATEGORY_ID = "1";
            //pro.SUB_CAT_ID = "1";
            //pro.PRODUCT_CAPTION = "Gizmobaba CAPTION";
            //pro.PRODUCT_SKU = "GB7";
            //pro.PRODUCT_DESC = "This is absolutely the latest gizmo to have hit the town. Even Rajni wants to buy it but does not know where to get it! What a delight for smokers. Now you can store all your cigarettes in this fancy case. Push a button and a cigarette will pop out. And then the case doubles up as a lighter and lights it.";
            //pro.BRAND = "Gizmobaba";
            //pro.NET_PRICE = "1300";
            //pro.MRP = "1349";
            //pro.GROSS_PRICE = "1299";
            //pro.GIZMOBABA_PRICE = "1299";
            //pro.QUANTITYS = "20";
            //pro.ACTIVATION_DATE =date "MM/DD/YYYY";
            //pro.DEACTIVATION_DATE = "MM/DD/YYYY";
            //pro.DELIVERY_TIME = "4";
            //pro.INVENTORY = "Test INVENTORY ";
            //pro.RESERVE_QUANTITY = "20";
            //pro.PKG_QUANTITY = "18";

            //pro.Product_Image_URL = "http://s932.photobucket.com/user/teamgizmobaba/media/HD%20PIX/GB7_zpsrhhotqr6.jpg.html?sort=3&o=329";
            //pro.GIF_IMAGE = "http://intermartindia.com/gbimg/gb7gif.gif";
            //pro.VIDEO_URL = "https://youtu.be/x6GkgiJa64U";

            //proList.Add(pro);
            //  var columns = new List<string> { "Brand_Name", "CATEGORY_ID", "SKU", "PRODUCT_NAME", "PRODUCT_CAPTION", "QUANTITY", "Product_Description_Bullet_1", "Product_Image_HD_URL", "gif", "video", "MRP", "WEB_PRICE", "GROSS_PRICE", "GIZMOBABA_PRICE", "ACTIVATION_DATE", "DEACTIVATION_DATE", "Delivery_Time" }.ToArray();
            //var columns = new List<string> { "PRODUCT_NAME", "PRODUCT_SKU", "CATEGORY_NAME", "SUB_CAT_NAME", "SHORT_DESCRIPITION", "LONG_DESCRIPITION", "BRAND", "MRP", "WEB_PRICE", "COST_PRICE", "TOKEN_PRICE", "QUANTITYS", "ACTIVATION_DATE", "DEACTIVATION_DATE", "DELIVERY_TIME", "INVENTORY", "RESERVE_QUANTITY", "PKG_QUANTITY", "Product_Image_URL", "GIF_IMAGE", "IMAGE_1", "IMAGE_2", "IMAGE_3", "VIDEO_URL" }.ToArray();
            var columns = new List<string> { "PRODUCT NAME *", "PRODUCT SKU", "CATEGORY NAME *", "SUB CAT NAME", "SHORT DESCRIPITION *", "LONG DESCRIPITION", "BRAND", "MRP *", "WEB PRICE *", "COST PRICE *", "TOKEN PRICE *", "QUANTITYS", "ACTIVATION DATE", "DEACTIVATION DATE", "DELIVERY TIME", "INVENTORY *", "RESERVE QUANTITY", "PKG QUANTITY", "Product Image URL *", "GIF IMAGE *", "IMAGE_1", "IMAGE_2", "IMAGE_3", "VIDEO URL" }.ToArray();
            var stream = new DataExportHandler<ProductExportModel>().ExportExcel(proList.ToList(), columns);
            stream.Position = 0;
            return File(stream, "application/vnd.ms-excel", "Products.xlsx");
        }
        [HttpGet]
        [AllowAnonymous]        
        public JsonResult GetSubCategories(int categoryId)
        {
            IEnumerable<SubCategoryDTO> data = _categoryService.GetSubCategories(categoryId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddBanner(BannerModel banner)
        {
            try
            {
                M_GB_BANNERS mBanner = new M_GB_BANNERS();
                if (banner.Image != null && banner.Image.ContentLength > 0)
                {
                    string imagetype = Path.GetExtension(banner.Image.FileName).ToLower();
                    var rondom = Guid.NewGuid() + imagetype;
                    var path = Path.Combine(ConfigurationManager.AppSettings["BannerUrl"] + "\\" + rondom);
                    if (imagetype == ".jpg" || imagetype == ".png" || imagetype == ".gif" || imagetype == ".jpeg")
                    {
                        mBanner.BANNER_NAME = banner.BANNER_NAME;
                        if (mBanner.BANNER_NAME == "Predefined Banner")
                            mBanner.BANNER_TYPE_ID = (int)CustomStatus.BannerTypes.Predefined;
                        else
                            mBanner.BANNER_TYPE_ID = (int)CustomStatus.BannerTypes.Home;
                        mBanner.BANNER_URL = rondom;
                        mBanner.BANNER_DEC = banner.BANNER_DEC;
                        mBanner.STATUS = (int)CustomStatus.BannerStatus.DeActive;
                        mBanner.CAREATED_ON = banner.CAREATED_ON;
                        if (banner.CAREATED_ON == default(DateTime))
                            mBanner.CAREATED_ON = DateTime.Now;
                        mBanner.BANNER_EXPIRY_DATE = banner.BANNER_EXPIRY_DATE;
                        banner.Image.SaveAs(path);
                        _productService.AddBanner(mBanner);
                        TempData["Message"]  = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Uploaded);
                        if (mBanner.BANNER_NAME == "Predefined Banner")
                            return RedirectToAction("Predefinedbanners");
                        return RedirectToAction("Banners");
                    }
                    else
                    {
                        TempData["Message"] = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Filetype);
                        if (mBanner.BANNER_NAME == "Predefined Banner")
                            return RedirectToAction("Predefinedbanners");
                        return View("Banners");
                    }
                }
                else
                {
                    TempData["Message"] = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Excelfile);
                    if (mBanner.BANNER_NAME == "Predefined Banner")
                        return RedirectToAction("Predefinedbanners");
                    return View("Banners");
                }
            }
            catch (Exception)
            {
                ViewBag.ErrorMsg = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.FaileAgian);
                return View("Banners");
            }
        }
        public ActionResult Banners()
        {
            Mapper.CreateMap<Banner, BannerModel>();
            IEnumerable<Banner> bannerList = _productService.GetBanners();
            IEnumerable<BannerModel> bannerCodeList = Mapper.Map<List<BannerModel>>(bannerList);
            BannerListModel model = new BannerListModel
            {
                bannerList = bannerCodeList
            };
            // IEnumerable<Banner> banner = _productService.GetBanners();

            return View(model);
        }
        [AllowAnonymous]
        public JsonResult DeleteBanner(int id)
        {
            _productService.DeleteBanner(id);
            return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Success), JsonRequestBehavior.DenyGet);
        }
        [AllowAnonymous]
        public JsonResult SetBanner(int id)
        {
            _productService.SetBanner(id);
            return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Success), JsonRequestBehavior.DenyGet);
        }
        //[Authorize(Roles = "Admin")]
        public ActionResult ProductImages(int id = 0)
        {
            if (id <= 0) return RedirectToAction("AddProduct");
            Mapper.CreateMap<ProductImagesDto, Images>();
            IEnumerable<ProductImagesDto> imagesofProduct = _productService.GetProductImages(id);
            IEnumerable<Images> imagesList = Mapper.Map<List<Images>>(imagesofProduct);
            if (!imagesList.Any())
            {
                ViewBag.Error = "No Images";
            }
            ViewBag.Id = id;
            return View(imagesList);
        }

        [HttpPost]
        public ActionResult UpdateProductImage(GB_PRODUCT_IMAGES productimages,HttpPostedFileBase imFileBase)
        {
            try
            {
                if (imFileBase != null && imFileBase.ContentLength > 0)
                {
                    string imagetype = Path.GetExtension(imFileBase.FileName).ToLower();
                    var rondom = Guid.NewGuid() + imagetype;
                    var path = Path.Combine(ConfigurationManager.AppSettings["ProimageUrl"] + "\\" + rondom);
                    if (imagetype == ".jpg" || imagetype == ".png" || imagetype == ".gif" || imagetype == ".jpeg")
                    {
                        productimages.IMAGE_URL = rondom;
                        imFileBase.SaveAs(path);
                        _productService.EditImage(productimages);
                        return RedirectToAction("ProductImages",new {Id= productimages.PRODUCT_ID});
                    }
                    else
                    {
                        ViewBag.ErrorMsg = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Filetype);
                        return View("ProductImages");
                    }
                }
                else
                {
                    ViewBag.ErrorMsg = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Excelfile);
                    return View("ProductImages");
                }
            }
            catch (Exception)
            {
                ViewBag.ErrorMsg = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.FaileAgian);
                return View("ProductImages");
            }

        }
        [HttpPost]
        public ActionResult AddProductImage(GB_PRODUCT_IMAGES productimages, HttpPostedFileBase imFileBase)
        {
            try
            {
                if (imFileBase != null && imFileBase.ContentLength > 0)
                {
                    string imagetype = Path.GetExtension(imFileBase.FileName).ToLower();
                    var rondom = Guid.NewGuid() + imagetype;
                    var path = Path.Combine(ConfigurationManager.AppSettings["ProimageUrl"] + "\\" + rondom);
                    if (imagetype == ".jpg" || imagetype == ".png" || imagetype == ".gif" || imagetype == ".jpeg")
                    {
                        productimages.IMAGE_URL = rondom;
                        imFileBase.SaveAs(path);
                       _productService.AddProductImage(productimages);
                        return RedirectToAction("ProductImages", new { Id = productimages.PRODUCT_ID });
                    }
                    else
                    {
                        ViewBag.ErrorMsg = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Filetype);
                        return View("ProductImages");
                    }
                }
                else
                {
                    ViewBag.ErrorMsg = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Excelfile);
                    return View("ProductImages");
                }
            }
            catch (Exception)
            {
                ViewBag.ErrorMsg = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.FaileAgian);
                return View("ProductImages");
            }

        }
        [AllowAnonymous]
        public JsonResult DeleteProductImage(string id)
        {
            if (id != null)
                 _productService.DeleteImage(Convert.ToInt32(id));
            return Json(id, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MultipleImages(HttpPostedFileBase[] files, string type)
        {
            try
            {
                /*Lopp for multiple files*/
                if (files != null)
                {
                    foreach (HttpPostedFileBase file in files)
                    {
                        string imagetype = Path.GetExtension(file.FileName).ToLower();
                        if (imagetype == ".jpg" || imagetype == ".png" || imagetype == ".gif" || imagetype == ".jpeg")
                        {
                            /*Geting the file name*/
                            string filename = Path.GetFileName(file.FileName);
                            /*Saving the file in server folder*/
                            var path = Path.Combine(ConfigurationManager.AppSettings["BulkImage"] + "\\" + filename);
                            file.SaveAs(path);
                            TempData["Message"]  = "Images Uploaded successfully.";
                        }
                        else
                            TempData["Error"] = "Images Types .jpg .png .gif .jpeg only";
                    }
                }
                else
                {
                    if (type == "GlobalDeals")
                        return RedirectToAction("GlobalDeals");
                    else
                        return RedirectToAction("ProductImport");
                }
            }
            catch
            {
                ViewBag.Error = "Error while uploading the Images.";
            }
            if (type == "GlobalDeals")
                return RedirectToAction("GlobalDeals");
            else
                return RedirectToAction("ProductImport");
        }

        public ActionResult ProductAdditionalinfo(int id = 0)
        {
            if (id > 0)
            {
                ProductDTO p = _productService.GetProductInfomation(id);
                ProductInfomationModel model = new ProductInfomationModel();
                if (p != null)
                {

                    Mapper.CreateMap<ProductDTO, ProductInfomationModel>();
                    model = Mapper.Map<ProductInfomationModel>(p);

                    // model = new ProductInfomationModel
                    //{
                    //    PRODUCT_ID = p.PRODUCT_ID,
                    //    PRODUCT_INFO_ID = p.PRODUCT_INFO_ID,
                    //    PRO_OFFER_DESC = p.PRO_OFFER_DESC,
                    //    PRODUCT_NOTE = p.PRODUCT_NOTE,
                    //    PRO_OFFER_IMAGE = p.PRO_OFFER_IMAGE,
                    //    BARCODE = p.BARCODE,
                    //    CATALOG_CODE = p.CATALOG_CODE,
                    //    MODELNUMBER = p.MODELNUMBER,
                    //    //VENDOR_ID = p.VENDOR_ID,
                    //    STANDARD_PRODUCT_CODE = p.STANDARD_PRODUCT_CODE,
                    //    STANDARD_PRODUCT_TYPE = p.STANDARD_PRODUCT_TYPE,
                    //    MPN = p.MPN,
                    //    PAGE_TITLE = p.PAGE_TITLE,
                    //    KEYWORD = p.KEYWORD,
                    //    SEO_DESC = p.SEO_DESC,
                    //    URL_KEY_REWRITER = p.URL_KEY_REWRITER,
                    //    SMALL_IMAGEALT = p.SMALL_IMAGEALT,
                    //    LARGE_IMAGEALT = p.LARGE_IMAGEALT,
                    //    CONDITION_ID = p.CONDITION_ID,
                    //    WARRANTY_ID = p.WARRANTY_ID,
                    //    CUSTOM_FIELDS_ID = p.CUSTOM_FIELDS_ID,
                    //    SECTION_ID = p.SECTION_ID,
                    //};
                }
                else
                {
                    model = new ProductInfomationModel
                    {
                        PRODUCT_ID = id,
                    };
                }
                ViewBag.Vendors = new SelectList(_productService.GetVendors().ToList(), "VEN_ID", "NAME");
                ViewBag.Condition = new SelectList(_productService.GetCondition().ToList(), "CONDITION_ID",
                    "CONDITION_DESC");
                ViewBag.Warranty = new SelectList(_productService.GetWarranty().ToList(), "WARRANTY_ID", "WARRANTY_DESC");
                ViewBag.CustomFields = new SelectList(_productService.GetCustomFields().ToList(), "CUSTOM_FIELDS_ID",
                    "CUSTOM_FIELDS_DESC");
                ViewBag.Section = new SelectList(_productService.GetSection().ToList(), "SECTION_ID", "SECTION_DESC");

                return View(model);
            }
            return RedirectToAction("AddProduct");
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult ProductAdditionalinfo(GB_PRODUCT_INFORMATION productInformation, string command)
        {
            if (productInformation != null)
            {
              int productId =  _productService.AddProductAdditionalinfo(productInformation);
                if (command == "Save")
                    return RedirectToAction("ProductAdditionalinfo", new {id = productId});
                else if (command == "Save&List")
                    return RedirectToAction("InventorySnapshot");
                else if (command == "Save&Next")
                    return RedirectToAction("ProductImages", new {id = productId});
            }
            return View();
        }
        public ActionResult Product(int id = 0)
        {
            if (id != 0)
            {
                GB_PRODUCT_DETAIL product = _productService.GetProduct(id);
                return View(BindAddProductModel(product));
            }
            else
                return View(BindAddProductModel());
        }
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult ProductTags(int id = 0)
        {
            Mapper.CreateMap<TagsDto, ProductTagsViewModel>();
            IEnumerable<TagsDto> tagsList = _productService.GetAllProductTags();
            IEnumerable<ProductTagsViewModel> productTagsViewModels = Mapper.Map<List<ProductTagsViewModel>>(tagsList);

            ProductTagsList model = new ProductTagsList
            {
                productTagsList = productTagsViewModels,
                productTagsViewModel = new ProductTagsViewModel()
            };
            return View(model);
        }
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult EditProductTags(int id = 0)
        {
            TagsDto productTag = _productService.GetProductTags(tagId: id);
            ProductTagsViewModel productTagsViewModel = new ProductTagsViewModel
            {
                TAG_ID = productTag.TAG_ID,
                TAG_DESC = productTag.TAG_DESC,
                STATUS = productTag.STATUS
            };
            return PartialView("~/Views/Shared/_masterProductTags.cshtml",  productTagsViewModel);

        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult AddProductTag(M_GB_TAGS tagsDto)
        {
            try
            {
                tagsDto.STATUS = (int)CustomStatus.ProductStatus.Active;
                _productService.AddProductTag(tagsDto);
                if (tagsDto.TAG_ID == 0)
                    TempData["SuccessMessage"] = "Master tag successfully added";
                else
                    TempData["SuccessMessage"] = "Master tag successfully updated";
            }
            catch (Exception exception)
            {
                TempData["ErrorMessage"] = exception.Message;
            }
            return RedirectToAction("ProductTags");
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [AllowAnonymous]
        public JsonResult DeleteProductTag(string id)
        {
            if (id != null)
                _productService.DeleteProductTag(Convert.ToInt32(id));
            return Json(id, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult Predefinedbanners()
        {
            Mapper.CreateMap<Banner, BannerModel>();
            IEnumerable<Banner> bannerList = _productService.GetPredefinedBanners();
            IEnumerable<BannerModel> bannerCodeList = Mapper.Map<List<BannerModel>>(bannerList);
            BannerListModel model = new BannerListModel
            {
                bannerList = bannerCodeList
            };
            return View(model);

        }
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult Setbannercategory()
        {
            Mapper.CreateMap<M_GB_CATEGORY, Category>();
            IEnumerable<M_GB_CATEGORY> catList = _productService.GetCategories();
            IEnumerable<Category> categoryList = Mapper.Map<List<Category>>(catList);
            Mapper.CreateMap<Banner, BannerModel>();
            IEnumerable<Banner> bannerList = _productService.GetCategoryBanners();
            IEnumerable<BannerModel> bannerCodeList = Mapper.Map<List<BannerModel>>(bannerList);
            BannerListModel model = new BannerListModel
            {
                bannerList = bannerCodeList,
                CategoryList = categoryList
            };
            return View(model);
        }
        public ActionResult Workingonit()
        {
            return View();
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult AddCategoryBanner(BannerModel banner)
        {
            try
            {
                M_GB_BANNERS mBanner = new M_GB_BANNERS();
                if (banner.Image != null && banner.Image.ContentLength > 0)
                {
                    string imagetype = Path.GetExtension(banner.Image.FileName).ToLower();
                    var rondom = Guid.NewGuid() + imagetype;
                    var path = Path.Combine(ConfigurationManager.AppSettings["BannerUrl"] + "\\" + rondom);
                    if (imagetype == ".jpg" || imagetype == ".png" || imagetype == ".gif" || imagetype == ".jpeg")
                    {
                        //string gifFile = imagetype;
                        //using (FileStream fs = File.OpenRead(gifFile))
                        //{
                        //    fs.Read(bytes, 0, 10); // type (3 bytes), version (3 bytes), width (2 bytes), height (2 bytes)
                        //}


                        mBanner.BANNER_NAME = banner.BANNER_NAME;
                        mBanner.BANNER_URL = rondom;
                        mBanner.STATUS = (int)CustomStatus.BannerStatus.Active;
                        mBanner.BANNER_TYPE_ID = (int)CustomStatus.BannerTypes.Category;
                        mBanner.CAREATED_ON = banner.CAREATED_ON;
                        mBanner.CATEGORY_ID = banner.CATEGORY_ID;
                        mBanner.SUB_CAT_ID = banner.SUB_CAT_ID;
                        mBanner.CAREATED_ON = DateTime.Now;
                        banner.Image.SaveAs(path);
                        _productService.AddBanner(mBanner);
                        TempData["Message"] = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Uploaded);
                        return RedirectToAction("Setbannercategory");
                    }
                    else
                    {
                        TempData["Message"] = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Filetype);
                        return RedirectToAction("Setbannercategory");

                    }
                }
                else
                {
                    TempData["Message"] = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Excelfile);
                        return RedirectToAction("Setbannercategory");
                }
            }
            catch (Exception)
            {
                ViewBag.ErrorMsg = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.FaileAgian);
                return RedirectToAction("Setbannercategory");
            }
        }
        [AllowAnonymous]
        public JsonResult GetBanner(int id)
        {
            Banner banner = _productService.GetBanner(id);
            return Json(banner, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult Adds()
        {
            Mapper.CreateMap<Banner, BannerModel>();
            IEnumerable<Banner> bannerList = _productService.GetAddsBanners();
            IEnumerable<BannerModel> bannerCodeList = Mapper.Map<List<BannerModel>>(bannerList);
            BannerListModel model = new BannerListModel
            {
                bannerList = bannerCodeList
            };
            return View(model);
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Adds(BannerModel banner)
        {
            try
            {
                M_GB_BANNERS mBanner = new M_GB_BANNERS();
                if (banner.Image != null && banner.Image.ContentLength > 0)
                {
                    string imagetype = Path.GetExtension(banner.Image.FileName).ToLower();
                    var rondom = Guid.NewGuid() + imagetype;
                    var path = Path.Combine(ConfigurationManager.AppSettings["BannerUrl"] + "\\" + rondom);
                    if (imagetype == ".jpg" || imagetype == ".png" || imagetype == ".gif" || imagetype == ".jpeg")
                    {
                        mBanner.BANNER_NAME = "Adds";
                        mBanner.BANNER_URL = rondom;
                        mBanner.STATUS = (int)CustomStatus.BannerStatus.DeActive;
                        mBanner.BANNER_TYPE_ID = (int)CustomStatus.BannerTypes.Adds;
                        mBanner.CAREATED_ON = DateTime.Now;
                        banner.Image.SaveAs(path);
                        _productService.AddBanner(mBanner);
                        TempData["Message"] = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Uploaded);
                        return RedirectToAction("Adds");
                    }
                    else
                    {

                        TempData["Error"] = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Filetype);
                        return RedirectToAction("Adds");

                    }
                }
                else
                {
                    mBanner.BANNER_NAME = "Adds";
                    mBanner.BANNER_DEC = banner.Code;
                    mBanner.STATUS = (int)CustomStatus.BannerStatus.DeActive;
                    mBanner.BANNER_TYPE_ID = (int)CustomStatus.BannerTypes.Adds;
                    mBanner.BANNER_URL = "No Banner Url";
                    mBanner.CAREATED_ON = DateTime.Now;
                    _productService.AddBanner(mBanner);

                    TempData["Message"] = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Excelfile);
                    return RedirectToAction("Adds");
                }
            }
            catch (Exception)
            {
                TempData["Error"] = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.FaileAgian);
                return RedirectToAction("Adds");
            }
        }
        [HttpGet]
        public ActionResult GlobalDeals()
        {
            BulkProductUploadModel model = new BulkProductUploadModel();
            Mapper.CreateMap<GB_BULK_UPLOAD, BulkProductUploadview>();
            IEnumerable<GB_BULK_UPLOAD> bulkUploadList = _productService.GetBulkuploadList();
            IEnumerable<BulkProductUploadview> bulkmodel = Mapper.Map<List<BulkProductUploadview>>(bulkUploadList);
            model.bulkProductUploadview = bulkmodel;
            return View(model);
        }
        [HttpPost]
        public ActionResult GlobalDeals(HttpPostedFileBase file)
        {
            BulkProductUploadModel model = new BulkProductUploadModel();
            Mapper.CreateMap<GB_BULK_UPLOAD, BulkProductUploadview>();
            IEnumerable<GB_BULK_UPLOAD> bulkUploadList = _productService.GetBulkuploadList();
            IEnumerable<BulkProductUploadview> bulkmodel = Mapper.Map<List<BulkProductUploadview>>(bulkUploadList);
            model.bulkProductUploadview = bulkmodel;
            try
            {
                if (file != null)
                {
                    if (file.FileName.EndsWith("xls") || file.FileName.EndsWith("xlsx"))
                    {
                        string error = "";
                        var imported =
                         new DataImportHandler(_productService).Process(new ImportRequest() { ImportStream = file.InputStream });
                        foreach (var item in imported.Results.Where(a => a.STATUS == (int)CustomStatus.ProductStatus.DeActive))
                        {
                            error += item.PRODUCT_NAME + " && ";
                        }
                        imported.Results.Where(a => a.STATUS == 1).Select(a => a.CREATED_BY = "Global Deals").ToList();
                        if (imported.Results.Count(a => a.STATUS == (int)CustomStatus.ProductStatus.Active) != 0)
                        {
                            List<ProductDTO> date = new List<ProductDTO>();
                            foreach(ProductDTO p in imported.Results)
                            {
                                ProductDTO pro = new ProductDTO
                                {
                                    PRODUCT_NAME = p.PRODUCT_NAME,
                                    PRODUCT_CAPTION = p.PRODUCT_CAPTION,
                                    SUB_CAT_ID = p.SUB_CAT_ID,
                                    PRODUCT_DESC = p.PRODUCT_DESC,
                                    PRODUCT_SKU = p.PRODUCT_SKU,
                                    CATEGORY_ID = p.CATEGORY_ID,
                                    QUANTITYS = p.QUANTITYS,
                                    BRAND = p.BRAND,
                                    MRP = p.MRP,
                                    NET_PRICE = p.NET_PRICE,
                                    GROSS_PRICE = p.GROSS_PRICE,
                                    GIZMOBABA_PRICE = p.GIZMOBABA_PRICE,
                                    PHOTO_URL = p.PHOTO_URL,
                                    VIDEO_URL = p.VIDEO_URL,
                                    productImages = p.productImages.ToList(),
                                    ACTIVATION_DATE = p.ACTIVATION_DATE,
                                    DEACTIVATION_DATE = p.DEACTIVATION_DATE,
                                    DELIVERY_TIME = p.DELIVERY_TIME,
                                    INVENTORY = p.INVENTORY,
                                    RESERVE_QUANTITY = p.RESERVE_QUANTITY,
                                    PKG_QUANTITY = p.PKG_QUANTITY,
                                    CREATED_BY = p.CREATED_BY,
                                    UPDATED_BY = "Admin",
                                    STATUS = 0,
                                    ISAVAILABLE = (int)CustomStatus.ProductStatus.ISAVAILABLE,
                                    CREATED_ON = DateTime.Now,
                                    UPDATED_ON = DateTime.Now
                                };
                                date.Add(pro);
                            }
                            _productService.AddProductList(date);
                        }
                        ViewBag.Message = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Uploaded);
                        if (imported.Results.Count(a => a.STATUS == (int)CustomStatus.ProductStatus.DeActive) != 0)
                            ViewBag.Message = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Uploaded) + " but " + error + "Products not uploaded";
                        GB_BULK_UPLOAD bulkUpload = new GB_BULK_UPLOAD
                            {
                                CREATED_ON =DateTime.Now,
                                FILE_NAME = file.FileName,
                                UPLOAD_STATUS = 1,
                                TOTAL_RECORDS = imported.Results.Count(),
                                UPDATED_RECORDS = imported.Results.Where(a=>a.STATUS == 1).Count(),
                                DESCRIPTION = ViewBag.Message,
                                STATUS = (int)CustomStatus.DefaultStatus.Active
                            };
                        _productService.AddBulkProUploadList(bulkUpload);
                        var path = Path.Combine(ConfigurationManager.AppSettings["BulkImage"] + "\\" + file.FileName);
                        file.SaveAs(path);
                        return View(model);
                    }
                    else
                    {
                        ViewBag.Error = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Filetype);
                        return View(model);
                    }
                }
                else
                {
                    ViewBag.Error = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Excelfile);
                    return View(model);
                }
            }
            catch (Exception)
            {
                ViewBag.Error = CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.FaileAgian);
                return View(model);
            }
        }
        [Authorize(Roles = "Admin")]
        public ActionResult DownloadUploadProductSheet(string file)
        {
            //var columns = new List<string> { "PRODUCT NAME *", "PRODUCT SKU", "CATEGORY NAME *", "SUB CAT NAME", "SHORT DESCRIPITION *", "LONG DESCRIPITION", "BRAND", "MRP *", "WEB PRICE *", "COST PRICE *", "TOKEN PRICE *", "QUANTITYS", "ACTIVATION DATE", "DEACTIVATION DATE", "DELIVERY TIME", "INVENTORY *", "RESERVE QUANTITY", "PKG QUANTITY", "Product Image URL *", "GIF IMAGE *", "IMAGE_1", "IMAGE_2", "IMAGE_3", "VIDEO URL" }.ToArray();
            //var stream = new DataExportHandler<ProductExportModel>().ExportExcel(proList.ToList(), columns);
            //stream.Position = 0;
            //return File(stream, "application/vnd.ms-excel", "Products.xlsx");
            byte[] fileBytes = System.IO.File.ReadAllBytes(ConfigurationManager.AppSettings["BulkImage"] + "\\" + file);
            return File(fileBytes, "application/vnd.ms-excel", file);
        }
        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetChartBar()
        {
            var valStrings = new List<dynamic>();
            DateTime toDate = DateTime.Now;
            DateTime frmDate = toDate.AddDays(-7);
           IEnumerable<UserDto> data = _userService.GetUsers(frmDate,toDate);
            for (int x = 0; x <= 7;x++ )
           {
               dynamic runTimeObject = new ExpandoObject();
               string name = "[gd(" + frmDate.Year + ", " + frmDate.Month + ", " + frmDate.AddDays(x).Day + ")," + data.Count(a => a.CREATED_ON >= frmDate.AddDays(x) && a.CREATED_ON <= frmDate.AddDays(x + 1)) + " ]";
               runTimeObject = name;
               valStrings.Add(runTimeObject);
           }
               //foreach (UserDto u in data)
               //{
               //    valStrings.Add(count.ToString());
               //    count++;
               //}
            return Json(valStrings, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult MailManage()
        {
            IEnumerable<MailTemplates> mailTemplateModel = new List<MailTemplates>();
            try
            {
                Mapper.CreateMap<GB_MAIL_TEMPLATES, MailTemplates>();
                IEnumerable<GB_MAIL_TEMPLATES> getmailTemplateList = _promotionService.GetmailTemplateList();
                mailTemplateModel = Mapper.Map<List<MailTemplates>>(getmailTemplateList);
            }
            catch (Exception ex)
            {
                TempData["Exception"] = ex.Message;
            }
            return View(mailTemplateModel);
        }
        [HttpGet]
        public ActionResult FlashPromotions()
        {
            IEnumerable<FlashPromotionModel> flashPromotionModel = new List<FlashPromotionModel>();
            try
            {
                Mapper.CreateMap<GB_FLASH_PROMOTION, FlashPromotionModel>();
                IEnumerable<GB_FLASH_PROMOTION> getflashPromotion = _promotionService.GetflashPromotionList();
                flashPromotionModel = Mapper.Map<List<FlashPromotionModel>>(getflashPromotion);
            }
            catch (Exception ex)
            {
                TempData["Exception"] = ex.Message;
            }
            return View(flashPromotionModel);           
        }
        [HttpPost]
        public ActionResult AddFlashPromotions(GB_FLASH_PROMOTION flashPromotionModel)
        {
            try
            {
                if (flashPromotionModel.PROMOTION_ID == 0)
                {
                    flashPromotionModel.STATUS = (int)CustomStatus.DefaultStatus.DeActive;
                    flashPromotionModel.CREATED_ON = DateTime.Now;
                     _promotionService.Add(flashPromotionModel);
                     TempData["Success"] = "Flash Promotions As Been Created";
                }else
                {
                    _promotionService.Update(flashPromotionModel);
                    TempData["Success"] = "Flash Promotions As Been Updated";

                }
            }
            catch (Exception ex)
            {
                TempData["Exception"] = ex.Message;
            }
            return RedirectToAction("FlashPromotions");
        }
        [HttpPost]
        [AllowAnonymous]
        public JsonResult DeleteFlashPromotions(string id)
        {
            if (id != null)
               _promotionService.Delete(Convert.ToInt32(id));
            return Json(id, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddMailTemplate(GB_MAIL_TEMPLATES mailTemplateModel)
        {
            try
            {
                     if (mailTemplateModel.ID == 0)
                     {
                         mailTemplateModel.CREATED_ON = DateTime.Now;
                         _promotionService.AddMailTemplate(mailTemplateModel);
                         TempData["Success"] = "Mail Template As Been Created";
                     }
                     else
                     {
                         _promotionService.Update(mailTemplateModel);
                         TempData["Success"] = "AddMailTemplate As Been Updated";
                     }
            }
            catch (Exception ex)
            {
                TempData["Exception"] = ex.Message;
            }
            return RedirectToAction("MailManage");
        }
        [HttpPost]
        [AllowAnonymous]
        public JsonResult DeleteMailTemplate(string id)
        {
            if (id != null)
                _promotionService.DeleteMailTemplate(Convert.ToInt32(id));
            return Json(id, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetMailTemplate(string id)
        {
            GB_MAIL_TEMPLATES data = new GB_MAIL_TEMPLATES();
            if (id != null)
              data =  _promotionService.GetmailTemplateById(Convert.ToInt32(id));
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [AllowAnonymous]
        public JsonResult UpdateFlashPromotionsStatus(string id)
        {
            if (id != null)
                 _promotionService.UpdateFlashPromotionsStatus(Convert.ToInt32(id));
            return Json(id, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetFlashPromotion(string id)
        {
            GB_FLASH_PROMOTION data = new GB_FLASH_PROMOTION();
            if (id != null)
                data = _promotionService.FindById(Convert.ToInt32(id));
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [AllowAnonymous]
        public JsonResult UpdateMailTemplateStatus(string id)
        {
            int statusId= 0;
            if (id != null)
              statusId =  _promotionService.updateMailTemplateStatus(Convert.ToInt32(id));
            return Json(statusId, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult Reviews()
        {
            IEnumerable<ReviewsModel> getReviewsModel = new List<ReviewsModel>();
            try
            {
                Mapper.CreateMap<PRODUCTREVIEWSDTO, ReviewsModel>();
                IEnumerable<PRODUCTREVIEWSDTO> getReviews = _productService.GetReviews();
                getReviewsModel = Mapper.Map<List<ReviewsModel>>(getReviews);
            }
            catch (Exception ex)
            {
                TempData["Exception"] = ex.Message;
            }
            return View(getReviewsModel);
        }
        [HttpPost]
        [AllowAnonymous]
        public JsonResult UpdateReviewStatus(string id)
        {
            if (id != null)
                _productService.UpdateReviewStatus(Convert.ToInt32(id));
            return Json(id, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [AllowAnonymous]
        public JsonResult DeleteReview(string id)
        {
            if (id != null)
                _productService.DeleteReview(Convert.ToInt32(id));
            return Json(id, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult FindProduct(string sku)
        {
            if (!string.IsNullOrEmpty(sku))
            {
                ProductDTO product = _productService.GetProductbySku(sku);
                if (product != null)
                {
                    if ( string.IsNullOrEmpty(product.INVENTORY)  || product.INVENTORY == "0")
                        return Json("Out of stock", JsonRequestBehavior.AllowGet);

                    return Json(product, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json("Product does not exist in this sku", JsonRequestBehavior.AllowGet);
            }
            return Json("Please enter product sku", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AdminMails()
        {
            Mails mails = new Mails();
            IEnumerable<EmailMessages> messageses = mails.AdminMails();
            return PartialView("_adminMailspartial", messageses);
        }
        public ActionResult GetProductInvSearch(string productkey)
        {
            InventorySnapshotViewModel model = new InventorySnapshotViewModel();
            IEnumerable<ProductDTO> proList = _productService.GetProductSearch(productkey);
            model.productsList = (from p in proList
                                                   select new ProductDetailsDTO
                                                   {
                                                       PRODUCT_ID = p.PRODUCT_ID,
                                                       PRODUCT_NAME = p.PRODUCT_NAME,
                                                       PRODUCT_SKU = p.PRODUCT_SKU,
                                                       QUANTITYS = p.QUANTITYS,
                                                       INVENTORY = p.INVENTORY,
                                                       MRP = p.MRP,
                                                       NET_PRICE = p.NET_PRICE,
                                                       STATUS = p.STATUS,
                                                       PRODUCT_CAPTION = p.PRODUCT_CAPTION,
                                                       PRODUCT_DESC = p.PRODUCT_DESC
                                                   }).ToList();
            return PartialView("invotorySnapshotPartial", model);
        }
        public ActionResult GetProductInvAdvSearch(string period)
        {

            DateTime fromDate = DateTime.Now;
            DateTime toDate = DateTime.Now;
            if (period == "thisweek")
                fromDate = fromDate.AddDays(-7);
            if(period == "lastweek")
            {
                fromDate = fromDate.AddDays(-14);
                toDate = toDate.AddDays(-7);
            }
            if (period == "thismonth")
                fromDate = fromDate.AddDays(-30);
            if(period == "lastmonth")
            {
                fromDate = fromDate.AddDays(-60);
                toDate = toDate.AddDays(-30);
            }


            InventorySnapshotViewModel model = new InventorySnapshotViewModel();
            IEnumerable<ProductDTO> proList = _productService.GetProductInvAdvSearch(fromDate,toDate);
            model.productsList = (from p in proList
                                                   select new ProductDetailsDTO
                                                   {
                                                       PRODUCT_ID = p.PRODUCT_ID,
                                                       PRODUCT_NAME = p.PRODUCT_NAME,
                                                       PRODUCT_SKU = p.PRODUCT_SKU,
                                                       QUANTITYS = p.QUANTITYS,
                                                       INVENTORY = p.INVENTORY,
                                                       MRP = p.MRP,
                                                       NET_PRICE = p.NET_PRICE,
                                                       STATUS = p.STATUS,
                                                       PRODUCT_CAPTION = p.PRODUCT_CAPTION,
                                                       PRODUCT_DESC = p.PRODUCT_DESC
                                                   }).ToList();
            return PartialView("invotorySnapshotPartial", model);
        }

        public ActionResult ShowEmail(int index)
        {
            EmailMessages msg = null;//= new EmailMessages();
            try
            {
                using (var client = new ImapClient())
                {
                    using (var cancel = new CancellationTokenSource())
                    {
                        client.Connect("imap.gmail.com", 993, true, cancel.Token);

                        // If you want to disable an authentication mechanism,
                        // you can do so by removing the mechanism like this:
                        client.AuthenticationMechanisms.Remove("XOAUTH");
                        client.Authenticate("gizmobaba99", "prisionbreak", cancel.Token);
                        // The Inbox folder is always available...
                        var inbox = client.Inbox;
                        inbox.Open(FolderAccess.ReadWrite, cancel.Token);
                        var message = inbox.GetMessage(index);

                        msg = new EmailMessages { Body = message.HtmlBody, subject = message.Subject, Sender = message.From.FirstOrDefault().ToString() };


                        inbox.AddFlags(index, MessageFlags.Seen, true);
                        client.Disconnect(true, cancel.Token);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return View(msg);
        }
    }
}