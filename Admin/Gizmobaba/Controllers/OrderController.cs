﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Gizmobaba.Models;
using Gizmobaba.Utility;
using Gizmobaba.Service.Interfaces;
using AutoMapper;

namespace Gizmobaba.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        //private readonly IOrderService _orderService;
        //public OrderController(IOrderService orderService)
        //{
        //    _orderService = orderService;
        //}
        //private readonly IOrderService _orderService = new OrderService();
        //private readonly IProductService _productService = new ProductService();
        //private readonly IUserService _userService = new UserService();
        //private readonly IDiscountService _discountService = new DiscountService();
        private readonly IOrderService _orderService;
        private readonly IProductService _productService;
        private readonly IUserService _userService;
        private readonly IDiscountService _discountService;
        public OrderController(IOrderService orderService, IProductService productService, IUserService userService, IDiscountService discountService)
        {
            _orderService = orderService;
            _productService = productService;
            _userService = userService;
            _discountService = discountService;
        }
        // GET: Order
        public ActionResult Index()
        {
            return View();
        }
        [Authorize(Roles = "Admin")]
        public ActionResult GetPaymentPendingOrders()
        {
            //int status = (int) CustomStatus.OrderStatus.Pending;
            //IEnumerable<OrderDetailDto> orderDetail = _orderService.GetPaymentPendingOrders(status);
            //IList<OrderViewModel> orderList = orderDetail.Select(item => new OrderViewModel
            //{
            //    STATUS = item.STATUS,
            //    ORDER_ID = item.ORDER_ID,
            //    CREATED_AT = item.CREATED_AT ?? DateTime.Now,
            //    PRODUCT_ID = item.PRODUCT_ID ?? 0,
            //    Qty = item.Qty ?? 0,
            //    UserName = item.UserName,
            //    Age = item.Age,
            //    UNIT_PRICE = item.UNIT_PRICE
            //}).ToList();
            Mapper.CreateMap<OrderDetailDto, OrderViewModel>();
            //IEnumerable<OrderDetailDto> orderDetail = _orderService.GetOrdersList((int)CustomStatus.OrderStatus.Pending);
            IEnumerable<OrderDetailDto> orderDetail = _orderService.GetPaymentPendingOrdersList().Where(a=>a.Qty != null && a.Qty != 0).ToList();
            IEnumerable<OrderViewModel> orderList = Mapper.Map<List<OrderViewModel>>(orderDetail);
            ViewBag.Category = new SelectList(_productService.GetCategories().ToList(), "CATEGORY_ID", "CATEGORY_DESC");
            IEnumerable<OrderViewModel> models = orderList.ToList();
            TempData["Order"] = "GetPaymentPendingOrders";
            return View(models);
        }
        public ActionResult GetOrders(int orderStatus)
        {
            int count = 10;
            IEnumerable<OrderDetailDto> orderDetail = _orderService.GetOrdersList(orderStatus, count);
            IList<OrderViewModel> orderList = orderDetail.Select(item => new OrderViewModel
            {
                STATUS = item.STATUS,
                ORDER_ID = item.ORDER_ID,
                CREATED_AT = item.CREATED_AT ?? DateTime.Now,
                PRODUCT_ID = item.PRODUCT_ID ?? 0,
                Qty = item.Qty ?? 0,
                UserName = item.UserName,
                Age = item.Age,
                UNIT_PRICE = item.UNIT_PRICE
            }).ToList();
            return View("_orderPartial", orderList);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Cancelledorders()
        {
            Mapper.CreateMap<OrderDetailDto, OrderViewModel>();
            IEnumerable<OrderDetailDto> orderDetail = _orderService.GetOrdersList((int)CustomStatus.OrderStatus.Cancel);
            IEnumerable<OrderViewModel> orderList = Mapper.Map<List<OrderViewModel>>(orderDetail);
            ViewBag.Category = new SelectList(_productService.GetCategories().ToList(), "CATEGORY_ID", "CATEGORY_DESC");
            ViewBag.Order = (int)CustomStatus.OrderStatus.Cancel;
            return View(orderList);
        }

        //public JsonResult CancelOrder(int id)
        //{
        //    _orderService.CancelOrder(id);
        //    return Json("", JsonRequestBehavior.AllowGet);
        //}

        public ActionResult AbandonedCart()
        {
            Mapper.CreateMap<CartDto, CartViewModel>();


           // IEnumerable<CartDto> getAbandonedCart = _orderService.GetAbandonedCart().Where(a => !string.IsNullOrEmpty(a.SESSION_ID) && !string.IsNullOrEmpty(a.USER_ID)).OrderByDescending(a=>a.CART_ID).ToList();
           // List<CartDto> cart = new List<CartDto>();
            //string UserId="";
            //foreach (CartDto data in getAbandonedCart)
            //{
            //    if (data.USER_ID != UserId)
            //    {
            //        UserId = data.USER_ID;
            //        cart.Add(data);
            //    }
            //}
            List<CartDto> cart = _orderService.GetAbandonedCart();
            IEnumerable<CartViewModel> abandonedCartList = Mapper.Map<List<CartViewModel>>(cart);
            return View(model: abandonedCartList);
        }

        public ActionResult AbandonedCartSearch(string fromDate, string toDate, string searchKey)
        {
            DateTime frmDate = new DateTime();
            DateTime tDate = new DateTime();
            if(!string.IsNullOrEmpty(fromDate))
             frmDate = Convert.ToDateTime(fromDate);
            if(!string.IsNullOrEmpty(toDate))
             tDate = Convert.ToDateTime(toDate);
            Mapper.CreateMap<CartDto, CartViewModel>();
            IEnumerable<CartDto> cart = _orderService.AbandonedCartSearch(frmDate, tDate, searchKey);
            //IEnumerable<CartDto> getAbandonedCart = _orderService.AbandonedCartSearch(fromDate,toDate,searchKey);
            //List<CartDto> cart = new List<CartDto>();
            //string session = "";
            //foreach (CartDto data in getAbandonedCart.OrderBy(a => a.USER_ID))
            //{
            //    if (data.SESSION_ID != session)
            //    {
            //        cart.Add(data);
            //        session = data.SESSION_ID;
            //    }
            //}
            IEnumerable<CartViewModel> models = Mapper.Map<List<CartViewModel>>(cart);
            return PartialView("AbandonedCartPartial", models);
        }

        public ActionResult WaitingPickup()
        {
            Mapper.CreateMap<OrderDetailDto, OrderViewModel>();
            IEnumerable<OrderDetailDto> orderDetail = _orderService.GetOrdersList((int)CustomStatus.OrderStatus.Pickup);
            IEnumerable<OrderViewModel> orderList = Mapper.Map<List<OrderViewModel>>(orderDetail);
            ViewBag.Category = new SelectList(_productService.GetCategories().ToList(), "CATEGORY_ID", "CATEGORY_DESC");
            return View(orderList);
        }

        public ActionResult Dispatched()
        {
            Mapper.CreateMap<OrderDetailDto, OrderViewModel>();
            int paymentTypeId = (int)CustomStatus.PaymentTypes.COD;
            IEnumerable<OrderDetailDto> orderDetail = _orderService.GetOrdersList((int)CustomStatus.OrderStatus.Dispatched);
            IEnumerable<OrderViewModel> orderList = Mapper.Map<List<OrderViewModel>>(orderDetail);
            ViewBag.Category = new SelectList(_productService.GetCategories().ToList(), "CATEGORY_ID", "CATEGORY_DESC");
            return View(orderList);
        }

        public ActionResult Delivered()
        {
            Mapper.CreateMap<OrderDetailDto, OrderViewModel>();
            int paymentTypeId = (int)CustomStatus.PaymentTypes.COD;
            IEnumerable<OrderDetailDto> orderDetail = _orderService.GetOrdersList((int)CustomStatus.OrderStatus.Delivered);
            IEnumerable<OrderViewModel> orderList = Mapper.Map<List<OrderViewModel>>(orderDetail);
            ViewBag.Category = new SelectList(_productService.GetCategories().ToList(), "CATEGORY_ID", "CATEGORY_DESC");
            return View(orderList);
        }
        [HttpGet]
        public ActionResult Shipped()
        {
            //Mapper.CreateMap<OrderDetailDto, OrderViewModel>();
            //int paymentTypeId = (int)CustomStatus.PaymentTypes.COD;
            //IEnumerable<OrderDetailDto> orderDetail = _orderService.GetOrdersList((int)CustomStatus.OrderStatus.Delivered);
            //IEnumerable<OrderViewModel> orderList = Mapper.Map<List<OrderViewModel>>(orderDetail);
            IEnumerable<OrderViewModel> orderList = new List<OrderViewModel>();
            ViewBag.Category = new SelectList(_productService.GetCategories().ToList(), "CATEGORY_ID", "CATEGORY_DESC");
            return View(orderList);
        }
        public ActionResult ShippedOrders(int id=0)
        {
            if (id == 0)
            {
                Mapper.CreateMap<OrderDetailDto, OrderViewModel>();
                IEnumerable<OrderDetailDto> orderDetail = _orderService.GetOrdersList((int)CustomStatus.OrderStatus.ReadyToPickUp);
                IEnumerable<OrderViewModel> orderList = Mapper.Map<List<OrderViewModel>>(orderDetail);
                return PartialView("_shippedOrders", orderList);
            }
            else
            {
                if (id == (int)CustomStatus.OrderStatus.All)
                {
                    Mapper.CreateMap<OrderDetailDto, OrderViewModel>();
                    int paymentTypeId = (int)CustomStatus.PaymentTypes.COD;
                    IEnumerable<OrderDetailDto> orderDetail = _orderService.GetAllShippedOrdersList();
                    IEnumerable<OrderViewModel> orderList = Mapper.Map<List<OrderViewModel>>(orderDetail);
                    return PartialView("_shippedOrders", orderList);
                }
                else
                {
                    Mapper.CreateMap<OrderDetailDto, OrderViewModel>();
                    int paymentTypeId = (int)CustomStatus.PaymentTypes.COD;
                    IEnumerable<OrderDetailDto> orderDetail = _orderService.GetOrdersList(id);
                    IEnumerable<OrderViewModel> orderList = Mapper.Map<List<OrderViewModel>>(orderDetail);
                    return PartialView("_shippedOrders", orderList);
                }
            }
        }

        public ActionResult Issues()
        {
            int status = (int)CustomStatus.OrderStatus.Delivered;
            IEnumerable<OrderDetailDto> orderDetail = _orderService.GetOrdersList(status);
            IEnumerable<OrderViewModel> models = Mapper.Map<List<OrderViewModel>>(orderDetail);
            //IList<OrderViewModel> orderList = orderDetail.Select(item => new OrderViewModel
            //{
            //    STATUS = item.STATUS,
            //    ORDER_ID = item.ORDER_ID,
            //    CREATED_AT = item.CREATED_AT ?? DateTime.Now,
            //    PRODUCT_ID = item.PRODUCT_ID ?? 0,
            //    Qty = 10,
            //    UserName = item.UserName,
            //    Age = item.Age,
            //    UNIT_PRICE = item.UNIT_PRICE
            //}).ToList();
            //IEnumerable<OrderViewModel> models = orderList.ToList();
            ViewBag.Category = new SelectList(_productService.GetCategories().ToList(), "CATEGORY_ID", "CATEGORY_DESC");
            return View(models);
        }
        public ActionResult Returns()
        {
            //IEnumerable<CartDto> getAbandonedCart = _orderService.GetAbandonedCart();
            //IList<CartViewModel> abandonedCartList = getAbandonedCart.Select(item => new CartViewModel
            //{
            //    CART_ID = item.CART_ID,
            //    CREATED_AT = item.CREATED_AT,
            //    STATUS = item.STATUS,
            //    SESSION_ID = item.SESSION_ID,
            //    PRICE_PER_QYT = item.PRICE_PER_QYT,
            //    QYT = item.QYT,
            //    TOTAL_AMOUNT = item.TOTAL_AMOUNT,
            //    TOTAL_QTY = item.TOTAL_QTY
            //}).ToList();
            //IEnumerable<CartViewModel> abandonedCart = abandonedCartList.ToList();
            OrderReturnsViewModel orderReturns = new OrderReturnsViewModel();
           // orderReturns.CartViewModels = abandonedCart;

            return View(orderReturns);   
        }
        [HttpGet]
        public ActionResult CreateOrder(int id = 0)
        {
            UserOrderModel model = new UserOrderModel();
            UserAddressModel userAddress = new UserAddressModel();
            model.BillingAddress = userAddress;
            model.ShippinghAddress = userAddress;
            if (id != 0)
            {
                Mapper.CreateMap<M_GB_USER, UserModel>();
                M_GB_USER userData = _userService.GetUser(id);
                model.UserModel = Mapper.Map<UserModel>(userData);
                //if(userData.GB_ORDER.Count() != 0)
                //{
                //    model.OrderViewModel.ORDER_ID = userData.GB_ORDER.LastOrDefault().ORDER_ID;
                //}
                //if(model != null && model.UserModel != null)
                //{
                //    model.ShippinghAddress.FIRSTNAME = model.UserModel.FIRST_NAME;
                //    model.ShippinghAddress.LASTNAME = model.UserModel.LAST_NAME;
                //    model.ShippinghAddress.ADDRESS_1 = model.UserModel.ADD1;
                //    model.ShippinghAddress.ADDRESS_2 = model.UserModel.ADD2;
                //    model.ShippinghAddress.COUNTRY = model.UserModel.COUNTRY;
                //    model.ShippinghAddress.STATE = model.UserModel.STATE;
                //    model.ShippinghAddress.CITY = model.UserModel.CITY;
                //    model.ShippinghAddress.ADDRESS_1 = model.UserModel.ADD1;
                //    model.ShippinghAddress.POSTCODE = model.UserModel.PIN.ToString();
                //    model.ShippinghAddress.MOB_NO = model.UserModel.MOB_NO;
                //    model.ShippinghAddress.EMAIL = model.UserModel.EMAIL;
                //    model.ShippinghAddress.ALT_MOB = model.UserModel.ALT_MOB;
                //}
                Mapper.CreateMap<GB_USER_ADDRESS, UserAddressModel>();
                IEnumerable<GB_USER_ADDRESS> AddressList = _userService.GetUserAddress(id.ToString());
                model.userAddressList = Mapper.Map<List<UserAddressModel>>(AddressList);
                //if (AddressList.Count() != 0)
                //{
                //    GB_USER_ADDRESS BillingAddress = AddressList.FirstOrDefault();
                //    model.BillingAddress.FIRSTNAME = BillingAddress.FIRSTNAME;
                //    model.BillingAddress.LASTNAME = BillingAddress.LASTNAME;
                //    model.BillingAddress.ADDRESS_1 = BillingAddress.ADDRESS_1;
                //    model.BillingAddress.ADDRESS_2 = BillingAddress.ADDRESS_2;
                //    model.BillingAddress.CITY = BillingAddress.CITY;
                //    model.BillingAddress.POSTCODE = BillingAddress.POSTCODE;
                //    model.BillingAddress.MOB_NO = BillingAddress.mob_no.ToString();

                //}
            }
            return View(model);   
        }
        [HttpPost]
        public ActionResult CreateOrder(UserOrderModel userOrder)
        {
            if (userOrder != null)
            {
                string UserId = userOrder.UserModel.USER_ID;
                if (userOrder.UserModel != null && userOrder.UserModel.USER_ID != "")
                {
                    M_GB_USER user = new M_GB_USER
                    {
                        USER_NAME = userOrder.ShippinghAddress.FIRSTNAME + " " + userOrder.ShippinghAddress.LASTNAME,
                        USER_TYPE = 7,
                        FIRST_NAME = userOrder.ShippinghAddress.FIRSTNAME,
                        LAST_NAME = userOrder.ShippinghAddress.LASTNAME
                    };
                   UserId =  _userService.AddUser(user);   
                }
               

                if (userOrder.UserModel != null && userOrder.UserModel.USER_ID != "")
                {
                    int AddressId = 0;
                    if (userOrder.ShippinghAddress != null)
                    {
                        GB_USER_ADDRESS userAddress = new GB_USER_ADDRESS
                        {
                            CUSTOMER_ID = UserId,
                            FIRSTNAME = userOrder.ShippinghAddress.FIRSTNAME,
                            LASTNAME = userOrder.ShippinghAddress.LASTNAME,
                            ADDRESS_1 = userOrder.ShippinghAddress.ADDRESS_1,
                            ADDRESS_2 = userOrder.ShippinghAddress.ADDRESS_2,
                            CITY = userOrder.ShippinghAddress.CITY,
                            POSTCODE = userOrder.ShippinghAddress.POSTCODE,
                            landmark = userOrder.ShippinghAddress.landmark,
                            mob_no = userOrder.ShippinghAddress.mob_no,

                        };
                     AddressId =   _userService.AddUserAddress(userAddress);
                    } 
                    if (userOrder.BillingAddress != null)
                    {
                        GB_USER_ADDRESS userAddress = new GB_USER_ADDRESS
                        {
                            CUSTOMER_ID = UserId,
                            FIRSTNAME = userOrder.BillingAddress.FIRSTNAME,
                            LASTNAME = userOrder.BillingAddress.LASTNAME,
                            ADDRESS_1 = userOrder.BillingAddress.ADDRESS_1,
                            ADDRESS_2 = userOrder.BillingAddress.ADDRESS_2,
                            CITY = userOrder.BillingAddress.CITY,
                            POSTCODE = userOrder.BillingAddress.POSTCODE,
                            landmark = userOrder.BillingAddress.landmark,
                            mob_no = userOrder.BillingAddress.mob_no,

                        };
                        _userService.AddUserAddress(userAddress);
                    }
                    GB_ORDER order = new GB_ORDER
                    {
                        CUSTOMER_ID = Convert.ToInt32(UserId),
                        CREATED_AT = DateTime.Now,
                        STATUS = 1,
                        ADDRESS_ID = AddressId,
                    };
                    long orderId = _orderService.AddOrder(order);
                    GB_ORDER_DETAIL orderDetails = new GB_ORDER_DETAIL
                    {
                        PRODUCT_ID = userOrder.ProductDto.PRODUCT_ID,
                        PRODUCT_NAME = userOrder.ProductDto.PRODUCT_NAME,
                        NUM_OF_PRO = userOrder.ProductDto.QUANTITYS,
                        UNIT_PRICE = userOrder.ProductDto.NET_PRICE,
                        CREATED_AT = DateTime.Now,
                        STATUS = 1,
                        ORDER_ID = orderId,
                        QUANTITY = userOrder.ProductDto.QUANTITYS
                    };
                    _orderService.AddOrderDetails(orderDetails);
                }
            }
            return RedirectToAction("CreateOrder");
        }
        [HttpPost]
        public ActionResult AddOrder(UserOrderModel userOrder)
        {
            if (userOrder != null)
            {
                string UserId = userOrder.UserModel.USER_ID;
                if (userOrder.UserModel != null && userOrder.UserModel.USER_ID != "")
                {
                    M_GB_USER user = new M_GB_USER
                    {
                        USER_NAME = userOrder.ShippinghAddress.FIRSTNAME + " " + userOrder.ShippinghAddress.LASTNAME,
                        USER_TYPE = 7,
                        FIRST_NAME = userOrder.ShippinghAddress.FIRSTNAME,
                        LAST_NAME = userOrder.ShippinghAddress.LASTNAME
                    };
                    UserId = _userService.AddUser(user);
                }


                if (userOrder.UserModel != null && userOrder.UserModel.USER_ID != "")
                {
                    int AddressId = 0;
                    if (userOrder.ShippinghAddress != null)
                    {
                        GB_USER_ADDRESS userAddress = new GB_USER_ADDRESS
                        {
                            CUSTOMER_ID = UserId,
                            FIRSTNAME = userOrder.ShippinghAddress.FIRSTNAME,
                            LASTNAME = userOrder.ShippinghAddress.LASTNAME,
                            ADDRESS_1 = userOrder.ShippinghAddress.ADDRESS_1,
                            ADDRESS_2 = userOrder.ShippinghAddress.ADDRESS_2,
                            CITY = userOrder.ShippinghAddress.CITY,
                            POSTCODE = userOrder.ShippinghAddress.POSTCODE,
                            landmark = userOrder.ShippinghAddress.landmark,
                            mob_no = userOrder.ShippinghAddress.mob_no,

                        };
                        AddressId = _userService.AddUserAddress(userAddress);
                    }
                    if (userOrder.BillingAddress != null)
                    {
                        GB_USER_ADDRESS userAddress = new GB_USER_ADDRESS
                        {
                            CUSTOMER_ID = UserId,
                            FIRSTNAME = userOrder.BillingAddress.FIRSTNAME,
                            LASTNAME = userOrder.BillingAddress.LASTNAME,
                            ADDRESS_1 = userOrder.BillingAddress.ADDRESS_1,
                            ADDRESS_2 = userOrder.BillingAddress.ADDRESS_2,
                            CITY = userOrder.BillingAddress.CITY,
                            POSTCODE = userOrder.BillingAddress.POSTCODE,
                            landmark = userOrder.BillingAddress.landmark,
                            mob_no = userOrder.BillingAddress.mob_no,

                        };
                        _userService.AddUserAddress(userAddress);
                    }
                    GB_ORDER order = new GB_ORDER
                    {
                        CUSTOMER_ID = Convert.ToInt32(UserId),
                        CREATED_AT = DateTime.Now,
                        STATUS = 1,
                        ADDRESS_ID = AddressId,
                    };
                    long orderId = _orderService.AddOrder(order);
                    GB_ORDER_DETAIL orderDetails = new GB_ORDER_DETAIL
                    {
                        PRODUCT_ID = userOrder.ProductDto.PRODUCT_ID,
                        PRODUCT_NAME = userOrder.ProductDto.PRODUCT_NAME,
                        NUM_OF_PRO = userOrder.ProductDto.QUANTITYS,
                        UNIT_PRICE = userOrder.ProductDto.NET_PRICE,
                        CREATED_AT = DateTime.Now,
                        STATUS = 1,
                        ORDER_ID = orderId,
                        QUANTITY = userOrder.ProductDto.QUANTITYS
                    };
                    _orderService.AddOrderDetails(orderDetails);
                }
            }
            return RedirectToAction("CreateOrder");
        }

        public ActionResult Reports()
        {
            return View();
        }
        [AllowAnonymous]
        [HttpGet]
        public JsonResult OrderSearch(int id)
        {

            Mapper.CreateMap<OrderDetailDto, OrderViewModel>();
            IEnumerable<OrderDetailDto> orderDetail = _orderService.GetOrder(id).ToList();
            IEnumerable<OrderViewModel> orderList = Mapper.Map<List<OrderViewModel>>(orderDetail);
            return Json(orderList, JsonRequestBehavior.AllowGet);
        }
        //public JsonResult SearchOrder(OrderSerch data)
        //{
        //    IEnumerable<OrderDetailDto> orderDetail = _orderService.OrderSerch(data);
        //    return Json(orderDetail, JsonRequestBehavior.AllowGet);
        //}
        public ActionResult SearchOrder(OrderSerch data)
        {

            Mapper.CreateMap<OrderDetailDto, OrderViewModel>();
            IEnumerable<OrderDetailDto> orderDetail = _orderService.OrderSerch(data);
            IEnumerable<OrderViewModel> orderList = Mapper.Map<List<OrderViewModel>>(orderDetail);
            ViewBag.Order = data.OrderType;
            return View("_orderPartial", orderList);
        }
        public ActionResult SearchAuthorizeCODOrder(OrderSerch serch)
        {
            if ( serch.Created_By == ((int)CustomStatus.UserTypes.Customer).ToString() || serch.Created_By == "0")
                serch.Created_By = "";
            Mapper.CreateMap<OrderDetailDto, OrderViewModel>();
            serch.PAYMENT_TYPE = (int) CustomStatus.PaymentTypes.COD;
            serch.STATUS = (int) CustomStatus.OrderStatus.Authorize;
            IEnumerable<GB_ORDER> orderDetail = _orderService.SearchOrders(serch);

            IList<NewOrderModel> orderModel = new List<NewOrderModel>();
            NewOrderViewModel model = new NewOrderViewModel();


           // IEnumerable<GB_ORDER> orderData = _orderService.GetNewOrderList((int)CustomStatus.OrderStatus.New, paymentTypeId);
           // IEnumerable<OrderViewModel> orderList = Mapper.Map<List<OrderViewModel>>(orderDetail);

            foreach (GB_ORDER r in orderDetail)
            {
                NewOrderModel singleOrder = new NewOrderModel();
                //if (r.SPLIT_ORDER_ID != null && r.SPLIT_ORDER_ID != 0 && r.IS_SPLIT == true)
                if (r.IS_SPLIT == true || (r.SPLIT_ORDER_ID != null && r.SPLIT_ORDER_ID != 0))
                {
                    IEnumerable<GB_ORDER_DETAIL> orderDetails;
                    singleOrder = new NewOrderModel();
                    singleOrder.message = "Order Split(";
                    singleOrder.ORDER_ID = r.ORDER_ID;
                    singleOrder.CREATED_AT = r.CREATED_AT ?? DateTime.Now;
                    if (r.M_GB_USER.USER_NAME != null)
                        singleOrder.UserName = r.M_GB_USER.USER_NAME + r.M_GB_USER.FIRST_NAME;
                    singleOrder.STATUS = r.STATUS;
                    orderDetails = _orderService.GetSplitOrderDetails(r.ORDER_ID);
                    if (orderDetails != null && orderDetails.Count() != 0)
                    {
                        singleOrder.Qty = orderDetails.Sum(a => a.QUANTITY) ?? 0;
                        singleOrder.CHECKOUTTYPE = orderDetails.FirstOrDefault().CHECKOUTTYPE;
                        singleOrder.UNIT_PRICE = orderDetails.Where(a => a.SPLIT_ORDER_ID == r.ORDER_ID).Sum(a => a.UNIT_PRICE);
                        singleOrder.message += orderDetails.FirstOrDefault().ORDER_ID.ToString();
                    }
                    //}
                    singleOrder.Address_Id = r.ADDRESS_ID ?? 0;
                    singleOrder.MERGEDORDERID = r.MERGEDORDERID;
                    singleOrder.ISMERGEITEM = r.ISMERGEITEM;
                    singleOrder.CREATED_BY = r.CREATED_BY;
                    singleOrder.IS_SPLIT = r.IS_SPLIT;
                    singleOrder.SPLIT_ORDER_ID = r.SPLIT_ORDER_ID;
                    singleOrder.message += ")";
                    IEnumerable<GB_ORDER> n = orderDetail.Where(a => a.ORDER_ID == r.SPLIT_ORDER_ID).ToList();
                }
                else
                {
                    singleOrder = new NewOrderModel
                    {
                        ORDER_ID = r.ORDER_ID,
                        CREATED_AT = r.CREATED_AT ?? DateTime.Now,
                        UserName = r.M_GB_USER.USER_NAME + r.M_GB_USER.FIRST_NAME,
                        STATUS = r.STATUS,
                        Qty = r.GB_ORDER_DETAIL.Sum(a => a.QUANTITY) ?? 0,
                        PAYMENT_TYPE = r.PAYMENT_TYPE,
                        Address_Id = r.ADDRESS_ID ?? 0,
                        MERGEDORDERID = r.MERGEDORDERID,
                        ISMERGEITEM = r.ISMERGEITEM,
                        CREATED_BY = r.CREATED_BY,
                        IS_SPLIT = r.IS_SPLIT,
                        SPLIT_ORDER_ID = r.SPLIT_ORDER_ID,
                        Role = r.M_GB_USER.ROLE.ToString()
                    };
                    if (r.GB_ORDER_DETAIL.Count() != 0)
                        singleOrder.UNIT_PRICE = r.GB_ORDER_DETAIL.Sum(a => a.UNIT_PRICE);
                    else
                    {
                        IEnumerable<GB_ORDER> n = orderDetail.Where(a => a.ORDER_ID == r.SPLIT_ORDER_ID).ToList();
                        foreach (GB_ORDER o in n)
                        {
                            if (singleOrder.UNIT_PRICE == null)
                                singleOrder.UNIT_PRICE = 0;
                            singleOrder.UNIT_PRICE += o.GB_ORDER_DETAIL.Sum(a => a.UNIT_PRICE);
                            singleOrder.Qty += o.GB_ORDER_DETAIL.Sum(a => a.QUANTITY) ?? 0;
                        }
                    }

                }
                if (r.ISMERGEITEM == true)
                {
                    IEnumerable<GB_ORDER> n = orderDetail.Where(a => a.MERGEDORDERID == r.ORDER_ID).ToList();
                    singleOrder.message = "Order Merged(";
                    foreach (GB_ORDER o in n)
                    {
                        if (singleOrder.UNIT_PRICE == null)
                            singleOrder.UNIT_PRICE = 0;
                        singleOrder.UNIT_PRICE += o.GB_ORDER_DETAIL.Sum(a => a.UNIT_PRICE);
                        singleOrder.Qty += o.GB_ORDER_DETAIL.Sum(a => a.QUANTITY) ?? 0;
                        singleOrder.message += o.ORDER_ID + " ";
                    }
                    singleOrder.message += ")";
                }
                if (r.MERGEDORDERID == null || r.MERGEDORDERID == 0)
                {
                    orderModel.Add(singleOrder);
                }
            }
            model.newOrderModel = orderModel;
            return View("authorizeOrderPartial", model.newOrderModel);
        }

        public ActionResult SearchAuthorizeOrder(OrderSerch orderSerch)
        {
            return View();
        }

        public ActionResult PickupOrdersSerch()
        {
            return View();
        }

        public ActionResult AuthorizePrepaid()
        {
            Mapper.CreateMap<OrderDetailDto, OrderViewModel>();
            int paymentTypeId = (int)CustomStatus.PaymentTypes.Online;
            IEnumerable<OrderDetailDto> orderDetail = _orderService.GetOrderList((int)CustomStatus.OrderStatus.New, paymentTypeId);
            IEnumerable<OrderViewModel> orderList = Mapper.Map<List<OrderViewModel>>(orderDetail);
            ViewBag.Category = new SelectList(_productService.GetCategories().ToList(), "CATEGORY_ID", "CATEGORY_DESC");
            return View(orderList);
        }

        public ActionResult AuthorizeCOD()
        {
            Mapper.CreateMap<OrderDetailDto, OrderViewModel>();
            int paymentTypeId = (int)CustomStatus.PaymentTypes.COD;
         //   IEnumerable<OrderDetailDto> orderDetail = _orderService.GetOrderList((int)CustomStatus.OrderStatus.New,paymentTypeId);
         //   IEnumerable<OrderViewModel> orderList = Mapper.Map<List<OrderViewModel>>(orderDetail);
            IList<NewOrderModel> orderModel = new List<NewOrderModel>();
            NewOrderViewModel model = new NewOrderViewModel();

            IEnumerable<GB_ORDER> orderData = _orderService.GetNewOrderList((int)CustomStatus.OrderStatus.New, paymentTypeId);
            foreach (GB_ORDER r in orderData)
            {

                if (r.GB_ORDER_DETAIL.Count() != 0 && r.GB_ORDER_DETAIL.FirstOrDefault().QUANTITY != null)
                {
                    NewOrderModel singleOrder = new NewOrderModel();
                    //if (r.SPLIT_ORDER_ID != null && r.SPLIT_ORDER_ID != 0 && r.IS_SPLIT == true)
                    if (r.IS_SPLIT == true || (r.SPLIT_ORDER_ID != null && r.SPLIT_ORDER_ID != 0))
                    {
                        // singleOrder = new NewOrderModel
                        //{
                        //    ORDER_ID = r.ORDER_ID,
                        //    CREATED_AT = r.CREATED_AT ?? DateTime.Now,
                        //    UserName = r.M_GB_USER.USER_NAME + r.M_GB_USER.FIRST_NAME,
                        //    STATUS = r.STATUS,
                        //    Qty = r.GB_ORDER_DETAIL.Sum(a => a.QUANTITY) ?? 0,
                        //    CHECKOUTTYPE = r.GB_ORDER_DETAIL.FirstOrDefault().CHECKOUTTYPE,
                        //    Address_Id = r.ADDRESS_ID ?? 0,
                        //    MERGEDORDERID = r.MERGEDORDERID,
                        //    ISMERGEITEM = r.ISMERGEITEM,
                        //    CREATED_BY = r.CREATED_BY,
                        //    IS_SPLIT = r.IS_SPLIT,
                        //    SPLIT_ORDER_ID = r.SPLIT_ORDER_ID
                        //};
                        IEnumerable<GB_ORDER_DETAIL> orderDetails;
                        singleOrder = new NewOrderModel();
                        singleOrder.message = "Order Split(";
                        singleOrder.ORDER_ID = r.ORDER_ID;
                        singleOrder.CREATED_AT = r.CREATED_AT ?? DateTime.Now;
                        if (r.M_GB_USER.USER_NAME != null)
                            singleOrder.UserName = r.M_GB_USER.USER_NAME + r.M_GB_USER.FIRST_NAME;
                        singleOrder.STATUS = r.STATUS;
                        //if (r.GB_ORDER_DETAIL != null && r.GB_ORDER_DETAIL.Count() != 0)
                        //{
                        //    singleOrder.Qty = r.GB_ORDER_DETAIL.Sum(a => a.QUANTITY) ?? 0;
                        //    singleOrder.CHECKOUTTYPE = r.GB_ORDER_DETAIL.FirstOrDefault().CHECKOUTTYPE;
                        //}
                        //else
                        //{
                        orderDetails = _orderService.GetSplitOrderDetails(r.ORDER_ID);
                        if (orderDetails != null && orderDetails.Count() != 0)
                        {
                            singleOrder.Qty = orderDetails.Sum(a => a.QUANTITY) ?? 0;
                            singleOrder.CHECKOUTTYPE = orderDetails.FirstOrDefault().CHECKOUTTYPE;
                            singleOrder.UNIT_PRICE = orderDetails.Where(a => a.SPLIT_ORDER_ID == r.ORDER_ID).Sum(a => a.UNIT_PRICE);
                            singleOrder.message += orderDetails.FirstOrDefault().ORDER_ID.ToString();
                        }
                        //}
                        singleOrder.Address_Id = r.ADDRESS_ID ?? 0;
                        singleOrder.MERGEDORDERID = r.MERGEDORDERID;
                        singleOrder.ISMERGEITEM = r.ISMERGEITEM;
                        singleOrder.CREATED_BY = r.CREATED_BY;
                        singleOrder.IS_SPLIT = r.IS_SPLIT;
                        singleOrder.SPLIT_ORDER_ID = r.SPLIT_ORDER_ID;
                        singleOrder.message += ")";
                        IEnumerable<GB_ORDER> n = orderData.Where(a => a.ORDER_ID == r.SPLIT_ORDER_ID).ToList();
                        //foreach (GB_ORDER o in n)
                        //{
                        //    singleOrder.UNIT_PRICE = o.GB_ORDER_DETAIL.Where(a=>a.SPLIT_ORDER_ID == r.ORDER_ID).Sum(a => a.UNIT_PRICE);
                        //}
                    }
                    else
                    {
                        singleOrder = new NewOrderModel
                        {
                            ORDER_ID = r.ORDER_ID,
                            CREATED_AT = r.CREATED_AT ?? DateTime.Now,
                            UserName = r.M_GB_USER.USER_NAME ?? r.M_GB_USER.FIRST_NAME,
                            STATUS = r.STATUS,
                            Qty = r.GB_ORDER_DETAIL.Sum(a => a.QUANTITY) ?? 0,
                            PAYMENT_TYPE = r.PAYMENT_TYPE,
                            Address_Id = r.ADDRESS_ID ?? 0,
                            MERGEDORDERID = r.MERGEDORDERID,
                            ISMERGEITEM = r.ISMERGEITEM,
                            CREATED_BY = r.CREATED_BY,
                            IS_SPLIT = r.IS_SPLIT,
                            SPLIT_ORDER_ID = r.SPLIT_ORDER_ID,
                            Role = r.M_GB_USER.ROLE.ToString()
                        };
                        if (r.GB_ORDER_DETAIL.Count() != 0)
                            singleOrder.UNIT_PRICE = r.GB_ORDER_DETAIL.Sum(a => a.UNIT_PRICE);
                        else
                        {
                            IEnumerable<GB_ORDER> n = orderData.Where(a => a.ORDER_ID == r.SPLIT_ORDER_ID).ToList();
                            foreach (GB_ORDER o in n)
                            {
                                if (singleOrder.UNIT_PRICE == null)
                                    singleOrder.UNIT_PRICE = 0;
                                singleOrder.UNIT_PRICE += o.GB_ORDER_DETAIL.Sum(a => a.UNIT_PRICE);
                                singleOrder.Qty += o.GB_ORDER_DETAIL.Sum(a => a.QUANTITY) ?? 0;
                            }
                        }

                    }
                    if (r.ISMERGEITEM == true)
                    {
                        IEnumerable<GB_ORDER> n = orderData.Where(a => a.MERGEDORDERID == r.ORDER_ID).ToList();
                        singleOrder.message = "Order Merged(";
                        foreach (GB_ORDER o in n)
                        {
                            if (singleOrder.UNIT_PRICE == null)
                                singleOrder.UNIT_PRICE = 0;
                            singleOrder.UNIT_PRICE += o.GB_ORDER_DETAIL.Sum(a => a.UNIT_PRICE);
                            singleOrder.Qty += o.GB_ORDER_DETAIL.Sum(a => a.QUANTITY) ?? 0;
                            singleOrder.message += o.ORDER_ID + " ";
                        }
                        singleOrder.message += ")";
                    }
                    if (r.MERGEDORDERID == null || r.MERGEDORDERID == 0)
                    {
                        orderModel.Add(singleOrder);
                    }
                }
            }
            model.newOrderModel = orderModel.OrderByDescending(a=>a.CREATED_AT);
            ViewBag.Category = new SelectList(_productService.GetCategories().ToList(), "CATEGORY_ID", "CATEGORY_DESC");
            return View(model);
        }

        public ActionResult OrderDetails(int id = 0)
        {
           // OrderDetailDto data = _orderService.GetOrderDetails(id);
            GB_ORDER orderData = _orderService.GetNewOrder(id);


            OrderDetailsModelView data = new OrderDetailsModelView();
            IList<OrderViewModel> orderDetail;
            if (id != 0)
            {
                Mapper.CreateMap<OrderDetailDto, OrderViewModel>();
                IEnumerable<OrderDetailDto> orderDetails = _orderService.GetOrderDetailsByOrderID(id);
                orderDetail = Mapper.Map<List<OrderViewModel>>(orderDetails);
                if(orderData.IS_SPLIT == true)
                {
                    orderDetail = orderDetail.Where(a => a.SPLIT_ORDER_ID == null).ToList();
                }
                if(orderData.SPLIT_ORDER_ID != null)
                {
                    Mapper.CreateMap<OrderDetailDto, OrderViewModel>();
                    orderDetails = _orderService.GetOrderDetailsByOrderID(Convert.ToInt32(orderData.SPLIT_ORDER_ID));
                    orderDetail = Mapper.Map<List<OrderViewModel>>(orderDetails);
                    orderDetail = orderDetail.Where(a => a.SPLIT_ORDER_ID == id).ToList();

                }
                if(orderData.ISMERGEITEM == true)
                {
                    IList<GB_ORDER> GetMergedOrders = _orderService.GetMergedOrders(id);
                    foreach(GB_ORDER mord in GetMergedOrders)
                    {
                        Mapper.CreateMap<OrderDetailDto, OrderViewModel>();
                        orderDetails = _orderService.GetOrderDetailsByOrderID(Convert.ToInt32(mord.ORDER_ID));
                        IEnumerable<OrderViewModel> orders = Mapper.Map<List<OrderViewModel>>(orderDetails);
                        foreach(OrderViewModel M in orders)
                        {
                            orderDetail.Add(M);
                        }
                    }
                   
                }
                if (orderDetails.Count() != 0)
                {
                    if (orderDetails.FirstOrDefault().User != null)
                    {
                        Mapper.CreateMap<M_GB_USER, UserModel>();
                        M_GB_USER userModel = orderDetails.FirstOrDefault().User;
                        data.userData = Mapper.Map<UserModel>(userModel);
                        orderDetail.FirstOrDefault().UserModel = data.userData;

                        Mapper.CreateMap<GB_USER_ADDRESS, UserAddressModel>();
                        IEnumerable<GB_USER_ADDRESS> userAddress = _userService.GetUserAddress(data.userData.USER_ID);
                        data.userAddress = Mapper.Map<List<UserAddressModel>>(userAddress);

                        data.Order = orderDetail.FirstOrDefault();
                        data.Order.Address_Id = orderDetails.FirstOrDefault().OdDetail.GB_ORDER.ADDRESS_ID ?? 0;
                    }
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

            data.orderlist = orderDetail;
            return View(data);
        }
        [AllowAnonymous]
        public JsonResult CancelOrder(OrderDto order)
        {
           int id = _orderService.CancelOrder(order);
           return Json("", JsonRequestBehavior.AllowGet);
        }
        [AllowAnonymous]
        public JsonResult GetUserSearch(string key)
        {
            UserDto daUserDto = _userService.UserDetails(key);
            UserOrderModel userModel = new UserOrderModel();
            if (daUserDto.UserAddresss != null && daUserDto.UserAddresss.Count() != 0)
            {
                if (daUserDto.UserAddresss.Count() <= 1)
                userModel.BillingAddress = daUserDto.UserAddresss.Select(a => new UserAddressModel
                {
                    FIRSTNAME = a.FIRSTNAME,
                    LASTNAME = a.LASTNAME,
                    ADDRESS_1 = a.ADDRESS_1,
                    ADDRESS_2 = a.ADDRESS_2,
                    CITY = a.CITY,
                    POSTCODE = a.POSTCODE,
                    landmark = a.landmark,
                    mob_no = a.mob_no
                }).First();
                if (daUserDto.UserAddresss.Count() <= 2)
                    userModel.ShippinghAddress = daUserDto.UserAddresss.Select(a => new UserAddressModel
                    {
                        FIRSTNAME = a.FIRSTNAME,
                        LASTNAME = a.LASTNAME,
                        ADDRESS_1 = a.ADDRESS_1,
                        ADDRESS_2 = a.ADDRESS_2,
                        CITY = a.CITY,
                        POSTCODE = a.POSTCODE,
                        landmark = a.landmark,
                        mob_no = a.mob_no
                    }).OrderByDescending(a=>a.ADDRESS_1).FirstOrDefault();
            }
            return Json(userModel, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SearchCancellOrder(OrderSerch data)
        {

            Mapper.CreateMap<OrderDetailDto, OrderViewModel>();
            data.OrderType = (int)CustomStatus.OrderStatus.Cancel;
            IEnumerable<GB_ORDER> orderDetail = _orderService.SearchOrders(data);
            IEnumerable<OrderViewModel> orderList = Mapper.Map<List<OrderViewModel>>(orderDetail);
            //IEnumerable<OrderDetailDto> orderDetail = _orderService.SearchOrders(data);
            //IEnumerable<OrderViewModel> orderList = Mapper.Map<List<OrderViewModel>>(orderDetail);
            ViewBag.Order = data.OrderType;
            return View("_orderPartial", orderList);
        }
        [HttpPost]
        public ActionResult CreateUser(M_GB_USER user)
        {
            string success = "";
            string error = "";
            try
            {
                string userId = _userService.AddUser(user);
                if (userId != "0")
                {
                    //AccountController reg = new AccountController();
                    //reg.UserRegister(user.EMAIL, user.PASSWORD ?? "123456");

                    if (user.USER_ID != "0")
                        success = "Details Successfully Added";
                    else if (user.USER_ID == "0")
                        error = "user already exists. please choose a different username";
                    TempData["SuccessMessage"] = success;
                    TempData["ErrorMessage"] = error;
                    return RedirectToAction("CreateOrder", new { id=user.USER_ID });
                    // return RedirectToAction("Register", "Account", new { Email = user.EMAIL, Password = user.PASSWORD });
                }
                else
                    error = "User already exists please choose another Email / Login Id";

            }
            catch (Exception exception)
            {
                error = exception.ToString();
            }
            finally
            {
                TempData["SuccessMessage"] = success;
                TempData["ErrorMessage"] = error;
            }
            return RedirectToAction("CreateOrder", new { id = user.USER_ID });

        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult VoucherSearch(string voucher, string[] orderIdList)
        {
            M_GB_USER_DISCOUNT data = _discountService.GetDiscountVoucher(voucher);
            int term = 1;
            if (data != null && orderIdList != null)
            {
                if (data.VOUCHER_EXPIRY_DATE != null && data.VOUCHER_EXPIRY_DATE.Value <= DateTime.Now)
                {
                    return Json("Voucher code expiry date completed", JsonRequestBehavior.AllowGet);
                }
                decimal discount = Convert.ToDecimal(data.DISCOUNT);
                    foreach (string ordId in orderIdList)
                    {
                        GB_ORDER_DETAIL order = _orderService.GetSubOrderDetails(Convert.ToInt32(ordId));
                        if (order != null)
                        {
                            decimal ordAmount = order.UNIT_PRICE ?? 0;
                            decimal disAmount = ((ordAmount)*(term)*(discount))/100;
                            _orderService.VoucherApply(order.ORDER_DETAIL_ID, data.VOUCHER_CODE, disAmount);
                        }
                    }
                    return Json("Success", JsonRequestBehavior.AllowGet);
            }
            if(data != null)
            return Json("Invalid Voucher Code", JsonRequestBehavior.AllowGet);
            if (orderIdList != null)
                return Json("select at least one product", JsonRequestBehavior.AllowGet);
            return Json("Invalid Voucher Code", JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult SendPaymentLink(string id, string OrderId)
        {
            try
            {
                int orderId = Convert.ToInt32(OrderId);
                OrderDetailDto data = _orderService.GetOrderDetails(orderId);
              //  Notifications notifications = new Notifications();
               // notifications.SendPaymentLink(data.User.EMAIL,data.UserName,"");
            }catch
            {

            }
            return Json("SendPaymentLink", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [AllowAnonymous]
        public JsonResult UpdateOrderStatus(string id, string status)
        {
            try
            {
                int data = _orderService.UpdateOrderStatus(Convert.ToInt32(id), Convert.ToInt32(status));
                return Json("", JsonRequestBehavior.AllowGet);                
            }
            catch
            {

            }
            return Json("SendPaymentLink", JsonRequestBehavior.AllowGet);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult UpdateOrderList(string[] orderIdList,int status)
        {
            try
            {
                if (orderIdList == null)
                    return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Empty),
                        JsonRequestBehavior.DenyGet);
                else
                {
                    foreach (string i in orderIdList)
                    {
                        _orderService.UpdateOrderStatus(Convert.ToInt32(i),status);
                    }
                    return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Success),
                        JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception)
            {
                return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Failed), JsonRequestBehavior.DenyGet);
            }

        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult UpdatePickupOrderList(string[] orderIdList, int status, string LogisticType)
        {
            try
            {
                if (orderIdList == null)
                    return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Empty),
                        JsonRequestBehavior.DenyGet);
                else
                {
                    foreach (string i in orderIdList)
                    {
                        _orderService.UpdatePickupOrderList(Convert.ToInt32(i), status, LogisticType,"");
                    }
                    return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Success),
                        JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception)
            {
                return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Failed), JsonRequestBehavior.DenyGet);
            }

        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult MergeOrder(string[] orderIdList)
        {
            try
            {
                if (orderIdList == null)
                    return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Empty),
                        JsonRequestBehavior.DenyGet);
                string status = _orderService.MergeOrder(orderIdList);
                return Json(status,
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Failed), JsonRequestBehavior.AllowGet);
            }

        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult SplitOrder(int orderId, int ordersCount)
        {
            try
            {
                if (orderId == 0)
                    return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Empty),
                        JsonRequestBehavior.AllowGet);
                int status = _orderService.SplitOrder(orderId, ordersCount);
                if (status == (int)CustomStatus.ReturnStatus.message)
                {
                    return Json("You cannot split order", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Success),
                       JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Failed), JsonRequestBehavior.AllowGet);
            }

        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult AddProQty(int orderId, int quantity,int price)
        {
            try
            {
                if (orderId == 0)
                    return Json("Please change the Quantity", JsonRequestBehavior.AllowGet);
                else
                {
                  int status =  _orderService.AddProQty(orderId,quantity,price);
                    if(status == (int)CustomStatus.ReturnStatus.success)
                        return Json("Quantity updated successfully", JsonRequestBehavior.AllowGet);
                    else if (status == (int)CustomStatus.ReturnStatus.message)
                        return Json("Change the Quantity", JsonRequestBehavior.AllowGet);
                    return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Success), JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception)
            {
                return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Failed), JsonRequestBehavior.AllowGet);
            }

        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult SendToBlueDart(string[] orderIdList, int status, string LogisticType)
        {
            try
            {
                if (orderIdList == null)
                    return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Empty),
                        JsonRequestBehavior.AllowGet);
                else
                {
                    foreach (string i in orderIdList)
                    {
                       OrderDetailDto order = _orderService.GetSingleOrderDetails(Convert.ToInt32(i));
                       PickUp pick = new PickUp();
                       string logisticToken = pick.AddPickup(order);
                       LogisticType = "Blue Dart";
                       // _orderService.SendToBlueDart(Convert.ToInt32(i), status, LogisticType);
                       _orderService.UpdatePickupOrderList(Convert.ToInt32(i), status, LogisticType, logisticToken);
                    }
                    return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Success),
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Failed), JsonRequestBehavior.AllowGet);
            }

        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult UpdateOrderListToShipping(string[] orderIdList, int status)
        {
            string res = "";
            try
            {
                if (orderIdList == null)
                    return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Empty),
                        JsonRequestBehavior.AllowGet);
                else
                {
                    foreach (string i in orderIdList)
                    {
                        OrderDetailDto order = _orderService.GetSingleOrderDetails(Convert.ToInt32(i));
                        BDWayBill wayBillData = new BDWayBill();
                        if(order != null)
                        {
                            res = i;
                            wayBillData.shipper = new BDShipperDTO();
                            wayBillData.services = new BDServicesDTO();
                            wayBillData.consignee = new BDConsigneeDTO();
                            if (order.userAddress != null){
                            wayBillData.shipper.CustomerName = wayBillData.consignee.ConsigneeName = order.userAddress.FIRSTNAME + " " + order.userAddress.LASTNAME;
                            wayBillData.shipper.CustomerAddress1 = wayBillData.consignee.ConsigneeAddress1 = order.userAddress.ADDRESS_1;
                            wayBillData.shipper.CustomerAddress2 = wayBillData.consignee.ConsigneeAddress2 = order.userAddress.landmark;
                            wayBillData.shipper.CustomerAddress3 = wayBillData.consignee.ConsigneeAddress3 = order.userAddress.landmark;
                            wayBillData.shipper.CustomerPincode = wayBillData.consignee.ConsigneePincode = "400030";
                            wayBillData.shipper.CustomerEmailID = order.EMAIL;
                            wayBillData.shipper.CustomerMobile = wayBillData.consignee.ConsigneeMobile = order.userAddress.mob_no.ToString();
                            wayBillData.services.ProductCode = "A";
                            wayBillData.services.ActualWeight = "1.2";
                            wayBillData.services.CreditReferenceNo = order.ORDER_ID.ToString();
                            //wayBillData.services.CreditReferenceNo = "1015";
                            wayBillData.services.PickupDate = DateTime.Now.AddDays(1);
                            wayBillData.services.PickupTime = "1600";
                            wayBillData.services.PieceCount = Convert.ToInt32(order.Qty);
                            wayBillData.services.CollectableAmount = wayBillData.services.DeclaredValue = order.UNIT_PRICE.ToString();
                            wayBillData.services.DeclaredValue = order.UNIT_PRICE.ToString();
                            WayBill wayBill = new WayBill();
                            BDResponseDTO response = wayBill.GenerateAWB(wayBillData);
                            _orderService.UpdateOrderListToShipping(Convert.ToInt32(i), status, response.AWBNo);
                            res = response.Message;
                        }
                        }
                    }
                    //return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Success),
                    //    JsonRequestBehavior.AllowGet);
                    return Json(res,JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Failed), JsonRequestBehavior.DenyGet);
            }

        }
        [HttpPost]
        public ActionResult SearchOrders(OrderSerch serch)
        {
            Mapper.CreateMap<GB_ORDER, OrderViewModel>();
            IEnumerable<GB_ORDER> orderDetail = _orderService.SearchOrders(serch);
            IEnumerable<OrderViewModel> orderList = Mapper.Map<List<OrderViewModel>>(orderDetail);
            if(orderList != null && orderList.Count() != 0)
            {
                foreach(OrderViewModel order in orderList)
                {
                    order.UNIT_PRICE = orderDetail.FirstOrDefault(a => a.ORDER_ID == order.ORDER_ID).GB_ORDER_DETAIL.Sum(a => a.UNIT_PRICE);
                    order.Qty = orderDetail.FirstOrDefault(a => a.ORDER_ID == order.ORDER_ID).GB_ORDER_DETAIL.Sum(a => a.QUANTITY) ?? 0;
                    order.UserName = orderDetail.FirstOrDefault(a => a.ORDER_ID == order.ORDER_ID).M_GB_USER.USER_NAME;
                }
            }
            return View("orderList", orderList);

        }

        public ActionResult SearchPaymentPendingOrder(OrderSerch serch)
        {
            Mapper.CreateMap<OrderDetailDto, OrderViewModel>();
            IEnumerable<GB_ORDER> orderDetail = _orderService.SearchPaymentPendingOrder(serch);
            IEnumerable<OrderDetailDto> data = (from r in orderDetail
                    select new OrderDetailDto
                    {
                        ORDER_ID = r.ORDER_ID,
                        CREATED_AT = r.CREATED_AT,
                        UserName = r.M_GB_USER.USER_NAME + r.M_GB_USER.FIRST_NAME,
                        STATUS = r.STATUS,
                        Qty = r.GB_ORDER_DETAIL.FirstOrDefault().QUANTITY,
                        CHECKOUTTYPE = r.GB_ORDER_DETAIL.FirstOrDefault().CHECKOUTTYPE,
                        UNIT_PRICE = r.GB_ORDER_DETAIL.Sum(a => a.UNIT_PRICE),
                        PAYMENT_TYPE = r.PAYMENT_TYPE,
                        LOGISTIC_TOKEN = r.LOGISTIC_TOKEN,
                        LOGISTIC_AWB = r.LOGISTIC_AWB,
                        DISCOUNT_AMOUNT = r.GB_ORDER_DETAIL.Sum(a => a.DISCOUNT_AMOUNT),
                        DISCOUNT_VOUCHER = r.GB_ORDER_DETAIL.FirstOrDefault().DISCOUNT_VOUCHER
                    });
            IEnumerable<OrderViewModel> orderList = Mapper.Map<List<OrderViewModel>>(data);
            //ViewBag.Category = new SelectList(_productService.GetCategories().ToList(), "CATEGORY_ID", "CATEGORY_DESC");
            IEnumerable<OrderViewModel> models = orderList.ToList();
            return View("orderPaymentPendingPartial", models);
        }
        public ActionResult OrderBack(int status = 0, int id = 0, int paymetType = 0)
        {
            switch(status)
            {
                case (int)CustomStatus.OrderStatus.Authorize:
                    if(paymetType == (int)CustomStatus.PaymentTypes.COD)
                    return RedirectToAction("AuthorizeCOD","Order");
                    else
                        return RedirectToAction("AuthorizeCOD", "Order");
                case (int)CustomStatus.OrderStatus.Cancel:
                    return RedirectToAction("AuthorizePrepaid", "Order");
                    break;
                case (int)CustomStatus.OrderStatus.Delivered:
                    return RedirectToAction("Shipped","Order");
                case (int)CustomStatus.OrderStatus.Dispatched:
                    return RedirectToAction("Dispatched","Order");
                case (int)CustomStatus.OrderStatus.Pickup:
                    return RedirectToAction("WaitingPickup","Order");
                case (int)CustomStatus.OrderStatus.ReadyToPickUp:
                    return RedirectToAction("Shipped", "Order");
            }
            return RedirectToAction("OrderDetails","Order", new {id=id});
        }
    }
}