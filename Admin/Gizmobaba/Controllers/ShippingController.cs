﻿using Gizmobaba.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Gizmobaba.Utility;
using AutoMapper;
using Gizmobaba.Models;

namespace Gizmobaba.Controllers
{
    [Authorize]
    public class ShippingController : BaseController<ShippingController>
    {
        private readonly IShippingService _shippingService;

        public ShippingController(IShippingService _shippingService)
        {
            this._shippingService = _shippingService;
        }

        // GET: Shipping
        public ActionResult Index(int id=0)
        {
            ViewBag.Country = new SelectList(_shippingService.GetShippingCountries(), "COUNTRY_ID", "COUNTRY_DESC");


            IEnumerable<M_GB_SHIPPING> shippingsList = new List<M_GB_SHIPPING>();
            Mapper.CreateMap<M_GB_SHIPPING, AvailableShippingCodes>();
            if (id == 0)
                shippingsList = _shippingService.GetShippingCodes().Take(10).ToList();
            else
                shippingsList = _shippingService.GetShippingCodesBycount(id);
            IEnumerable<AvailableShippingCodes> shippingCodeList = Mapper.Map<List<AvailableShippingCodes>>(shippingsList);
            if(id == 0)
            return View(shippingCodeList);
            else
                return View("_shippingPartial", shippingCodeList);
        }

        // GET: Shipping/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Shipping/Create
        public ActionResult Create()
        {
            Mapper.CreateMap<ShippingMethodDto, ShippingMethodModel>();
            IEnumerable<ShippingMethodDto> shippingMethod = _shippingService.GetShippingMethods();
            IEnumerable<ShippingMethodModel> shippingMethodList = Mapper.Map<List<ShippingMethodModel>>(shippingMethod);

            Mapper.CreateMap<M_GB_COUNTRY, ShippingCountriesModel>();
            IEnumerable<M_GB_COUNTRY> shippingCountries = _shippingService.GetShippingCountries().OrderBy(a=>a.COUNTRY_DESC);
            IEnumerable<ShippingCountriesModel> shippingCountriesList = Mapper.Map<List<ShippingCountriesModel>>(shippingCountries);

            AvailableShippingCodes model = new AvailableShippingCodes
            {
                ShippingMethod = shippingMethodList,
                ShippingCountries = shippingCountriesList
            };
            return View(model);
        }

        // POST: Shipping/Create
        [HttpPost]
        public ActionResult Create(AvailableShippingCodes shippingdto)
        {
            try
            {
                // TODO: Add insert logic here
                if (shippingdto.SelectedShippingCodes != null)
                    foreach (int i in shippingdto.SelectedShippingCodes)
                    {
                        M_GB_SHIPPING shipping = new M_GB_SHIPPING
                        {
                            SHIPPING_CODE = i.ToString(),
                            SHIPPING_DESC = shippingdto.SHIPPING_DESC,
                            STATUS = 1,      
                            CREATED_ON = DateTime.Now
                        };
                        _shippingService.Add(shipping);
                    }
                return RedirectToAction("Index");

            }
            catch
            {
                return View();
            }
        }

        // GET: Shipping/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Shipping/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
           int shippingId = _shippingService.Delete(id);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        // POST: Shipping/Delete/5
        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetStates(int id)
        {
            IEnumerable<StatesDto> data = _shippingService.GetStates(id).OrderBy(a=>a.STATE);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetRegions(int id)
        {
            IEnumerable<RegionDto> data = _shippingService.GetRegions(id).OrderBy(a=>a.REGION);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
