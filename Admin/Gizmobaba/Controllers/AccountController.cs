﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Gizmobaba.Models;
using Gizmobaba.Service.Interfaces;
using Gizmobaba.Service;
using Gizmobaba.Utility;
using System.Collections.Generic;
using AutoMapper;

namespace Gizmobaba.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }
        private readonly IPermissionsService _permissionsService = new PermissionsService();
        private readonly IUserService _userService = new UserService();
        private readonly IShippingService _shippingService =  new ShippingService();
        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager )
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            Mails mails = new Mails();
            mails.AdminMails();
            var user = User.Identity;
            if (user.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        private ApplicationSignInManager _signInManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set { _signInManager = value; }
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            //if (!ModelState.IsValid)
            //{
            //    return View(model);
            //}

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    
                        var user = await UserManager.FindByNameAsync(model.Email);
                        string roleId = user.Roles.FirstOrDefault().RoleId;
                        if (roleId == ((int)CustomStatus.UserTypes.Admin).ToString())
                        {
                            IEnumerable<FeatureTypeDto> getFeatureTypes = _permissionsService.GetFeatureTypes();
                            IEnumerable<FeatureTypeDto> getFeatureType = _permissionsService.GetFeatureAccessByRoleId(Convert.ToInt32(roleId));
                            IEnumerable<FeatureTypeModel> getFeatureTypeList = getFeatureType.Select(featureTypeDto => new FeatureTypeModel
                            {
                                FEATURE_TYPE = featureTypeDto.FEATURE_TYPE,
                                FEATURE_TYPE_ID = featureTypeDto.FEATURE_TYPE_ID,
                                STATUS = featureTypeDto.STATUS,
                                ROLE_ID = featureTypeDto.ROLE_ID,
                                ID = featureTypeDto.ID,
                            }).ToList();
                            TempData["Role"] = getFeatureTypeList;

                            Mapper.CreateMap<M_GB_USER, UserModel>();
                            M_GB_USER userData = _userService.GetUserByEmailID(model.Email);
                            UserModel userDetails = Mapper.Map<UserModel>(userData);
                            TempData["User"] = userDetails;
                            if (roleId == "3")
                                return RedirectToAction("MasterFranchiseDashboard", "Home");
                            if (roleId == "5")
                                return RedirectToAction("VendorDashboard", "Home");
                            if (!string.IsNullOrEmpty(returnUrl))
                return RedirectToLocal(returnUrl);
                            else
                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            M_GB_USER data = _userService.GetUserByEmailID(model.Email);
                            IEnumerable<FeatureTypeDto> getFeatureTypes = _permissionsService.GetFeatureTypes();
                            IEnumerable<FeatureTypeDto> getFeatureType = _permissionsService.GetFeatureAccessByUser(data.USER_ID);
                            IEnumerable<FeatureTypeModel> getFeatureTypeList = getFeatureType.Select(featureTypeDto => new FeatureTypeModel
                            {
                                FEATURE_TYPE = featureTypeDto.FEATURE_TYPE,
                                FEATURE_TYPE_ID = featureTypeDto.FEATURE_TYPE_ID,
                                STATUS = featureTypeDto.STATUS,
                                ROLE_ID = featureTypeDto.ROLE_ID,
                                ID = featureTypeDto.ID,
                            }).ToList();
                            TempData["Role"] = getFeatureTypeList;
                            Mapper.CreateMap<M_GB_USER, UserModel>();
                            M_GB_USER userData = _userService.GetUserByEmailID(model.Email);
                            UserModel userDetails = Mapper.Map<UserModel>(userData);
                            TempData["User"] = userDetails;
                            if (roleId == "3")
                                return RedirectToAction("MasterFranchiseDashboard", "Home");
                            if (roleId == "5")
                                return RedirectToAction("VendorDashboard", "Home");
                              if (string.IsNullOrEmpty(returnUrl))
                return RedirectToLocal(returnUrl);
                            else
                            return RedirectToAction("UserProfile", "User");
                        }
                return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            var user = await UserManager.FindByIdAsync(await SignInManager.GetVerifiedUserIdAsync());
            if (user != null)
            {
                var code = await UserManager.GenerateTwoFactorTokenAsync(user.Id, provider);
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent:  model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
       // [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);
                    
                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                    TempData["SuccessMessage"] = "Details Successfully Added";
                    return RedirectToAction("Create", "User");
                    //  return RedirectToAction("Index", "Home");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            //TempData["ErrorMessage"] = "Try again";
            //return RedirectToAction("Create", "User");
            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
       // [HttpPost]
      // [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Login", "Account");
        }
        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        public async Task<ActionResult> CreateUser(UserModel model)
        {
            //if (ModelState.IsValid)
            //{
                var user = new ApplicationUser { UserName = model.EMAIL, Email = model.EMAIL };
                var account = new AccountController(); 
                var result = await UserManager.CreateAsync(user, model.PASSWORD);
                if (result.Succeeded)
                {
                  //  account.UserManager.AddToRole(user.Id, "5");
                    var data = await UserManager.AddToRoleAsync(user.Id, model.RoleName);
                   // await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                    TempData["SuccessMessage"] = "Details Successfully Added";
                    return RedirectToAction("Create", "User");
                    //  return RedirectToAction("Index", "Home");
                }
                AddErrors(result);
            //}

            // If we got this far, something failed, redisplay form
            TempData["ErrorMessage"] = result.Errors.FirstOrDefault();
            TempData["User"] = model;
            return RedirectToAction("Create", "User",model);
            //return View(model);
        }

        [AllowAnonymous]
        public async void UserRegister(string email,string password)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = email, Email = email };
                var account = new AccountController();
                var result = await UserManager.CreateAsync(user, password);
                if (result.Succeeded)
                {
                   // await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
        }
        [HttpGet]
        public ActionResult GetRoles(string url)
        {
            string id = User.Identity.GetUserId();
            var user =  UserManager.FindByIdAsync(id);
            var email = User.Identity.Name;
            var identityUserRole = user.Result.Roles.FirstOrDefault();
            if (identityUserRole != null)
            {
                string roleId = identityUserRole.RoleId;
                //  IEnumerable<FeatureTypeDto> getFeatureTypes = _permissionsService.GetFeatureTypes();
                IEnumerable<FeatureTypeDto> getFeatureType = _permissionsService.GetFeatureAccessByRoleId(Convert.ToInt32(roleId));
                IEnumerable<FeatureTypeModel> getFeatureTypeList = getFeatureType.Select(featureTypeDto => new FeatureTypeModel
                {
                    FEATURE_TYPE = featureTypeDto.FEATURE_TYPE,
                    FEATURE_TYPE_ID = featureTypeDto.FEATURE_TYPE_ID,
                    STATUS = featureTypeDto.STATUS,
                    ROLE_ID = featureTypeDto.ROLE_ID,
                    ID = featureTypeDto.ID,
                }).ToList();
                TempData["Role"] = getFeatureTypeList;

                Mapper.CreateMap<M_GB_USER, UserModel>();
                M_GB_USER userData = _userService.GetUserByEmailID(email);
                UserModel userDetails = Mapper.Map<UserModel>(userData);
                TempData["User"] = userDetails;
            }
            return RedirectToLocal(url);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ResetPasswordViewModel model, string returnUrl)
        {
            //if (!ModelState.IsValid)
            //{
            //    return View(model);
            //}
            try
            {
                string id = User.Identity.GetUserId();
                var data = await UserManager.ChangePasswordAsync(id, model.OldPassword, model.Password);
                TempData["SuccessMessage"] = "Password changed successfully";
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
            }
            return RedirectToAction("UserProfile", "User");
        }
        [AllowAnonymous]
        [HttpGet]
        public ActionResult testPincode()
        {
            return View();
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult testPincode(string pincode)
        {
            //WayBill wayBill = new WayBill();
            //wayBill.WayBillGenration();

            //Pincodes data = new Pincodes();
            //int status = data.TestPincode(pincode);
            //if(status == 1)
            //ViewBag.Message = "Succes";
            //else
            //    ViewBag.Message = "Fail";
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddDealer(Dealer dealer)
        {
            List<GB_USER_CATEGORY> SelectedCategoryLists = new List<GB_USER_CATEGORY>();
            if (dealer != null && dealer.USERDETAIL_ID != 0)
            {
                if (dealer.SelectedCategory != null)
                    foreach (int i in dealer.SelectedCategory)
                    {
                        SelectedCategoryLists.Add(new GB_USER_CATEGORY
                        {
                            CATEGORY_ID = i,
                            USER_ID = dealer.USERDETAIL_ID,
                            STATUS = (int)CustomStatus.DefaultStatus.Active
                        });
                    }
                M_GB_USERDETAILS userdetails = new M_GB_USERDETAILS
                {
                    NAME = dealer.USER_NAME,
                    COMPANY = dealer.COMPANY,
                    ROLE_ID = (int) CustomStatus.UserTypes.Dealer,
                    CREATED_ON = DateTime.Now,
                    AMOUNT = dealer.AMOUNT,
                    WALLET = dealer.WALLET,
                    COUNTRY = dealer.COUNTRY,
                    STATE = dealer.STATE,
                    CITY = dealer.CITY,
                    STATUS = (int) CustomStatus.DefaultStatus.Active,
                    EMAIL = dealer.EMAIL,
                    USER_ID = dealer.USER_ID,
                    USERDETAIL_ID = dealer.USERDETAIL_ID,
                    ADDRESS = dealer.ADDRESS,
                    PINCODE = dealer.PIN,
                    MOBILE_NUMBER = dealer.ALT_MOB
                };
                userdetails.GB_USER_CATEGORY = SelectedCategoryLists;
                if (dealer.CREATED_BY != null)
                    userdetails.CREATED_BY = Convert.ToInt32(dealer.CREATED_BY);
                else
                    userdetails.CREATED_BY = (int) CustomStatus.UserTypes.Admin;
                string userDetailsId = _userService.AddDealerDetails(userdetails);
                GB_DEALER_RECHARGE recharge = new GB_DEALER_RECHARGE
                {
                    RECHARGE_POINTS = Convert.ToInt32(dealer.WALLET),
                    ROLE_ID = (int)CustomStatus.UserTypes.Dealer,
                    USERDETAIL_ID = Convert.ToInt32(userDetailsId),
                    USER_ADMIN_ID = dealer.USER_ADMIN_ID,
                    STATUS = (int)CustomStatus.DefaultStatus.Active,
                    CREATED_ON = DateTime.Now,
                    CREATED_BY = userDetailsId
                };
                string status = _userService.UpdateRecharge(recharge);
                TempData["SuccessMessage"] = "Dealer has been Updated.";
                return RedirectToAction("Dealer", "User");
            }
            else
            {
                var user = new ApplicationUser {UserName = dealer.EMAIL, Email = dealer.EMAIL};
                new AccountController();
                var result = await UserManager.CreateAsync(user, "Welcome@123");
                if (result.Succeeded)
                {
                    var data = await UserManager.AddToRoleAsync(user.Id, "Dealer");
                    dealer.CREATED_ON = DateTime.Now;
                    dealer.UPDATED_ON = DateTime.Now;
                    dealer.ROLE_ID = (int) CustomStatus.UserTypes.Dealer;
                    string userId = _userService.AddDealerUser(dealer);
                    M_GB_USERDETAILS userdetails = new M_GB_USERDETAILS
                    {
                        NAME = dealer.USER_NAME,
                        COMPANY = dealer.COMPANY,
                        ROLE_ID = (int) CustomStatus.UserTypes.Dealer,
                        CREATED_ON = DateTime.Now,
                        AMOUNT = dealer.AMOUNT,
                        WALLET = dealer.WALLET,
                        CREATED_BY = Convert.ToInt32(dealer.CREATED_BY),
                        EMAIL = dealer.EMAIL,
                        STATE = dealer.STATE,
                        STATUS = (int)CustomStatus.DefaultStatus.Active,
                        COUNTRY = dealer.COUNTRY,
                        USER_ID = userId,
                        ADDRESS = dealer.ADDRESS,
                        PINCODE = dealer.PIN,
                        MOBILE_NUMBER = dealer.ALT_MOB,
                        CITY = dealer.CITY
                    };
                    if(dealer.SelectedCategory != null)
                    foreach (int i in dealer.SelectedCategory)
                    {
                        SelectedCategoryLists.Add(new GB_USER_CATEGORY
                        {
                            CATEGORY_ID = i,
                            USER_ID = dealer.USERDETAIL_ID,
                            STATUS = (int)CustomStatus.DefaultStatus.Active
                        });
                    }
                    userdetails.GB_USER_CATEGORY = SelectedCategoryLists;
                    string userDetailsId = _userService.AddDealerDetails(userdetails);
                    GB_DEALER_RECHARGE recharge = new GB_DEALER_RECHARGE
                    {
                        RECHARGE_POINTS = Convert.ToInt32(dealer.WALLET),
                        ROLE_ID = (int) CustomStatus.UserTypes.Dealer,
                        USERDETAIL_ID = Convert.ToInt32(userDetailsId),
                        USER_ADMIN_ID =  Convert.ToInt32(dealer.CREATED_BY),
                        STATUS = (int) CustomStatus.DefaultStatus.Active,
                        CREATED_ON = DateTime.Now,
                        CREATED_BY = userDetailsId
                    };
                    _userService.UpdateRecharge(recharge);


                    TempData["SuccessMessage"] = "Dealer has been Created.";
                    return RedirectToAction("Dealer", "User");
                }
                else
                {
                    TempData["ErrorMessage"] = result.Errors.FirstOrDefault();
                }
            }
            DealerDto model = new DealerDto();
            Mapper.CreateMap<Dealer, DealerDto>();
            model = Mapper.Map<DealerDto>(dealer);
            Mapper.CreateMap<M_GB_COUNTRY, ShippingCountriesModel>();
            IEnumerable<M_GB_COUNTRY> countries = _shippingService.GetShippingCountries().OrderBy(a => a.COUNTRY_DESC);
            IEnumerable<ShippingCountriesModel> countriesList = Mapper.Map<List<ShippingCountriesModel>>(countries);
            model.Countries = countriesList;
            DealerView dealerView = new DealerView();
            dealerView.Dealer = model;
            return View("~/Views/User/DealerProfile.cshtml", dealerView);
        }
        [HttpPost]
        public async Task<ActionResult> AddMasterFranchise(Dealer dealer)
        {
            if (dealer != null && dealer.USERDETAIL_ID != 0)
            {
                M_GB_USERDETAILS userdetails = new M_GB_USERDETAILS
                {
                    NAME = dealer.USER_NAME,
                    COMPANY = dealer.COMPANY,
                    ROLE_ID = (int) CustomStatus.UserTypes.Master_Franchise,
                    CREATED_ON = DateTime.Now,
                    AMOUNT = dealer.AMOUNT,
                    WALLET = dealer.WALLET,
                    CREATED_BY = (int) CustomStatus.UserTypes.Admin,
                    COUNTRY = dealer.COUNTRY,
                    STATE = dealer.STATE,
                    CITY = dealer.CITY,
                    STATUS = (int) CustomStatus.DefaultStatus.Active,
                    EMAIL = dealer.EMAIL,
                    USER_ID = dealer.USER_ID,
                    USERDETAIL_ID = dealer.USERDETAIL_ID,
                    ADDRESS = dealer.ADDRESS,
                    PINCODE = dealer.PIN,
                    MOBILE_NUMBER = dealer.ALT_MOB
                };
                string userDetailsId = _userService.AddMasterFranchiseDetails(userdetails);
                GB_DEALER_RECHARGE recharge = new GB_DEALER_RECHARGE
                {
                    USERDETAIL_ID = dealer.USERDETAIL_ID,
                    RECHARGE_POINTS = Convert.ToInt32(dealer.WALLET),
                    ROLE_ID = (int)CustomStatus.UserTypes.Master_Franchise,
                    CREATED_BY = CustomStatus.UserStatus.Admin.ToString(),
                    CREATED_ON = DateTime.Now
                };
                string status = _userService.UpdateRecharge(recharge);
                TempData["SuccessMessage"] = "Master Franchise has been Updated.";
                return RedirectToAction("MasterFranchise", "User");
            }
            else
            {
                var user = new ApplicationUser {UserName = dealer.EMAIL, Email = dealer.EMAIL};
                var result = await UserManager.CreateAsync(user, "Welcome@123");
                if (result.Succeeded)
                {
                    var data = await UserManager.AddToRoleAsync(user.Id, "Master Franchise");
                    dealer.CREATED_ON = DateTime.Now;
                    dealer.UPDATED_ON = DateTime.Now;
                    dealer.ROLE_ID = (int) CustomStatus.UserTypes.Master_Franchise;
                    string userId = _userService.AddDealerUser(dealer);
                    if (userId != "0")
                    {
                        M_GB_USERDETAILS userdetails = new M_GB_USERDETAILS
                        {
                            NAME = dealer.USER_NAME,
                            COMPANY = dealer.COMPANY,
                            ROLE_ID = (int)CustomStatus.UserTypes.Master_Franchise,
                            CREATED_ON = DateTime.Now,
                            AMOUNT = dealer.AMOUNT,
                            WALLET = dealer.WALLET,
                            CREATED_BY = (int)CustomStatus.UserTypes.Admin,
                            COUNTRY = dealer.COUNTRY,
                            STATE = dealer.STATE,
                            CITY = dealer.CITY,
                            STATUS = (int)CustomStatus.DefaultStatus.Active,
                            EMAIL = dealer.EMAIL,
                            USER_ID = userId,
                            PINCODE = dealer.PIN,
                            ADDRESS = dealer.ADDRESS,
                            MOBILE_NUMBER = dealer.ALT_MOB
                        };
                        string userDetailsId = _userService.AddMasterFranchiseDetails(userdetails);
                        GB_DEALER_RECHARGE recharge = new GB_DEALER_RECHARGE
                        {
                            RECHARGE_POINTS = Convert.ToInt32(dealer.WALLET),
                            ROLE_ID = (int)CustomStatus.UserTypes.Dealer,
                            USERDETAIL_ID = Convert.ToInt32(userDetailsId),
                            USER_ADMIN_ID = (int)CustomStatus.UserTypes.Admin,
                            STATUS = (int)CustomStatus.DefaultStatus.Active,
                            CREATED_ON = DateTime.Now,
                            CREATED_BY = CustomStatus.UserTypes.Admin.ToString()
                        };
                        _userService.UpdateRecharge(recharge);

                        string smsBody = SMSTempletes.GetSMSBody(SMSTempleteTypes.Dealer);
                        smsBody = string.Format(smsBody, dealer.NAME, dealer.EMAIL, "Welcome@123");
                        int smsResult = Notifications.SendSMS(dealer.ALT_MOB, smsBody);

                        TempData["SuccessMessage"] = "Master Franchise has been Created.";
                        return RedirectToAction("MasterFranchise", "User");
                    }
                    else
                        TempData["ErrorMessage"] = "email id already exists";
                }
                else
                {
                    TempData["ErrorMessage"] = result.Errors.FirstOrDefault();
                }
            }
            DealerDto model = new DealerDto();
            Mapper.CreateMap<Dealer, DealerDto>();
            model = Mapper.Map<DealerDto>(dealer);
            Mapper.CreateMap<M_GB_COUNTRY, ShippingCountriesModel>();
            IEnumerable<M_GB_COUNTRY> countries = _shippingService.GetShippingCountries().OrderBy(a => a.COUNTRY_DESC);
            IEnumerable<ShippingCountriesModel> countriesList = Mapper.Map<List<ShippingCountriesModel>>(countries);
            model.Countries = countriesList;
            if (model.COUNTRY != null && model.COUNTRY != 0)
            {
                Mapper.CreateMap<StatesDto, StatesModel>();
                IEnumerable<StatesDto> states = _shippingService.GetStates(Convert.ToInt32(dealer.COUNTRY)).OrderBy(a => a.STATE);
                IEnumerable<StatesModel> StatesList = Mapper.Map<List<StatesModel>>(states);
                model.States = StatesList;
            }
            if (model.STATE != 0)
            {
                Mapper.CreateMap<RegionDto, RegionModel>();
                IEnumerable<RegionDto> Cities = _shippingService.GetRegions(dealer.STATE).OrderBy(a => a.REGION);
                IEnumerable<RegionModel> CityList = Mapper.Map<List<RegionModel>>(Cities);
                model.Cityes = CityList;
            }
            DealerView dealerView = new DealerView();
            dealerView.Dealer = model;
            return View("~/Views/User/MasterFranchiseProfile.cshtml", dealerView);
        }
        [HttpPost]
        public async Task<ActionResult> AddVendere(Dealer dealer)
        {
            if (string.IsNullOrEmpty(dealer.USER_ID))
            {
                var user = new ApplicationUser { UserName = dealer.EMAIL, Email = dealer.EMAIL };
                var result = await UserManager.CreateAsync(user, "Welcome@123");
                if (result.Succeeded)
                {
                    var data = await UserManager.AddToRoleAsync(user.Id, "Global Vender");
                    dealer.CREATED_ON = DateTime.Now;
                    dealer.UPDATED_ON = DateTime.Now;
                    dealer.ROLE_ID = (int)CustomStatus.UserTypes.Global_Vender;
                    string userId = _userService.AddDealerUser(dealer);
                        TempData["SuccessMessage"] = "Global Vender has been Created.";
                    return RedirectToAction("Globalvendor", "User");
                }
                TempData["ErrorMessage"] = result.Errors.FirstOrDefault();
            }
            else
            {
                string userId = _userService.AddDealerUser(dealer);
                TempData["SuccessMessage"] = "Global Vender has been Updated.";
                return RedirectToAction("Globalvendor", "User");
            }
            DealerDto model = new DealerDto();
            Mapper.CreateMap<Dealer, DealerDto>();
            model = Mapper.Map<DealerDto>(dealer);
            Mapper.CreateMap<M_GB_COUNTRY, ShippingCountriesModel>();
            IEnumerable<M_GB_COUNTRY> countries = _shippingService.GetShippingCountries().OrderBy(a => a.COUNTRY_DESC);
            IEnumerable<ShippingCountriesModel> countriesList = Mapper.Map<List<ShippingCountriesModel>>(countries);
            model.Countries = countriesList;
            return View("~/Views/User/VenderProfile.cshtml", model);
        }
       
        [HttpPost]
        [AllowAnonymous]
        public JsonResult EmailConfirmation(string email)
        {
            int result = _userService.CheckEmail(email);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}