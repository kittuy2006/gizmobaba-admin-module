﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Gizmobaba.Models;
using Gizmobaba.Service.Interfaces;
using Gizmobaba.Utility;
using AutoMapper;

namespace Gizmobaba.Controllers
{
    [Authorize]
    public class SettingsController : BaseController<SettingsController>
    {
        // GET: Settings
        private readonly IWalletService _walletService;
        private readonly IPermissionsService _permissionsService;
        private readonly IProductService _productService;
        // GET: CategoryManage

        public SettingsController(IWalletService walletService, IPermissionsService permissionsService, IProductService productService)
        {
            this._walletService = walletService;
            this._permissionsService = permissionsService;
            this._productService = productService;
        }
        public ActionResult Index()
        {

            Mapper.CreateMap<WalletDto, WalletViewModel>();
            WalletDto walletData = _walletService.GetWallet();
            WalletViewModel walletViewModel = Mapper.Map<WalletViewModel>(walletData);


            //WalletViewModel walletViewModel = new WalletViewModel();
            //WalletDto data = _walletService.GetWallet();
            //if (data != null)
            //{
            //    walletViewModel = new WalletViewModel
            //    {
            //        WALLET_ID = data.WALLET_ID,
            //        LINKINGPOSTS = data.LINKINGPOSTS,
            //        MAX_USER_PURCHASE = data.MAX_USER_PURCHASE,
            //        OWN_PURCHASE = data.OWN_PURCHASE,
            //        PURCHASE_CERTAIN_TIME = data.PURCHASE_CERTAIN_TIME,
            //        REFERRING_FRIENDS = data.REFERRING_FRIENDS,
            //        REFFERED_FRIENDS_PURCHASES = data.REFFERED_FRIENDS_PURCHASES,
            //        ROLE_ID = data.ROLE_ID,
            //        SHARINGPOSTS = data.SHARINGPOSTS,
            //        SIGNUP = data.SIGNUP
            //    };
            //}
            return View("AddWallet", walletViewModel);
        }
        [HttpGet]
        public ActionResult LoyaltyPrograms(int id = 0)
        {
            int roleId = id;
            IEnumerable<BonusDto> bonusDto;
            Mapper.CreateMap<BonusDto, BonusView>();
            if (roleId == 0)
                bonusDto = _walletService.GetWalletList();
            else
                bonusDto = _walletService.GetWalletListByRole(roleId);

            IEnumerable<BonusView> bonusData = Mapper.Map<List<BonusView>>(bonusDto);
            BonusTypesViewModel model = new BonusTypesViewModel();
            model.BonusView = bonusData;
            return View(model);
        }
        [HttpPost]
        public ActionResult LoyaltyPrograms(GB_BONUS bonus)
        {
            if (bonus != null)
            {
                if (bonus.ROLE_ID == 0)
                    bonus.ROLE_ID = (int)CustomStatus.UserTypes.Customer;
                _walletService.CreateWallet(bonus);
            }
            return RedirectToAction("LoyaltyPrograms");
        }


        [HttpPost]
        public ActionResult AddWallet(M_GB_WALLET wallet)
        {
            wallet.ROLE_ID = (int)CustomStatus.UserTypes.Customer;
            wallet.CREATEDON = DateTime.Now;
            wallet.STATUS = (int)CustomStatus.Status.Active;
            _walletService.AddWallet(wallet);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult AddDealerWallet(M_GB_WALLET wallet)
        {
            wallet.ROLE_ID = (int)CustomStatus.UserStatus.Dealer;
            wallet.CREATEDON = DateTime.Now;
            wallet.STATUS = 1;
            _walletService.AddWallet(wallet);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult AddMasterFranchiseWallet(M_GB_WALLET wallet)
        {
            wallet.ROLE_ID = (int)CustomStatus.UserStatus.MasterFranchise;
            wallet.CREATEDON = DateTime.Now;
            wallet.STATUS = 1;
            _walletService.AddWallet(wallet);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult AddRole(int id = 0)
        {
            FeatureTypeViewModel featureTypeViewModel = new FeatureTypeViewModel();
            if (id <= 0)
            {
                IEnumerable<FeatureTypeDto> getFeatureType = _permissionsService.GetFeatureTypes();
                IEnumerable<FeatureTypeModel> getFeatureTypeList = getFeatureType.Select(featureTypeDto => new FeatureTypeModel
                {
                    FEATURE_TYPE = featureTypeDto.FEATURE_TYPE,
                    FEATURE_TYPE_ID = featureTypeDto.FEATURE_TYPE_ID,
                    STATUS = featureTypeDto.STATUS,
                    ROLE_ID = featureTypeDto.ROLE_ID,
                    ID = featureTypeDto.ID
                }).ToList();
                featureTypeViewModel.FeatureType = getFeatureTypeList;
            }
            else
            {
                M_GB_ROLES roles = _permissionsService.GetRoleDetails(id);
                RolesModel rolesModel = new RolesModel
                {
                    ROLE_ID = roles.ROLE_ID,
                    ROLE_DESC = roles.ROLE_DESC,
                    DESCRIPTION = roles.DESCRIPTION
                };
                IEnumerable<FeatureTypeDto> getFeatureTypes = _permissionsService.GetFeatureTypes();
                IEnumerable<FeatureTypeDto> getFeatureType = _permissionsService.GetFeatureAccessByRoleId(id);
                IEnumerable<FeatureTypeModel> getFeatureTypeList = getFeatureTypes.Select(featureTypeDto => new FeatureTypeModel
                {
                    FEATURE_TYPE = featureTypeDto.FEATURE_TYPE,
                    FEATURE_TYPE_ID = featureTypeDto.FEATURE_TYPE_ID,
                    STATUS = featureTypeDto.STATUS,
                    ROLE_ID = featureTypeDto.ROLE_ID,
                    ID = featureTypeDto.ID,
                }).ToList();
                foreach (FeatureTypeDto item in getFeatureType)
                {
                    if (item.STATUS == 1)
                    {
                        FeatureTypeModel featureTypeModel =
                            getFeatureTypeList.FirstOrDefault(a => a.FEATURE_TYPE_ID == item.FEATURE_TYPE_ID);
                        if (featureTypeModel != null)
                            featureTypeModel.chkBox = 1;
                    }
                }
                featureTypeViewModel.FeatureType = getFeatureTypeList;
                featureTypeViewModel.RolesModel = rolesModel;
            }
            return View(featureTypeViewModel);
        }

        [HttpPost]
        public ActionResult AddRole(FormCollection froCollection)
        {
            int id = 0;
            string sucMessage = "";
            string errorMessage = "";
            try
            {
                if (Request.Form["RolesModel.ROLE_ID"] != null) id = Convert.ToInt32(Request.Form["RolesModel.ROLE_ID"]);
                M_GB_ROLES role = new M_GB_ROLES
                {
                    ROLE_DESC = Request.Form["RolesModel.ROLE_DESC"],
                    DESCRIPTION = Request.Form["RolesModel.DESCRIPTION"],
                    ROLE_ID = id
                };
                if (role.ROLE_ID == 0)
                    sucMessage += "Role Added";
                else
                    sucMessage += "Role Updated";
                int roleId = _permissionsService.AddRole(role);
                if (roleId != 0)
                {
                    IEnumerable<FeatureTypeDto> getFeatureType = _permissionsService.GetFeatureTypes().Where(a => a.STATUS == 2);
                    List<M_GB_FEATURE_ACCESS> featureTypesList = getFeatureType.Select(item => new M_GB_FEATURE_ACCESS
                    {
                        FEATURE_TYPE_ID = item.FEATURE_TYPE_ID,
                        ROLE_ID = roleId,
                        STATUS = 0,
                        CREATED_ON = DateTime.Now
                    }).ToList();
                    int count = 3;
                    // ReSharper disable once UnusedVariable
                    foreach (var key in froCollection.AllKeys)
                    {
                        if (count < froCollection.AllKeys.Length)
                        {
                            string list = Request.Form[froCollection.AllKeys[count]];
                            if (list.Split(',').Count() != 0)
                            {
                                foreach (string featureTypeId in list.Split(','))
                                {
                                    int Id = Convert.ToInt32(featureTypeId);
                                    var mGbFeatureAccess = featureTypesList.FirstOrDefault(x => x.FEATURE_TYPE_ID == Id);
                                    if (mGbFeatureAccess != null)
                                        mGbFeatureAccess.STATUS = 1;
                                }
                            }
                        }
                        count++;
                    }
                    foreach (M_GB_FEATURE_ACCESS featureAccess in featureTypesList)
                    {
                        _permissionsService.AddFeatureAccessToRole(featureAccess);
                    }
                    sucMessage = "succefuly " + sucMessage;
                }
            }
            catch (Exception exception)
            {
                errorMessage = exception.ToString();
            }
            TempData["SuccessMessage"] = sucMessage;
            TempData["ErrorMessage"] = errorMessage;
            return RedirectToAction("Roles");
        }

        public ActionResult Roles()
        {
            Mapper.CreateMap<RolesDto, RolesModel>();
            IEnumerable<RolesDto> rolesModels = _permissionsService.GetAllRoles();
            IEnumerable<RolesModel> roles = Mapper.Map<List<RolesModel>>(rolesModels);
            return View(roles);
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult DeleteRole(int id)
        {
            _permissionsService.DeleteRole(id);
            return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Success), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult ProductTax(int id = 0)
        {
            TaxModelView model = new TaxModelView();
            Mapper.CreateMap<M_GB_TAX, TaxModel>();
            IEnumerable<M_GB_TAX> taxList = _productService.GetTaxes();
            IEnumerable<TaxModel> taxData = Mapper.Map<List<TaxModel>>(taxList);
            model.taxList = taxData;
            model.taxModel = new TaxModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult AddProductTax(M_GB_TAX data)
        {
            try
            {

                data.STATUS = (int)CustomStatus.DefaultStatus.Active;
                _productService.AddTax(data);
                TempData["Success"] = "Added successfully";
            }
            catch (Exception ex)
            {
                TempData["Exception"] = ex.Message;
            }
            return RedirectToAction("ProductTax");
        }
        [HttpGet]
        public ActionResult EditProductTax(int id = 0)
        {
            M_GB_TAX productTax = _productService.GetProductTax(id);
            TaxModel productTagsViewModel = new TaxModel
            {
                TAX_ID = productTax.TAX_ID,
                TAX = productTax.TAX,
                STATUS = productTax.STATUS,
                TAX_DESC = productTax.TAX_DESC
            };
            return PartialView("~/Views/Shared/_taxPartial.cshtml", productTagsViewModel);
        }
        [HttpPost]
        [AllowAnonymous]
        public JsonResult DeleteProductTax(string id)
        {
            if (id != null)
                id = _productService.DeleteProductTax(Convert.ToInt32(id));
            return Json(id, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CheckoutOptions()
        {
            Mapper.CreateMap<M_GB_Payment_GatWay, PaymentModel>();
            IEnumerable<M_GB_Payment_GatWay> paymentList = _walletService.GetPaymentgatwayList();
            IEnumerable<PaymentModel> paymentModelList = Mapper.Map<List<PaymentModel>>(paymentList);
            return View(paymentModelList);
        }
        public ActionResult Payment(int id = 0)
        {
            PaymentModel payment = new PaymentModel();
            if (id != 0)
            {
                M_GB_Payment_GatWay paymentGatway = _walletService.GetPaymentgatway(id);
                payment = Mapper.Map<PaymentModel>(paymentGatway);
            }
            return View(payment);
        }
        [HttpPost]
        public ActionResult LoyaltyProgramsRole(int ROLE_ID = 0)
        {
            int roleId = ROLE_ID;
            IEnumerable<BonusDto> bonusDto = new List<BonusDto>();
            Mapper.CreateMap<BonusDto, BonusView>();
            if (roleId == 0)
                bonusDto = _walletService.GetWalletList();
            else
                bonusDto = _walletService.GetWalletListByRole(roleId);

            IEnumerable<BonusView> bonusData = Mapper.Map<List<BonusView>>(bonusDto);
            BonusTypesViewModel model = new BonusTypesViewModel();
            model.BonusView = bonusData;
            return PartialView("~/Views/Shared/_loyaltyProgramsPartial.cshtml", model);
        }
    }
}