﻿using AutoMapper;
using Gizmobaba.Models;
using Gizmobaba.Service.Interfaces;
using Gizmobaba.Utility;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Gizmobaba.Controllers
{
    public class MarketController : Controller
    {
        private readonly IDiscountService _discountService;
        // GET: CategoryManage

        public MarketController(IDiscountService discountService)
         {
             this._discountService = discountService;
         }

        // GET: Market/Create
        public ActionResult Create(int id=0)
        {
            DiscountModel model = new DiscountModel();
            try
            {
                if(id != 0)
                {
                    Mapper.CreateMap<M_GB_USER_DISCOUNT, DiscountModel>();
                    M_GB_USER_DISCOUNT discount = _discountService.GetDiscountVoucher(id);
                    model = Mapper.Map<DiscountModel>(discount);
                }
            }
            catch (Exception exception)
            {

            }
            return View(model);
        }

        // POST: Market/Create
        [HttpPost]
        public ActionResult Create(M_GB_USER_DISCOUNT userDiscount)
        {
           // DiscountModel Model = new DiscountModel();
            try
            {
                // TODO: Add insert logic here
                if (userDiscount != null)
                {
                    userDiscount.CREATED_ON = DateTime.Now;
                    if (_discountService != null) _discountService.AddUsersDiscount(userDiscount);
                    //if(userDiscount.DISCOUNT_ID == 0)
                    //    TempData["Message"] = " Discount voucher successfully added";
                    //else
                    //    TempData["Message"] = " Discount voucher successfully updated";

                    //Mapper.CreateMap<DiscountModel, M_GB_USER_DISCOUNT>();
                    //Model = Mapper.Map<DiscountModel>(userDiscount);
                }
            }
            catch (Exception exception)
            {
                TempData["Error"] = exception.Message;
            }
            return RedirectToAction("DiscountVouchers");
            //return View(Model);

        }
        // POST: Market/Delete/5
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Delete(int id)
        {
            try
            {
                if (id == 0)
                    return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Empty),
                        JsonRequestBehavior.DenyGet);
                else
                {
                    _discountService.DeleteDiscount(id);
                    //return RedirectToAction("CreateOrder");
                    return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Success),
                        JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception)
            {
                return Json(CustomStatus.ErrorCodeToString(CustomStatus.ProductMessageStatus.Failed), JsonRequestBehavior.DenyGet);
            }

        }
        [HttpGet]
        public ActionResult DiscountVouchers()
        {
            Mapper.CreateMap<M_GB_USER_DISCOUNT, DiscountModel>();
            IEnumerable<M_GB_USER_DISCOUNT> disList = _discountService.GetUsersDiscounts();
            IEnumerable<DiscountModel> dicountList = Mapper.Map<List<DiscountModel>>(disList);
            return View(dicountList);
        }
        public ActionResult DiscountVouchersSearch(string title, DateTime? fromDate, DateTime? toDate, string voucher)
        {
            Mapper.CreateMap<M_GB_USER_DISCOUNT, DiscountModel>();
            IEnumerable<M_GB_USER_DISCOUNT> disList = _discountService.DiscountVouchersSearch(title,fromDate,toDate,voucher);
            IEnumerable<DiscountModel> dicountList = Mapper.Map<List<DiscountModel>>(disList);
            return PartialView("DiscountVouchersPartial", dicountList);
        }
    }
}
