[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Gizmobaba.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Gizmobaba.App_Start.NinjectWebCommon), "Stop")]

namespace Gizmobaba.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Gizmobaba.Service.Interfaces;
    using Gizmobaba.Service;
    using Gizmobaba.Repository.Interfaces;
    using Gizmobaba.Repository;


    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
// Bindings section
            kernel.Bind<IProductService>().To<ProductService>();
            kernel.Bind<IOrderService>().To<OrderService>();
            kernel.Bind<ICategoryService>().To<CategoryService>();
            kernel.Bind<IWalletService>().To<WalletService>();
            kernel.Bind<IShippingService>().To<ShippingService>();
            kernel.Bind<IDashboardService>().To<DashboardService>();
            kernel.Bind<IPermissionsService>().To<PermissionsService>();
            kernel.Bind<IUserService>().To<UserService>();
            kernel.Bind<IDiscountService>().To<DiscountService>();
            kernel.Bind<IPromotionService>().To<PromotionService>();


            kernel.Bind<IProductRepository>().To<ProductRepository>();
            kernel.Bind<IOrderRepository>().To<OrderRepository>();
            kernel.Bind<ICategoryRepository>().To<CategoryRepository>();
            kernel.Bind<IWalletRepository>().To<WalletRepository>();
            kernel.Bind<IShippingRepository>().To<ShippingRepository>();
            kernel.Bind<IPermissionsRepository>().To<PermissionsRepository>();
            kernel.Bind<IUserRepository>().To<UserRepository>();
            kernel.Bind<IDiscountRepository>().To<DiscountRepository>();
            kernel.Bind<IPromotionRepository>().To<PromotionRepository>();

           
        }        
    }
}
