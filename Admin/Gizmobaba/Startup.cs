﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Gizmobaba.Startup))]
namespace Gizmobaba
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
