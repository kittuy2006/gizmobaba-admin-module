﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gizmobaba.Models
{
    public class PaymentModel
    {
        public string Payment_Gateway { get; set; }
        public int Payment_GateWay_ID { get; set; }
        public string Merchant_Key { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string File { get; set; }
        public string Currency_Type { get; set; }
        public string Other { get; set; }
        public DateTime Created_On { get; set; }
    }
}