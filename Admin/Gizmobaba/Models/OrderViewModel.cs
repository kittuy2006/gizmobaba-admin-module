﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Services.Description;
using Gizmobaba.Utility;

namespace Gizmobaba.Models
{
    public class OrderViewModel
    {
        public long PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public int? NUM_OF_PRO { get; set; }
        public decimal? UNIT_PRICE { get; set; }
        public string NOTE { get; set; }
        public long? VENDOR_ID { get; set; }
        public int? TAX_RATE_ID { get; set; }
        public DateTime CREATED_AT { get; set; }
        public DateTime? UPDATED_AT { get; set; }
        public int? STATUS { get; set; }
        public long ORDER_ID { get; set; }
        public long ORDER_DETAIL_ID { get; set; }
        public int Qty { get; set; }
        public string UserName { get; set; }

        public decimal OrderAmount
        {
            get
            {
                decimal amount = 0;
                if (DISCOUNT_VOUCHER != null && DISCOUNT_AMOUNT != null && DISCOUNT_AMOUNT != 0)
                    amount = (decimal) ((UNIT_PRICE ?? 0) - DISCOUNT_AMOUNT);
                else
                    amount = UNIT_PRICE ?? 0;
                return amount;
            }
            set { }
        }

        public string Age
        {
            get
            {
                DateTime date = CREATED_AT;
                double duedaysin = (DateTime.Now - date).TotalDays;
                return string.Format(" {0}days", Convert.ToInt32(duedaysin));
            }
            set { }
        }
        public string CHECKOUTTYPE { get; set; }
        public UserModel UserModel { get; set; }
        public IEnumerable<ProductDetailsDTO> ProductDetails { get; set; } 
        public ProductViewModel ProductModel { get; set; }
        public string VideoURL { get; set; }
        public string ProSku { get; set; }
        public int? PAYMENT_TYPE { get; set; }
        public string SourceImage { get; set; }
        public string DISCOUNT_VOUCHER { get; set; }
        public decimal? DISCOUNT_AMOUNT { get; set; }
        public string Image
        {
            get
            {
                string imageurl = "";
                if (SourceImage != null)
                    imageurl = ConfigurationManager.AppSettings["GetProimageUrl"].ToString() + "/" + SourceImage;
                return imageurl;

            }
            set { }
        }
        public string OrderStatus
        {
            get
            {
                string _orderStatus = "";
                if (STATUS == (int)CustomStatus.OrderStatus.Authorize)
                    _orderStatus = CustomStatus.OrderStatus.Authorize.ToString();
                else if (STATUS == (int)CustomStatus.OrderStatus.Cancel)
                    _orderStatus = CustomStatus.OrderStatus.Cancel.ToString();
                else if (STATUS == (int)CustomStatus.OrderStatus.Delivered)
                    _orderStatus = CustomStatus.OrderStatus.Delivered.ToString();
                else if (STATUS == (int)CustomStatus.OrderStatus.Dispatched)
                    _orderStatus = CustomStatus.OrderStatus.Dispatched.ToString();
                else if (STATUS == (int)CustomStatus.OrderStatus.Pending)
                    _orderStatus = CustomStatus.OrderStatus.Pending.ToString();
                else if (STATUS == (int)CustomStatus.OrderStatus.Pickup)
                    _orderStatus = CustomStatus.OrderStatus.Pickup.ToString();
                    return _orderStatus;
            }
            set { }
        }
        public int Address_Id { get; set; }
        public long? CATEGORY_ID { get; set; } 
        public bool? ISMERGEITEM { get; set; }
        public long? MERGEDORDERID { get; set; }
        public Nullable<bool> IS_SPLIT { get; set; }
        public Nullable<long> SPLIT_ORDER_ID { get; set; }
        public string CREATED_BY { get; set; }
        public string LOGISTIC_TOKEN { get; set; }
        public string LOGISTIC_AWB { get; set; }
        public string Awb_pdf
        {
            get
            {
                string awburl = "";
                if (LOGISTIC_AWB != null)
                    awburl = ConfigurationManager.AppSettings["AWBUrl"].ToString() + "/" + LOGISTIC_AWB + ".pdf";
                return awburl;

            }
            set { }
        }
    }
    public class OrderDetailsModelView
    {
        public IEnumerable<OrderViewModel> orderlist { get; set; }
        public IEnumerable<UserAddressModel> userAddress { get; set; }
        public UserModel userData { get; set; }
        public OrderViewModel Order { get; set; }

    }

    public class OrderReturnsViewModel
    {
        public IEnumerable<OrderViewModel> OrderViewModels { get; set; }
        public IEnumerable<CartViewModel> CartViewModels { get; set; } 
    }

    public class UserModel
    {
        public long SEQ_NO { get; set; }
        public long ID { get; set; }
        public string USER_NAME { get; set; }
        public string USER_ID { get; set; }
        public string PASSWORD { get; set; }
        public string CITY { get; set; }
        public string MOB_NO { get; set; }
        public string EMAIL { get; set; }
        public int? STATUS { get; set; }
        public long? ROLE { get; set; }
        public DateTime? CREATED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public int? USER_TYPE { get; set; }
        public string ADD1 { get; set; }
        public string ADD2 { get; set; }
        public int? PIN { get; set; }
        public string STATE { get; set; }
        public string COUNTRY { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public int CityId { get; set; }
        public string ALT_EMAIL { get; set; }
        public string ALT_MOB { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string PICTURE { get; set; }
        public string F_ID { get; set; }
        public DateTime? DOB { get; set; }
        public string GENDER { get; set; }
        public string UserGroup { get; set; }
        public int OrderCount { get; set; }
        public string UserStatus
        {
        get{
          string userStatus ="";
            if(STATUS != 0)
                userStatus = CustomStatus.UserStatusType(STATUS).ToString();
                return userStatus;
        }
            set { }
        }
        public string RoleName { get; set; }
        public string COMPANY { get; set; }
        public string WALLET { get; set; }
        public decimal? AMOUNT { get; set; }

        public string Image
        {
            get
            {
                string image = "";
                if (PICTURE != null)
                    image = ConfigurationManager.AppSettings["UserImageUrl"].ToString() + PICTURE;
                //image = PHOTO_URL;
                return image;
            }
            set { }
        }
        public IEnumerable<ShippingCountriesModel> Countries { get; set; }

    }

    public class UserOrderModel
    {
        public UserAddressModel ShippinghAddress { get; set; }
        public UserAddressModel BillingAddress { get; set; }
        public UserModel UserModel { get; set; }
        public ProductDetailsDTO ProductDto { get; set; }
        public OrderViewModel OrderViewModel { get; set; }

        public List<UserAddressModel> userAddressList { get; set; }

    }

    public class UserAddressModel
    {
        public int ADDRESS_ID { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string COMPANY { get; set; }
        public string ADDRESS_1 { get; set; }
        public string ADDRESS_2 { get; set; }
        public string CITY { get; set; }
        public string POSTCODE { get; set; }
        public int? COUNTRY_ID { get; set; }
        public string STATE { get; set; }
        public string COUNTRY { get; set; }
        public int? ZONE_ID { get; set; }
        public string CUSTOM_FIELD { get; set; }
        public string landmark { get; set; }
        public long? mob_no { get; set; }
        public string MOB_NO { get; set; }
        public string EMAIL { get; set; }
        public string ALT_MOB { get; set; }

    }
    public class NewOrderModel
    {
        public long ORDER_ID { get; set; }
        public DateTime? CREATED_AT { get; set; }
        public DateTime? UPDATED_AT { get; set; }
        public long? CUSTOMER_ID { get; set; }
        public Nullable<int> STATUS { get; set; }
        public string EMAIL { get; set; }
        public Nullable<int> ADDRESS_ID { get; set; }
        public string COMMENTS { get; set; }
        public string RESON { get; set; }
        public Nullable<bool> USERSHOWTHECOMMENTS { get; set; }
        public string LOGISTIC_TYPE { get; set; }
        public Nullable<bool> ISMERGEITEM { get; set; }
        public Nullable<long> MERGEDORDERID { get; set; }
        public Nullable<bool> IS_SPLIT { get; set; }
        public Nullable<long> SPLIT_ORDER_ID { get; set; }
        public string CREATED_BY { get; set; }
        public string UserName { get; set; }
        public int Qty { get; set; }
        public string CHECKOUTTYPE { get; set; }
        public decimal? UNIT_PRICE { get; set; }
        public int Address_Id { get; set; }
        public Nullable<int> PAYMENT_TYPE { get; set; }
        public string DISCOUNT_VOUCHER { get; set; }
        public decimal? DISCOUNT_AMOUNT { get; set; }
        public string Age
        {
            get
            {
                DateTime date = CREATED_AT ?? DateTime.Now;
                double duedaysin = (DateTime.Now - date).TotalDays;
                return string.Format(" {0}days", Convert.ToInt32(duedaysin));
            }
            set { }
        }
        public decimal OrderAmount
        {
            get
            {
                decimal amount = 0;
                if (DISCOUNT_VOUCHER != null && DISCOUNT_AMOUNT != null && DISCOUNT_AMOUNT != 0)
                    amount = (decimal)((UNIT_PRICE ?? 0) - DISCOUNT_AMOUNT);
                else
                    amount = UNIT_PRICE ?? 0;
                return amount;
            }
            set { }
        }
        public string message { get; set; }
        public string Role { get; set; }


    }
    public class SubOrderModel
    {
        public long ORDER_DETAIL_ID { get; set; }
        public Nullable<long> PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public Nullable<int> NUM_OF_PRO { get; set; }
        public Nullable<decimal> UNIT_PRICE { get; set; }
        public string NOTE { get; set; }
        public Nullable<long> VENDOR_ID { get; set; }
        public Nullable<int> TAX_RATE_ID { get; set; }
        public Nullable<System.DateTime> CREATED_AT { get; set; }
        public Nullable<System.DateTime> UPDATED_AT { get; set; }
        public Nullable<int> STATUS { get; set; }
        public long ORDER_ID { get; set; }
        public Nullable<int> QUANTITY { get; set; }
        public string CHECKOUTTYPE { get; set; }
        public Nullable<long> SPLIT_ORDER_ID { get; set; }
    
    }
    public class NewOrderViewModel
    {
        public IEnumerable<NewOrderModel> newOrderModel { get; set; }
        public IEnumerable<SubOrderModel> newSubOrderModel { get; set; }
    }
}