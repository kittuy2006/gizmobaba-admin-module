﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gizmobaba.Models
{
    public class PROMOTION
    {
    }
    public class FlashPromotionModel
    {
        public int PROMOTION_ID { get; set; }
        public string TITLE { get; set; }
        public string TYPE { get; set; }
        public Nullable<DateTime> FROM_DATE { get; set; }
        public Nullable<DateTime> TO_DATE { get; set; }
        public string CODE { get; set; }
        public Nullable<DateTime> CREATED_ON { get; set; }
        public int STATUS { get; set; }
    }
    public class MailTemplates
    {
        public int ID { get; set; }
        public string SUBJECT { get; set; }
        public string DESCRIPTION { get; set; }
        public int STATUS { get; set; }
        public string PERIOD { get; set; }
        public DateTime CREATED_ON { get; set; }
        public string FROMMAIL { get; set; }
        public Nullable<DateTime> UPDATED_ON { get; set; }
    }
}