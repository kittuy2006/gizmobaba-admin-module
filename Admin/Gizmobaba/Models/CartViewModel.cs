﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gizmobaba.Models
{
    public class CartViewModel
    {
        public long CART_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public int? QYT { get; set; }
        public string SESSION_ID { get; set; }
        public decimal? PRICE_PER_QYT { get; set; }
        public string USER_ID { get; set; }
        public DateTime? CREATED_AT { get; set; }
        public string COUPAN_ID { get; set; }
        public decimal? TOTAL_AMOUNT { get; set; }
        public int? TOTAL_QTY { get; set; }
        public int? STATUS { get; set; }
        public long? MOB_NO { get; set; }
        public int? ADDRESS_ID { get; set; }
        public string EMAIL_ID { get; set; }
    }
}