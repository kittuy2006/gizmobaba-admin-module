﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gizmobaba.Models
{
    public class ProductTagsViewModel
    {
        public int PRODUCT_TAG_ID { get; set; }
        public int? TAG_ID { get; set; }
        public string TAG_DESC { get; set; }
        public long? PRODUCT_ID { get; set; }
        public int? STATUS { get; set; }
        public int ProductCount { get; set; }

    }
    public class ProductTagsList
    {
        public IEnumerable<ProductTagsViewModel> productTagsList { get; set; }
        public ProductTagsViewModel productTagsViewModel { get; set; }
    }
}