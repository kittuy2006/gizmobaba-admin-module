﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gizmobaba.Models
{
    public class ProductViewModel
    {
        public int TEMP_PRODUCT_ID { get; set; }
        public int TEMP_PRODUCT_IMAGE_ID { get; set; }
        public ProductDetailsDTO details { get; set; }
        public IEnumerable<Category> categories { get; set; }
        public IEnumerable<SubCategoriesViewModel> SubCategories { get; set; }
        public IEnumerable<AvailableShippingCodes> shippingCodes { get; set; }
        public IEnumerable<Images> productImages { get; set; }
        public IEnumerable<ProductPayment> productPayments { get; set; }
        public IEnumerable<ProductDelivery> productDelivery { get; set; }

        public string Weight { get; set; }
        public bool LocationBasedInventory { get; set; }
        public bool PreOrder { get; set; }
        public bool BackOrder { get; set; }
        public bool CreditCard { get; set; }
        public bool Cheque { get; set; }
        public bool COD { get; set; }
        public bool Ship { get; set; }
        public bool Offline { get; set; }
        public bool Online { get; set; }
        public bool Deli_Online { get; set; }
        public bool Instore_Pickup { get; set; }

        public HttpPostedFileBase sourceImage { get; set; }
        public HttpPostedFileBase gifImage { get; set; }
        public HttpPostedFileBase image1 { get; set; }
        public HttpPostedFileBase image2 { get; set; }
        public HttpPostedFileBase image3 { get; set; }
        public HttpPostedFileBase image4 { get; set; }
        public HttpPostedFileBase image5 { get; set; }

        public List<int> AvailableShippingCodes { get; set; }
        public List<int> SelectedShippingCodes { get; set; }
        public IEnumerable<TaxModel> taxes { get; set; }
    }
}