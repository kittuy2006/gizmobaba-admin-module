﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Gizmobaba.Models
{
    public partial class BannerModel
    {
        public int BANNER_ID { get; set; }
        public string BANNER_NAME { get; set; }
        public string BANNER_DEC { get; set; }
        public string BANNER_URL { get; set; }
        public DateTime CAREATED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public int STATUS { get; set; }
        public DateTime? BANNER_EXPIRY_DATE { get; set; }
        public long? CATEGORY_ID { get; set; }
        public long? SUB_CAT_ID { get; set; }
        public int? BANNER_TYPE_ID { get; set; }
        public string CreatedDate
        {
            get
            {
                string createdDate = "";
                if (CAREATED_ON != null)
                    createdDate = CAREATED_ON.ToShortDateString();
                return createdDate;
            }
            set { }
        }
        public string ExpiryDate
        {
            get
            {
                string createdDate = "";
                if (BANNER_EXPIRY_DATE != null)
                    createdDate = BANNER_EXPIRY_DATE.ToString();
                return createdDate;
            }
            set { }
        }
        public string Code { get; set; }
        public HttpPostedFileBase Image { get; set; }
        public string BannerImage
        {
            get
            {
                string image = "";
                if (BANNER_URL != null)
                    image = ConfigurationManager.AppSettings["GetBannerUrl"].ToString() + BANNER_URL;
                return image;
            }
            set { }
        }
        public string Sub_Cat_Name { get; set; }


    }
    public class BannerListModel
    {
        public IEnumerable<BannerModel> bannerList { get; set; }
        public IEnumerable<Category> CategoryList { get; set; }
    }
}