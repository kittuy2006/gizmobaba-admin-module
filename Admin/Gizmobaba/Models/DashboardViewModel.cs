﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gizmobaba.Models
{
    public class DashboardViewModel
    {
        public int TotalUsers { get; set; }
        public int TotalOrders { get; set; }
        public int TotalSales { get; set; }
        public int TotalInventory { get; set; }
        public int TotalVendor { get; set; }
        public int TotalDealer { get; set; }
        public int TotalMasterFranchise { get; set; }
        public long TotalProductCount { get; set; }
        public long TotalOrdersCount { get; set; }
        public long NewOrdersCount { get; set; }
        public long ApprovedOrdersCount { get; set; }
        public string UserId { get; set; }
        public IEnumerable<RolesModel> roleModel { get; set; }
        public IEnumerable<EmailMessages> AdminMailses { get; set; }
    }
}