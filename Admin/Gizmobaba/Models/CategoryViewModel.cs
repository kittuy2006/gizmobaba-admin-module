﻿using Gizmobaba.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gizmobaba.Models
{
    public class CategoryViewModel
    {
        public int? SEQ_NO { get; set; }
        public long CATEGORY_ID { get; set; }
        public string CATEGORY_DESC { get; set; }
        public int? STATUS { get; set; }
        public DateTime? CREATEDF_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public string ICON { get; set; }
        public int ProductsCount { get; set; }
        public string CATEGORY_RANK { get; set; }
        public string REFERENCE_CODE { get; set; }
        public HttpPostedFileBase Image { get; set; }

        public IEnumerable<M_GB_SUB_CATEGORY> SubCategory { get; set; }
        public CategoryContentInformation CategoryContentInformation { get; set; }
        public CategorySeoInformation CategorySeoInformation { get; set; }
    }
    public class CategoryList
    {
        public IEnumerable<Category> categoryList { get; set; }
        public CategoryViewModel categoryViewModel { get; set; }
    }
    public class Category
    {
        public int? SEQ_NO { get; set; }
        public long CATEGORY_ID { get; set; }
        public string CATEGORY_DESC { get; set; }
        public int? STATUS { get; set; }
        public DateTime? CREATEDF_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public string ICON { get; set; }
        public int ProductCount { get; set; }
        public string REFERENCE_CODE { get; set; }
        public IList<SubCategoriesViewModel> SubCategories { get; set; } 
    }
    public class CategoryContentInformation
    {
        public long CATEGORY_CONTENT_ID { get; set; }
        public long CATEGORY_ID { get; set; }
        public string TITLE { get; set; }
        public string BODY { get; set; }
        public string IMAGE_URL { get; set; }
        public System.DateTime CREATEDF_ON { get; set; }
        public int STATUS { get; set; }
    }
    public class CategorySeoInformation
    {
        public long CATEGORY_INFORMATION_ID { get; set; }
        public long CATEGORY_ID { get; set; }
        public string META_KEYWORD { get; set; }
        public string META_DESC { get; set; }
        public string META_REFERENCE_CODE { get; set; }
        public System.DateTime CREATED_ON { get; set; }
        public int STATUS { get; set; }
    }
}