﻿using Gizmobaba.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gizmobaba.Models
{
    public class BonusTypesModel
    {
        public int BONUS_TYPE_ID { get; set; }
        public string BONUS_TYPE { get; set; }
        public int STATUS { get; set; }
        public DateTime? CRETAED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public string CREATED_BY { get; set; }
        public string UPDATED_BY { get; set; }

        public IEnumerable<BonusModel> BonusDto { get; set; }
    }
    public class BonusModel
    {
        public int ID { get; set; }
        public int BONUS_TYPE_ID { get; set; }
        public int ROLE_ID { get; set; }
        public double? VALUE { get; set; }
        public int? STATUS { get; set; }
        public DateTime CREATED_ON { get; set; }
        public DateTime? ACTIVE_DATE { get; set; }
        public DateTime? EXPIRE_DATET { get; set; }
        public DateTime? UPDATED_ON { get; set; }

        public BonusTypesDto BonusTypesDto { get; set; }
    }
    public class BonusTypesViewModel
    {
        //public IEnumerable<BonusTypesModel> bonusTypesModel { get; set; }
        //public IEnumerable<BonusModel> bonusModel { get; set; }
        public IEnumerable<BonusView> BonusView { get; set; }
    }
    public class BonusView
    {
        public int BONUS_TYPE_ID { get; set; }
        public string BONUS_TYPE { get; set; }
        public int STATUS { get; set; }
        public DateTime? CRETAED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public string CREATED_BY { get; set; }
        public string UPDATED_BY { get; set; }
        public int ID { get; set; }
        public int ROLE_ID { get; set; }
        public double? VALUE { get; set; }
        public DateTime CREATED_ON { get; set; }
        public DateTime? ACTIVE_DATE { get; set; }
        public DateTime? EXPIRE_DATET { get; set; }
    }
}