﻿using Gizmobaba.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Gizmobaba.Models
{
    public class ProductDetailsDTO
    {
        public long PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public long? CATEGORY_ID { get; set; }
        public long? SUB_CAT_ID { get; set; }
        public string PRODUCT_CAPTION { get; set; }
        public string PRODUCT_DESC { get; set; }
        public string BRAND { get; set; }
        public decimal? MRP { get; set; }
        public decimal? GROSS_PRICE { get; set; }
        public decimal? NET_PRICE { get; set; }
        public decimal? GIZMOBABA_PRICE { get; set; }
        public int? QUANTITYS { get; set; }
        public int? ATTR_ID { get; set; }
        public int? LEVEL_ID { get; set; }
        public int? TAX_RATE_ID { get; set; }
        public int? AVAILABLE_QTY { get; set; }
        public DateTime? CREATED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public int? STATUS { get; set; }
        public int? ISAVAILABLE { get; set; }
        public int? IMAGE_ID { get; set; }
        public string LINK { get; set; }
        public string CREATED_BY { get; set; }
        public string UPDATED_BY { get; set; }
        public string VENDOR_ID { get; set; }
        public string PHOTO_URL { get; set; }
        public DateTime? ACTIVATION_DATE { get; set; }
        public DateTime? DEACTIVATION_DATE { get; set; }
        public string DELIVERY_TIME { get; set; }
        public string INVENTORY { get; set; }
        public int? RESERVE_QUANTITY { get; set; }
        public int? PKG_QUANTITY { get; set; }
        public string REORDER_STOCK_LEVEL { get; set; }
        public int? STOCK_ALERT_QUANTITY { get; set; }
        public int? MINIMUM_ORDER_QUANTITY { get; set; }
        public int? MAXIMUM_ORDER_QUANTITY { get; set; }
        public bool? PREORDER { get; set; }
        public bool? BACKORDER { get; set; }
        public string VIDEO_URL { get; set; }
        public string STOCK_AVAILABLE_MESS { get; set; }
        public string PRODUCT_SKU { get; set; }
        public string SKUCODE { get; set; }
        public double? M_F_PRICE { get; set; }
        public double? DEALER_PRICE { get; set; }
        public string Image {
            get { 
                string image="";
                if (PHOTO_URL != null)
                    image = ConfigurationManager.AppSettings["GetProimageUrl"].ToString() + PHOTO_URL;
                    //image = PHOTO_URL;
                return image;
        }
            set{}
        }
        public IEnumerable<ProductTagsViewModel> ProductTagsList { get; set; }


    }

    public class AvailableShippingCodes {
        public int SHIPPING_ID { get; set; }
        public string SHIPPING_CODE { get; set; }
        public string SHIPPING_DESC { get; set; }
        public DateTime CREATED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public int? STATUS { get; set; }
        public string ICON { get; set; }
        public string LOGISTICS { get; set; }
        public string MARKETPLACE { get; set; }
        public int? REGION_ID { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public int RegionId { get; set; }

        public IEnumerable<ShippingMethodModel> ShippingMethod { get; set; }
        public IEnumerable<ShippingCountriesModel> ShippingCountries { get; set; }
        public IEnumerable<ProductTagsViewModel> ProductTags { get; set; }
        public List<int> SelectedShippingCodes { get; set; }

    }
    public class ShippingMethodModel
    {
        public int SHIPPINGMETHOD_ID { get; set; }
        public string METHOD_NAME { get; set; }
        public DateTime? CREATED_ON { get; set; }
        public int? STATUS { get; set; }
    }
    public class ShippingCountriesModel
    {
        public int COUNTRY_ID { get; set; }
        public string COUNTRY { get; set; }
        public string COUNTRY_DESC { get; set; }
        public string COUNTRY_CODE { get; set; }
        public DateTime CREATED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public int? STATUS { get; set; }
    }
    public class StatesModel
    {
        public int STATE_ID { get; set; }
        public int COUNTRY_ID { get; set; }
        public string STATE { get; set; }
        public string STATE_DESC { get; set; }
        public DateTime CREATED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public int? STATUS { get; set; }
    }
    public class RegionModel
    {
        public int REGION_ID { get; set; }
        public int STATE_ID { get; set; }
        public string REGION { get; set; }
        public string REGION_DESC { get; set; }
        public DateTime CRETAED_ON { get; set; }
        public int? STATUS { get; set; }
    }
    public class Images
    {
        public long? PRODUCT_ID { get; set; }
        public string ALTERNATE_NAME { get; set; }
        public int? DISPLAY_IMAGE { get; set; }
        public int PRODUCT_IMAGE_ID { get; set; }
        public string IMAGE_URL { get; set; }

        public string Image
        {
            get
            {
                string imageurl = "";
                if (IMAGE_URL != null)
                    imageurl = ConfigurationManager.AppSettings["GetProimageUrl"].ToString() + "/" + IMAGE_URL;
                return imageurl;

            }
            set { }
        }

    }
    public class ProductPayment
    {
        public int PRODUCT_PAYMENT_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public int PAYMENT_ID { get; set; }
        public string PRODUCT_PAYMENT_DESC { get; set; }
        public int? STATUS { get; set; }
    }
    public class ProductDelivery
    {
        public int PRODUCT_DELIVERY_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public int DELIVERY_ID { get; set; }
        public string PRODUCT_DELIVERY_DESC { get; set; }
        public int? STATUS { get; set; }
    }

    public class ProductInfomationModel
    {
        public long PRODUCT_INFO_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public string PRO_OFFER_DESC { get; set; }
        public string PRO_OFFER_IMAGE { get; set; }
        public string BARCODE { get; set; }
        public string MODELNUMBER { get; set; }
        public string CATALOG_CODE { get; set; }
        public string STANDARD_PRODUCT_TYPE { get; set; }
        public string MPN { get; set; }
        public string STANDARD_PRODUCT_CODE { get; set; }
        public string PAGE_TITLE { get; set; }
        public string KEYWORD { get; set; }
        public string SEO_DESC { get; set; }
        public string URL_KEY_REWRITER { get; set; }
        public string SMALL_IMAGEALT { get; set; }
        public string LARGE_IMAGEALT { get; set; }
        public int? CONDITION_ID { get; set; }
        public DateTime? CONDITION_PURCHASE_DATE { get; set; }
        public int? WARRANTY_ID { get; set; }
        public string WARRANTY_DESC { get; set; }
        public int? CUSTOM_FIELDS_ID { get; set; }
        public int? SECTION_ID { get; set; }
        public string PRODUCT_NOTE { get; set; }
        public DateTime CREATED_ON { get; set; }
        public int? STATUS { get; set; }
    }
    public class BulkProductUploadview
    {
        public int BULK_PRO_UPLOAD_ID { get; set; }
        public string FILE_NAME { get; set; }
        public int? UPLOAD_STATUS { get; set; }
        public int? TOTAL_RECORDS { get; set; }
        public int? UPDATED_RECORDS { get; set; }
        public string DESCRIPTION { get; set; }
        public int? STATUS { get; set; }
        public DateTime CREATED_ON { get; set; }
    }
    public class BulkProductUploadModel
    {
        public IEnumerable<BulkProductUploadview> bulkProductUploadview { get; set; }
    }
    public class ReviewsModel
    {
        public int ID { get; set; }
        public Nullable<int> USER_ID { get; set; }
        public string UserName { get; set; }
        public Nullable<long> PRODUCT_ID { get; set; }
        public string REVIEW { get; set; }
        public string REVIEW_DESC { get; set; }
        public DateTime CREATED_ON { get; set; }
        public Nullable<DateTime> UPDATED_ON { get; set; }
        public int STATUS { get; set; }
    }
}