﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace Gizmobaba.Models
{
    public class WalletViewModel
    {
        public int WALLET_ID { get; set; }
        public int ROLE_ID { get; set; }
        public int? SIGNUP { get; set; }
        public int? LINKINGPOSTS { get; set; }
        public int? SHARINGPOSTS { get; set; }
        public int? REFERRING_FRIENDS { get; set; }
        public int? REFFERED_FRIENDS_PURCHASES { get; set; }
        public int? OWN_PURCHASE { get; set; }
        public int? PURCHASE_CERTAIN_TIME { get; set; }
        public int? MAX_USER_PURCHASE { get; set; }
        public DateTime CREATEDON { get; set; }
        public DateTime? UPDATEON { get; set; }
        public int? STATUS { get; set; }
        public int Dealer_Convenience_Fee { get; set; }
        public string WALLET_RECHARGE { get; set; }
        public string DEALERS_EARNINGS { get; set; }
        public string DEALERS_REFERAL { get; set; }

    }

    public class FeatureTypeViewModel
    {
        public IEnumerable<FeatureTypeModel> FeatureType { get; set; }
        public RolesModel RolesModel { get; set; }
        public UserModel UserModel { get; set; }
    }

    public class FeatureTypeModel
    {
        public int FEATURE_TYPE_ID { get; set; }
        public string FEATURE_TYPE { get; set; }
        public int? FEATURE_ACCESS_ID { get; set; }
        public int? ROLE_ID { get; set; }
        public int? ID { get; set; }
        public DateTime? CREATED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public int STATUS { get; set; }
        public int chkBox { get; set; }
    }

    public class RolesModel
    {
        public int SEQ_NO { get; set; }
        public int ROLE_ID { get; set; }
        public string ROLE_DESC { get; set; }
        public int? STATUS { get; set; }
        public DateTime? CREATED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public int PERMISSIONSCOUNT { get; set; }
        public string DESCRIPTION { get; set; }
        public bool UserAllow { get; set; }
        public long? Value { get; set; }
        public int UserCount { get; set; }
        public IEnumerable<UserModel> userModel { get; set; }
    }
}