﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gizmobaba.Models
{
    public class MultiImagesUpload
    {
        public HttpPostedFileBase[] files { get; set; }
        public HttpPostedFileBase file { get; set; }

    }
}