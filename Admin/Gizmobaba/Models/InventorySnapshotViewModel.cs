﻿using Gizmobaba.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gizmobaba.Models
{
    public class InventorySnapshotViewModel
    {
        public IEnumerable<ProductDetailsDTO> productsList { get; set; }
        public IEnumerable<ProductTagsViewModel> tagsList { get; set; }

    }
}