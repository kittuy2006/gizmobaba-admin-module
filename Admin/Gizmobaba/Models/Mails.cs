﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using  MailKit.Search;
using MailKit;
using MailKit.Net.Imap;
using System.Threading;

namespace Gizmobaba.Models
{
      public class EmailMessages
    {
    public int Index { get; set; }
    public string subject { get; set; }
    public string Sender { get; set; }
    public UniqueId UId { get; set; }
    public DateTimeOffset mailDate { get; set; }

    public string Body { get; set; }
    }

    public class Mails
    {
        public IEnumerable<EmailMessages> AdminMails()
        {
            List<EmailMessages> msgs = new List<EmailMessages>();
            try
            {
                using (var client = new ImapClient())
                {
                    using (var cancel = new CancellationTokenSource())
                    {
                        client.Connect("imap.gmail.com", 993, true, cancel.Token);

                        // If you want to disable an authentication mechanism,
                        // you can do so by removing the mechanism like this:
                        client.AuthenticationMechanisms.Remove("XOAUTH");

                        client.Authenticate("gizmobaba99", "prisionbreak", cancel.Token);

                        // The Inbox folder is always available...
                        var inbox = client.Inbox;
                        inbox.Open(FolderAccess.ReadOnly, cancel.Token);
                        var unreadMessageIds = inbox.Search(SearchQuery.NotSeen, cancel.Token);
                        if (unreadMessageIds != null && unreadMessageIds.Count > 0)
                            foreach (var summary in inbox.Fetch(unreadMessageIds, MessageSummaryItems.Full | MessageSummaryItems.UniqueId))
                            {
                                msgs.Add(new EmailMessages { UId = summary.UniqueId, Index = summary.Index, subject = summary.Envelope.Subject, Sender = summary.Envelope.From.FirstOrDefault().ToString(), mailDate = summary.Date });
                            }

                        client.Disconnect(true, cancel.Token);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return msgs;

        }
    }
    
}