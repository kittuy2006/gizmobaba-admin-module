﻿using Gizmobaba.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gizmobaba.Models
{
    public class SubCategoriesViewModel
    {
        public int SEQ_NO { get; set; }
        public long SUB_CAT_ID { get; set; }
        public string SUB_CAT_DESC { get; set; }
        public long CATEGORY_ID { get; set; }
        public int? STATUS { get; set; }
        public DateTime? CREATED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public string REFERENCE_CODE { get; set; }
        public string DESCRIPTION { get; set; }
        public string NUMBER_OF_COLUMNS { get; set; }
        public string SUB_CAT_GROUP { get; set; }
        public string RANK { get; set; }
        public int ProductCount { get; set; }

       // public virtual M_GB_CATEGORY M_GB_CATEGORY { get; set; }
    }
    public class SubCategoriesList
    {
        public IEnumerable<SubCategoriesViewModel> subCategoriesList { get; set; }
        public SubCategoriesViewModel subCategoriesViewModel { get; set; }
    }
}