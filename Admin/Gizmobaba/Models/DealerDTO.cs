﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gizmobaba.Models
{
    public class DealerDto
    {
        public long ID { get; set; }
        public string USER_NAME { get; set; }
        public string USER_ID { get; set; }
        public string PASSWORD { get; set; }
        public string EMAIL { get; set; }
        public int? STATUS { get; set; }
        public long? ROLE { get; set; }
        public DateTime? CREATED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public int? USER_TYPE { get; set; }
        public string ADD1 { get; set; }
        public string ADD2 { get; set; }
        public decimal? PIN { get; set; }
        public string ALT_EMAIL { get; set; }
        public string ALT_MOB { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string PICTURE { get; set; }
        public string F_ID { get; set; }
        public string GENDER { get; set; }
        public string REFERRD_BY { get; set; }
        public int USERDETAIL_ID { get; set; }
        public string NAME { get; set; }
        public string COMPANY { get; set; }
        public string COMPANY_TYPE { get; set; }
        public int ROLE_ID { get; set; }
        public string BUSINESS { get; set; }
        public string PAN_NUMBER { get; set; }
        public string TAN_NUMBER { get; set; }
        public string WALLET { get; set; }
        public decimal? AMOUNT { get; set; }
        public int STATE { get; set; }
        public int? CITY { get; set; }
        public int? COUNTRY { get; set; }
        public string ADDRESS { get; set; }
        public string ADDRESS_LINE { get; set; }
        public long RECHARGE_ID { get; set; }
        public int RECHARGE_POINTS { get; set; }
        public int USER_ADMIN_ID { get; set; }
        public string CREATED_BY { get; set; }
        public string CITY_NAME { get; set; }
        public IEnumerable<ShippingCountriesModel> Countries { get; set; }
        public IEnumerable<StatesModel> States { get; set; }
        public IEnumerable<RegionModel> Cityes { get; set; }

    }

    public class DealerView
    {
        public DealerDto Dealer { get; set; }
        public IEnumerable<DealerDto> DealerList { get; set; }
        public List<int> SelectedCategoryList { get; set; }
        public List<int> SelectedCategory { get; set; }
        public IEnumerable<Category> CategoryList { get; set; }
    }
}