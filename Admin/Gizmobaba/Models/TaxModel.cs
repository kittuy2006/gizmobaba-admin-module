﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gizmobaba.Models
{
    public class TaxModel
    {
        public int TAX_ID { get; set; }
        public string TAX { get; set; }
        public string TAX_DESC { get; set; }
        public string TAX_TYPE { get; set; }
        public DateTime CREATED_ON { get; set; }
        public Nullable<DateTime> UPDATED_ON { get; set; }
        public string CREATED_BY { get; set; }
        public int STATUS { get; set; }
    }
    public class TaxModelView
    {
        public IEnumerable<TaxModel> taxList {get; set;}
        public TaxModel taxModel {get; set;}
    }
}