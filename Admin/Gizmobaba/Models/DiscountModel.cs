﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gizmobaba.Models
{
    public class DiscountModel
    {
        public int DISCOUNT_ID { get; set; }
        public Nullable<int> USER_GROUP_ID { get; set; }
        public string VOUCHER_CODE { get; set; }
        public string DISCOUNT { get; set; }
        public Nullable<int> NUMBEROFUSERS { get; set; }
        public Nullable<int> NUMBEROFVOUCHER { get; set; }
        public Nullable<System.DateTime> VOUCHER_DATE { get; set; }
        public System.DateTime CREATED_ON { get; set; }
        public string REMARKS { get; set; }
        public Nullable<int> STATUS { get; set; }

        public string CAMPAIGN_TITLE { get; set; }
        public Nullable<System.DateTime> VOUCHER_EXPIRY_DATE { get; set; }

    }
}