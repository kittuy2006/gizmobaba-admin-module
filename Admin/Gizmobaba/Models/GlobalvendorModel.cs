﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gizmobaba.Models
{
    public class GlobalvendorModel
    {
        public int TotalProductCount {get; set;}
        public int TotalOrdersCount { get; set; }
        public int NewOrdersCount { get; set; }
        public int ApprovedOrdersCount { get; set; }
        public IEnumerable<OrderViewModel> ordersList { get; set; }

    }
}