﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gizmobaba.Models
{
    public class ProductExportModel
    {
        public string PRODUCT_NAME { get; set; }
        public string PRODUCT_CAPTION { get; set; }
        public string MRP { get; set; }
        public string GROSS_PRICE { get; set; }
        public string GIZMOBABA_PRICE { get; set; }
        public string ACTIVATION_DATE { get; set; }
        public string DEACTIVATION_DATE { get; set; }
        public string CATEGORY_ID { get; set; }
        public string SUB_CAT_ID { get; set; }
        public string PRODUCT_DESC { get; set; }
        public string BRAND { get; set; }
        public string NET_PRICE { get; set; }
        public string QUANTITYS { get; set; }
        public string DELIVERY_TIME { get; set; }
        public string INVENTORY { get; set; }
        public string RESERVE_QUANTITY { get; set; }
        public string PRODUCT_SKU { get; set; }
        public string PKG_QUANTITY { get; set; }
        public string Product_Image_HD_URL { get; set; }
        public string VIDEO_URL { get; set; }
        public string Product_Image_URL { get; set; }
        public string GIF_IMAGE { get; set; }
    }
}