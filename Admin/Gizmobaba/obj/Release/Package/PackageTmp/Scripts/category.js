﻿//var URL = "http://localhost:1859/";
var URL = "http://www.pakkadial.com/Admin/";

$(document).ready(function () {

    $('[href="#normaldelete"]').click(function () {
        $('.categoryDeleteLink').attr('onclick', "CategoryDeleteMethod(" + $(this).parents('tr').attr('id') + ")");
    });

    $("#btnSearch").click(function () {
        var key = $('#txtCategorySearch').val();
        if (key != '') {
            $('.loader-container').removeClass('hide');
            var categoty = key;
            $.ajax({
                url: URL + '/CategoryManage/CategorySearch/',
                //url: '/CategoryManage/CategorySearch/',
                type: "Get",
                contentType: 'application/json; charset=utf-8',
                async: false,
                data: { categoty: categoty },
                dataType: "json",
                success: function(data) {
                    Binddata(data);
                    $('.loader-container').addClass('hide');
                },
                error: function(xhr) {
                    alert(xhr);
                    $('.loader-container').addClass('hide');
                    $('#divcategory').append(response);
                    var response = '<div class="alert alertnew alert-danger fade in">' +
                       '<a href="#" class="close" data-dismiss="alert">×</a>' +
                       '<strong>Error!</strong>Please try again</div>';
                    $('#divcategory').append(response);
                    $('.alert').fadeOut(10000);
                }
            });
        }
    });
});

function CategoryDeleteMethod(id) {
    id = $('#deleteCategoryId').val();
    $('.loader-container').removeClass('hide');
    $.ajax({
        //url: '/CategoryManage/Delete/' + id,
        url: URL + '/CategoryManage/Delete/' + id,
        type: "Post",
        contentType: 'application/json; charset=utf-8',
        async: false,
        dataType: "json",
        success: function (data) {
            CategoryDeleteCallback(id);
            $('li[id=' + id + ']').remove();
            var response = '<div class="alert alertnew alert-success fade in">' +
                        '<a href="#" class="close" data-dismiss="alert">×</a>' +
                        '<strong>Success!</strong> Category successfully deleted</div>';
            $('#divcategory').append(response);
            $('.loader-container').addClass('hide');
        },
        error: function (xhr) {
            alert(xhr);
            $('.loader-container').addClass('hide');
            $('#divcategory').append(response);
            var response = '<div class="alert alertnew alert-danger fade in">' +
               '<a href="#" class="close" data-dismiss="alert">×</a>' +
               '<strong>Error!</strong>Please try again</div>';
            $('#divcategory').append(response);
            $('.loader-container').addClass('hide');
        }
    });
    $('.alert').fadeOut(10000);
}
function CategoryDeleteCallback(id) {
    $('tr[id=' + id + ']').remove();
    $('.dismissModal').click();
}

function Binddata(dataObj) {
    var body = "";
    body = '<li>' +
        '<div class="catg-name_cptn">' +
        '<h1>Category</h1>' +
        '<h2>' +
        '<div class="pull-right">' +
        '<form class="" role="search">' +
        '<div class="input-group">' +
        '<input type="text" class="form-control" placeholder="Search" name="q" id="txtCategorySearch">' +
        '<div class="input-group-btn">' +
        '<button class="btn btn-default btnsrchgap" id="btnSearch" type="submit"><i class="glyphicon glyphicon-search"></i></button>' +
        '</div>' +
        '</div>' +
        '</form>' +
        '</div>' +
        '</h2>' +
        '</div>' +
        '<div class="catg-refcode_cptn">Ref. Code</div>' +
        '<div class="catg-pcount_cptn">Product Count</div>' +
        '<div class="catg-action_cptn">Actions</div>' +
        '</li>';
    $.each(dataObj, function (key, item) {
        //body += "<tr id='" + item.PRODUCT_ID + "' style='display: ;'>";
        //body += '<td><i class="fa fa-caret-right"></i> ' + item.CATEGORY_DESC + '</td>';
        //body += '<td>' + item.CATEGORY_ID + '</td>';
        //body += '<td>' + item.ProductsCount + '</td>';
        //body += '<td class="categoryActionsBody">' +
        //    '<a href="/CategoryManage/SubCategories/' + item.CATEGORY_ID + '" title="Add"><i class="fa fa-plus"></i></a>&nbsp;' +
        //    '<a data-toggle="modal" href="#normaledit" title="Edit"><i class="fa fa-pencil-square-o"></i>&nbsp;</a>&nbsp;' +
        //    '<a href="/CategoryManage/SubCategories/' + item.CATEGORY_ID + '" title="Attribute"><i class="fa fa-list"></i></a>&nbsp;' +
        //    '<a data-toggle="modal" href="#normaldelete" title="Delete"><i class="fa fa-trash-o"></i></a></td>';
        //body += "</td></tr>";

        body += '<li id="' + item.CATEGORY_ID + '" class="catInsubcat">' +
            '<div class="catg-name" id="catg_navshow" style="cursor: pointer;"><i class="fa fa-caret-down"></i> ' + item.CATEGORY_DESC + '</div>' +
            '<div class="catg-refcode">' + item.REFERENCE_CODE + '</div>' +
            '<div class="catg-pcount">' + item.ProductCount + '</div>' +
            '<div class="catg-action">' +
            '<a href="/CategoryManage/SubCategories/' + item.CATEGORY_ID + '" title="Add"><i class="fa fa-plus"></i></a>&nbsp;' +
            '<a data-toggle="modal" href="#normaledit" title="Edit"><i class="fa fa-pencil-square-o"></i>&nbsp;</a>&nbsp;' +
            '<a href="/CategoryManage/SubCategories/' + item.CATEGORY_ID + '" title="Attribute"><i class="fa fa-list"></i></a>&nbsp;' +
            '<a data-toggle="modal" href="#normaldelete" title="Delete" onclick="deleteCategory(' + item.CATEGORY_ID  + ')" ><i class="fa fa-trash-o"></i></a>' +
            '</div></li>';
        $.each(item.SubCategories, function (count, list) {
            body += '<li class="catg_subname_holder" >';
          body += '<ul class="catg_adddsply subcat ' + list.CATEGORY_ID + '" style="display: none;">' +
                '<li><div class="catg-name">'+
                        '<i class="fa fa-caret-down"></i> '+list.SUB_CAT_DESC+'</div>'+
                    //'<div class="catg-refcode">'+list.REFERENCE_CODE+'</div>'+
                    '<div class="catg-refcode">' + list.REFERENCE_CODE + '</div>' +
                    '<div class="catg-pcount"> '+list.SUB_CAT_ID+'</div>'+
                    '<div class="catg-action">'+
            //'<a href="/CategoryManage/SubCategories/' + list.CATEGORY_ID + '" title="Add"><i class="fa fa-plus"></i></a>&nbsp;' +
                '<a data-toggle="modal" href="/CategoryManage/SubCategories/' + list.CATEGORY_ID + '" title="Edit"><i class="fa fa-pencil-square-o"></i>&nbsp;</a>&nbsp;' +
                //'<a href="/CategoryManage/SubCategories/' + item.CATEGORY_ID + '" title="Attribute"><i class="fa fa-list"></i></a>&nbsp;' +
                '<a data-toggle="modal" href="#normaldelete" title="Delete"><i class="fa fa-trash-o"></i></a>' +
                '</div></li>' +
                '</ul>';
            body += '</li>';
        });
       
    });
    $("#ulCat").html(body);
    $(".catInsubcat").click(function () {
        $('.' + this.id).slideToggle("slow");
    });
}

function DeleteSubCat(id) {
    var result = confirm("Want to delete?");
    if (result) {
        $.ajax({
            //url: '/CategoryManage/DeleteSubCategory/' + id,
            //url: '/Admin/Home/GetProductSearch/',
            url: URL + '/CategoryManage/DeleteSubCategory/' + id,
            type: "Post",
            contentType: 'application/json; charset=utf-8',
            async: false,
            dataType: "json",
            success: function (data) {
                $('tr[id=' + id + ']').remove();
            },
            error: function (xhr) {
                alert(xhr);
            }
        });
    }
}

function EditCategoryCallBack() {
    $('[href="#normaladd"]').click();
}

function deleteCategory(id) {
    $('#deleteCategoryId').val(id);
}