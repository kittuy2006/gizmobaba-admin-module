﻿//var URL = "http://localhost:1859/";
var URL = "http://www.pakkadial.com/Admin/";
//var URL = "http://gizmobaba.in.103-21-58-98.sdin-pp-wb4.webhostbox.net/Test";




$(document).ready(function () {
    //$('#COUNTRY_ID').hide();
    //$('#STATE_ID').hide();
    //$('.rbtnCounter').click(function() {
    //    $('#COUNTRY_ID').show();
    //    $('#STATE_ID').show();
    //});
});
function FillStates() {
    $("#sbOne").html('');
    var id = $('#COUNTRY_ID').val();
    $('.loader-container').removeClass('hide');
    $.ajax({
        url: URL + '/Shipping/GetStates/' + id,
        type: "Get",
        dataType: "json",
        success: function (states) {
            $("#STATE_ID").html("<option value=''>Select States</option>"); // clear before appending new list
            if (states.length === 0) {
                $("#STATE_ID").html("<option value=''>No States</option>"); // clear before appending new list
                $("#STATE_ID").attr('disabled', 'disabled');
            }
            else {
                $("#STATE_ID").removeAttr('disabled');
                $.each(states, function (i, states) {
                    $("#STATE_ID").append(
                        $('<option></option>').val(states.STATE_ID).html(states.STATE));
                });
            }
            $('.loader-container').addClass('hide');
        },
        error: function (xhr) {
            $('.loader-container').addClass('hide');
            alert(xhr);
        }
    });
}

function FillRegionId() {
    var id = $('#STATE_ID').val();
    $('.loader-container').removeClass('hide');
    $("#sbOne").html('');
    $.ajax({
        url: URL + '/Shipping/GetRegions/' + id,
        //url: '/Home/GetSubCategories',
        type: "Get",
        dataType: "json",
        success: function (regions) {
           // $("#sbOne").html("<option value=''>Select Subcategory</option>"); // clear before appending new list
            if (regions.length === 0) {
                $("#sbOne").html("<option value=''>No Regions</option>"); // clear before appending new list
                $("#sbOne").attr('disabled', 'disabled');
            }
            else {
                $("#sbOne").removeAttr('disabled');
                $.each(regions, function (i, regions) {
                    $("#sbOne").append(
                        $('<option></option>').val(regions.REGION_ID).html(regions.REGION));
                });
            }
            $('.loader-container').addClass('hide');
        },
        error: function (xhr) {
            $('.loader-container').addClass('hide');
            alert(xhr);
    }
    });
}
function DeleteShippingCode(id) {
    var status = confirm("Are you sure you want to delete?");   
    if(status)
    {
        $.ajax({
            url: URL + '/Shipping/Delete/' + id,
            //url: '/Home/GetSubCategories',
            type: "Post",
            dataType: "json",
            success: function (regions) {
                $('tr[id=' + id + ']').remove();
            }
        });
    }
}
function FillRegions() {
    var id = $('#STATE_ID').val();
    $('.loader-container').removeClass('hide');
    $.ajax({
        url: URL + '/Shipping/GetRegions/' + id,
        //url: '/Home/GetSubCategories',
        type: "Get",
        dataType: "json",
        success: function (regions) {
            // $("#sbOne").html("<option value=''>Select Subcategory</option>"); // clear before appending new list
            if (regions.length === 0) {
                $("#REGION_ID").html("<option value=''>No Regions</option>"); // clear before appending new list
                $("#REGION_ID").attr('disabled', 'disabled');
                $('#REGION_ID').hide();
            }
            else {
                $("#REGION_ID").removeAttr('disabled');
                $('#REGION_ID').show();
                $.each(regions, function (i, regions) {
                    $("#REGION_ID").html(
                       $('<option></option>').val(regions.REGION_ID).html(regions.REGION));
                });
            }
            $('.loader-container').addClass('hide');
        }
    });
}

