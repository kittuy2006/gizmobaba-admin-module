﻿//var URL = "http://localhost:1859/";
var URL = "http://www.pakkadial.com/Admin/";
//var URL = "http://gizmobaba.in.103-21-58-98.sdin-pp-wb4.webhostbox.net/Test";
$(document).ready(function () {
    //$('#chkSameAddress').attr('checked', true);
    $('#btnExport').click(function () {
        var tbl = $('#tblPro').clone();
        tbl.find('tr td:last-child, tr th:last-child').remove();
        tbl.find('tr td:last-child, tr th:last-child').remove();
        //$("#tblPro").table2excel({
        //    exclude: ".noExl",
        //    name: "Excel Document Name"
        //});
        $(tbl).table2excel({
                exclude: ".noExl",
                name: "Excel Document Name"
            });
    });
    $(".sreachbtnhs-slide").click(function () {
        $("#searchslidehs-panel").slideToggle("slow");
        $(this).toggleClass("active"); return false;
    });

    $(".sreachbtnhs-slide").click(function () {
        $("#searchslidehs-panel2").slideToggle("slow");
        $(this).toggleClass("active"); return false;
    });
    $('#ddlPaging').change(function () {
        var fromdate = '';
        var todate = '';
        if ($('#ddlPaging').val() == "thisweek") {
            var date = getMonday(new Date());
            date = $.datepicker.formatDate('mm/dd/yy', new Date(date));

            var curr = new Date(date);
            var day = curr.getDay();
            var firstday = new Date(curr.getTime() - 60 * 60 * 24 * day * 1000);
            //will return firstday (ie sunday) of the week
            fromdate = $.datepicker.formatDate('mm/dd/yy', new Date(curr.getTime() - 60 * 60 * 24 * day * 1000));
            //adding (60*60*6*24*1000) means adding six days to the firstday which results in lastday (saturday) of the week
            todate = $.datepicker.formatDate('mm/dd/yy', new Date());
            //firstday.getTime() + 60 * 60 * 24 * 6 * 1000        - To get last day of curr week
        }
        else if ($('#ddlPaging').val() == "lastweek") {
            var curr = new Date(getLastWeek());
            var day = curr.getDay();
            var firstday = new Date(curr.getTime() - 60 * 60 * 24 * day * 1000);
            //will return firstday (ie sunday) of the week
            fromdate = $.datepicker.formatDate('mm/dd/yy', new Date(curr.getTime() - 60 * 60 * 24 * day * 1000));
            //adding (60*60*6*24*1000) means adding six days to the firstday which results in lastday (saturday) of the week
            todate = $.datepicker.formatDate('mm/dd/yy', new Date(firstday.getTime() + 60 * 60 * 24 * 6 * 1000));
        }
        else if ($('#ddlPaging').val() == "thismonth") {
            var curr = new Date();
            fromdate = $.datepicker.formatDate('mm/dd/yy', new Date(curr.getFullYear(), curr.getMonth(), 1));
            todate = $.datepicker.formatDate('mm/dd/yy', new Date());
            //curr.getFullYear(), curr.getMonth() + 1, 0     - To get last day of curr mnth
        }
        else if ($('#ddlPaging').val() == "lastmonth") {
            var curr = new Date();
            fromdate = $.datepicker.formatDate('mm/dd/yy', new Date(curr.getFullYear(), curr.getMonth() - 1, 1));
            todate = $.datepicker.formatDate('mm/dd/yy', new Date(curr.getFullYear(), curr.getMonth(), 0));
        }
        $('#fromDate').attr('readonly', true).val(fromdate);
        $('#toDate').attr('readonly', true).val(todate);
        $('#fromDate, #toDate').datepicker('destroy');
        if ($('#ddlPaging').val() == "custom") {
            $('#fromDate').removeAttr('readonly').val('');
            $('#toDate').removeAttr('readonly').val('');
            $('#fromDate, #toDate').datepicker();
        }
    });
    //$('.search-plus-btnsm').click(function (event) {
    //        event.preventDefault();
    //        var target = $(this).attr('href');
    //        $(target).toggleClass('hidden show');
    //
    //    });

    $(".search-plus-btnsm").click(function () {
        $("#search-plus-btnsmhs").slideToggle();
    });
    $('#chkSameAddress').click(function () {
        if ($(this).is(":checked")) {
            $('#divBillingAddress').fadeOut(1000);
            $('#BillingAddress_FIRSTNAME').val($('#ShippinghAddress_FIRSTNAME').val());
            $('#BillingAddress_LASTNAME').val($('#ShippinghAddress_LASTNAME').val());
            $('#BillingAddress_ADDRESS_1').val($('#ShippinghAddress_ADDRESS_1').val());
            $('#BillingAddress_ADDRESS_2').val($('#ShippinghAddress_ADDRESS_2').val());
            $('#BillingAddress_CITY').val($('#ShippinghAddress_CITY').val());
            $('#BillingAddress_POSTCODE').val($('#ShippinghAddress_POSTCODE').val);
            $('#BillingAddress_ALT_MOB').val($('#ShippinghAddress_ALT_MOB').val());
            $('#BillingAddress.MOB_NO').val($('#ShippinghAddress_MOB_NO').val());
            $('#BillingAddress_COUNTRY').val($('#ShippinghAddress_COUNTRY').val());
            $('#BillingAddress_STATE').val($('#ShippinghAddress_STATE').val());
            $('#BillingAddress_EMAIL').val($('#ShippinghAddress_EMAIL').val());
        } else {
            $('#divBillingAddress').fadeIn(1000);
            $('#BillingAddress_FIRSTNAME').val('');
            $('#BillingAddress_LASTNAME').val('');
            $('#BillingAddress_ADDRESS_1').val('');
            $('#BillingAddress_ADDRESS_2').val('');
            $('#BillingAddress_CITY').val('');
            $('#BillingAddress_POSTCODE').val('');
            $('#BillingAddress_ALT_MOB').val('');
            $('#BillingAddress.MOB_NO').val('');
            $('#BillingAddress_COUNTRY').val('');
            $('#BillingAddress_STATE').val('');
            $('#BillingAddress_EMAIL').val('');
        }
    });
    $('#ddlMarketPeriod').change(function () {
        var fromdate = '';
        var todate = '';
        if ($('#ddlMarketPeriod').val() == "thisweek") {
            var date = getMonday(new Date());
            date = $.datepicker.formatDate('mm/dd/yy', new Date(date));

            var curr = new Date(date);
            var day = curr.getDay();
            var firstday = new Date(curr.getTime() - 60 * 60 * 24 * day * 1000);
            //will return firstday (ie sunday) of the week
            fromdate = $.datepicker.formatDate('mm/dd/yy', new Date(curr.getTime() - 60 * 60 * 24 * day * 1000));
            //adding (60*60*6*24*1000) means adding six days to the firstday which results in lastday (saturday) of the week
            todate = $.datepicker.formatDate('mm/dd/yy', new Date());
            //firstday.getTime() + 60 * 60 * 24 * 6 * 1000        - To get last day of curr week
        }
        else if ($('#ddlMarketPeriod').val() == "lastweek") {
            var curr = new Date(getLastWeek());
            var day = curr.getDay();
            var firstday = new Date(curr.getTime() - 60 * 60 * 24 * day * 1000);
            //will return firstday (ie sunday) of the week
            fromdate = $.datepicker.formatDate('mm/dd/yy', new Date(curr.getTime() - 60 * 60 * 24 * day * 1000));
            //adding (60*60*6*24*1000) means adding six days to the firstday which results in lastday (saturday) of the week
            todate = $.datepicker.formatDate('mm/dd/yy', new Date(firstday.getTime() + 60 * 60 * 24 * 6 * 1000));
        }
        else if ($('#ddlMarketPeriod').val() == "thismonth") {
            var curr = new Date();
            fromdate = $.datepicker.formatDate('mm/dd/yy', new Date(curr.getFullYear(), curr.getMonth(), 1));
            todate = $.datepicker.formatDate('mm/dd/yy', new Date());
            //curr.getFullYear(), curr.getMonth() + 1, 0     - To get last day of curr mnth
        }
        else if ($('#ddlMarketPeriod').val() == "lastmonth") {
            var curr = new Date();
            fromdate = $.datepicker.formatDate('mm/dd/yy', new Date(curr.getFullYear(), curr.getMonth() - 1, 1));
            todate = $.datepicker.formatDate('mm/dd/yy', new Date(curr.getFullYear(), curr.getMonth(), 0));
        }
        $('#marketfromDate').attr('readonly', true).val(fromdate);
        $('#markettoDate').attr('readonly', true).val(todate);
        $('#marketfromDate, #markettoDate').datepicker('destroy');
        if ($('#ddlMarketPeriod').val() == "custom") {
            $('#marketfromDate').removeAttr('readonly').val('');
            $('#markettoDate').removeAttr('readonly').val('');
            $('#marketfromDate, #marketfromDate').datepicker();
        }
    });
});
function getMonday(d) {
    d = new Date(d);
    var day = d.getDay(),
        diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
    return new Date(d.setDate(diff));
}
function getLastWeek() {
    var today = new Date();
    var lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
    return lastWeek;
}
function FillSubCategory() {
            $('.loader-container').removeClass('hide');
            var categoryid = $('#CATEGORY_ID').val();
    $.ajax({
        //url: '/Admin/Home/GetSubCategories',
        url: URL + '/Home/GetSubCategories',
        //url: '/Home/GetSubCategories',
        type: "Get",
        dataType: "json",
        data: { CategoryID: categoryid },
        success: function (subCategories) {
            $("#SUB_CAT_ID").html("<option value=''>Select Subcategory</option>"); // clear before appending new list
            if (subCategories.length === 0) {
                $("#SUB_CAT_ID").html("<option value=''>No Subcategories</option>"); // clear before appending new list
                $("#SUB_CAT_ID").attr('disabled', 'disabled');
                $('.loader-container').addClass('hide');
            }
            else {
                $("#SUB_CAT_ID").removeAttr('disabled');
                $.each(subCategories, function (i, subCategory) {
                    $("#SUB_CAT_ID").append(
                        $('<option></option>').val(subCategory.SUB_CAT_ID).html(subCategory.SUB_CAT_DESC));
                });
                $('.loader-container').addClass('hide');
            }
        }
    });
}
function serchBy() {
    var value = $('#ddlSerch').val();
    var textBox = '<input id="' + value + '" name="' + value + '" placeholder="" required="" class="form-control" type="text">';
    $('#searchBy').html(textBox);
}
function SearchOrder() {
    var CATEGORY_ID = $('#CATEGORY_ID').val();
    var From_Date = $('#fromDate').val();
    var To_Date = $('#toDate').val();
    var SUB_CAT_ID = $('#SUB_CAT_ID').val();
    var Order_No = $('#Order_No').val();
    var SKU = $('#SKU').val();
    var Email_Id = $('#Email_Id').val();
    var Phone_No = $('#Phone_No').val();
    var Coupon_Code = $('#Coupon_Code').val();
    var Checkout_Type = $('#Checkout_Type').val();
    var Shipment_ID = $('#Shipment_ID').val();
    var Issue_Type = $('#Issue_Type').val();
    var Order_Source = $('#Order_Source').val();

    $.ajax({
        url: URL + '/Order/SearchOrder',
        type: "Get",
        dataType: "json",
        data: { CATEGORY_ID: CATEGORY_ID, From_Date: From_Date, To_Date: To_Date, SUB_CAT_ID: SUB_CAT_ID, Order_No: Order_No, SKU: SKU, Email_Id: Email_Id, Phone_No: Phone_No, Coupon_Code: Coupon_Code, Checkout_Type: Checkout_Type, Shipment_ID: Shipment_ID, Issue_Type: Issue_Type, Order_Source: Order_Source },
        success: function (data) {
            if (data.length == 0) {
                BindData(data);
            } else {
                $('#divdata').html('<h3>No Product</h3>');
            }

        }
    });
}

function BindData(dataObj) {
    var htmlcode = '<div class="tab-content">' +
        '<div role="tabpanel" class="tab-pane active" id="MyOrders">' +
        '<div class="mmctrl_box">' +
        '<div class="mmcaption">Available Orders</div>';
    htmlcode += '<table class="table table-striped" id="tblPro" style="margin-bottom:6px; border-bottom:1px solid #DDD;">' +
        '<thead><tr>' +
        '<th>Age</th>' +
        '<th>Order ID</th>' +
        '<th>Order Date</th>' +
        '<th>Chekout Type</th>' +
        '<th>Qty</th>' +
        '<th>Order Vaule</th>' +
        '<th>Customer Name</th>' +
        '<th>Authorize</th>' +
        '<th>Cancel Orders</th>' +
        '</tr></thead>';
    htmlcode += '<tbody>';
    $.each(dataObj, function (key, item) {
        if (key < 10) {
            htmlcode += "<tr id='" + item.ORDER_ID + "' style='display: ;'>";
            htmlcode += ' <td><a>' + item.Age + '</a></td>';
            htmlcode += ' <td><a>' + item.ORDER_ID + '</a></td>';
            htmlcode += ' <td>' + item.CREATED_AT + '</td>';
            htmlcode += ' <td>' + item.CHECKOUTTYPE + '</td>';
            htmlcode += ' <td>' + item.Qty + '</td>';
            htmlcode += ' <td>' + item.UNIT_PRICE + '</td>';
            htmlcode += ' <td><a>' + item.UserName + '</a></td>';
            htmlcode += ' <td><a href="../Order/OrderDetails/' + item.ORDER_ID + '">Authorize</a></td>';
            htmlcode += ' <td><a href="../Order/OrderDetails/' + item.ORDER_ID + '">Cancel</a></td>';
            //htmlcode += '<td><a href="javascript:;" onclick="cancelOrder(' + item.ORDER_ID + ');">Cancel</a></td>';
            htmlcode += "</tr>";
        }
    });
    htmlcode += '</tbody>';
    htmlcode += '</table><br>';

    htmlcode += '<div class="dataTables_length" id="example_length">' +
        '<label>' +
        '&nbsp; Records per page' +
        '<select aria-controls="example" size="1" style="width: 56px;padding: 6px;" id="ddlPaging" name="example_length">' +
        '<option value="10">10</option>' +
        '<option value="25">25</option>' +
        '<option value="50">50</option>' +
        '<option value="100">100</option>' +
        '</select>' +
        '</label>' +
        '<label> Go to <input type="text" class="gototxt_no" maxlength="4" id="pagingVal" aria-controls="example"></label></div>' +
        '<div class="clearfix"></div><br>' +
        '</div><hr>' +
        '<div class="clearfix"></div>' +
        '<div class="col-md-12">' +
        '<button type="button" class="btn btn-dark pull-right">Export to Excel</button>' +
        '</div><br><br><br>' +
        '</div></div>';
    $('#divdata').html(htmlcode);

}

function deleteOrder() {
        var status = confirm("Are you sure you want to cancel?");
        if (status) {
            var orderDetails = {
                ORDER_ID: $('#ORDER_ID').val(),
                COMMENTS: $('#COMMENTS').val(),
                RESON: $('#RESON').val(),
                USERSHOWTHECOMMENTS: $('#USERSHOWTHECOMMENTS').val()
            }
            $.ajax({
                url: URL + '/Order/CancelOrder/',
                //url: '/Order/CancelOrder/',
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                async: false,
                dataType: "json",
                data: JSON.stringify(orderDetails),
                success: function (data) {
                    alert('Order is canceld');
                    $('.close').click();
                },
                error: function (xhr) {
                    alert(xhr);
                }
            });
        }
}

function UserSearch(parameters) {
    $('.loader-container').removeClass('hide');
    var key = $('#txtUser').val();
    if (key != '') {
        var categoty = key;
        $.ajax({
            url: URL + '/Order/GetUserSearch/',
            //url: '/Order/GetUserSearch/',
            type: "Get",
            contentType: 'application/json; charset=utf-8',
            async: false,
            data: { key: categoty },
            dataType: "json",
            success: function (data) {
                BindUserData(data);
                $('.loader-container').addClass('hide');
            },
            error: function (xhr) {
                alert(xhr);
                $('.loader-container').addClass('hide');
            }
        });
    }
}

function BindUserData(obj) {
    if (obj != null && obj.length != 0) {
        if (obj.ShippinghAddress.length != 0) {
            $('#ShippinghAddress_FIRSTNAME').val(obj.ShippinghAddress.FIRSTNAME);
            $('#ShippinghAddress_LASTNAME').val(obj.ShippinghAddress.LASTNAME);
            $('#ShippinghAddress_ADDRESS_1').val(obj.ShippinghAddress.ADDRESS_1);
            $('#ShippinghAddress_ADDRESS_2').val(obj.ShippinghAddress.ADDRESS_2);
            $('#ShippinghAddress_CITY').val(obj.ShippinghAddress.CITY);
            $('#ShippinghAddress_POSTCODE').val(obj.ShippinghAddress.POSTCODE);
            $('#ShippinghAddress_landmark').val(obj.ShippinghAddress.landmark);
            $('#ShippinghAddress_mob_no').val(obj.ShippinghAddress.mob_no);
        } if (obj.BillingAddress.length != 0) {
        $('#BillingAddress_FIRSTNAME').val(obj.BillingAddress.FIRSTNAME);
            $('#BillingAddress_LASTNAME').val(obj.BillingAddress.LASTNAME);
            $('#BillingAddress_ADDRESS_1').val(obj.BillingAddress.ADDRESS_1);
            $('#BillingAddress_ADDRESS_2').val(obj.BillingAddress.ADDRESS_2);
            $('#BillingAddress_CITY').val(obj.BillingAddress.CITY);
            $('#BillingAddress_POSTCODE').val(obj.BillingAddress.POSTCODE);
            $('#BillingAddress_landmark').val(obj.BillingAddress.landmark);
            $('#BillingAddress_mob_no').val(obj.BillingAddress.mob_no);

        }
    }
}

function DeleteRow(thi){
    $(thi).parents('tr').remove();
}

function AddNewOrder() {
    var data = $('#trOrder').clone().html();
    var htmoCode= '<tr>' + data + '<td><div class="prdcts-action"><a href="javascript:;" onclick="DeleteRow(this);" title=""><i class="fa fa-close"></i></a></div></td>'+'</tr>';
   $('#tblOrder').append(htmoCode);
}
function Voucher()
{
    if ($('#txtVoucher').val() != '') {
        var value = $('#txtVoucher').val();
        $.ajax({
            url: URL + '/Order/VoucherSearch/',
            //url: '/Order/VoucherSearch/',
            type: "Get",
            contentType: 'application/json; charset=utf-8',
            async: false,
            dataType: "json",
            data: { Voucher: value },
            success: function (data) {
                 if(data == "Invalid Voucher Code" || data == "Voucher code expiry date completed")  {
                    var Message = '<div class="alert alertnew alert-danger fade in"><a href="#" class="close" data-dismiss="alert">×</a>' +
       '<strong>! </strong>'+data+'</div>';
                    $('#txtVoucher').parent('div').append(Message);
                } else {
                     var Message = '<div class="alert alertnew alert-success fade in"><a href="#" class="close" data-dismiss="alert">×</a>' +
           '<strong>Success!</strong>Voucher Code applied</div>';
                     $('#txtVoucher').parent('div').append(Message);
                     var DISCOUNT = parseInt(data.DISCOUNT);
                     var amount = parseInt($('#Amount').val());
                     var AmountiwthDis = parseInt(amount - DISCOUNT);
                     $('#Amount').val(AmountiwthDis);

                }
            },
            error: function (xhr) {
                alert(xhr);
            }
        });
    } else {
        alert('Enter Voucher Code');
    }
    $('.alert').fadeOut(10000);

}