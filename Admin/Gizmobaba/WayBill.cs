﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Gizmobaba.WayBillServiceReference;
using Gizmobaba.Utility;
using System.Configuration;


namespace Gizmobaba
{
    public class WayBill
    {
        private BindingConfigType _Type;
        UserProfile awbuser;

        public BDResponseDTO GenerateAWB(BDWayBill waybill)
        {
            BDResponseDTO responseStatus = new BDResponseDTO();
            string AWBNo = "";
            string DestArea = "";
            string DestScrcd = "";
            string errorMsg = string.Empty;
            try
            {
                string d1 = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
               WayBillGenerationClient client;
                //WaybillGeneration.WayBillGenerationClient client 

                //if (_Type == BindingConfigType.BasicHttpBinding)
                //    client = new WayBillGenerationClient("BasicHttpBinding_IWayBillGeneration");
                //else if (_Type == BindingConfigType.WSHttpBinding)
                //    client = new WayBillGenerationClient("WSHttpBinding_IWayBillGeneration");
                //else
                //    client = new WayBillGenerationClient();
               client = new WayBillGenerationClient("WSHttpBinding_IWayBillGeneration");


                WayBillGenerationRequest req = new WayBillGenerationRequest();
                req.Shipper = new Shipper();
                req.Shipper.OriginArea = "BOM";
                req.Shipper.CustomerCode = "344400";
                req.Shipper.CustomerName = waybill.shipper.CustomerName;
                //req.Shipper.CustomerAddress1 = waybill.shipper.CustomerAddress1.Substring(0,10);
                req.Shipper.CustomerAddress1 = waybill.shipper.CustomerAddress1;
                req.Shipper.CustomerAddress2 = waybill.shipper.CustomerAddress2;
                req.Shipper.CustomerAddress3 = waybill.shipper.CustomerAddress3;
                req.Shipper.CustomerPincode = waybill.shipper.CustomerPincode;
                req.Shipper.CustomerEmailID = waybill.shipper.CustomerEmailID;
                req.Shipper.CustomerMobile = waybill.shipper.CustomerMobile;
                req.Shipper.CustomerTelephone = waybill.shipper.CustomerName;
                req.Shipper.IsToPayCustomer = (bool)waybill.shipper.IsToPayCustomer;
                req.Shipper.Sender = waybill.shipper.Sender;
                req.Shipper.VendorCode = waybill.shipper.CustomerName;
                req.Consignee = new Consignee();
                req.Consignee.ConsigneeName = waybill.consignee.ConsigneeName;
                //req.Consignee.ConsigneeAddress1 = waybill.consignee.ConsigneeAddress1.Substring(0, 10);
                req.Consignee.ConsigneeAddress1 = waybill.consignee.ConsigneeAddress1;
                req.Consignee.ConsigneeAddress2 = waybill.consignee.ConsigneeAddress2;
                req.Consignee.ConsigneeAddress3 = waybill.consignee.ConsigneeAddress3;
                req.Consignee.ConsigneePincode = waybill.consignee.ConsigneePincode;
                req.Consignee.ConsigneeAttention = waybill.consignee.ConsigneeAttention; ;
                req.Consignee.ConsigneeTelephone = waybill.consignee.ConsigneeTelephone;
                req.Consignee.ConsigneeMobile = waybill.consignee.ConsigneeMobile;

                req.Services = new Services();

                if (!string.IsNullOrEmpty(waybill.services.ActualWeight)) req.Services.ActualWeight = Convert.ToDouble(waybill.services.ActualWeight);
                req.Services.CreditReferenceNo = waybill.services.CreditReferenceNo;
                //if (!string.IsNullOrEmpty(waybill.services.CollectableAmount)) req.Services.CollectableAmount = Convert.ToDouble(waybill.services.CollectableAmount);
                if (!string.IsNullOrEmpty(waybill.services.DeclaredValue)) req.Services.DeclaredValue = Convert.ToDouble(waybill.services.DeclaredValue);

                req.Services.InvoiceNo = waybill.services.InvoiceNo;
                req.Services.PackType = waybill.services.PackType;
                req.Services.PieceCount = waybill.services.PieceCount;
                req.Services.ProductCode = waybill.services.ProductCode;
                req.Services.ProductType = ((bool)waybill.services.ProductType) ? ProductType.Docs : ProductType.Dutiables;
                req.Services.SpecialInstruction = waybill.services.SpecialInstruction;
                req.Services.SubProductCode = waybill.services.SubProductCode;
                req.Services.PickupDate = waybill.services.PickupDate;
                req.Services.PickupTime = waybill.services.PickupTime;
                req.Services.Commodity = new CommodityDetail();
                if (waybill.services.commodity != null)
                {
                    req.Services.Commodity.CommodityDetail1 = waybill.services.commodity.CommodityDetail1;
                    req.Services.Commodity.CommodityDetail2 = waybill.services.commodity.CommodityDetail2;
                    req.Services.Commodity.CommodityDetail3 = waybill.services.commodity.CommodityDetail3;
                }
                req.Services.PDFOutputNotRequired = ((bool)waybill.services.PDFOutputNotRequired) ? true : false;
                req.Services.AWBNo = waybill.services.InvoiceNo;
                req.Services.RegisterPickup = ((bool)waybill.services.RegisterPickup) ? true : false;
                req.Services.IsReversePickup = ((bool)waybill.services.IsReversePickup) ? true : false;
                //req.Services.DeclaredValue = Convert waybill.services.DeclaredValue;
                req.Services.ParcelShopCode = waybill.services.ParcelShopCode;
                List<Dimension> dims = GetDimension();
                req.Services.Dimensions = dims.ToArray();
                LoadAWBUserProfile();
                req.Shipper.VendorCode = "344400";
                WayBillGenerationResponse response = client.GenerateWayBill(req, awbuser);

                string d2 = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");


              //  label23.Content = d1;
              //  label26.Content = d2;
                //MessageBox.Show((response.IsError) ? "Error : " +  response.Status[0].StatusInformation : "AWB NO : " + response.AWBNo);
              //  txtIsErrorAWB.Text = response.IsError.ToString();

                foreach (var item in response.Status)
                {
                    if (!string.IsNullOrEmpty(item.StatusInformation))
                    {
                        errorMsg = (string.IsNullOrEmpty(errorMsg)) ? string.Format("{0} : {1}", item.StatusCode, item.StatusInformation) : errorMsg + string.Format("\n{0} : {1}", item.StatusCode, item.StatusInformation);
                    }
                    else
                    {
                        errorMsg = (string.IsNullOrEmpty(errorMsg)) ? string.Format("{0}", item.StatusCode, item.StatusInformation) : errorMsg + string.Format("\n{0}", item.StatusCode, item.StatusInformation);
                    }
                }
                //if (response.IsError)
                //{
                //    return "";
                //}
               responseStatus.AWBNo =  AWBNo = response.AWBNo;
               responseStatus.DestArea = DestArea = response.DestinationArea;
               responseStatus.DestScrcd = DestScrcd = response.DestinationLocation;
              //  txterror1.Text = errorMsg;
               if (response.AWBPrintContent != null)
               {
                   string filepath = GetConfigValue("AWBFILELOCATION");
                   if (string.IsNullOrEmpty(filepath))
                   {
                       filepath = response.AWBNo + ".pdf";
                   }
                   else
                   {

                       if (!System.IO.Directory.Exists(filepath))
                       {
                           // MessageBox.Show("Could not find location '" + filepath + "' specified in config file as 'AWBFILELOCATION' to save Waybill in PDF format.");
                       }
                       else
                       {
                           if (!filepath.EndsWith("/"))
                           {
                               filepath = filepath + "/";
                           }
                           filepath = filepath + response.AWBNo + ".pdf";
                       }
                   }
                   System.IO.Stream sm = null;
                   try
                   {
                       sm = new System.IO.MemoryStream(response.AWBPrintContent);
                       using (System.IO.Stream destination = System.IO.File.Create(filepath))
                       {
                           for (int a = sm.ReadByte(); a != -1; a = sm.ReadByte())
                           {
                               destination.WriteByte((byte)a);
                           }
                       }
                   }
                   finally
                   {
                       sm.Close();
                       if (sm != null)
                       {
                           sm.Dispose();
                           sm = null;
                       }
                   }
               }

            }
            catch (System.ServiceModel.CommunicationException ex)
            {
                errorMsg = "Unable to reach Shipping API service";
               // MessageBox.Show(ex.Message.ToString());
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
               // MessageBox.Show(ex.Message);
            }
            responseStatus.Message = errorMsg;
          //  return AWBNo;
            return responseStatus;
        }
        public enum BindingConfigType
        {
            BasicHttpBinding = 0,
            WSHttpBinding = 1
        }

        private void LoadAWBUserProfile()
        {

            awbuser = new UserProfile();
            awbuser.LoginID = System.Configuration.ConfigurationManager.AppSettings["LoginID"].ToString();
            //pickupuser.Password = System.Configuration.ConfigurationManager.AppSettings["Password"].ToString();
            awbuser.LicenceKey = System.Configuration.ConfigurationManager.AppSettings["LicenceKey"].ToString();
            awbuser.Api_type = System.Configuration.ConfigurationManager.AppSettings["Api_type"].ToString();
            awbuser.Version = System.Configuration.ConfigurationManager.AppSettings["Version"].ToString();
            awbuser.Area = System.Configuration.ConfigurationManager.AppSettings["Area"].ToString();
            //pickupuser.Customercode = System.Configuration.ConfigurationManager.AppSettings["Customercode"].ToString();
            //pickupuser.IsAdmin = System.Configuration.ConfigurationManager.AppSettings["IsAdmin"].ToString();

        }
        public static string GetConfigValue(string Key)
        {
            try
            {
                return ConfigurationManager.AppSettings.Get(Key);
            }
            catch
            {
                return string.Empty;
            }

        }
        private List<Dimension> GetDimension()
        {
            List<Dimension> dimes = new List<Dimension>();
           // UserControl1 cntrl;
           // Dimension _dimensn;
            //foreach (var item in lstDims.Items)
            //{
            //    //StackPanel item1 =  Template.FindName("Dims", (FrameworkElement)item) as StackPanel;
            //    //if (item1 != null)
            //    //    MessageBox.Show("StackPanel null");
            //    //TextBox textBox = Template.FindName("txtServiceL1", item1) as TextBox;

            //    cntrl = (UserControl1)item;
            //    _dimensn = new ShippingAPI.Client.WaybillGeneration.Dimension();
            //    if (!string.IsNullOrEmpty(cntrl.txtServiceL1.Text)) _dimensn.Length = Convert.ToDouble(cntrl.txtServiceL1.Text);
            //    if (!string.IsNullOrEmpty(cntrl.txtServiceB1.Text)) _dimensn.Breadth = Convert.ToDouble(cntrl.txtServiceB1.Text);
            //    if (!string.IsNullOrEmpty(cntrl.txtServiceH1.Text)) _dimensn.Height = Convert.ToDouble(cntrl.txtServiceH1.Text);
            //    if (!string.IsNullOrEmpty(cntrl.txtServiceW1.Text)) _dimensn.Count = Convert.ToInt32(cntrl.txtServiceW1.Text);
            //    if (!(_dimensn.Length == 0 && _dimensn.Breadth == 0 && _dimensn.Height == 0))
            //    {
            //        dimes.Add(_dimensn);
            //    }
            //}
            return dimes;
        }
    }
}