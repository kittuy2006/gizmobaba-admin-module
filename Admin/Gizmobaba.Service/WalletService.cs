﻿using System;
using System.Collections.Generic;
using Gizmobaba.Repository;
using Gizmobaba.Repository.Interfaces;
using Gizmobaba.Service.Interfaces;
using Gizmobaba.Utility;

namespace Gizmobaba.Service
{
    public class WalletService : IWalletService
    {
        private readonly IWalletRepository _walletRepository = new WalletRepository();
        public int AddWallet(M_GB_WALLET wallet)
        {
            return _walletRepository.AddWallet(wallet);
        }
        public WalletDto GetWallet()
        {
            return _walletRepository.GetWallet();
        }
        public IEnumerable<BonusDto> GetWalletList()
        {
            return _walletRepository.GetWalletList();
        }
        public int CreateWallet(GB_BONUS bonus)
        {
            return _walletRepository.CreateWallet(bonus);
        }


        public IEnumerable<BonusDto> GetWalletListByRole(int roleId)
        {
            return _walletRepository.GetWalletListByRole(roleId);
        }


        public IEnumerable<M_GB_Payment_GatWay> GetPaymentgatwayList()
        {
            return _walletRepository.GetPaymentgatwayList();
        }


        public M_GB_Payment_GatWay GetPaymentgatway(int id)
        {
            return _walletRepository.GetPaymentgatway(id);
        }
    }
}
