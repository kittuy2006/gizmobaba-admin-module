﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizmobaba.Service.Interfaces;
using Gizmobaba.Utility;
using Gizmobaba.Repository.Interfaces;
using Gizmobaba.Repository;

namespace Gizmobaba.Service
{
    public class UserService :IUserService
    {
        private readonly IUserRepository _userRepository = new UserRepository(); 
        public UserDto UserDetails(string name)
        {
            return _userRepository.UserDetails(name);
        }

        public string AddUser(M_GB_USER user)
        {
            return _userRepository.AddUser(user);
        }

        public int AddUserAddress(GB_USER_ADDRESS userAddress)
        {
            int status = _userRepository.AddUserAddress(userAddress);
            return status;
        }


        public List<UserDto> GetAllUsers(int id)
        {
            return _userRepository.GetAllUsers(id);
        }


        public List<UserDto> UserSearch(int key, string serarchKey, DateTime frmDate, DateTime toDate)
        {
            return _userRepository.UserSearch( key, serarchKey, frmDate, toDate);
        }


        public int UpdateUserStatus(int userId, int roleId)
        {
            return _userRepository.UpdateUserStatus(userId,roleId);
        }



        public IEnumerable<UserDto> GetUsersbyCount(int count, int roleID)
        {
            return _userRepository.GetUsersbyCount(count, roleID);
        }


        public M_GB_USER GetUser(int userId)
        {
            return _userRepository.GetUser(userId);
        }


        public IEnumerable<UserDto> GetUsers(DateTime frmDate, DateTime toDate)
        {
            return _userRepository.GetUsers(frmDate,toDate);
        }


        public IEnumerable<UserDto> GetUsersSearch(UserDto data)
        {
            return _userRepository.GetUsersSearch(data);
        }


        public M_GB_USER GetUserByEmailID(string email)
        {
            return _userRepository.GetUserByEmailID(email);
        }


        public IEnumerable<GB_USER_ADDRESS> GetUserAddress(string user_Id)
        {
            return _userRepository.GetUserAddress(user_Id);
        }

        public IEnumerable<Dealer> GetActiveDealers(int status)
        {
            return _userRepository.GetActiveDealers(status);
        }

        public IEnumerable<Dealer> GetSearchDealer(DateTime frmDate, DateTime tDate, string userName, string userId,string pincode)
        {
            return _userRepository.GetSearchDealer(frmDate, tDate,userName,userId,pincode);
        }

        public IEnumerable<Dealer> GetMasterFranchise(int status)
        {
            return _userRepository.GetMasterFranchise(status);
        }

        public IEnumerable<Dealer> GetSearchMasterFranchise(DateTime fromDate, DateTime toDate)
        {
            return _userRepository.GetSearchMasterFranchise(fromDate, toDate);
        }

        public string AddDealerDetails(M_GB_USERDETAILS userdetails)
        {
            return _userRepository.AddDealerDetails(userdetails);
        }

        public IEnumerable<UserDto> GetGlobalvendor(int status)
        {
            return _userRepository.GetGlobalvendor(status);
        }

        public string AddDealerUser(Dealer dealer)
        {
            return _userRepository.AddDealerUser(dealer);
        }

        public string UpdateRecharge(GB_DEALER_RECHARGE recharge)
        {
            return _userRepository.UpdateRecharge(recharge);
        }

        public string AddMasterFranchiseDetails(M_GB_USERDETAILS userdetails)
        {
            return _userRepository.AddMasterFranchiseDetails(userdetails);
        }

        public IEnumerable<UserDto> GetMasterFranchisebyStateId(int id)
        {
            return _userRepository.GetMasterFranchisebyStateId(id);
        }

        public Dealer GetMasterWallet(int masterWalletId)
        {
            return _userRepository.GetMasterWallet(masterWalletId);
        }

        public Dealer GethDealer(int id)
        {
            return _userRepository.GetSearchDealer(id);
        }

        public GB_DEALER_RECHARGE GetMasterFranchiseRecharge(int id)
        {
            return _userRepository.GetMasterFranchiseRecharge(id);
        }

        public Dealer GetVender(int id)
        {
            return _userRepository.GetVender(id);
        }

        public IEnumerable<Dealer> GetrechargeWalletDeatils(int userDetailsId)
        {
            return _userRepository.GetrechargeWalletDeatils(userDetailsId);
        }


        public int CheckEmail(string email)
        {
            return _userRepository.CheckEmail(email);
        }


        public int CheckMasterFranchiseByState(int stateId)
        {
            return _userRepository.CheckMasterFranchiseByState(stateId);
        }


        public int UpdateUserStatus(int userId)
        {
            return _userRepository.UpdateUserStatus(userId);
        }


        public string UpdateMFRecharge(GB_DEALER_RECHARGE recharge)
        {
            return _userRepository.UpdateMFRecharge(recharge);
        }


        public string findUserNamebyId(string userId)
        {
            return _userRepository.findUserNamebyId(userId);
        }


        public IEnumerable<Dealer> GetDealersnyMFId(int id)
        {
            return _userRepository.GetDealersnyMFId(id);
        }
    }
}
