﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizmobaba.Repository;
using Gizmobaba.Service.Interfaces;
using Gizmobaba.Utility;
using Gizmobaba.Repository.Interfaces;

namespace Gizmobaba.Service
{
    public class DashboardService : IDashboardService
    {
        private readonly IDashboardRepository _dashboardRepository = new DashboardRepository();

        public DashboardDto Get()
        {
            return _dashboardRepository.Get();
        }


        public DashboardDto GetGlobalVenderData(string id)
        {
            return _dashboardRepository.GetGlobalVenderData(id);
        }


        public DashboardDto GetMasterFranchiseDashboard(string id)
        {
            return _dashboardRepository.GetMasterFranchiseDashboard(id);
        }
    }
}
