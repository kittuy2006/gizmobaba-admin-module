﻿using Gizmobaba.Utility;
using System;
using System.Collections.Generic;

namespace Gizmobaba.Service.Interfaces
{
    public interface IProductService
    {
        Int64 AddProduct(GB_PRODUCT_DETAIL productDetail);
        IEnumerable<GB_PRODUCT_DETAIL> GetAll();

        int Delete(int productId);
        IEnumerable<M_GB_CATEGORY> GetCategories();

        IEnumerable<M_GB_SHIPPING> GetAvailableShippingCodes();

        GB_PRODUCT_DETAIL GetProduct(int productId);

        IEnumerable<ProductDTO> GetProductSearch(string product);

        int DeactivateProduct(int productId);
        int AddProductList(IList<ProductDTO> product);

        int AddBanner(M_GB_BANNERS banner);
        IEnumerable<Banner> GetBanners();

        int DeleteBanner(int id);

        int SetBanner(int id);

        int Activate(int id);

        int DeActivate(int id);

        IEnumerable<ProductImagesDto> GetProductImages(int productId);

        int DeleteImage(int imageId);

        int EditImage(GB_PRODUCT_IMAGES img);

        int CheckSku(string sku);

        int AddProductImage(GB_PRODUCT_IMAGES productimages);

        IEnumerable<VendorDto> GetVendors();
        IEnumerable<WarrantyDto> GetWarranty();
        IEnumerable<CustomFieldsDto> GetCustomFields();
        IEnumerable<SectionDto> GetSection();
        IEnumerable<ConditiongDto> GetCondition();

        int AddProductAdditionalinfo(GB_PRODUCT_INFORMATION productInformation);

        ProductDTO GetProductInfomation(int productId);

        IEnumerable<TagsDto> GetProductTags();

        IEnumerable<TagsDto> GetAllProductTags();

        TagsDto GetProductTags(int tagId);

        int AddProductTag(M_GB_TAGS tagsDto);

        int DeleteProductTag(int tagId);

        int AddTagToProduct(GB_PRODUCT_TAG tags);

        IEnumerable<Banner> GetPredefinedBanners();

        Banner GetBanner(int id);

        IEnumerable<Banner> GetCategoryBanners();

        IEnumerable<Banner> GetAddsBanners();

        IEnumerable<M_GB_SUB_CATEGORY> GetAllSubCat();

        IEnumerable<GB_BULK_UPLOAD> GetBulkuploadList();

        int AddBulkProUploadList(GB_BULK_UPLOAD bulkUpload);

        IEnumerable<PRODUCTREVIEWSDTO> GetReviews();

        int UpdateReviewStatus(int p);

        int DeleteReview(int p);

        IEnumerable<M_GB_TAX> GetTaxes();

        int AddTax(M_GB_TAX tax);

        M_GB_TAX GetProductTax(int id);

        string DeleteProductTax(int p);
        string GetProSku();

        IEnumerable<ProductDTO> GetProductInvAdvSearch(DateTime fromDate, DateTime toDate);

        ProductDTO GetProductbySku(string sku);
    }
}
