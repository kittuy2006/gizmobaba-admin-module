﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizmobaba.Utility;

namespace Gizmobaba.Service.Interfaces
{
    public interface IShippingService : ICommonService<M_GB_SHIPPING>
    {
        IEnumerable<ShippingMethodDto> GetShippingMethods();

        IEnumerable<M_GB_SHIPPING> GetShippingCodes();

        IEnumerable<M_GB_COUNTRY> GetShippingCountries();
        IEnumerable<StatesDto> GetStates(int countryId);
        IEnumerable<RegionDto> GetRegions(int stateId);
        IEnumerable<M_GB_SHIPPING> GetShippingCodesBycount(int count);
    }
}
