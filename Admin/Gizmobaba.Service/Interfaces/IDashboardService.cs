﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizmobaba.Utility;

namespace Gizmobaba.Service.Interfaces
{
    public interface IDashboardService
    {
        DashboardDto Get();
        DashboardDto GetGlobalVenderData(string id);

        DashboardDto GetMasterFranchiseDashboard(string id);
    }
}
