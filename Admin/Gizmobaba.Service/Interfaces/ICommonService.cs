﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizmobaba.Service.Interfaces
{
    public interface ICommonService<T>
    {
        IEnumerable<T> List { get; }
        int Add(T entity);
        int Delete(int id);
        T Update(T entity);
        T FindById(int id);
    }
}
