﻿using Gizmobaba.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizmobaba.Service.Interfaces
{
    public interface IDiscountService 
    {
        int AddUsersDiscount(M_GB_USER_DISCOUNT usersDiscount);
        IEnumerable<M_GB_USER_DISCOUNT> GetUsersDiscounts();

        M_GB_USER_DISCOUNT GetDiscountVoucher(string Voucher);

        M_GB_USER_DISCOUNT GetDiscountVoucher(int id);

        int DeleteDiscount(int id);

        IEnumerable<M_GB_USER_DISCOUNT> DiscountVouchersSearch(string title, DateTime? fromDate, DateTime? toDate, string voucher);
    }
}
