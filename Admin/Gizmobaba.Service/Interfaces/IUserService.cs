﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizmobaba.Utility;

namespace Gizmobaba.Service.Interfaces
{
    public interface IUserService
    {
        UserDto UserDetails(string name);

        string AddUser(M_GB_USER user);

        int AddUserAddress(GB_USER_ADDRESS userAddress);
        List<UserDto> GetAllUsers(int id);
        List<UserDto> UserSearch(int key,string serarchKey,DateTime frmDate,DateTime toDate);
        int UpdateUserStatus(int userId,int roleId);
        IEnumerable<UserDto> GetUsersbyCount(int count, int roleID);
        M_GB_USER GetUser(int id);
        IEnumerable<UserDto> GetUsers(DateTime frmDate, DateTime toDate);

        IEnumerable<UserDto> GetUsersSearch(UserDto data);

        M_GB_USER GetUserByEmailID(string email);

       IEnumerable<GB_USER_ADDRESS> GetUserAddress(string userId);

       IEnumerable<Dealer> GetActiveDealers(int status);

       IEnumerable<Dealer> GetSearchDealer(DateTime frmDate, DateTime tDate, string userName, string userId, string pincode);

       IEnumerable<Dealer> GetMasterFranchise(int status);

       IEnumerable<Dealer> GetSearchMasterFranchise(DateTime dateTime1, DateTime dateTime2);

       string AddDealerDetails(M_GB_USERDETAILS userdetails);

       IEnumerable<UserDto> GetGlobalvendor(int status);

       string AddDealerUser(Dealer dealer);

       string UpdateRecharge(GB_DEALER_RECHARGE recharge);

       string AddMasterFranchiseDetails(M_GB_USERDETAILS userdetails);

       IEnumerable<UserDto> GetMasterFranchisebyStateId(int id);
       Dealer GetMasterWallet(int masterWalletId);

       Dealer GethDealer(int id);

       GB_DEALER_RECHARGE GetMasterFranchiseRecharge(int id);


       Dealer GetVender(int id);

       IEnumerable<Dealer> GetrechargeWalletDeatils(int p);


       int CheckEmail(string email);

       int CheckMasterFranchiseByState(int statteId);

       int UpdateUserStatus(int id);

       string UpdateMFRecharge(GB_DEALER_RECHARGE recharge);

       string findUserNamebyId(string userId);

       IEnumerable<Dealer> GetDealersnyMFId(int id);
    }
}
