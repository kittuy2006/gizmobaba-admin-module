﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizmobaba.Utility;

namespace Gizmobaba.Service.Interfaces
{
    public  interface IPermissionsService
    {
        IEnumerable<FeatureTypeDto> GetFeatureTypes();

        int AddRole(M_GB_ROLES role);

        int AddFeatureAccessToRole(M_GB_FEATURE_ACCESS featureAccess);

        IEnumerable<FeatureTypeDto> GetFeatureAccessByRoleId(int roleId);

        M_GB_ROLES GetRoleDetails(int roleId);
        IEnumerable<RolesDto> GetAllRoles();

        int DeleteRole(int id);

        int AddFeatureAccessToUser(GB_USER_FEATURE_ACCESS userData);

        IEnumerable<FeatureTypeDto> GetFeatureAccessByUser(string p);
    }
}
