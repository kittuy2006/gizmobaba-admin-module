﻿using System;
using System.Collections.Generic;
using Gizmobaba.Utility;

namespace Gizmobaba.Service.Interfaces
{
    public interface ICategoryService
    {
        Int64 Create(CategoryDto categoryDto);
        IEnumerable<CategoryDto> GetCategories();
        CategoryDto GetCategory(int categoryId);
        Int64 UpdateCategory(CategoryDto categoryDto);
        int DeleteCategory(int categoryId);
        IEnumerable<SubCategoryDTO> GetSubCategories(int categoryId);
        Int64 AddSubCategory(M_GB_SUB_CATEGORY subCategory);

        IEnumerable<CategoryDto> CategorySearch(string categoty);

        int DeleteSubCategory(int categoryId);

        SubCategoryDTO GetSubCategory(int id);

        int GetProductCountbySubCatId(long p);
    }
}
