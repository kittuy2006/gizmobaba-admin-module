﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizmobaba.Utility;

namespace Gizmobaba.Service.Interfaces
{
    public interface IWalletService
    {
        int AddWallet(M_GB_WALLET wallet);
        WalletDto GetWallet();

        IEnumerable<BonusDto> GetWalletList();
        int CreateWallet(GB_BONUS bonus);

        IEnumerable<BonusDto> GetWalletListByRole(int roleId);

        IEnumerable<M_GB_Payment_GatWay> GetPaymentgatwayList();

        M_GB_Payment_GatWay GetPaymentgatway(int id);
    }
}
