﻿using Gizmobaba.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizmobaba.Service.Interfaces
{
    public interface IPromotionService : ICommonService<GB_FLASH_PROMOTION>
    {
        IEnumerable<GB_FLASH_PROMOTION> GetflashPromotionList();
        int AddMailTemplate(GB_MAIL_TEMPLATES mailTemplate);
        int DeleteMailTemplate(int id);
        GB_MAIL_TEMPLATES Update(GB_MAIL_TEMPLATES mailTemplate);
        GB_MAIL_TEMPLATES GetmailTemplateById(int id);
        IEnumerable<GB_MAIL_TEMPLATES> GetmailTemplateList();


        int UpdateFlashPromotionsStatus(int p);

        int updateMailTemplateStatus(int id);
    }
}
