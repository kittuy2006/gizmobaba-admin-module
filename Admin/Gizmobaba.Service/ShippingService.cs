﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizmobaba.Repository;
using Gizmobaba.Service.Interfaces;
using Gizmobaba.Utility;
using Gizmobaba.Repository.Interfaces;

namespace Gizmobaba.Service
{
    public class ShippingService : IShippingService
    {
        private readonly IShippingRepository _shippingRepository = new ShippingRepository();
        public IEnumerable<M_GB_SHIPPING> List
        {
            get { throw new NotImplementedException(); }
        }

        public int Add(M_GB_SHIPPING shipping)
        {
            return _shippingRepository.Add(shipping);
        }

        public int Delete(int id)
        {
           return _shippingRepository.Delete(id);
        }

        public M_GB_SHIPPING Update(M_GB_SHIPPING shipping)
        {
            throw new NotImplementedException();
        }

        public M_GB_SHIPPING FindById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ShippingMethodDto> GetShippingMethods()
        {
            return _shippingRepository.GetShippingMethods();
        }

        public IEnumerable<M_GB_SHIPPING> GetShippingCodes()
        {
            return _shippingRepository.GetShippingCodes();
        }

        public IEnumerable<M_GB_COUNTRY> GetShippingCountries()
        {
            return _shippingRepository.GetShippingCountries();
        }

        public IEnumerable<StatesDto> GetStates(int countryId)
        {
            return _shippingRepository.GetStates(countryId);
        }

        public IEnumerable<RegionDto> GetRegions(int stateId)
        {
            return _shippingRepository.GetRegions(stateId);
        }


        public IEnumerable<M_GB_SHIPPING> GetShippingCodesBycount(int count)
        {
            return _shippingRepository.GetShippingCodesBycount(count);
        }
    }
}
