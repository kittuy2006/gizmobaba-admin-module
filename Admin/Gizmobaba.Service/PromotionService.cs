﻿using Gizmobaba.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizmobaba.Utility;
using Gizmobaba.Repository.Interfaces;
using Gizmobaba.Repository;

namespace Gizmobaba.Service
{
    public class PromotionService : IPromotionService
    {
        private readonly IPromotionRepository _promotionRepository = new PromotionRepository();
        public IEnumerable<GB_FLASH_PROMOTION> GetflashPromotionList()
        {
            return _promotionRepository.GetflashPromotionList();
        }

        public int AddMailTemplate(GB_MAIL_TEMPLATES mailTemplate)
        {
            return _promotionRepository.AddMailTemplate(mailTemplate);
        }

        public int DeleteMailTemplate(int id)
        {
            return _promotionRepository.DeleteMailTemplate(id);
        }

        public GB_MAIL_TEMPLATES Update(GB_MAIL_TEMPLATES mailTemplate)
        {
            return _promotionRepository.Update(mailTemplate);
        }

        public GB_MAIL_TEMPLATES GetmailTemplateById(int id)
        {
            return _promotionRepository.GetmailTemplateById(id);
        }

        public IEnumerable<GB_MAIL_TEMPLATES> GetmailTemplateList()
        {
            return _promotionRepository.GetmailTemplateList();
        }

        public int Add(GB_FLASH_PROMOTION flashPromotion)
        {
            return _promotionRepository.Add(flashPromotion);
        }

        public int Delete(int id)
        {
            return _promotionRepository.Delete(id);
        }

        public GB_FLASH_PROMOTION Update(GB_FLASH_PROMOTION flashPromotion)
        {
            return _promotionRepository.Update(flashPromotion);
        }

        public GB_FLASH_PROMOTION FindById(int id)
        {
            return _promotionRepository.FindById(id);
        }

        public IEnumerable<GB_FLASH_PROMOTION> List
        {
            get { throw new NotImplementedException(); }
        }


        public int UpdateFlashPromotionsStatus(int id)
        {
            return _promotionRepository.UpdateFlashPromotionsStatus(id);
        }


        public int updateMailTemplateStatus(int id)
        {
            return _promotionRepository.updateMailTemplateStatus(id);
        }
    }
}
