﻿using Gizmobaba.Service.Interfaces;
using System;
using System.Collections.Generic;
using Gizmobaba.Utility;
using Gizmobaba.Repository.Interfaces;
using Gizmobaba.Repository;

namespace Gizmobaba.Service
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository = new CategoryRepository();
        readonly M_GB_CATEGORY _category = new M_GB_CATEGORY();
        public Int64 Create(CategoryDto categoryDto)
        {
                _category.CATEGORY_DESC = categoryDto.CATEGORY_DESC;
            _category.REFERENCE_CODE = categoryDto.REFERENCE_CODE;
            _category.CATEGORY_RANK =  Convert.ToInt32(categoryDto.CATEGORY_RANK);
                _category.STATUS = (int)CustomStatus.CategoryStatus.Active;
            _category.CATEGORY_ID = categoryDto.CATEGORY_ID;
                _category.CREATEDF_ON  = DateTime.Now;
                _category.UPDATED_ON = DateTime.Now;
                 _category.CATEGORY_CONTENT_INFORMATION.Add(categoryDto.CategoryContentInformation);
                _category.CATEGORY_SEO_INFORMATION.Add(categoryDto.CategorySeoInformation);
                return _categoryRepository.Create(_category);
        }

        public IEnumerable<CategoryDto> GetCategories()
        {
            return _categoryRepository.GetCategories();
        }

        public CategoryDto GetCategory(int categoryId)
        {
            if (categoryId <= 0) throw new ArgumentOutOfRangeException("categoryId");
            try
            {
                return _categoryRepository.GetCategory(categoryId);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Int64 UpdateCategory(CategoryDto categoryDto)
        {
            try
            {
                _category.CATEGORY_ID = categoryDto.CATEGORY_ID;
                _category.CATEGORY_DESC = categoryDto.CATEGORY_DESC;
                _category.ICON = _category.ICON;
                _category.STATUS = (int)CustomStatus.ProductStatus.Active;
                _category.UPDATED_ON = DateTime.Now;
                return _categoryRepository.UpdateCategory(_category);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteCategory(int categoryId)
        {
            try
            {
                return _categoryRepository.DeleteCategory(categoryId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IEnumerable<SubCategoryDTO> GetSubCategories(int categoryId)
        {
            return _categoryRepository.GetSubCategories(categoryId);
        }

        public Int64 AddSubCategory(M_GB_SUB_CATEGORY subCategory)
        {
            return _categoryRepository.AddSubCategory(subCategory);
        }

        public IEnumerable<CategoryDto> CategorySearch(string categoty)
        {
            return _categoryRepository.CategorySearch(categoty);
        }

        public int DeleteSubCategory(int subCategoryId)
        {
            return _categoryRepository.DeleteSubCategory(subCategoryId);
        }

        public SubCategoryDTO GetSubCategory(int subCategoryId)
        {
            return _categoryRepository.GetSubCategory(subCategoryId);
        }


        public int GetProductCountbySubCatId(long subCatId)
        {
            return _categoryRepository.GetProductCountbySubCatId(subCatId);
        }
    }
}
