﻿using System;
using System.Collections.Generic;
using Gizmobaba.Repository;
using Gizmobaba.Service.Interfaces;
using Gizmobaba.Utility;
using Gizmobaba.Repository.Interfaces;
using System.Data;
using System.Linq;
namespace Gizmobaba.Service
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository = new OrderRepository();


        public IEnumerable<OrderDto> List
        {
            get { throw new NotImplementedException(); }
        }

        public int Add(OrderDto orderDto)
        {
            return _orderRepository.Add(orderDto);
        }

        public int Delete(int orderId)
        {
            return _orderRepository.Delete(orderId);
        }

        public OrderDto Update(OrderDto gbOrderDto)
        {
            return _orderRepository.Update(gbOrderDto);
        }

        public OrderDto FindById(int orderId)
        {
            return _orderRepository.FindById(orderId);
        }

        public IEnumerable<OrderDetailDto> GetPaymentPendingOrders(int status)
        {
            return _orderRepository.GetPaymentPendingOrders(status);
        }

        public int CancelOrder(int orderId)
        {
            return _orderRepository.CancelOrder(orderId);
        }

        public List<CartDto> GetAbandonedCart()
        {
            IEnumerable<CartDto> getAbandonedCart = _orderRepository.GetAbandonedCart().Where(a => !string.IsNullOrEmpty(a.SESSION_ID) && !string.IsNullOrEmpty(a.USER_ID)).OrderByDescending(a => a.CART_ID).ToList();

            List<CartDto> cart = new List<CartDto>();
            string UserId = "";
            foreach (CartDto data in getAbandonedCart)
            {
                if (data.USER_ID != UserId)
                {
                    UserId = data.USER_ID;
                    cart.Add(data);
                }
            }
            return cart;

        }

        public IEnumerable<CartDto> AbandonedCartSearch(DateTime fromDate, DateTime toDate, string searchKey)
        {
            IEnumerable<CartDto> getAbandonedCart = _orderRepository.AbandonedCartSearch(fromDate, toDate, searchKey);
            List<CartDto> cart = new List<CartDto>();
            string UserId = "";
            foreach (CartDto data in getAbandonedCart)
            {
                if (data.USER_ID != UserId)
                {
                    UserId = data.USER_ID;
                    cart.Add(data);
                }
            }
            return cart;
        }

        public IEnumerable<OrderDetailDto> GetOrdersList(int status)
        {
            return _orderRepository.GetOrdersList(status);
        }

        public IEnumerable<OrderDetailDto> GetOrder(int orderId)
        {
            return _orderRepository.GetOrder(orderId);
        }

        public IEnumerable<OrderDetailDto> OrderSerch(OrderSerch orderDetail)
        {
            return _orderRepository.OrderSerch(orderDetail);

        }

        public OrderDetailDto GetOrderDetails(int orderId)
        {
            return _orderRepository.GetOrderDetails(orderId);
        }

        public int CancelOrder(OrderDto order)
        {
            return _orderRepository.CancelOrder(order);
        }

        public long AddOrder(GB_ORDER order)
        {
            return _orderRepository.AddOrder(order);
        }

        public int AddOrderDetails(GB_ORDER_DETAIL orderDetails)
        {
            return _orderRepository.AddOrderDetails(orderDetails);
        }


        public IEnumerable<OrderDetailDto> GetOrdersList(int orderStatus, int count)
        {
            return _orderRepository.GetOrdersList(orderStatus,count);
        }


        public IEnumerable<OrderDetailDto> GetOrderDetailsByOrderID(int orderId)
        {
            return _orderRepository.GetOrderDetailsByOrderID(orderId);
        }


        public IEnumerable<OrderDetailDto> GetOrderList(int status, int paymentTypeId)
        {
            IEnumerable<OrderDetailDto> orderList = _orderRepository.GetOrderList(status, paymentTypeId);
            IList<OrderDetailDto> newOrderList;
            newOrderList = orderList.Where(a => a.ISMERGEITEM == false && a.MERGEDORDERID == null).ToList();
            if (orderList != null && orderList.Count() != 0)
            {
                foreach (OrderDetailDto data in orderList.Where(a=>a.MERGEDORDERID != null))
                {
                        var marOrder = orderList.FirstOrDefault(a => a.MERGEDORDERID == data.ORDER_ID);
                    if (marOrder != null)
                    {
                        data.ORDER_ID = marOrder.ORDER_ID;
                        data.Qty = marOrder.Qty;
                        data.UNIT_PRICE = marOrder.UNIT_PRICE;
                        newOrderList.Add(data);
                    }
                }
                return newOrderList;
            }
            else
                return orderList;
        }


        public int UpdateOrderStatus(int orderId, int status)
        {
            return _orderRepository.UpdateOrderStatus(orderId,status);
        }


        public int UpdatePickupOrderList(int orderId, int status, string LogisticType, string logisticToken)
        {
            return _orderRepository.UpdatePickupOrderList(orderId, status, LogisticType, logisticToken);
        }


        public string MergeOrder(string[] orderIdList)
        {
            int count = 0;
            long newOrderId = 0;
            string successmessage = "Merging Order(Order has been Merged Successfully!!From( ";
            try
            {
                int userAddressId = 0;
                foreach (string i in orderIdList)
                {
                    successmessage += i;
                    OrderDetailDto order = _orderRepository.GetOrderDetails(Convert.ToInt32(i));
                    if (count == 0)
                    {
                        if (order != null)
                        {
                            GB_ORDER newOrder = new GB_ORDER
                            {
                                CREATED_AT = order.CREATED_AT,
                                UPDATED_AT = order.UPDATED_AT,
                                CUSTOMER_ID = order.CUSTOMER_ID,
                                STATUS = order.STATUS,
                                ADDRESS_ID = order.Address_Id,
                                EMAIL = order.EMAIL,
                                PAYMENT_TYPE = order.PAYMENT_TYPE,
                                ISMERGEITEM = true
                            };
                            newOrderId = _orderRepository.AddOrder(newOrder);
                        }
                    }
                    int Status = _orderRepository.UpdateMergeStatus(Convert.ToInt32(i), newOrderId);
                    count++;
                }
                successmessage += " )";
                return successmessage;
            }
            catch (Exception ex)
            {
                return CustomStatus.ReturnStatus.failure.ToString();
            }
        }

        public int SplitOrder(int orderId,int ordersCount)
        {
            long newOrderId = 0;
            try
            {
                OrderDetailDto order = _orderRepository.GetOrderDetails(Convert.ToInt32(orderId));
                if (order.Qty > 1)
                {
                    IEnumerable<GB_ORDER_DETAIL> getSubOrderList = _orderRepository.GetSubOrderList(orderId);
                    ordersCount = Convert.ToInt32(getSubOrderList.Count()/2);
                    if (order != null)
                    {
                        bool SplitStatus = true;
                        int orderStatus = _orderRepository.UpdateSplitStatus(orderId, SplitStatus);
                        for (int x = 0; x < ordersCount; x++)
                        {
                            GB_ORDER newOrder = new GB_ORDER
                            {
                                CREATED_AT = order.CREATED_AT,
                                UPDATED_AT = order.UPDATED_AT,
                                CUSTOMER_ID = order.CUSTOMER_ID,
                                STATUS = order.STATUS,
                                ADDRESS_ID = order.Address_Id,
                                EMAIL = order.EMAIL,
                                PAYMENT_TYPE = order.PAYMENT_TYPE,
                                IS_SPLIT = false,
                                SPLIT_ORDER_ID = order.ORDER_ID
                            };
                            newOrderId = _orderRepository.AddOrder(newOrder);
                        }
                        int updateOrderCount = 0;
                        if (getSubOrderList.Count() > 1)
                        {
                            int subOrderCount = getSubOrderList.Count();
                            int splitCount = Convert.ToInt32(subOrderCount/2);
                            foreach (GB_ORDER_DETAIL subOrd in getSubOrderList)
                            {
                                if (splitCount == updateOrderCount)
                                {
                                    int status = _orderRepository.updateSubOrder(subOrd.ORDER_DETAIL_ID,
                                        Convert.ToInt32(newOrderId));
                                }
                                else
                                {
                                    int status = _orderRepository.updateSubOrder(subOrd.ORDER_DETAIL_ID, orderId);
                                    updateOrderCount++;
                                }
                            }
                        }
                    }

                    return (int) CustomStatus.ReturnStatus.success;
                }
                return (int) CustomStatus.ReturnStatus.message;
            }
            catch (Exception)
            {
                return (int)CustomStatus.ReturnStatus.failure;
            }
        }


        public IEnumerable<OrderDetailDto> GetAllShippedOrdersList()
        {
            return _orderRepository.GetAllShippedOrdersList();
        }


        public int AddProQty(int orderId, int quantity, int price)
        {
            return _orderRepository.AddProQty(orderId,quantity,price);
        }


        public IEnumerable<GB_ORDER> GetNewOrderList(int status, int paymentTypeId)
        {
            return _orderRepository.GetNewOrderList(status,paymentTypeId);
        }


        public GB_ORDER GetNewOrder(int id)
        {
            return _orderRepository.GetNewOrder(id);
        }


        public IList<GB_ORDER> GetMergedOrders(int id)
        {
            return _orderRepository.GetMergedOrders(id);
        }


        public int SendToBlueDart(int orderId, int status, string LogisticType)
        {

            try
            {
                string response = "";
                OrderDetailDto order = _orderRepository.GetSingleOrderDetails(orderId);
               // string token =  pickUp.AddPickup(order);
                PickupRegistrationDTO pick = new PickupRegistrationDTO();

                pick.AreaCode = "BOM";
                pick.ContactPersonName = order.userAddress.FIRSTNAME;
                pick.CustomerAddress1 = order.userAddress.ADDRESS_1;
                pick.CustomerAddress2 = order.userAddress.ADDRESS_2;
               // pickUp.CustomerAddress3 = order.userAddress.FIRSTNAME;
                pick.CustomerCode = "344400";
                pick.CustomerName = order.userAddress.FIRSTNAME + " " + order.userAddress.LASTNAME; ;
                pick.CustomerPincode = "400030";
                pick.CustomerTelephoneNumber = order.userAddress.mob_no.ToString();
                //req.DoxNDox = ((bool)Docx.IsChecked) ? "1" : "2";
                pick.DoxNDox = "1";
                pick.EmailID = order.EMAIL;
                pick.MobileTelNo = order.userAddress.mob_no.ToString();
                int pcs = 0;
                pcs = Convert.ToInt32(order.Qty);
                //int pcs = 0; int.TryParse(order.Qty ? 0 , out pcs);
                pick.NumberofPieces = pcs;
                pick.OfficeCloseTime = "16:00";
                pick.ProductCode = "A";
                pick.Remarks = "Test";
                pick.RouteCode = "";
                pick.ShipmentPickupDate = DateTime.Now.AddDays(1);

                pick.ShipmentPickupTime = "19:00";
                pick.ReferenceNo = "123456";

                // req.AWBNo = new[] { "59314414493", "59314414482" };
                //req.AWBNo = new[] { (txtpuAwbno.Text) };
                pick.AWBNo = null;
                //req.IsReversePickup = ((bool)rbRVPYes.IsChecked) ? true : false;
                pick.IsReversePickup = false;
               // response = pickUp.AddPickup(pick, response);
            }
            catch
            {

            }
            return 0;
        }


        public OrderDetailDto GetSingleOrderDetails(int orderId)
        {
            return _orderRepository.GetSingleOrderDetails(orderId);
        }


        public int UpdateOrderListToShipping(int orderId, int status, string awb)
        {
            return _orderRepository.UpdateOrderListToShipping(orderId,status,awb);
        }

        public GB_ORDER_DETAIL GetSubOrder(int orderId)
        {
            return _orderRepository.GetSubOrder(orderId);
        }


        public int VoucherApply(long orderId, string voucherCode, decimal descountAmount)
        {
            return _orderRepository.VoucherApply(orderId, voucherCode, descountAmount);
        }

        public GB_ORDER_DETAIL GetSubOrderDetails(int subOrderId)
        {
            return _orderRepository.GetSubOrderDetails(subOrderId);
        }

        public IEnumerable<GB_ORDER_DETAIL> GetSplitOrderDetails(long splitOrderId)
        {
            return _orderRepository.GetSplitOrderDetails(splitOrderId);
        }

        public IEnumerable<GB_ORDER> SearchAuthorizeCodOrder(OrderSerch serch)
        {
            return _orderRepository.SearchAuthorizeCodOrder(serch);
        }


        public IEnumerable<GB_ORDER> SearchOrders(OrderSerch serch)
        {
            return _orderRepository.SearchOrders(serch);
        }


        public IEnumerable<OrderDetailDto> GetPaymentPendingOrdersList()
        {
            return _orderRepository.GetPaymentPendingOrdersList();
        }


        public IEnumerable<GB_ORDER> SearchPaymentPendingOrder(OrderSerch serch)
        {
            return _orderRepository.SearchPaymentPendingOrder(serch);
        }


        public IEnumerable<OrderDetailDto> GetVendorOrderList(string id)
        {
            return _orderRepository.GetVendorOrderList(id);
        }
    }
}
