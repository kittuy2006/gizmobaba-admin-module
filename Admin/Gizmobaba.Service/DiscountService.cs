﻿using Gizmobaba.Repository;
using Gizmobaba.Repository.Interfaces;
using Gizmobaba.Service.Interfaces;
using Gizmobaba.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizmobaba.Service
{
    public class DiscountService : IDiscountService
    {
        private readonly IDiscountRepository _discountRepository = new DiscountRepository();
        public int AddUsersDiscount(M_GB_USER_DISCOUNT usersDiscount)
        {
            return _discountRepository.AddUsersDiscount(usersDiscount);
        }
        public IEnumerable<M_GB_USER_DISCOUNT> GetUsersDiscounts()
        {
            return _discountRepository.GetUsersDiscounts();
        }



        public M_GB_USER_DISCOUNT GetDiscountVoucher(string voucher)
        {
            return _discountRepository.GetDiscountVoucher(voucher);
        }


        public M_GB_USER_DISCOUNT GetDiscountVoucher(int id)
        {
            return _discountRepository.GetDiscountVoucher(id);
        }


        public int DeleteDiscount(int id)
        {
            return _discountRepository.DeleteDiscount(id);
        }


        public IEnumerable<M_GB_USER_DISCOUNT> DiscountVouchersSearch(string title, DateTime? fromDate, DateTime? toDate, string voucher)
        {
            return _discountRepository.DiscountVouchersSearch( title, fromDate, toDate, voucher);
        }
    }
}
