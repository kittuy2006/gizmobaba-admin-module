﻿using Gizmobaba.Repository;
using Gizmobaba.Repository.Interfaces;
using Gizmobaba.Service.Interfaces;
using Gizmobaba.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Gizmobaba.Service
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository = new ProductRepository();
       public Int64 AddProduct(GB_PRODUCT_DETAIL productDetail)
        {
            return _productRepository.AddProduct(productDetail);
        }



       public IEnumerable<GB_PRODUCT_DETAIL> GetAll()
       {
           return _productRepository.GetAll();
       }


       public int Delete(int productId)
       {
           return _productRepository.Delete(productId);
       }

       public int Activate(int productId)
       {
           return _productRepository.ActivateProduct(productId);
       }

       public int DeActivate(int productId)
       {
           return _productRepository.DeactivateProduct(productId);
       }

        public IEnumerable<ProductImagesDto> GetProductImages(int productId)
        {
            return _productRepository.GetProductImages(productId);
        }

        public int DeleteImage(int imageId)
        {
            return _productRepository.DeleteImage(imageId);
        }

        public int EditImage(GB_PRODUCT_IMAGES img)
        {
            return _productRepository.EditImage(img);
        }

        public int CheckSku(string sku)
        {
            return _productRepository.CheckSku(sku);
        }

        public int AddProductImage(GB_PRODUCT_IMAGES productimages)
        {
            return _productRepository.AddProductImage(productimages);
        }


        public IEnumerable<M_GB_CATEGORY> GetCategories()
       {
           return _productRepository.GetCategories();
       }


       public IEnumerable<M_GB_SHIPPING> GetAvailableShippingCodes()
       {
           return _productRepository.GetAvailableShippingCodes();
       }


       public GB_PRODUCT_DETAIL GetProduct(int productId)
       {
           return _productRepository.GetProduct(productId);
       }


       public IEnumerable<ProductDTO> GetProductSearch(string product)
       {
           return _productRepository.GetProductSearch(product);
       }


       public int DeactivateProduct(int productId)
       {
           return _productRepository.DeactivateProduct(productId);
       }


       public int AddProductList(IList<ProductDTO> products)
       {
           foreach(ProductDTO p in products)
           {
               GB_PRODUCT_DETAIL pro = new GB_PRODUCT_DETAIL
               {
                   PRODUCT_NAME = p.PRODUCT_NAME,
                   PRODUCT_CAPTION = p.PRODUCT_CAPTION,
                   SUB_CAT_ID = p.SUB_CAT_ID,
                   PRODUCT_DESC = p.PRODUCT_DESC,
                   PRODUCT_SKU = p.PRODUCT_SKU,
                   CATEGORY_ID = p.CATEGORY_ID,
                   QUANTITYS = p.QUANTITYS,
                   BRAND = p.BRAND,
                   MRP = p.MRP,
                   NET_PRICE = p.NET_PRICE,
                   GROSS_PRICE = p.GROSS_PRICE,
                   GIZMOBABA_PRICE = p.GIZMOBABA_PRICE,
                   PHOTO_URL = p.PHOTO_URL,
                   VIDEO_URL = p.VIDEO_URL,
                   GB_PRODUCT_IMAGES = p.productImages.ToList(),
                   ACTIVATION_DATE = p.ACTIVATION_DATE,
                   DEACTIVATION_DATE = p.DEACTIVATION_DATE,
                   DELIVERY_TIME = p.DELIVERY_TIME,
                   INVENTORY = p.INVENTORY,
                   RESERVE_QUANTITY = p.RESERVE_QUANTITY,
                   PKG_QUANTITY = p.PKG_QUANTITY,
                   CREATED_BY = p.CREATED_BY,
                   UPDATED_BY = "Admin",
                   STATUS = p.STATUS,
                   ISAVAILABLE = (int)CustomStatus.ProductStatus.ISAVAILABLE,
                   CREATED_ON = DateTime.Now,
                   UPDATED_ON = DateTime.Now
               };
               _productRepository.AddProduct(pro);
           }
           return (int)CustomStatus.ReturnStatus.success;
       }


       public int AddBanner(M_GB_BANNERS banner)
       {
           return _productRepository.AddBanner(banner);
       }


       public IEnumerable<Banner> GetBanners()
       {
           return _productRepository.GetBanners();
       }


       public int DeleteBanner(int bannerId)
       {
           return _productRepository.DeleteBanner(bannerId);
       }


       public int SetBanner(int bannerId)
       {
           return _productRepository.SetBanner(bannerId);
       }

        public IEnumerable<VendorDto> GetVendors()
        {
            return _productRepository.GetVendors();
        }


        public IEnumerable<WarrantyDto> GetWarranty()
        {
            return _productRepository.GetWarranty();
        }

        public IEnumerable<CustomFieldsDto> GetCustomFields()
        {
            return _productRepository.GetCustomFields();
        }

        public IEnumerable<SectionDto> GetSection()
        {
            return _productRepository.GetSection();
        }

        public IEnumerable<ConditiongDto> GetCondition()
        {
            return _productRepository.GetCondition();
        }

        public int AddProductAdditionalinfo(GB_PRODUCT_INFORMATION productInformation)
        {
            productInformation.STATUS = 1;
            productInformation.CREATED_ON = DateTime.Now;
            return _productRepository.AddProductAdditionalinfo(productInformation);
        }

        public ProductDTO GetProductInfomation(int productId)
        {
            return _productRepository.GetProductInfomation(productId);
        }

        public IEnumerable<TagsDto> GetProductTags()
        {
            return _productRepository.GetProductTags();
        }

        public IEnumerable<TagsDto> GetAllProductTags()
        {
            return _productRepository.GetAllProductTags();
        }

        public TagsDto GetProductTags(int productId)
        {
            return _productRepository.GetProductTags(productId);
        }

        public int AddProductTag(M_GB_TAGS tagsDto)
        {
            return _productRepository.AddProductTag(tagsDto);
        }

        public int DeleteProductTag(int tagId)
        {
            return _productRepository.DeleteProductTag(tagId);
        }

        public int AddTagToProduct(GB_PRODUCT_TAG tags)
        {
            return _productRepository.AddTagToProduct(tags);
        }

        public IEnumerable<Banner> GetPredefinedBanners()
        {
            return _productRepository.GetPredefinedBanners();
        }

        public Banner GetBanner(int bannerId)
        {
            return _productRepository.GetBanner(bannerId);
        }

        public IEnumerable<Banner> GetCategoryBanners()
        {
            return _productRepository.GetCategoryBanners();
        }


        public IEnumerable<Banner> GetAddsBanners()
        {
            return _productRepository.GetAddsBanners();
        }


        public IEnumerable<M_GB_SUB_CATEGORY> GetAllSubCat()
        {
            return _productRepository.GetAllSubCat();
        }


        public IEnumerable<GB_BULK_UPLOAD> GetBulkuploadList()
        {
            return _productRepository.GetBulkuploadList();
        }


        public int AddBulkProUploadList(GB_BULK_UPLOAD bulkUpload)
        {
            return _productRepository.AddBulkProUploadList(bulkUpload);
        }


        public IEnumerable<PRODUCTREVIEWSDTO> GetReviews()
        {
            return _productRepository.GetReviews();
        }


        public int UpdateReviewStatus(int id)
        {
            return _productRepository.UpdateReviewStatus(id);
        }

        public int DeleteReview(int id)
        {
            return _productRepository.DeleteReview(id);
        }



        public IEnumerable<M_GB_TAX> GetTaxes()
        {
            return _productRepository.GetTaxes();
        }


        public int AddTax(M_GB_TAX tax)
        {
            return _productRepository.AddTax(tax);
        }


        public M_GB_TAX GetProductTax(int id)
        {
            return _productRepository.GetProductTax(id);
        }


        public string DeleteProductTax(int id)
        {
            return _productRepository.DeleteProductTax(id);
        }


        public string GetProSku()
        {
            return _productRepository.GetProSku();
        }


        public IEnumerable<ProductDTO> GetProductInvAdvSearch(DateTime fromDate, DateTime toDate)
        {
            return _productRepository.GetProductInvAdvSearch(fromDate,toDate);
        }


        public ProductDTO GetProductbySku(string sku)
        {
            return _productRepository.GetProductbySku(sku);
        }
    }
}
