﻿using Gizmobaba.Repository;
using Gizmobaba.Repository.Interfaces;
using Gizmobaba.Service.Interfaces;
using Gizmobaba.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizmobaba.Service
{
    public class PermissionsService : IPermissionsService
    {
        private readonly IPermissionsRepository _permissionsRepository = new PermissionsRepository();

        public IEnumerable<FeatureTypeDto> GetFeatureTypes()
        {
            return _permissionsRepository.GetFeatureTypes();
        }

        public int AddRole(M_GB_ROLES role)
        {
            return _permissionsRepository.AddRole(role);
        }

        public int AddFeatureAccessToRole(M_GB_FEATURE_ACCESS featureAccess)
        {
            return _permissionsRepository.AddFeatureAccessToRole(featureAccess);
        }

        public IEnumerable<FeatureTypeDto> GetFeatureAccessByRoleId(int roleId)
        {
            return _permissionsRepository.GetFeatureAccessByRoleId(roleId);
        }

        public M_GB_ROLES GetRoleDetails(int roleId)
        {
            return _permissionsRepository.GetRoleDetails(roleId);
        }

        public IEnumerable<RolesDto> GetAllRoles()
        {
            return _permissionsRepository.GetAllRoles();
        }

        public int DeleteRole(int roleId)
        {
            return _permissionsRepository.DeleteRole(roleId);
        }


        public int AddFeatureAccessToUser(GB_USER_FEATURE_ACCESS userData)
        {
            return _permissionsRepository.AddFeatureAccessToUser(userData);
        }


        public IEnumerable<FeatureTypeDto> GetFeatureAccessByUser(string userId)
        {
            return _permissionsRepository.GetFeatureAccessByUser(userId);
        }
    }
}
