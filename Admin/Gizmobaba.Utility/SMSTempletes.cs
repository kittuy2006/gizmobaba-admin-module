﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizmobaba.Utility
{
    public static class SMSTempletes
    {
        public static string GetSMSBody(SMSTempleteTypes smsType)
        {
            string sms = "";
            switch (smsType)
            {
                case SMSTempleteTypes.OTP:
                    sms = "Dear {0} Your OTP is {1}";
                    break;
                case SMSTempleteTypes.Dealer:
                    sms = "Dear {0}, Your successfully registered with Gizmobaba, Your login credentials are user name {1} password {2}";
                    break;
                case SMSTempleteTypes.Orders:
                    break;
                case SMSTempleteTypes.Payment:
                    break;
                default:
                    break;
            }
            return sms;
        }
    }

    public enum SMSTempleteTypes
    {
        OTP,
        Orders,
        Payment,
        Dealer,
    }
}
