//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Gizmobaba.Utility
{
    using System;
    using System.Collections.Generic;
    
    public partial class GB_ORDER_DETAIL
    {
        public long ORDER_DETAIL_ID { get; set; }
        public Nullable<long> PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public Nullable<int> NUM_OF_PRO { get; set; }
        public Nullable<decimal> UNIT_PRICE { get; set; }
        public string NOTE { get; set; }
        public Nullable<long> VENDOR_ID { get; set; }
        public Nullable<int> TAX_RATE_ID { get; set; }
        public Nullable<System.DateTime> CREATED_AT { get; set; }
        public Nullable<System.DateTime> UPDATED_AT { get; set; }
        public Nullable<int> STATUS { get; set; }
        public long ORDER_ID { get; set; }
        public Nullable<int> QUANTITY { get; set; }
        public string CHECKOUTTYPE { get; set; }
        public Nullable<long> SPLIT_ORDER_ID { get; set; }
        public string DISCOUNT_VOUCHER { get; set; }
        public Nullable<decimal> DISCOUNT_AMOUNT { get; set; }
    
        public virtual GB_ORDER GB_ORDER { get; set; }
        public virtual GB_PRODUCT_DETAIL GB_PRODUCT_DETAIL { get; set; }
    }
}
