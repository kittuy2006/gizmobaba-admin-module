//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Gizmobaba.Utility
{
    using System;
    using System.Collections.Generic;
    
    public partial class GB_PRODUCT_INFORMATION
    {
        public long PRODUCT_INFO_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public string PRO_OFFER_DESC { get; set; }
        public string PRO_OFFER_IMAGE { get; set; }
        public string BARCODE { get; set; }
        public string MODELNUMBER { get; set; }
        public string CATALOG_CODE { get; set; }
        public string STANDARD_PRODUCT_TYPE { get; set; }
        public string MPN { get; set; }
        public string STANDARD_PRODUCT_CODE { get; set; }
        public string PAGE_TITLE { get; set; }
        public string KEYWORD { get; set; }
        public string SEO_DESC { get; set; }
        public string URL_KEY_REWRITER { get; set; }
        public string SMALL_IMAGEALT { get; set; }
        public string LARGE_IMAGEALT { get; set; }
        public Nullable<int> CONDITION_ID { get; set; }
        public Nullable<System.DateTime> CONDITION_PURCHASE_DATE { get; set; }
        public Nullable<int> WARRANTY_ID { get; set; }
        public string WARRANTY_DESC { get; set; }
        public Nullable<int> CUSTOM_FIELDS_ID { get; set; }
        public Nullable<int> SECTION_ID { get; set; }
        public string PRODUCT_NOTE { get; set; }
        public System.DateTime CREATED_ON { get; set; }
        public Nullable<int> STATUS { get; set; }
    
        public virtual GB_PRODUCT_DETAIL GB_PRODUCT_DETAIL { get; set; }
        public virtual M_GB_CONDITION M_GB_CONDITION { get; set; }
        public virtual M_GB_CUSTOM_FIELDS M_GB_CUSTOM_FIELDS { get; set; }
        public virtual M_GB_SECTION M_GB_SECTION { get; set; }
        public virtual M_GB_WARRANTY M_GB_WARRANTY { get; set; }
    }
}
