﻿using System;
using System.Collections.Generic;

namespace Gizmobaba.Utility
{
    public class OrderDetailDto
    {
        //public int _productId;
        //public string _productName;
        //public int _numOfPro;
        //public decimal _unitPrice;
        //public string _note;
        //public int _vendorId;
        //public int _taxRateId;
        //public DateTime _createdAt;
        //public DateTime _updatedAt;
        //public int _status;
        //public int _orderId;
        //public int _qty;
        //public string userName;
        public long? PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public int? NUM_OF_PRO { get; set; }
        public decimal? UNIT_PRICE { get; set; }
        public string NOTE { get; set; }
        public long? VENDOR_ID { get; set; }
        public int? TAX_RATE_ID { get; set; }
        public DateTime? CREATED_AT { get; set; }
        public DateTime? UPDATED_AT { get; set; }
        public int? STATUS { get; set; }
        public long ORDER_ID { get; set; }
        public long ORDER_DETAIL_ID { get; set; }
        public int? Qty { get; set; }
        public string UserName { get; set; }
        public string CHECKOUTTYPE { get; set; }
        public string SourceImage { get; set; }
        public string ProSku { get; set; }
        public string VideoURL { get; set; }
        public string DISCOUNT_VOUCHER { get; set; }
        public decimal? DISCOUNT_AMOUNT { get; set; }

        public string Image
        {
            get
            {
                string imageurl = "";
                if (SourceImage != null)
                    imageurl = "http://www.pakkadial.com/Images/Product/" + SourceImage;
                return imageurl;

            }
            set { }
        }
        public string Age
        {
            get
            {
                DateTime date = CREATED_AT ?? DateTime.Now;
                double duedaysin = (DateTime.Now - date).TotalDays;
                return  string.Format(" {0}days", Convert.ToInt32(duedaysin));
            }
            set  {}
        }
        public string Date
        {
            get
            {
                string date = CREATED_AT.ToString();
                return date;
            }
            set { }
        }
        public GB_ORDER_DETAIL OdDetail { get; set; }
        public M_GB_USER User { get; set; }
        public GB_USER_ADDRESS userAddress { get; set; }
        public GB_PRODUCT_DETAIL Product { get; set; }
        public IEnumerable<GB_ORDER_DETAIL> subOrders { get; set; }

        public IEnumerable<ProductDTO> ProductDetails { get; set; }
        public int? PAYMENT_TYPE { get; set; }
        public int Address_Id { get; set; }
        public Nullable<long> CUSTOMER_ID { get; set; }
        public string EMAIL { get; set; }
        public long? CATEGORY_ID { get; set; }
        public Nullable<bool> ISMERGEITEM { get; set; }
        public Nullable<long> MERGEDORDERID { get; set; }
        public Nullable<bool> IS_SPLIT { get; set; }
        public Nullable<long> SPLIT_ORDER_ID { get; set; }
        public string CREATED_BY { get; set; }
        public string LOGISTIC_TOKEN { get; set; }
        public string LOGISTIC_AWB { get; set; }
        //public OrderDetailDto(int productId, string PRODUCT_NAME, int NUM_OF_PRO, decimal UNIT_PRICE, string NOTE,
        //    int VENDOR_ID, int TAX_RATE_ID, DateTime CREATED_AT, DateTime UPDATED_AT, int STATUS, int ORDER_ID, int Qty, string UserName)
        //{
        //    _productId = productId;
        //    _productName = PRODUCT_NAME;
        //    _numOfPro = NUM_OF_PRO;
        //    _unitPrice = UNIT_PRICE;
        //    _note = NOTE;
        //    _vendorId = VENDOR_ID;
        //    _taxRateId = TAX_RATE_ID;
        //    _createdAt = CREATED_AT;
        //    _updatedAt = UPDATED_AT;
        //    _status = STATUS;
        //    _orderId = ORDER_ID;
        //    _qty = Qty;
        //    userName = UserName;
        //}

    }

    public class OrderSerch
    {
        public string Period { get; set; }
        public DateTime? From_Date { get; set; }
        public DateTime? To_Date { get; set; }
        public int CATEGORY_ID { get; set; }
        public int SUB_CAT_ID { get; set; }
        public int Location { get; set; }
        public string Order_No { get; set; }
        public string SKU { get; set; }
        public string Awb_No { get; set; }
        public string Email_Id { get; set; }
        public string Phone_No { get; set; }
        public string Coupon_Code { get; set; }
        public string Checkout_Type { get; set; }
        public string Shipment_ID { get; set; }
        public string Issue_Type { get; set; }
        public string Order_Source { get; set; }
        public int OrderType { get; set; }
        public int STATUS { get; set; }
        public int ORDER_ID { get; set; }
        public int PAYMENT_TYPE { get; set; }
        public string Created_By { get; set; }
        
    }
}