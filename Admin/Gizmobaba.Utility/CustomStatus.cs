﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizmobaba.Utility
{
    public static class CustomStatus
    {
        public enum ReturnStatus
        {
            success = 1,
            failure = 2,
            exception = 3,
            message = 4
        }
        public enum DefaultStatus
        {
            Enable = 1,
            Disable = 2,
            Active = 1,
            Delete = 3,
            DeActive = 0
        }
        public enum ProductStatus
        {
            Enable = 1,
            Disable = 2,
            Active = 1,
            ISAVAILABLE = 0,
            Delete = 3,
            DeActive = 0
        }
        public enum PaymentTypes
        {
            CreditCard = 1,
            Online = 2,
            Cheque = 3,
            COD = 4
        }
        public enum DeliveryTypes
        {
            Ship = 1,
            Offline = 2,
            Deli_Online = 3,
            Instore_Pickup = 4
        }
        public enum ImageTypes
        {
            Source = 1,
            Gif = 2,
            Gallery = 0
        }
        public enum ProductMessageStatus
        {
            Empty = 0,
            Success = 1,
            Failed = 2,
            Filetype =3,
            Excelfile = 4,
            Uploaded = 5,
            FaileAgian = 6
        }
        public enum BannerStatus
        {
            DeActive = 0,
            Active = 1,
            Delete = 3,
            New = 4,
            Category = 5,
        }
        public enum BannerTypes
        {
            Home = 1,
            Predefined = 2,
            Category = 3,
            Adds = 4,
        }
        public enum CategoryStatus
        {
            Active = 0,
            DeActive = 1,
            Delete = 3
        }
        public enum CategoryMessage
        {
            Updated = 1,
            Failed = 2,
            Deleted = 3
        }
        public enum OrderStatus
        {
            New = 1,
            Authorize = 1,
            Pending = 2,
            Cancel = 3,
            Dispatched = 4,
            Delivered = 5,
            Pickup = 6,
            ReadyToPickUp = 7,
            InTransit = 8,
            RtoReturns = 9,
            All = 10
        }
        public enum UserStatus
        {
            Admin = 1,
            User = 2,
            Dealer = 3,
            MasterFranchise = 4
        }
        public enum Status
        {
            Active = 1,
            DeActive = 0,
            Deleted = 3
        }
        public enum UserTypes
        {
            Admin = 1,
            Employee = 2,
            Master_Franchise = 3,
            Dealer = 4,
            Global_Vender = 5,
            Customer = 6,
            Gold = 18,
            Silver = 19,
            Platinum = 20
        }

        public static object ErrorCodeToString(ProductMessageStatus productMessageStatus)
        {
            switch (productMessageStatus)
            {
                case ProductMessageStatus.Empty:
                    return "Plese select check boxs";
                case ProductMessageStatus.Success:
                    return "Successfully completed";
                case ProductMessageStatus.Failed:
                    return "Failed";
                case ProductMessageStatus.Filetype:
                    return "File type is incorrect";
                case ProductMessageStatus.Excelfile:
                    return "Please select a excel file";
                case ProductMessageStatus.Uploaded:
                    return "Successfully uploaded";
                case ProductMessageStatus.FaileAgian:
                    return "Upload failed. Please try agian";
                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }

        public static object CategoryMessages(CategoryMessage categoryMessage)
        {
            switch (categoryMessage)
            {
                case CategoryMessage.Deleted:
                    return "Category successfully Deleted";
                case CategoryMessage.Failed:
                    return "Failed";
                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        public static object UserStatusType(int? statusId)
        {
            switch (statusId)
            {
                case (int)UserTypes.Admin:
                    return "Admin";
                case (int)UserTypes.Global_Vender:
                    return "Global Vender";
                case (int)UserTypes.Employee:
                    return "Employee";
                case (int)UserTypes.Silver:
                    return "Silver";
                case (int)UserTypes.Gold:
                    return "Gold";
                case (int)UserTypes.Platinum:
                    return "Platinum";
               
                default:
                    return "No User Status";
            }
        }
      
    }
}
