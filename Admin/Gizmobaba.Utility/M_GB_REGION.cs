//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Gizmobaba.Utility
{
    using System;
    using System.Collections.Generic;
    
    public partial class M_GB_REGION
    {
        public int REGION_ID { get; set; }
        public int STATE_ID { get; set; }
        public string REGION { get; set; }
        public string REGION_DESC { get; set; }
        public System.DateTime CRETAED_ON { get; set; }
        public Nullable<int> STATUS { get; set; }
    
        public virtual M_GB_SATES M_GB_SATES { get; set; }
    }
}
