﻿
using System;
using System.Collections.Generic;

namespace Gizmobaba.Utility
{
    public class DTO
    {
    }
    public class CategoryDto
    {
        public int? SEQ_NO { get; set; }
        public long CATEGORY_ID { get; set; }
        public string CATEGORY_DESC { get; set; }
        public int? STATUS { get; set; }
        public DateTime? CREATEDF_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public string ICON { get; set; }
        public int ProductsCount { get; set; }
        public string CATEGORY_RANK { get; set; }
        public string REFERENCE_CODE { get; set; }

        public IEnumerable<M_GB_SUB_CATEGORY> SubCategory { get; set; }
        public CATEGORY_CONTENT_INFORMATION CategoryContentInformation { get; set; }
        public CATEGORY_SEO_INFORMATION CategorySeoInformation { get; set; }

    }
    public class SubCategoryDTO
    {
        public int SEQ_NO { get; set; }
        public long SUB_CAT_ID { get; set; }
        public string SUB_CAT_DESC { get; set; }
        public long? CATEGORY_ID { get; set; }
        public int? STATUS { get; set; }
        public DateTime? CREATED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public string REFERENCE_CODE { get; set; }
        public string DESCRIPTION { get; set; }
        public string NUMBER_OF_COLUMNS { get; set; }
        public string SUB_CAT_GROUP { get; set; }
        public string RANK { get; set; }
    }
    public class Banner
    {
        public int BANNER_ID { get; set; }
        public string BANNER_NAME { get; set; }
        public string BANNER_DEC { get; set; }
        public string BANNER_URL { get; set; }
        public DateTime CAREATED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public int STATUS { get; set; }
        public DateTime? BANNER_EXPIRY_DATE { get; set; }
        public string CreatedDate
        {
            get
            {
                string createdDate = "";
                if (CAREATED_ON != null)
                    createdDate = CAREATED_ON.ToShortDateString();
                return createdDate;
            }
            set { }
        }
        public string ExpiryDate
        {
            get
            {
                string createdDate = "";
                if (BANNER_EXPIRY_DATE != null)
                    createdDate = BANNER_EXPIRY_DATE.ToString();
                return createdDate;
            }
            set { }
        }

        public int NumOfDays
        {
            get
            {
                double days = 0;
                if (CAREATED_ON != null && BANNER_EXPIRY_DATE != null)
                {
                    DateTime d1 = CAREATED_ON;
                    DateTime d2 = BANNER_EXPIRY_DATE ?? DateTime.Now;
                    days = (d2 - d1).TotalDays;
                }
                return Convert.ToInt32(days);
            }
            set { }
        }
        public long? CATEGORY_ID { get; set; }
        public long? SUB_CAT_ID { get; set; }
        public string Sub_Cat_Name { get; set; }
    }
    public class ProductImagesDto
    {
        public long PRODUCT_ID { get; set; }
        public string ALTERNATE_NAME { get; set; }
        public int? DISPLAY_IMAGE { get; set; }
        public int PRODUCT_IMAGE_ID { get; set; }
        public string IMAGE_URL { get; set; }
    }

    public class OrderDto
    {
        public long ORDER_ID { get; set; }
        public DateTime? CREATED_AT { get; set; }
        public DateTime? UPDATED_AT { get; set; }
        public long? CUSTOMER_ID { get; set; }
        public int? STATUS { get; set; }
        public string EMAIL { get; set; }
        public int? ADDRESS_ID { get; set; }
        public string COMMENTS { get; set; }
        public string RESON { get; set; }
        public bool USERSHOWTHECOMMENTS { get; set; }
    }

    public class Shippingdto
    {
        public int SHIPPING_ID { get; set; }
        public string SHIPPING_CODE { get; set; }
        public string SHIPPING_DESC { get; set; }
        public DateTime CREATED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public int? STATUS { get; set; }
        public string ICON { get; set; }
        public string LOGISTICS { get; set; }
        public string MARKETPLACE { get; set; }
        public int? REGION_ID { get; set; }

        public virtual ICollection<GB_PRODUCT_SHIPPING> GB_PRODUCT_SHIPPING { get; set; }
    }

    public class VendorDto
    {
        public string NAME { get; set; }
        public string VEN_DESCRIPTION { get; set; }
        public string VEN_ID { get; set; }
        public string EMAIL { get; set; }
        public decimal? MOB_NO { get; set; }
        public decimal? ALT_MOB_NO { get; set; }
        public decimal? PHONE_NO { get; set; }
        public string URL { get; set; }
    }
    public class ConditiongDto
    {
        public int CONDITION_ID { get; set; }
        public string CONDITION_DESC { get; set; }
        public int? STATAUS { get; set; }
    }
    public class SectionDto
    {
        public int SECTION_ID { get; set; }
        public string SECTION_DESC { get; set; }
        public int? STATUS { get; set; }
    }
    public class WarrantyDto
    {
        public int WARRANTY_ID { get; set; }
        public string WARRANTY_DESC { get; set; }
        public int? STATUS { get; set; }
    }
    public class CustomFieldsDto
    {
        public int CUSTOM_FIELDS_ID { get; set; }
        public string CUSTOM_FIELDS_DESC { get; set; }
        public int? STATUS { get; set; }
    }

    public class ProductInformationDto
    {
        public long PRODUCT_INFO_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public string PRO_OFFER_DESC { get; set; }
        public string PRO_OFFER_IMAGE { get; set; }
        public string BARCODE { get; set; }
        public string MODELNUMBER { get; set; }
        public string CATALOG_CODE { get; set; }
        public string STANDARD_PRODUCT_TYPE { get; set; }
        public string MPN { get; set; }
        public string STANDARD_PRODUCT_CODE { get; set; }
        public string PAGE_TITLE { get; set; }
        public string KEYWORD { get; set; }
        public string SEO_DESC { get; set; }
        public string URL_KEY_REWRITER { get; set; }
        public string SMALL_IMAGEALT { get; set; }
        public string LARGE_IMAGEALT { get; set; }
        public int? CONDITION_ID { get; set; }
        public DateTime? CONDITION_PURCHASE_DATE { get; set; }
        public int? WARRANTY_ID { get; set; }
        public string WARRANTY_DESC { get; set; }
        public int? CUSTOM_FIELDS_ID { get; set; }
        public int? SECTION_ID { get; set; }
        public string PRODUCT_NOTE { get; set; }
        public DateTime CREATED_ON { get; set; }
        public int? STATUS { get; set; }
    }

    public class TagsDto
    {
        public int PRODUCT_TAG_ID { get; set; }
        public int? TAG_ID { get; set; }
        public string TAG_DESC { get; set; }
        public long? PRODUCT_ID { get; set; }
        public int? STATUS { get; set; }
        public int ProductCount { get; set; }
    }

    public class WalletDto
    {
        public int WALLET_ID { get; set; }
        public int ROLE_ID { get; set; }
        public int? SIGNUP { get; set; }
        public int? LINKINGPOSTS { get; set; }
        public int? SHARINGPOSTS { get; set; }
        public int? REFERRING_FRIENDS { get; set; }
        public int? REFFERED_FRIENDS_PURCHASES { get; set; }
        public int? OWN_PURCHASE { get; set; }
        public int? PURCHASE_CERTAIN_TIME { get; set; }
        public int? MAX_USER_PURCHASE { get; set; }
        public DateTime CREATEDON { get; set; }
        public DateTime? UPDATEON { get; set; }
        public int? STATUS { get; set; }
    }
    public class ShippingMethodDto
    {
        public int SHIPPINGMETHOD_ID { get; set; }
        public string METHOD_NAME { get; set; }
        public DateTime? CREATED_ON { get; set; }
        public int? STATUS { get; set; }
    }

    public class DashboardDto
    {
        public int TotalUsers { get; set; }
        public int TotalOrders { get; set; }
        public int TotalSales { get; set; }
        public int TotalDealer { get; set; }
        public int TotalMasterFranchise { get; set; }
        public long TotalInventory { get; set; }
        public long TotalProductCount { get; set; }
        public long TotalOrdersCount { get; set; }
        public long NewOrdersCount { get; set; }
        public long ApprovedOrdersCount { get; set; }
        public int TotalVendor { get; set; }

 
    }

    public class StatesDto
    {
        public int STATE_ID { get; set; }
        public int COUNTRY_ID { get; set; }
        public string STATE { get; set; }
        public string STATE_DESC { get; set; }
        public DateTime CREATED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public int? STATUS { get; set; }

        public virtual M_GB_COUNTRY M_GB_COUNTRY { get; set; }
        public virtual ICollection<M_GB_REGION> M_GB_REGION { get; set; }
    }
    public class RegionDto
    {
        public int REGION_ID { get; set; }
        public int STATE_ID { get; set; }
        public string REGION { get; set; }
        public string REGION_DESC { get; set; }
        public DateTime CRETAED_ON { get; set; }
        public int? STATUS { get; set; }

        public virtual M_GB_SATES M_GB_SATES { get; set; }
        public virtual ICollection<M_GB_SHIPPING> M_GB_SHIPPING { get; set; }
    }
    public class FeatureTypeDto
    {
        public int FEATURE_TYPE_ID { get; set; }
        public int? ROLE_ID { get; set; }
        public string FEATURE_TYPE { get; set; }
        public int? ID { get; set; }
        public DateTime? CREATED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public int STATUS { get; set; }
    }

    public class RolesDto
    {
        public int SEQ_NO { get; set; }
        public int ROLE_ID { get; set; }
        public string ROLE_DESC { get; set; }
        public int? STATUS { get; set; }
        public DateTime? CREATED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public string DESCRIPTION { get; set; }
        public int PERMISSIONSCOUNT { get ; set ; }
        public bool UserAllow { get; set; }
        public long? Value { get; set; }
        public int UserCount { get; set; }

    }

    public class UserDto
    {
        public long SEQ_NO { get; set; }
        public long ID { get; set; }
        public string USER_NAME { get; set; }
        public string USER_ID { get; set; }
        public string PASSWORD { get; set; }
        public string CITY { get; set; }
        public string MOB_NO { get; set; }
        public string EMAIL { get; set; }
        public int? STATUS { get; set; }
        public long? ROLE { get; set; }
        public DateTime? CREATED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public int? USER_TYPE { get; set; }
        public string ADD1 { get; set; }
        public string ADD2 { get; set; }
        public int? PIN { get; set; }
        public string STATE { get; set; }
        public string COUNTRY { get; set; }
        public string ALT_EMAIL { get; set; }
        public string ALT_MOB { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string PICTURE { get; set; }
        public string F_ID { get; set; }
        public DateTime? DOB { get; set; }
        public string GENDER { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public IEnumerable<UserAddressDto> UserAddress { get; set; }
        public IEnumerable<GB_USER_ADDRESS> UserAddresss { get; set; }
        public IEnumerable<GB_ORDER> OrderList { get; set; }

    }

    public class UserAddressDto
    {
        public int ADDRESS_ID { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string COMPANY { get; set; }
        public string ADDRESS_1 { get; set; }
        public string ADDRESS_2 { get; set; }
        public string CITY { get; set; }
        public string POSTCODE { get; set; }
        public int? COUNTRY_ID { get; set; }
        public int? ZONE_ID { get; set; }
        public string CUSTOM_FIELD { get; set; }
        public string landmark { get; set; }
        public long? mob_no { get; set; }
    }
    public class BonusTypesDto
    {
        public int BONUS_TYPE_ID { get; set; }
        public string BONUS_TYPE { get; set; }
        public int STATUS { get; set; }
        public DateTime? CRETAED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public string CREATED_BY { get; set; }
        public string UPDATED_BY { get; set; }

        public IEnumerable<BonusDto> BonusDto { get; set; }
    }
    public class BonusDto
    {
        public int ID { get; set; }
        public int BONUS_TYPE_ID { get; set; }
        public int ROLE_ID { get; set; }
        public double? VALUE { get; set; }
        public int? STATUS { get; set; }
        public DateTime CREATED_ON { get; set; }
        public DateTime? ACTIVE_DATE { get; set; }
        public DateTime? EXPIRE_DATET { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public string BONUS_TYPE { get; set; }



        public BonusTypesDto BonusTypesDto { get; set; }
    }
    public class GlobalvendorDTO
    {
        public int TotalProductCount { get; set; }
        public int TotalOrdersCount { get; set; }
        public int NewOrdersCount { get; set; }
        public int ApprovedOrdersCount { get; set; }
    }
    public class PRODUCTREVIEWSDTO
    {
        public int ID { get; set; }
        public int? USER_ID { get; set; }
        public string UserName { get; set; }
        public long? PRODUCT_ID { get; set; }
        public string REVIEW { get; set; }
        public string REVIEW_DESC { get; set; }
        public DateTime CREATED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public int STATUS { get; set; }
    }
    public class Dealer
    {
        public long ID { get; set; }
        public string USER_NAME { get; set; }
        public string USER_ID { get; set; }
        public string PASSWORD { get; set; }
        public string EMAIL { get; set; }
        public int? STATUS { get; set; }
        public long? ROLE { get; set; }
        public DateTime? CREATED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public int? USER_TYPE { get; set; }
        public string ADD1 { get; set; }
        public string ADD2 { get; set; }
        public decimal? PIN { get; set; }
        public string ALT_EMAIL { get; set; }
        public string ALT_MOB { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string PICTURE { get; set; }
        public string F_ID { get; set; }
        public string GENDER { get; set; }
        public string REFERRD_BY { get; set; }
        public int USERDETAIL_ID { get; set; }
        public string NAME { get; set; }
        public string COMPANY { get; set; }
        public string COMPANY_TYPE { get; set; }
        public int ROLE_ID { get; set; }
        public string BUSINESS { get; set; }
        public string PAN_NUMBER { get; set; }
        public string TAN_NUMBER { get; set; }
        public string WALLET { get; set; }
        public decimal? AMOUNT { get; set; }
        public int STATE { get; set; }
        public int? CITY { get; set; }
        public int? COUNTRY { get; set; }
        public string ADDRESS { get; set; }
        public string ADDRESS_LINE { get; set; }
        public long RECHARGE_ID { get; set; }
        public int RECHARGE_POINTS { get; set; }
        public int USER_ADMIN_ID { get; set; }
        public string CREATED_BY { get; set; }
        public int UserAdmin { get; set; }
        public string CITY_NAME { get; set; }
        public List<int> SelectedCategoryList { get; set; }
        public List<int> SelectedCategory { get; set; }
        public IEnumerable<GB_USER_CATEGORY> CategoryList { get; set; }
    }
}
