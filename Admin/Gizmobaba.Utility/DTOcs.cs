﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizmobaba.Utility
{
    class DTOcs
    {
    }
    public class CategoryDTO
    {
        public Nullable<int> SEQ_NO { get; set; }
        public long CATEGORY_ID { get; set; }
        public string CATEGORY_DESC { get; set; }
        public Nullable<int> STATUS { get; set; }
        public Nullable<System.DateTime> CREATEDF_ON { get; set; }
        public Nullable<System.DateTime> UPDATED_ON { get; set; }
        public string ICON { get; set; }
    }
}
