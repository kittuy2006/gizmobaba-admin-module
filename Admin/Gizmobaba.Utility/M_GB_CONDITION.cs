//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Gizmobaba.Utility
{
    using System;
    using System.Collections.Generic;
    
    public partial class M_GB_CONDITION
    {
        public M_GB_CONDITION()
        {
            this.GB_PRODUCT_INFORMATION = new HashSet<GB_PRODUCT_INFORMATION>();
        }
    
        public int CONDITION_ID { get; set; }
        public string CONDITION_DESC { get; set; }
        public Nullable<int> STATAUS { get; set; }
    
        public virtual ICollection<GB_PRODUCT_INFORMATION> GB_PRODUCT_INFORMATION { get; set; }
    }
}
