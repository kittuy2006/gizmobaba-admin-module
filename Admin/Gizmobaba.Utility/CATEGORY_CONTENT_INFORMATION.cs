//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Gizmobaba.Utility
{
    using System;
    using System.Collections.Generic;
    
    public partial class CATEGORY_CONTENT_INFORMATION
    {
        public long CATEGORY_CONTENT_ID { get; set; }
        public long CATEGORY_ID { get; set; }
        public string TITLE { get; set; }
        public string BODY { get; set; }
        public string IMAGE_URL { get; set; }
        public System.DateTime CREATEDF_ON { get; set; }
        public int STATUS { get; set; }
    
        public virtual M_GB_CATEGORY M_GB_CATEGORY { get; set; }
    }
}
