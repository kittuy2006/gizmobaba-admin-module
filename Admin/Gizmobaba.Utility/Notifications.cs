﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Gizmobaba.Utility
{
    public static class Notifications
    {
        //public string SMSSenderCode = "GIZBBA";
        public static int SendPaymentLink(string to, string toName, string Id)
        {
            var fromAddress = new MailAddress("nagulsk1991@gmail.com", "Gizmobaba Payment Link");
            var toAddress = new MailAddress(to, toName);
            const string fromPassword = "123@nagul";
            const string subject = "Subject";
            string mailBody = @"<!--Mail BODY-->
                        <table bgcolor ='#ffffff' border ='0' cellpadding = '0' cellspacing ='0' dir ='ltr' style ='border-collapse:collapse;font-family:Arial,sans-serif;font-weight:normal;font-size:13px;line-height:19px;color:#444444;border-left-width:1px;border-left-color:#dddddd;border-left-style:solid;border-right-width:1px;border-right-color:#dddddd;border-right-style:solid' width ='100%' >
                                         <tbody>
                                             <tr>
                                                 <td height ='20' style ='border-collapse:collapse;font-family:Arial,sans-serif;font-weight:normal;font-size:0;line-height:0' width ='4%' > &nbsp;</td>
                                                          <td height = '20' style ='border-collapse:collapse;font-family:Arial,sans-serif;font-weight:normal;font-size:0;line-height:0' width ='92%' > &nbsp;</td>
                                                                   <td height ='20' style ='border-collapse:collapse;font-family:Arial,sans-serif;font-weight:normal;font-size:0;line-height:0' width ='4%' > &nbsp;</td>
                                                                        </tr>
                                                                        
                                <tr>
                                    <td colspan = '3' height='22' style='border-collapse:collapse;font-family:Arial,sans-serif;font-weight:normal;font-size:0;line-height:0'>&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>

                        
                        <table bgcolor = '#ffffff' border='0' cellpadding='0' cellspacing='0' dir='ltr' style='border-collapse:collapse;font-family:Arial,sans-serif;font-weight:normal;font-size:13px;line-height:19px;color:#444444;border-left-width:1px;border-left-color:#dddddd;border-left-style:solid;border-right-width:1px;border-right-color:#dddddd;border-right-style:solid' width='100%'>
                            <tbody>
                                <tr>
                                    <td style = 'border-collapse:collapse;font-family:Arial,sans-serif;font-weight:normal;font-size:0;line-height:0' width='4%'>&nbsp;</td>
                                    <td style = 'border-collapse:collapse' width='92%'>
                                        <table border = '0' cellpadding='0' cellspacing='0' style='border-collapse:collapse;font-family:Arial,sans-serif;font-weight:normal;font-size:13px;line-height:19px;color:#444444' width='100%'>
                                            <tbody>
                                                <tr>
                                                    <td dir = 'ltr' style='border-collapse:collapse' valign='top' width='65%'>
                                                        <p dir = 'ltr' > Hi " + toName + @",</p>
                                                        <p dir = 'ltr' ><span>Gizmobaba Payment Link <a href='http://pakkadial.com/gba'>Click</a> </span></p>
                     
                                   
                                                        </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td style = 'border-collapse:collapse;font-family:Arial,sans-serif;font-weight:normal;font-size:0;line-height:0' width= '4%' > &nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan = '3' height='20' style='border-collapse:collapse;font-family:Arial,sans-serif;font-weight:normal;font-size:0;line-height:0'>&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <!--END-->";
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = mailBody,
                IsBodyHtml = true
            })
            {
                smtp.Send(message);
            }
            return 1;
        }

        public static int SendSMS(string toMobileNumber, string message)
        {
            string SMSSenderCode = "GIZBBA";

            string strUrl = "http://api.alerts.solutionsinfini.com/v3/?method=sms&api_key=A6e4336e058d568ae3442921ad612aa72&"
                          + "to=" + toMobileNumber + "&sender=" + SMSSenderCode + "&message=" + message + " &format=json&custom=1,2&flash=0";
            // Create a request object  
            WebRequest request = HttpWebRequest.Create(strUrl);
            // Get the response back  
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream s = (Stream)response.GetResponseStream();
            StreamReader readStream = new StreamReader(s);
            string dataString = readStream.ReadToEnd();
            response.Close();
            s.Close();
            readStream.Close();

            return (int)CustomStatus.ReturnStatus.success;
        }
    }
}
