﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizmobaba.Utility
{
    public class ProductDTO
    {
        public long PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public long? CATEGORY_ID { get; set; }
        public long? SUB_CAT_ID { get; set; }
        public string PRODUCT_CAPTION { get; set; }
        public string PRODUCT_DESC { get; set; }
        public string BRAND { get; set; }
        public decimal? MRP { get; set; }
        public decimal? GROSS_PRICE { get; set; }
        public decimal? NET_PRICE { get; set; }
        public decimal? GIZMOBABA_PRICE { get; set; }
        public int? QUANTITYS { get; set; }
        public int? ATTR_ID { get; set; }
        public int? LEVEL_ID { get; set; }
        public int? TAX_RATE_ID { get; set; }
        public int? AVAILABLE_QTY { get; set; }
        public DateTime? CREATED_ON { get; set; }
        public DateTime? UPDATED_ON { get; set; }
        public int? STATUS { get; set; }
        public int? ISAVAILABLE { get; set; }
        public int? IMAGE_ID { get; set; }
        public string LINK { get; set; }
        public string CREATED_BY { get; set; }
        public string UPDATED_BY { get; set; }
        public string VENDOR_ID { get; set; }
        public string PHOTO_URL { get; set; }
        public DateTime? ACTIVATION_DATE { get; set; }
        public DateTime? DEACTIVATION_DATE { get; set; }
        public string DELIVERY_TIME { get; set; }
        public string INVENTORY { get; set; }
        public int? RESERVE_QUANTITY { get; set; }
        public int? PKG_QUANTITY { get; set; }
        public string REORDER_STOCK_LEVEL { get; set; }
        public int? STOCK_ALERT_QUANTITY { get; set; }
        public int? MINIMUM_ORDER_QUANTITY { get; set; }
        public int? MAXIMUM_ORDER_QUANTITY { get; set; }
        public bool? PREORDER { get; set; }
        public bool? BACKORDER { get; set; }
        public string VIDEO_URL { get; set; }
        public string STOCK_AVAILABLE_MESS { get; set; }
        public string PRODUCT_SKU { get; set; }
        public long PRODUCT_INFO_ID { get; set; }
        public string PRO_OFFER_DESC { get; set; }
        public string PRO_OFFER_IMAGE { get; set; }
        public string BARCODE { get; set; }
        public string MODELNUMBER { get; set; }
        public string CATALOG_CODE { get; set; }
        public string STANDARD_PRODUCT_TYPE { get; set; }
        public string MPN { get; set; }
        public string STANDARD_PRODUCT_CODE { get; set; }
        public string PAGE_TITLE { get; set; }
        public string KEYWORD { get; set; }
        public string SEO_DESC { get; set; }
        public string URL_KEY_REWRITER { get; set; }
        public string SMALL_IMAGEALT { get; set; }
        public string LARGE_IMAGEALT { get; set; }
        public int? CONDITION_ID { get; set; }
        public DateTime? CONDITION_PURCHASE_DATE { get; set; }
        public int? WARRANTY_ID { get; set; }
        public string WARRANTY_DESC { get; set; }
        public int? CUSTOM_FIELDS_ID { get; set; }
        public int? SECTION_ID { get; set; }
        public string PRODUCT_NOTE { get; set; }
        public IEnumerable<GB_PRODUCT_IMAGES> productImages { get; set; }
    }
}
