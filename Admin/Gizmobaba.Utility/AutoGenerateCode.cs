﻿using System;
using System.IO;
using System.Linq;

namespace Gizmobaba.Utility
{
    public class AutoGenerateCode
    {
        public static string GenerateSkuCode()
        {
            // var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var chars = "0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 4)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            result = "GB -" + result;
            return result;
        }

        public static int DeleteFile(string file)
        {
            if (String.IsNullOrEmpty(file)) throw new ArgumentException("Argument is null or empty", "file");
            file = @"C:\Publish\Images\Product\" + file;
            if (Directory.Exists(Path.GetDirectoryName(file)))
            {
                File.Delete(file);
               return (int)CustomStatus.ReturnStatus.success;
            }
            return (int)CustomStatus.ReturnStatus.failure;
        }
    }
}
