//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Gizmobaba.Utility
{
    using System;
    using System.Collections.Generic;
    
    public partial class GB_PRODUCT_DELIVERY
    {
        public int PRODUCT_DELIVERY_ID { get; set; }
        public long PRODUCT_ID { get; set; }
        public int DELIVERY_ID { get; set; }
        public string PRODUCT_DELIVERY_DESC { get; set; }
        public Nullable<int> STATUS { get; set; }
    
        public virtual GB_PRODUCT_DETAIL GB_PRODUCT_DETAIL { get; set; }
        public virtual M_GB_DELIVERY M_GB_DELIVERY { get; set; }
    }
}
