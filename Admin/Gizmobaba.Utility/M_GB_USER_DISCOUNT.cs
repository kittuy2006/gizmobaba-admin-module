//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Gizmobaba.Utility
{
    using System;
    using System.Collections.Generic;
    
    public partial class M_GB_USER_DISCOUNT
    {
        public int DISCOUNT_ID { get; set; }
        public Nullable<int> USER_GROUP_ID { get; set; }
        public string VOUCHER_CODE { get; set; }
        public string DISCOUNT { get; set; }
        public Nullable<int> NUMBEROFUSERS { get; set; }
        public Nullable<int> NUMBEROFVOUCHER { get; set; }
        public Nullable<System.DateTime> VOUCHER_DATE { get; set; }
        public System.DateTime CREATED_ON { get; set; }
        public string REMARKS { get; set; }
        public Nullable<int> STATUS { get; set; }
        public string CAMPAIGN_TITLE { get; set; }
        public Nullable<System.DateTime> VOUCHER_EXPIRY_DATE { get; set; }
    }
}
