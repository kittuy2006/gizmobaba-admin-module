﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizmobaba.Utility
{
    public class BluedartDTO
    {
    }
    public class BDWayBill
    {
        public BDShipperDTO shipper { get; set; }
        public BDConsigneeDTO consignee { get; set; }
        public BDServicesDTO services { get; set; }

    }
    public class BDShipperDTO
    {
        public string OriginArea { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress1 { get; set; }
        public string CustomerAddress2 { get; set; }
        public string CustomerAddress3 { get; set; }
        public string CustomerPincode { get; set; }
        public string CustomerEmailID { get; set; }
        public string CustomerMobile { get; set; }
        public string CustomerTelephone { get; set; }
        public bool IsToPayCustomer { get; set; }
        public string Sender { get; set; }
        public string VendorCode { get; set; }
    }
    public class BDConsigneeDTO
    {
        public string ConsigneeName { get; set; }
        public string ConsigneeAddress1 { get; set; }
        public string ConsigneeAddress2 { get; set; }
        public string ConsigneeAddress3 { get; set; }
        public string ConsigneePincode { get; set; }
        public string ConsigneeAttention { get; set; }
        public string ConsigneeTelephone { get; set; }
        public string ConsigneeMobile { get; set; }

    }
    public class BDServicesDTO
    {
        public string ActualWeight { get; set; }
        public string CreditReferenceNo { get; set; }
        public string CollectableAmount { get; set; }
        public string DeclaredValue { get; set; }
        public string InvoiceNo { get; set; }
        public string PackType { get; set; }
        public int PieceCount { get; set; }
        public string ProductCode { get; set; }
        public bool ProductType { get; set; }
        public string SpecialInstruction { get; set; }
        public string SubProductCode { get; set; }
        public string PickupTime { get; set; }
        public string Commodity { get; set; }
        public DateTime PickupDate { get; set; }
        public bool PDFOutputNotRequired { get; set; }
        public string AWBNo { get; set; }
        public bool RegisterPickup { get; set; }
        public bool IsReversePickup { get; set; }
        public string DeliveryTimeSlot { get; set; }
        public string ParcelShopCode { get; set; }
        public BDCommodityDetailDTO commodity { get; set; }
    }
    public class BDCommodityDetailDTO
    {
        public string CommodityDetail1 { get; set; }
        public string CommodityDetail2 { get; set; }
        public string CommodityDetail3 { get; set; }

    }
    public class BDResponseDTO
    {
        public string AWBNo { get; set; }
        public string DestArea { get; set; }
        public string DestScrcd { get; set; }
        public string Message { get; set; }

    }
}
