//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Gizmobaba.Utility
{
    using System;
    using System.Collections.Generic;
    
    public partial class M_GB_ROLES
    {
        public M_GB_ROLES()
        {
            this.M_GB_FEATURE_ACCESS = new HashSet<M_GB_FEATURE_ACCESS>();
            this.M_GB_WALLET = new HashSet<M_GB_WALLET>();
        }
    
        public int SEQ_NO { get; set; }
        public int ROLE_ID { get; set; }
        public string ROLE_DESC { get; set; }
        public Nullable<int> STATUS { get; set; }
        public Nullable<System.DateTime> CREATED_ON { get; set; }
        public Nullable<System.DateTime> UPDATED_ON { get; set; }
        public string DESCRIPTION { get; set; }
        public Nullable<bool> UserAllow { get; set; }
        public Nullable<long> Value { get; set; }
    
        public virtual ICollection<M_GB_FEATURE_ACCESS> M_GB_FEATURE_ACCESS { get; set; }
        public virtual ICollection<M_GB_WALLET> M_GB_WALLET { get; set; }
    }
}
